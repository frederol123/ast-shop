<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Спасибо");
$APPLICATION->SetTitle("Спасибо");

$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb", 
	"agro", 
	array(
		"START_FROM" => "0",
		"PATH" => "/thanks",
		"SITE_ID" => "s1",
		"COMPONENT_TEMPLATE" => "agro"
	),
	false
);

$APPLICATION->AddChainItem("Главная", "/");
?>

<div class="">
    <div class="">
        <div class="">
            <br>
            <br>
            Ваше письмо успешно отправлено. Наш менеджер свяжется с вами в ближайшее время.
            <br>
            <br>
            <br>
            <br>
        </div>
    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
