<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");

$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb", 
	"ast", 
	array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "s1",
		"COMPONENT_TEMPLATE" => "ast"
	),
	false
);
?>

<div class="h3-contacts"><h3>Наш адрес</h3></div>
<div class="contacts-list-block">
    <ul class="">
        <li><a href=""><img src="/bitrix/templates/ast/images/footer1.svg" alt="" class="">г.Москва, ул. Новая Басманная, 23, стр. 2</a></li>
        <li><a href="tel:+78006003905"><img src="/bitrix/templates/ast/images/footer2.svg" alt="" class="">+7 (800) 600-39-05 </a></li>
        <li><a href=""><img src="/bitrix/templates/ast/images/footer3.svg" alt="" class="">Ежедневно с 9:00 до 18:00</a></li>
    </ul>
</div>
<div class="h3-contacts" style="padding-top:20px"><h3>Схема проезда</h3></div>
<div style='margin-bottom:50px'>
<div style="position:relative;overflow:hidden;">

<iframe src="https://yandex.ru/map-widget/v1/-/CCU4F6ff1B" width="100%" height="400" frameborder="1" allowfullscreen="true" style="position:relative;"></iframe>

</div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>

