<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<?
use Bitrix\Main\UI\Extension;
Extension::load('ui.bootstrap4');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?echo LANG_CHARSET;?>">
<?$APPLICATION->ShowMeta("keywords");?>
<?$APPLICATION->ShowMeta("description");?>
<meta name="viewport" content="width=1920, initial-scale=1">
<meta name="yandex-verification" content="cf498f8bf55b23d2" />
<script src="https://kit.fontawesome.com/419821370e.js" crossorigin="anonymous"></script>
<link rel="stylesheet" href="/bitrix/templates/ast/styles.css">


<?$APPLICATION->ShowHead()?>
</head>

<?$APPLICATION->ShowPanel();?>

<!-- Modal -->
<div class="modal modal-wrapper" id="order" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="header-modal">ЗАКАЗАТЬ</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-custom">
        <form action="/mailto.php" method="POST">
			<div class="form-group">
				<label for=""></label>
				<input type="text" name="fio" id="fio" placeholder="ФИО" class="form-control custom-input" required>
			</div>
			<div class="form-group">
				<label for=""></label>
				<input type="mail" name="mail" id="mail" placeholder="e-mail" class="form-control custom-input">
			</div>
			<div class="form-group">
				<label for=""></label>
				<input type="text" name="phone" id="phone" placeholder="ТЕЛЕФОН" class="form-control custom-input" required>
			</div>
			<div class="form-group">
				<label for=""></label>
				<textarea class="form-control custom-textarea" placeholder="ВАШ ВОПРОС" id="question" rows="3" name="question"></textarea>
			</div>
			<div class="form-group" style="margin-top:10px;">
				<input type="checkbox" class="form-check-input" id="politics" required checked>
				<label class="form-check-label" for="politics">Согласие на обработку<br> персональных данных</label>
			</div>
			<div class="modal-footer" style="margin-top:20px;">
        		<input type="submit" class="btn btn-primary order-button" value="ОСТАВИТЬ ЗАЯВКУ">
      		</div>
		</form>
      </div>
    </div>
  </div>
</div>

<div class="modal modal-wrapper" id="orderBuy" tabindex="-1" role="dialog"  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="header-modal">ЗАКАЗАТЬ</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body modal-custom">
        <form action="/mailtobuy.php" method="POST">
			<div class="form-group">
				<label for=""></label>
				<input type="text" name="fio" id="fio" placeholder="ФИО" class="form-control custom-input" required>
			</div>
			<div class="form-group">
				<label for=""></label>
				<input type="mail" name="mail" id="mail" placeholder="e-mail" class="form-control custom-input">
			</div>
			<div class="form-group">
				<label for=""></label>
				<input type="text" name="phone" id="phone" placeholder="ТЕЛЕФОН" class="form-control custom-input" required>
			</div>
			<div class="form-group" style="margin-top:10px;">
				<input type="checkbox" class="form-check-input" id="politics" required checked>
				<label class="form-check-label" for="politics">Согласие на обработку<br> персональных данных</label>
			</div>
				<input type="hidden" name="product" class="hidden-product">
			<div class="modal-footer" style="margin-top:20px;">
        		<input type="submit" class="btn btn-primary order-button" value="ОСТАВИТЬ ЗАЯВКУ">
      		</div>
		</form>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->


<div class="content">
    <header class="">
        <div class="header-grid">
            <div class="logo-container">
                <div class="logo">
                    <img src="/bitrix/templates/ast/images/logo.jpg" alt="">
                </div>
                <div class="logo-text">
                Серверное&nbsp;&nbsp;оборудование<br>
                и комплектующие Lenovo
                </div>
            </div>
            <div class="address">
                <img src="/bitrix/templates/ast/images/home.svg" alt="" style="display: block;float: left;">
                <span style="position:relative;top:-10px;">г.Москва, ул. Новая Басманная, 23, стр. 2</span>
            </div>
            <div class="search-wrapper">
                <div class="phones">
                    <div class="gray-text">
                    Ежедневно с 9:00 до 20:00
                    </div>
                    <div class="phones">
                        <a href="tel:+78006003905"><img src="/bitrix/templates/ast/images/phone.svg" alt="">+7 (800) 600-39-05</a>
                        <div class="btn btn-primary btn-form" data-toggle="modal" data-target="#order">Заказать консультацию</div>
                    </div>
                    <!-- <div class="search-block">
                        <input type="text" class="srch-input form-control" placeholder="Поиск по сайту">
                        <input type="submit" value="Поиск"  class="srch-btn form-control">
                    </div> -->
                    <?$APPLICATION->IncludeComponent("bitrix:search.form", "ast", Array(
                        "USE_SUGGEST" => "N",	// Показывать подсказку с поисковыми фразами
                            "PAGE" => "#SITE_DIR#search/index.php",	// Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
                            "COMPONENT_TEMPLATE" => "ast"
                        ),
                        false
                    );?> 

                </div>
            </div>
            <div class="lk">
                <img src="/bitrix/templates/ast/images/lk.svg" alt=""> <span>Войти</span>
            </div>
        </div>
    </header>
</div>

<div class="content-menu">
    <div class="content">
        <div class="menu-grid">
            <div class=""><a href="/" class="">Главная</a></div>
            <div class=""><a href="/catalog" class="">Каталог</a></div>
            <div class=""><a href="/#about" class="">О компании</a></div>
            <div class=""><a href="/news" class="">Новости</a></div>
            <div class=""><a href="/kontakty" class="">Контакты</a></div>
        </div>
    </div>
</div>

<div class="content">

<?php
$siteDir = 'http://' . SITE_SERVER_NAME . '/catalog/detail?ELEMENT_ID=';

?>

<script type="text/javascript">
    jQuery(document).ready(function() {
        document.title = '<?=$APPLICATION->ShowTitle(false);?>';
        $('.open-vac, .buy-product').click(function(e){
            e.preventDefault();
            e.stopPropagation();
            $('#orderBuy').modal({
                show:true
            });
            
            var url = "<?=$siteDir?>";
            var message = 'Артикул: ' + $(this).parent().find('.articul-text').text() + ' <br>Продукт: ' + $(this).parent().find('.title-name').text() + 
            '<br>' + '<a href="' + url + $(this).data('id') + '">Ссылка на продукт</a>';

            $('.hidden-product').val(message);
        });

        $('.buy-product').click(function(e){
            e.preventDefault();
            e.stopPropagation();
            $('#orderBuy').modal({
                show:true
            });
            
            var url = "<?=$siteDir?>";
            var message = 'Артикул: ' + $(this).parent().parent().find('.prod-detail-articul').text() + ' <br>Продукт: ' + $(this).parent().parent().find('.prod-detail-title').text() + 
            '<br>' + '<a href="' + url + $(this).data('id') + '">Ссылка на продукт</a>';

            $('.hidden-product').val(message);
        });
    });
</script>