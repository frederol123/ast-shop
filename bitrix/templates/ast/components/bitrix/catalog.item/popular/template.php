<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogProductsViewedComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

CModule::IncludeModule("iblock");

$res = CIBlockElement::GetProperty(4, $arResult['ITEM']["ID"], "sort", "asc");
while ($ob = $res->GetNext())
{
	$props[$ob['CODE']] = $ob['VALUE'];
}

$res = CIBlockElement::GetByID($arResult['ITEM']["ID"]);
if($ar_res = $res->GetNext())
	$sectId = $ar_res['IBLOCK_SECTION_ID']; 
?>
	<div class="product-card-wrap-popular">
		<a href="/catalog/detail?SECTION_ID=<?=$sectId?>&ELEMENT_ID=<?=$arResult['ITEM']["ID"]?>">
			<img src="<?=$arResult['ITEM']["DETAIL_PICTURE"]["SRC"]?>" width="230px" height="210px" alt="">
			<div class="articul-text">
				<?=$props['articul']?>
			</div>
			<div class="title-name">
					<?=$arResult['ITEM']["NAME"]?>
			</div>
			<button class="btn btn-primary open-vac">ЗАПРОС ЦЕНЫ</button>
		</a>
	</div>
