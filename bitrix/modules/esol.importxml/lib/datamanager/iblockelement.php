<?php
namespace Bitrix\EsolImportxml\DataManager;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class IblockElementTable
{
	protected static $arIblockClasses = array();
	protected static $elemListHash = array();
	
	public static function GetList($arFilter, $arKeys, $arOrder=array(), $limit=false)
	{
		if(empty($arOrder)) $arOrder = array('ID'=>'ASC');
		if(!isset($arFilter['CHECK_PERMISSIONS'])) $arFilter['CHECK_PERMISSIONS'] = 'N';
		$arKeys = array_diff($arKeys, array('SECTION_PATH'));
		$arFilterKeys = array_keys($arFilter);
		$hash = md5(serialize(array($arFilterKeys, $arKeys, $arOrder, $limit)));
		if(!isset(self::$elemListHash[$hash]))
		{
			$mtype = '';
			if(class_exists('\Bitrix\Iblock\ElementTable'))
			{
				$arNeedKeys = array_merge($arKeys, array_keys($arOrder));
				$arNeedFilterKeys = array();
				foreach($arFilter as $key=>$val)
				{
					$needKey = preg_replace('/^[^\d\w]*([\d\w]|$)/', '$1', $key);
					if($needKey!='CHECK_PERMISSIONS') $arNeedFilterKeys[] = $needKey;
				}
				$arFields = array_keys(\Bitrix\Iblock\ElementTable::getMap());

				if(count(array_diff(array_merge($arNeedKeys, $arNeedFilterKeys), $arFields))==0)
				{
					$mtype = 'd7';
				}
				elseif(\Bitrix\EsolImportxml\DataManager\ElementPropertyTable::issetValueIndex() && count(preg_grep('/^(IBLOCK_ID|CHECK_PERMISSIONS|[=%]PROPERTY_\d+|[=%]PROPERTY_\d+_VALUE)$/', $arFilterKeys))==count($arFilterKeys) && isset($arFilter['IBLOCK_ID']) && is_numeric($arFilter['IBLOCK_ID']) && $limit===false)
				{
					$arFilter['IBLOCK_ID'] = (int)$arFilter['IBLOCK_ID'];
					$arIblock = \Bitrix\Iblock\IblockTable::getList(array('filter'=>array('ID'=>$arFilter['IBLOCK_ID']), 'select'=>array('VERSION')))->Fetch();
					if($arIblock['VERSION']==1)
					{
						if(!isset(self::$arIblockClasses[$arFilter['IBLOCK_ID']]) || !class_exists('\Bitrix\EsolImportxml\DataManager\ElementProperty'.$arFilter['IBLOCK_ID'].'Table'))
						{
							eval('namespace Bitrix\EsolImportxml\DataManager;'."\r\n".
								'class ElementProperty'.$arFilter['IBLOCK_ID'].'Table extends ElementPropertyTable{'."\r\n".
									'public static function getMap(){return parent::getMapForIblock('.$arFilter['IBLOCK_ID'].');}'.
								'}');
							self::$arIblockClasses[$arFilter['IBLOCK_ID']] = $arFilter['IBLOCK_ID'];
						}
						if(count(array_diff($arNeedKeys, $arFields))==0)
						{
							$mtype = 'd7_props';
						}
						else $mtype = 'props';
					}
				}
			}
			self::$elemListHash[$hash] = $mtype;
		}
		$mtype = self::$elemListHash[$hash];
		
		$dbResult = false;
		if($mtype=='d7')
		{
			if(isset($arFilter['CHECK_PERMISSIONS'])) unset($arFilter['CHECK_PERMISSIONS']);
			$arKeys = array_diff($arKeys, array('IBLOCK_SECTION'));
			$arParams = array('filter'=>$arFilter, 'select'=>$arKeys);
			if(!empty($arOrder)) $arParams['order'] = $arOrder;
			if($limit!==false) $arParams['limit'] = $limit;
			$dbResult = \Bitrix\Iblock\ElementTable::getList($arParams);
		}
		elseif(in_array($mtype, array('d7_props', 'props')))
		{
			$iblockId = (int)$arFilter['IBLOCK_ID'];
			$className = '\Bitrix\EsolImportxml\DataManager\ElementProperty'.$iblockId.'Table';
			$arNewFilter = array();
			$i = 0;
			foreach($arFilter as $k=>$v)
			{
				$emptyVal = !(strlen(is_array($v) ? implode('', $v) : $v) > 0);
				if(preg_match('/^([=%])PROPERTY_(\d+)$/', $k, $m) || preg_match('/^([=%])PROPERTY_(\d+)_(VALUE)$/', $k, $m))
				{
					$op = $m[1];
					$propId = $m[2];
					if($emptyVal)
					{
						$arNewFilter[] = array('LOGIC'=>'OR', array('=P'.$propId.'.VALUE'=>''), array('=P'.$propId.'.ID'=>false));
					}
					else
					{
						$prefix = str_repeat('SP.', $i++);
						$arNewFilter['='.$prefix.'IBLOCK_PROPERTY_ID'] = $propId;
						if($m[3]=='VALUE') $arNewFilter[$op.$prefix.'PROP_ENUM_VAL.VALUE'] = $v;
						else $arNewFilter[$op.$prefix.'VALUE'] = $v;
					}
					unset($arFilter[$k]);
				}
			}
			
			if(!empty($arNewFilter))
			{
				$arIds = array();
				$dbRes = $className::getList(array('filter'=>$arNewFilter, 'select'=>array('IBLOCK_ELEMENT_ID')));
				while($arr = $dbRes->Fetch())
				{
					$arIds[] = $arr['IBLOCK_ELEMENT_ID'];
				}
				if(!empty($arIds)) $arFilter['=ID'] = $arIds;
				else  $arFilter['=ID'] = -1;
				if($mtype=='d7_props')
				{
					if(isset($arFilter['CHECK_PERMISSIONS'])) unset($arFilter['CHECK_PERMISSIONS']);
					$arKeys = array_diff($arKeys, array('IBLOCK_SECTION'));
					$arParams = array('filter'=>$arFilter, 'select'=>$arKeys);
					if(!empty($arOrder)) $arParams['order'] = $arOrder;
					if($limit!==false) $arParams['limit'] = $limit;
					$dbResult = \Bitrix\Iblock\ElementTable::getList($arParams);
				}
				else
				{
					$dbResult = \CIblockElement::GetList($arOrder, $arFilter, false, ($limit===false ? false : array('nTopCount'=>$limit)), $arKeys);
				}
			}
		}
		
		if($dbResult===false)
		{
			$dbResult = \CIblockElement::GetList($arOrder, $arFilter, false, ($limit===false ? false : array('nTopCount'=>$limit)), $arKeys);
		}
		return $dbResult;
	}
	
	public static function SelectedRowsCount($dbRes)
	{
		if(is_callable(array($dbRes, 'getSelectedRowsCount'))) return $dbRes->getSelectedRowsCount();
		elseif(is_callable(array($dbRes, 'SelectedRowsCount'))) return $dbRes->SelectedRowsCount();
		else return 0;
	}
	
	public static function ExistsElement($arFilter)
	{
		if(class_exists('\Bitrix\Iblock\ElementTable'))
		{
			if(\Bitrix\Iblock\ElementTable::getList(array('filter'=>array($arFilter), 'select'=>array('ID'), 'limit'=>1))->Fetch()) return true;
			else return false;
		}
		else
		{
			return (bool)(\CIblockElement::GetList(array(), array_merge($arFilter, array('CHECK_PERMISSIONS' => 'N')), array()) > 0);
		}
		return false;
	}
}