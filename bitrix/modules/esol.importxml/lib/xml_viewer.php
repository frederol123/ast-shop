<?php
namespace Bitrix\EsolImportxml;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class XMLViewer 
{
	protected $arXPathsMulti = array();
	protected $arParamNames = array();
	
	public function __construct($DATA_FILE_NAME='', $SETTINGS_DEFAULT=array())
	{
		$this->filename = $_SERVER['DOCUMENT_ROOT'].$DATA_FILE_NAME;
		$this->params = $SETTINGS_DEFAULT;
		$this->fileEncoding = 'utf-8';
		$this->siteEncoding = \Bitrix\EsolImportxml\Utils::getSiteEncoding();
		//$this->fl = new \Bitrix\EsolImportxml\FieldList($SETTINGS_DEFAULT);
	}
	
	public function GetXPathsMulti()
	{
		return $this->arXPathsMulti;
	}
	
	public function GetFileStructure()
	{
		$this->arXPathsMulti = array();
		$file = $this->filename;
		//$arXml = simplexml_load_file($file);
		$arXml = $this->getLigthSimpleXml($file);
		
		$arStruct = array();
		$this->GetStructureFromSimpleXML($arStruct, $arXml);
		
		if($this->siteEncoding!=$this->fileEncoding)
		{
			$arStruct = \Bitrix\Main\Text\Encoding::convertEncodingArray($arStruct, $this->fileEncoding, $this->siteEncoding);
		}
		
		return $arStruct;
	}
	
	public function getLigthSimpleXml($fn)
	{
		if(!file_exists($fn))
		{
			return new \SimpleXMLElement('<d></d>');
		}

		if(!class_exists('\XMLReader'))
		{
			return simplexml_load_file($fn);
		}
		
		$xml = new \XMLReader();
		$res = $xml->open($fn);

		$arObjects = array();
		$arObjectNames = array();
		$arXPaths = array();
		$arValues = array();
		$arXPathsMulti = array();
		$arParamNames = array();
		$paramCnt = 0;
		$curDepth = 0;
		$isRead = false;
		$maxTime = 10;
		if(isset($this->params['MAX_READ_FILE_TIME']) && strlen($this->params['MAX_READ_FILE_TIME']) > 0)
		{
			$maxTime = (int)$this->params['MAX_READ_FILE_TIME'];
			if($maxTime==0) $maxTime = 3600;
		}
		$beginTime = time();
		while(($isRead || $xml->read()) && $endTime-$beginTime < $maxTime) 
		{
			$isRead = false;
			if($xml->nodeType == \XMLReader::ELEMENT) 
			{
				$curDepth = $xml->depth;
				$arObjectNames[$curDepth] = $xml->name;
				$extraDepth = $curDepth + 1;
				while(isset($arObjectNames[$extraDepth]))
				{
					unset($arObjectNames[$extraDepth]);
					$extraDepth++;
				}
				$xPath = implode('/', $arObjectNames);
				
				$arAttributes = array();
				if($xml->moveToFirstAttribute())
				{
					$attrXPath = $xPath.'/@'.$xml->name;
					if(!isset($arXPaths[$attrXPath]))
					{
						$arXPaths[$attrXPath] = $attrXPath;
						$arAttributes[] = array('name'=>$xml->name, 'value'=>$xml->value, 'namespaceURI'=>$xml->namespaceURI);
						if(preg_match('/param[^\/]*\/@name$/', $attrXPath)) $arParamNames[$attrXPath] = array($xml->value);
					}
					elseif(isset($arParamNames[$attrXPath]) && !in_array($xml->value, $arParamNames[$attrXPath]) && $paramCnt++ < 1000) $arParamNames[$attrXPath][] = $xml->value;
					while($xml->moveToNextAttribute ())
					{
						$attrXPath = $xPath.'/@'.$xml->name;
						if(!isset($arXPaths[$attrXPath]))
						{
							$arXPaths[$attrXPath] = $attrXPath;
							$arAttributes[] = array('name'=>$xml->name, 'value'=>$xml->value, 'namespaceURI'=>$xml->namespaceURI);
							if(stripos($attrXPath, 'param/@name')!==false) $arParamNames[$attrXPath] = array($xml->value);
						}
						elseif(isset($arParamNames[$attrXPath]) && !in_array($xml->value, $arParamNames[$attrXPath]) && $paramCnt++ < 1000) $arParamNames[$attrXPath][] = $xml->value;
					}
				}
				
				$xml->moveToElement();
				$xmlName = $xml->name;
				$xmlNamespaceURI = $xml->namespaceURI;
				$xmlValue = null;
				$isSubRead = false;
				while(($xml->read() && ($isSubRead = true)) && ($xml->nodeType == \XMLReader::SIGNIFICANT_WHITESPACE)){}
				if($xml->nodeType == \XMLReader::TEXT || $xml->nodeType == \XMLReader::CDATA)
				{
					$xmlValue = $xml->value;
				}
				else
				{
					$isRead = $isSubRead;
				}
				
				$setObj = false;
				if(!isset($arXPaths[$xPath]) || (isset($xmlValue) && !isset($arValues[$xPath])))
				{
					$setObj = true;
					$arXPaths[$xPath] = $xPath;
					$curName = $xmlName;
					$curValue = null;
					$curNamespace = null;
					$nsPrefix = '';
					if($xmlNamespaceURI && mb_strpos($curName, ':')!==false)
					{
						$curNamespace = $xmlNamespaceURI;
						$nsPrefix = mb_substr($curName, 0, mb_strpos($curName, ':'));
					}
					if(isset($xmlValue))
					{
						$curValue = $xmlValue;
						if(strlen(trim($curValue)) > 0) $arValues[$xPath] = true;
					}

					if($curDepth == 0)
					{
						if(strlen($nsPrefix) > 0)
							$xmlObj = new \SimpleXMLElement('<'.$nsPrefix.':'.$curName.'></'.$nsPrefix.':'.$curName.'>');
						else
							$xmlObj = new \SimpleXMLElement('<'.$curName.'></'.$curName.'>');
						$arObjects[$curDepth] = &$xmlObj;
					}
					else
					{
						$parentXPath = implode('/', array_slice(explode('/', $xPath), 0, -1));
						$parentDepth = $curDepth - 1;
						/*$arObjects[$parentDepth] = $xmlObj->xpath('/'.$parentXPath);
						if(is_array($arObjects[$parentDepth])) $arObjects[$parentDepth] = current($arObjects[$parentDepth]);*/
						if($curNamespace) $xmlObj->registerXPathNamespace($nsPrefix, $curNamespace);
						$arParentObject = $xmlObj->xpath('/'.$parentXPath);
						if(is_array($arParentObject) && !empty($arParentObject))
						{
							$arObjects[$parentDepth] = current($arParentObject);
						}
						/*else
						{
							$arParentPath = explode('/', $parentXPath);
							array_shift($arParentPath);
							$subObj = $xmlObj;
							while((count($arParentPath) > 0) && ($subPath = array_shift($arParentPath)) && isset($subObj->{$subPath}))
							{
								$subObj = $subObj->{$subPath};
							}
							if(empty($arParentPath) && is_object($subObj) && !empty($subObj))
							{
								$arObjects[$parentDepth] = $subObj;
							}
						}*/
						
						$curValue = str_replace('&', '&amp;', $curValue);
						$arObjects[$curDepth] = $arObjects[$parentDepth]->addChild($curName, $curValue, $curNamespace);
					}
				}
				elseif(!isset($arXPathsMulti[$xPath]))
				{
					$arXPathsMulti[$xPath] = true;
				}

				if(!empty($arAttributes))
				{
					if(!$setObj)
					{
						$arObjects[$curDepth] = $xmlObj->xpath('/'.$xPath);
						if(is_array($arObjects[$curDepth])) $arObjects[$curDepth] = current($arObjects[$curDepth]);
					}
					foreach($arAttributes as $arAttr)
					{
						if(!is_object($arObjects[$curDepth])) continue;
						if(mb_strpos($arAttr['name'], ':')!==false && $arAttr['namespaceURI']) $arObjects[$curDepth]->addAttribute($arAttr['name'], $arAttr['value'], $arAttr['namespaceURI']);
						else $arObjects[$curDepth]->addAttribute($arAttr['name'], $arAttr['value']);
					}
				}
				$endTime = time();
			}
		}
		$xml->close();

		$this->arParamNames = $arParamNames;
		$this->arXPathsMulti = array_keys($arXPathsMulti);
		return $xmlObj;
	}
	
	public function GetStructureFromSimpleXML(&$arStruct, $simpleXML, $xpath = '', $level = 0, $nsKey = false)
	{
		if(!($simpleXML instanceof \SimpleXMLElement)) return;
		if($level==0)
		{
			$k = $simpleXML->getName();
			while(count(explode(':', $k)) > 2) $k = mb_substr($k, mb_strpos($k, ':') + 1);
			$arStruct[$k] = array();
			$attrs = $simpleXML->attributes();
			if(!empty($attrs) && $attrs instanceof \Traversable)
			{
				$arStruct[$k]['@attributes'] = array();
				foreach($attrs as $k2=>$v2)
				{
					$arStruct[$k]['@attributes'][$k2] = (string)$v2;
					if(array_key_exists($k.'/@'.$k2, $this->arParamNames))
					{
						$arStruct[$k]['@attributes'][$k2] = $this->arParamNames[$k.'/@'.$k2];
					}
				}
			}
			$this->GetStructureFromSimpleXML($arStruct[$k], $simpleXML, $k, ($level + 1));
			return;
		}
		
		$nss = $simpleXML->getNamespaces(true);
		if($nsKey!==false && isset($nss[$nsKey])) $nss = array($nsKey => $nss[$nsKey]);
		foreach($nss as $key=>$ns)
		{
			foreach($simpleXML->children($ns) as $k=>$v)
			{
				$k = $key.':'.$k;
				
				if(!isset($arStruct[$k]))
				{
					$arStruct[$k] = array();
				}
				$attrs = $v->attributes();
				if(!empty($attrs) && $attrs instanceof \Traversable)
				{
					if(!isset($arStruct[$k]['@attributes']))
					{
						$arStruct[$k]['@attributes'] = array();
					}
					foreach($attrs as $k2=>$v2)
					{
						if(!isset($arStruct[$k]['@attributes'][$k2]))
						{
							$arStruct[$k]['@attributes'][$k2] = (string)$v2;
							if(array_key_exists($xpath.'/'.$k.'/@'.$k2, $this->arParamNames))
							{
								$arStruct[$k]['@attributes'][$k2] = $this->arParamNames[$xpath.'/'.$k.'/@'.$k2];
							}
						}
					}
				}
				if(strlen((string)$v) > 0 && !isset($arStruct[$k]['@value']))
				{
					$arStruct[$k]['@value'] = trim((string)$v);
				}
				if($v instanceof \Traversable)
				{
					$this->GetStructureFromSimpleXML($arStruct[$k], $v, $xpath.'/'.$k, ($level + 1), $key);
				}
			}
		}
		
		//$arCounts = array();
		if($nsKey===false)
		{
			foreach($simpleXML as $k=>$v)
			{
				/*if(!isset($arCounts[$k])) $arCounts[$k] = 0;
				$arCounts[$k]++;*/
				
				if(!isset($arStruct[$k]))
				{
					$arStruct[$k] = array();
				}
				$attrs = $v->attributes();
				if(!empty($attrs) && $attrs instanceof \Traversable)
				{
					if(!isset($arStruct[$k]['@attributes']))
					{
						$arStruct[$k]['@attributes'] = array();
					}
					foreach($attrs as $k2=>$v2)
					{
						if(!isset($arStruct[$k]['@attributes'][$k2]))
						{
							$arStruct[$k]['@attributes'][$k2] = (string)$v2;
							if(array_key_exists($xpath.'/'.$k.'/@'.$k2, $this->arParamNames))
							{
								$arStruct[$k]['@attributes'][$k2] = $this->arParamNames[$xpath.'/'.$k.'/@'.$k2];
							}
						}
					}
				}
				if(strlen((string)$v) > 0 && !isset($arStruct[$k]['@value']))
				{
					$arStruct[$k]['@value'] = trim((string)$v);
				}
				if($v instanceof \Traversable)
				{
					$this->GetStructureFromSimpleXML($arStruct[$k], $v, $xpath.'/'.$k, ($level + 1));
				}
			}
		}
		
		/*foreach($arCounts as $k=>$cnt)
		{
			if(!isset($arStruct[$k]['@count']) || $cnt > $arStruct[$k]['@count'])
			{
				$arStruct[$k]['@count'] = $cnt;
			}
		}*/
		return $arStruct;
	}
	
	public function ShowXmlTag($arStruct)
	{
		foreach($arStruct as $k=>$v)
		{
			echo '<div class="esol_ix_xml_struct_item" data-name="'.htmlspecialcharsex($k).'">';
			echo '&lt;<a href="javascript:void(0)" onclick="EIXPreview.ShowBaseElements(this)" class="esol_ix_open_tag">'.$k.'</a>';
			if(is_array($v) && !empty($v['@attributes']))
			{
				foreach($v['@attributes'] as $k2=>$v2)
				{
					echo ' '.$k2.'="<span class="esol_ix_str_value" data-attr="'.htmlspecialcharsex($k2).'"><span class="esol_ix_str_value_val" title="'.htmlspecialcharsex($v2).'">'.$this->GetShowVal($v2).'</span></span>"';
				}
				unset($v['@attributes']);
			}
			echo '&gt;';
			/*if(is_array($v) && isset($v['@value']))
			{
				echo '<span class="esol_ix_str_value"><span class="esol_ix_str_value_val">'.$this->GetShowVal($v['@value']).'</span></span>';
				unset($v['@value']);
			}*/
			if((is_array($v) && isset($v['@value'])) || empty($v))
			{
				$val = ((is_array($v) && isset($v['@value'])) ? $v['@value'] : '');
				echo '<span class="esol_ix_str_value"><span class="esol_ix_str_value_val" title="'.htmlspecialcharsex($val).'">'.$this->GetShowVal($val).'</span></span>';
			}
			if(is_array($v) && isset($v['@value'])) 
			{
				unset($v['@value']);
			}
			
			if(is_array($v) && !empty($v))
			{
				$this->ShowXmlTagChoose();
				foreach($v as $k2=>$v2)
				{
					if(mb_substr($k2, 0, 1)!='@')
					{
						$this->ShowXmlTag(array($k2=>$v2));
					}
				}
				echo '&lt;/'.$k.'&gt;';
			}
			else
			{
				echo '&lt;/'.$k.'&gt;';
				$this->ShowXmlTagChoose();
			}
			echo '</div>';
		}
	}
	
	public function GetShowVal($v)
	{
		if(is_array($v)) $v = array_shift($v);
		if(mb_strlen(trim($v)) > 50) $v = mb_substr($v, 0, 50).'...';
		elseif(strlen(trim($v)) == 0) $v = '...';
		if($this->params['HTML_ENTITY_DECODE']=='Y')
		{
			$v = html_entity_decode($v);
		}
		$v = htmlspecialcharsex($v);
		return $v;
	}
	
	public function ShowXmlTagChoose()
	{
		//echo '<a href="javascript:void(0)" onclick="" class="esol_ix_dropdown_btn"></a>';
		echo '<span class="esol_ix_group_value"></span>';
	}
	
	public function GetAvailableTags(&$arTags, $path, $arStruct)
	{
		$arTags[$path] = Loc::getMessage("ESOL_IX_VALUE").' '.$path;
		foreach($arStruct as $k=>$v)
		{
			if($k == '@attributes')
			{
				foreach($v as $k2=>$v2)
				{
					$arTags[$path.'/@'.$k2] = Loc::getMessage("ESOL_IX_ATTRIBUTE").' '.$path.'/@'.$k2;
					if(is_array($v2))
					{
						foreach($v2 as $k3=>$v3)
						{
							if(strlen(trim($v3)) > 0)
							{
								$arTags[$path.'[@'.$k2.'="'.$v3.'"]'] = Loc::getMessage("ESOL_IX_VALUE").' '.$path.'[@'.$k2.'="'.$v3.'"]';
							}
						}
					}
				}
				continue;
			}
			
			if(mb_substr($k, 0, 1)=='@')
			{
				continue;
			}
			
			$this->GetAvailableTags($arTags, $path.'/'.$k, $arStruct[$k]);
		}
	}
	
	public function GetXpathVals($xpath, $parentXpath='', $arFieldParams=array(), $arProfileParams=array())
	{
		if(strlen($parentXpath) > 0)
		{
			$arExtra = array();
			\Bitrix\EsolImportxml\Extrasettings::HandleParams($arExtra, $arFieldParams);
			if(count($arExtra) > 0) $arExtra = current($arExtra);
			if(isset($arExtra['CONVERSION'])) $arConv = $arExtra['CONVERSION'];
			else $arConv = array();
			
			$arXpaths = array($xpath);
			$prefixPattern = '/(\{([^\s\'"\{\}]+[\'"][^\'"\{\}]*[\'"])*[^\s\'"\{\}]+\}|'.'\$\{[\'"]([^\s\{\}]*[\'"][^\'"\{\}]*[\'"])*[^\s\'"\{\}]*[\'"]\})/';
			foreach($arConv as $k=>$v)
			{
				foreach($v as $k2=>$v2)
				{
					if(!is_array($v2) && preg_match_all($prefixPattern, (string)$v2, $m))
					{
						foreach($m[0] as $xpath2)
						{
							$xpath2 = preg_replace('/\/@.*$/', '', trim($xpath2, '${}\'"'));
							while(strpos($xpath2, '/')!==false && mb_strlen($xpath2) > mb_strlen($parentXpath) && mb_strpos($xpath2, $parentXpath)===0 && !in_array($xpath2, $arXpaths))
							{
								$arXpaths[] = $xpath2;
								$xpath2 = preg_replace('/\/[^\/]*$/', '', $xpath2);
							}
						}
					}
				}
			}
			$rows = $this->GetXpathRows($parentXpath, $arXpaths);
			$ie = new \Bitrix\EsolImportxml\Importer(substr($this->filename, strlen($_SERVER['DOCUMENT_ROOT'])), $arProfileParams, array(), array());
			
			$xpath = mb_substr($xpath, mb_strlen($parentXpath) + 1);
			$arPath = explode('/', $xpath);
			$attr = $ie->GetPathAttr($arPath);
			$xpath = implode('/', $arPath);
					
			$arVals = array();
			if(is_array($rows))
			{
				foreach($rows as $row)
				{
					$val = $ie->Xpath($row, $xpath);
					if(is_array($val)) $val = current($val);
					$ie->currentXmlObj = $row;
					$ie->xpath = '/'.ltrim($parentXpath, '/');
					$val = $ie->ApplyConversions($val, $arConv, array());
					if($attr!==false && is_callable(array($val, 'attributes'))) $val = $val->attributes()->{$attr};
					$val = mb_substr((string)$val, 0, 1000);
					if(strlen($val) > 0 && !in_array($val, $arVals))
					{
						$arVals[] = $val;
						if(count($arVals) >= 10000) break;
					}
				}
			}
			elseif($rows!==false)
			{
				$val = $ie->Xpath($rows, $xpath);
				if(is_array($val)) $val = current($val);
				$arVals[] = (string)$val;
			}
			if($this->siteEncoding!=$this->fileEncoding)
			{
				$arVals = \Bitrix\Main\Text\Encoding::convertEncodingArray($arVals, $this->fileEncoding, $this->siteEncoding);
			}
			return $arVals;
		}
		else
		{
			$rows = $this->GetXpathRows($xpath);
			$arVals = array();
			if(is_array($rows))
			{
				$attr = false;
				$arPath = explode('/', $xpath);
				if(mb_strpos($arPath[count($arPath)-1], '@')===0)
				{
					$attr = mb_substr(array_pop($arPath), 1);
				}
				foreach($rows as $row)
				{
					$val = $row;
					if($attr!==false && is_callable(array($val, 'attributes'))) $val = $val->attributes()->{$attr};
					$val = mb_substr((string)$val, 0, 1000);
					if(strlen($val) > 0 && !in_array($val, $arVals))
					{
						$arVals[] = $val;
						if(count($arVals) >= 10000) break;
					}
				}
			}
			elseif($rows!==false)
			{
				$arVals[] = (string)$rows;
			}
			if($this->siteEncoding!=$this->fileEncoding)
			{
				$arVals = \Bitrix\Main\Text\Encoding::convertEncodingArray($arVals, $this->fileEncoding, $this->siteEncoding);
			}
			return $arVals;
		}
	}
	
	public function GetXpathRows($xpath, $wChild=false, $xpathsMulti=array(), $uniqueXPath='')
	{
		if(!is_array($xpathsMulti))
		{
			if(strlen($xpathsMulti) > 0) $xpathsMulti = unserialize(base64_decode($xpathsMulti));
			else $xpathsMulti = array();
		}
		if(!is_array($xpathsMulti)) $xpathsMulti = array();
		
		$checkChildXparts = false;
		$arChildXparts = array();
		if(is_array($wChild))
		{
			$checkChildXparts = true;
			$arChildXparts = $wChild;
			$wChild = true;
		}
		
		$xpath = trim(trim($xpath), '/');
		if(strlen($xpath) == 0) return;

		if(!class_exists('\XMLReader'))
		{
			$xmlObject = simplexml_load_file($this->filename);
			$rows = $this->Xpath($xmlObject, '/'.$xpath);
			return $rows;
		}
		
		$xpath = preg_replace('/\[\d+\]/', '', $xpath);
		$arXpath = $arXpathOrig = explode('/', trim($xpath, '/'));
		
		$onlyUnique = $skipNode = false;
		$this->arUniqueVals = array();
		$uniqueAttr = '';
		if(strlen($uniqueXPath) > 0)
		{
			$onlyUnique = true;
			$uniqueXPath = rtrim($xpath, '/').'/'.ltrim($uniqueXPath, '/');
			$arUniquePath = explode('/', $uniqueXPath);
			if(mb_strpos($arUniquePath[count($arUniquePath)-1], '@')===0)
			{
				$uniqueAttr = mb_substr(array_pop($arUniquePath), 1);
				$uniqueXPath = implode('/', $arUniquePath);
			}
		}

		$xml = new \XMLReader();
		$res = $xml->open($this->filename);
		
		$arObjects = array();
		$arObjectNames = array();
		$arXPaths = array();
		$curDepth = 0;
		$isRead = false;
		$break = false;
		while(($isRead || $xml->read()) && !$break) 
		{
			$isRead = false;
			if($xml->nodeType == \XMLReader::ELEMENT) 
			{
				$curDepth = $xml->depth;
				$arObjectNames[$curDepth] = $xml->name;
				$extraDepth = $curDepth + 1;
				while(isset($arObjectNames[$extraDepth]))
				{
					unset($arObjectNames[$extraDepth]);
					$extraDepth++;
				}
				
				$curXPath = implode('/', $arObjectNames);
				$curXPath = \Bitrix\EsolImportxml\Utils::ConvertDataEncoding($curXPath, $this->fileEncoding, $this->siteEncoding);
				if(mb_strpos($xpath.'/', $curXPath.'/')!==0 && mb_strpos($curXPath.'/', $xpath.'/')!==0)
				{
					if(isset($arObjects[$curDepth]) && !empty($xpathsMulti) && !in_array(implode('/', array_slice($arXpathOrig, 0, $curDepth+1)), $xpathsMulti))
					{
						$break = true;
					}
					continue;
				}
				if(mb_strlen($curXPath)>mb_strlen($xpath)) 
				{
					if(!$wChild) continue;
					if($checkChildXparts && !in_array($curXPath, $arChildXparts)) continue;
				}
				
				$arAttributes = $arAttributesValues = array();
				if($xml->moveToFirstAttribute())
				{
					$arAttributes[] = array('name'=>$xml->name, 'value'=>$xml->value, 'namespaceURI'=>$xml->namespaceURI);
					while($xml->moveToNextAttribute())
					{
						$arAttributes[] = array('name'=>$xml->name, 'value'=>$xml->value, 'namespaceURI'=>$xml->namespaceURI);
					}
					if($onlyUnique)
					{
						foreach($arAttributes as $arAttr)
						{
							$arAttributesValues[$arAttr['name']] = $arAttr['value'];
						}
					}
				}
				$xml->moveToElement();
				

				$curName = $xml->name;
				$curValue = null;
				//$curNamespace = ($xml->namespaceURI ? $xml->namespaceURI : null);
				$curNamespace = null;
				if($xml->namespaceURI && mb_strpos($curName, ':')!==false)
				{
					$curNamespace = $xml->namespaceURI;
				}

				$isSubRead = false;
				while(($xml->read() && ($isSubRead = true)) && ($xml->nodeType == \XMLReader::SIGNIFICANT_WHITESPACE)){}
				if($xml->nodeType == \XMLReader::TEXT || $xml->nodeType == \XMLReader::CDATA)
				{
					$curValue = $xml->value;
				}
				else
				{
					$isRead = $isSubRead;
				}
				
				if($onlyUnique)
				{
					if($curXPath==$uniqueXPath)
					{
						if(strlen($uniqueAttr) > 0) $uniqueVal = $arAttributesValues[$uniqueAttr];
						else $uniqueVal = $curValue;
						if(isset($this->arUniqueVals[$uniqueVal]))
						{
							$this->arUniqueVals[$uniqueVal]++;
							$skipNode = true;
							continue;
						}
						$this->arUniqueVals[$uniqueVal] = 1;
						$skipNode = false;
					}
					elseif($skipNode) continue;
				}

				if($curDepth == 0)
				{
					//$xmlObj = new \SimpleXMLElement('<'.$curName.'></'.$curName.'>');
					if(($pos = mb_strpos($curName, ':'))!==false)
					{
						$rootNS = mb_substr($curName, 0, $pos);
						$curName = mb_substr($curName, mb_strlen($rootNS) + 1);
					}
					$xmlObj = new \SimpleXMLElement('<'.$curName.'></'.$curName.'>', 0, false, $rootNS, true);
					$arObjects[$curDepth] = &$xmlObj;
				}
				else
				{
					$curValue = str_replace('&', '&amp;', $curValue);
					$arObjects[$curDepth] = $arObjects[$curDepth - 1]->addChild($curName, $curValue, $curNamespace);
				}			

				foreach($arAttributes as $arAttr)
				{
					if(mb_strpos($arAttr['name'], ':')!==false && $arAttr['namespaceURI']) $arObjects[$curDepth]->addAttribute($arAttr['name'], $arAttr['value'], $arAttr['namespaceURI']);
					else $arObjects[$curDepth]->addAttribute($arAttr['name'], $arAttr['value']);
				}
				
				//if(mb_strlen($xpath)==mb_strlen($curXPath) && !$wChild) $break = true;
			}
		}
		$xml->close();

		if(is_object($xmlObj))
		{
			//return $xmlObj->xpath('/'.$xpath);
			return $this->Xpath($xmlObj, '/'.$xpath);
		}
		return false;
	}
	
	public function GetSectionStruct($xpath, $arFields, $innerGroups=array(), $xpathsMulti=array())
	{
		$arXpaths = array();
		$arSubXpaths = array();
		if(!is_array($arFields)) $arFields = array();
		foreach($arFields as $k=>$v)
		{
			list($fieldXpath, $fieldName) = explode(';', $v);
			if(in_array($fieldName, array('ISECT_TMP_ID', 'ISECT_PARENT_TMP_ID', 'ISECT_NAME')))
			{
				$fieldName = mb_substr($fieldName, 6);
				$arXpaths[$fieldName] = trim(mb_substr($fieldXpath, mb_strlen($xpath)), '/');
			}
			if(in_array($fieldName, array('ISUBSECT_TMP_ID', 'ISUBSECT_NAME')))
			{
				$fieldName = mb_substr($fieldName, 9);
				$arSubXpaths[$fieldName] = trim(mb_substr($fieldXpath, mb_strlen($xpath)), '/');
			}
		}
		if(!array_key_exists('TMP_ID', $arXpaths) || !array_key_exists('NAME', $arXpaths))
		{
			return false;
		}
		
		$subsectionXpath = (array_key_exists('TMP_ID', $arSubXpaths) || !array_key_exists('NAME', $arSubXpaths) && array_key_exists('SUBSECTION', $innerGroups) && strlen($innerGroups['SUBSECTION']) > 0 && mb_strpos($xpath, $innerGroups['SUBSECTION'])===0 ? trim(mb_substr($innerGroups['SUBSECTION'], mb_strlen($xpath)), '/') : '');
		if(strlen($subsectionXpath) > 0)
		{
			foreach($arSubXpaths as $k=>$v)
			{
				$arSubXpaths[$k] = trim(mb_substr($v, mb_strlen($subsectionXpath)), '/');
			}
		}
		$isParents = (bool)array_key_exists('PARENT_TMP_ID', $arXpaths);
		$arSections = array();
		$rows = $this->GetXpathRows($xpath, true, $xpathsMulti);
		if(!is_array($rows)) return false;
		foreach($rows as $row)
		{
			$name = trim($this->GetStringByXpath($row, $arXpaths['NAME']));
			$tmpId = trim($this->GetStringByXpath($row, $arXpaths['TMP_ID']));
			$parentTmpId = ($isParents ? trim($this->GetStringByXpath($row, $arXpaths['PARENT_TMP_ID'])) : false);
			$arSections[$tmpId] = array(
				'NAME' => $name,
				'ORIG_NAME' => $name,
				'PARENT_ID' => $parentTmpId,
				'ROOT_PARENT_ID' => $tmpId,
				'LEVEL' => 1
			);
			if(strlen($subsectionXpath) > 0)
			{
				$this->AddSubSectionStruct($arSections, $row, $arSubXpaths, $subsectionXpath, $tmpId, 2);
			}
		}
		
		if($isParents || strlen($subsectionXpath) > 0)
		{
			foreach($arSections as $k=>$v)
			{
				$parentId = $v['PARENT_ID'];
				$parentIds = array($parentId);
				while($parentId!==false && strlen($parentId) > 0 && array_key_exists($parentId, $arSections) && !in_array($arSections[$parentId]['PARENT_ID'], $parentIds))
				{
					$arSections[$k]['LEVEL']++;
					$arSections[$k]['NAME'] = $arSections[$parentId]['ORIG_NAME'].' / '.$arSections[$k]['NAME'];
					$arSections[$k]['ROOT_PARENT_ID'] = $parentId;
					$parentId = $arSections[$parentId]['PARENT_ID'];
					$parentIds[] = $parentId;
				}
			}
		}
		if($this->siteEncoding!=$this->fileEncoding)
		{
			$arSections = \Bitrix\Main\Text\Encoding::convertEncodingArray($arSections, $this->fileEncoding, $this->siteEncoding);
		}
		uasort($arSections, create_function('$a,$b', 'return ($a["NAME"] < $b["NAME"]) ? -1 : 1;'));
		return $arSections;
	}
	
	public function AddSubSectionStruct(&$arSections, $parentRow, $arXpaths, $subsectionXpath, $parentTmpId, $level)
	{
		$rows = $this->Xpath($parentRow, $subsectionXpath);
		if(!is_array($rows)) return false;
		foreach($rows as $row)
		{
			$name = trim($this->GetStringByXpath($row, $arXpaths['NAME']));
			$tmpId = trim($this->GetStringByXpath($row, $arXpaths['TMP_ID']));
			$arSections[$tmpId] = array(
				'NAME' => $name,
				'ORIG_NAME' => $name,
				'PARENT_ID' => $parentTmpId,
				'ROOT_PARENT_ID' => $tmpId,
				'LEVEL' => $level
			);
			if(strlen($subsectionXpath) > 0)
			{
				$this->AddSubSectionStruct($arSections, $row, $arXpaths, $subsectionXpath, $tmpId, $level+1);
			}
		}
	}
	
	public function GetPropertyList($xpath, $arFields, $isOffers=false)
	{
		$arXpaths = array();
		foreach($arFields as $k=>$v)
		{
			list($fieldXpath, $fieldName) = explode(';', $v);
			if(in_array($fieldName, array(($isOffers ? 'OFF' : '').'PROPERTY_NAME')))
			{
				$fieldName = mb_substr($fieldName, mb_strpos($fieldName, '_') + 1);
				$arXpaths[$fieldName] = trim(mb_substr($fieldXpath, mb_strlen($xpath)), '/');
			}
		}
		if(!array_key_exists('NAME', $arXpaths))
		{
			return false;
		}
		
		$rows = $this->GetXpathRows($xpath, true, array(), $arXpaths['NAME']);
		$arProperties = array();
		foreach($rows as $row)
		{
			$name = trim($this->GetStringByXpath($row, $arXpaths['NAME']));
			if(strlen($name)==0) continue;
			$arProperties[$name] = array(
				'NAME' => $name,
				//'CNT' => (isset($arProperties[$name]) ? $arProperties[$name]['CNT'] + 1 : 1)
				'CNT' => (isset($this->arUniqueVals[$name]) ? $this->arUniqueVals[$name] : 1)
			);
		}
		if($this->siteEncoding!=$this->fileEncoding)
		{
			$arProperties = \Bitrix\Main\Text\Encoding::convertEncodingArray($arProperties, $this->fileEncoding, $this->siteEncoding);
		}
		return $arProperties;
	}
	
	public function GetStringByXpath($simpleXmlObj, $xpath)
	{
		$val = $this->Xpath($simpleXmlObj, $xpath);
		while(is_array($val)) $val = current($val);
		return (string)$val;
	}
	
	public function Xpath($simpleXmlObj, $xpath)
	{
		$xpath = \Bitrix\EsolImportxml\Utils::ConvertDataEncoding($xpath, $this->siteEncoding, $this->fileEncoding);
		if(preg_match('/((^|\/)[^\/]+):/', $xpath, $m))
		{
			if(mb_strpos($m[1], '/')===0) $xpath = '/'.mb_substr($xpath, mb_strlen($m[1]) + 1);
			$nss = $simpleXmlObj->getNamespaces(true);
			$nsKey = trim($m[1], '/');
			if(isset($nss[$nsKey]))
			{
				$simpleXmlObj->registerXPathNamespace($nsKey, $nss[$nsKey]);
			}
		}
		$xpath = trim($xpath);
		
		$arPath = explode('/', $xpath);
		$attr = false;
		if(mb_strpos($arPath[count($arPath)-1], '@')===0)
		{
			$attr = mb_substr(array_pop($arPath), 1);
			$xpath = implode('/', $arPath);
		}
		if(strlen($xpath) > 0 && $xpath!='.') $simpleXmlObj = $simpleXmlObj->xpath($xpath);
		if($attr!==false && is_callable(array($simpleXmlObj, 'attributes'))) return $simpleXmlObj->attributes()->{$attr};
		else return $simpleXmlObj;
	}
}