<?
$MESS["ESOL_IX_SECTION_LOAD_MODE"] = "Section load mode";
$MESS["ESOL_IX_SECTION_LOAD_MODE_DEFAULT"] = "Load all sections";
$MESS["ESOL_IX_SECTION_LOAD_MODE_MAPPED"] = "Mapped sections only";
$MESS["ESOL_IX_SECTION_LOAD_MODE_MAPPED_CHILD"] = "Only mapped sections with subsections";
$MESS["ESOL_IX_SECTION_NOT_CHOOSE_FIELDS"] = "To build the structure of the sections, select the fields &laquo;Section name&raquo; and &laquo;Temporary ID&raquo;";
$MESS["ESOL_IX_SECTION_NO_STRUCT"] = "Failed to build section structure";
$MESS["ESOL_IX_NOT_CHOOSE"] = "- not chosen -";
$MESS["ESOL_IX_SECTION_IN_FILE"] = "Section in the import file";
$MESS["ESOL_IX_SECTION_ON_SITE"] = "Section on the site";
$MESS["ESOL_IX_SECTION_MAPPING_TITLE"] = "Section mapping";
$MESS["ESOL_IX_ADD_FIELD"] = "Add field";
?>