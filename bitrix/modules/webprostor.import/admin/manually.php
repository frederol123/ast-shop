<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/webprostor.import/prolog.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/webprostor.import/include.php");

IncludeModuleLangFile(__FILE__);

$module_id = 'webprostor.import';
$moduleAccessLevel = $APPLICATION->GetGroupRight($module_id);

if ($moduleAccessLevel == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$cData = new CWebprostorImportPlan;

@set_time_limit(0);

$arErrors = "";
$arMessages = array();

if($_SERVER["REQUEST_METHOD"] == "POST" && $_REQUEST["Import"]=="Y")
{
	require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_js.php");
	
	if(array_key_exists("NS", $_POST) && is_array($_POST["NS"]))
	{
		$NS = $_POST["NS"];
	}
	else
	{
		if(!$PLAN_ID)
		{
			$arErrors .= GetMessage("WEBPROSTOR_IMPORT_PLAN_ID_ERROR")."<br />";
		}
		else
		{
			$planRes = $cData->GetByID($PLAN_ID);
			$planParams = $planRes->Fetch();
			
			if($LOAD_FILES == "Y")
				CWebprostorImport::Load($PLAN_ID);
			
			$IMPORT_FILE = $_SERVER["DOCUMENT_ROOT"].$planParams["IMPORT_FILE"];
			if(!is_file($IMPORT_FILE))
			{
				$arErrors .= GetMessage("WEBPROSTOR_IMPORT_IMPORT_FILE_ERROR")."<br />";
			}
			else
			{
				$GLOBALS["PLAN_ID"] = $PLAN_ID;
				
				switch($planParams["IMPORT_FORMAT"])
				{
					case("CSV"):
						$scriptData = new CWebprostorImportCSV;
						$fileData = $scriptData->ParseFile($IMPORT_FILE, $planParams["IMPORT_FILE_SHARSET"], false, $planParams["CSV_DELIMITER"]);
						break;
					case("XML"):
						$scriptData = new CWebprostorImportXML;
						$fileData = $scriptData->ParseFile($IMPORT_FILE, $planParams["IMPORT_FILE_SHARSET"], $planParams["XML_ENTITY"], $planParams["XML_PARSE_PARAMS_TO_PROPERTIES"], $planParams["ITEMS_PER_ROUND"], $startFrom);
						$fileData["ITEMS_COUNT"] = $scriptData->GetTotalCount($IMPORT_FILE, $planParams["XML_ENTITY"]);
						break;
					case("XLS"):
						$scriptData = new CWebprostorImportXLS;
						$fileData = $scriptData->ParseFile($IMPORT_FILE, $planParams["IMPORT_FILE_SHARSET"], $planParams["XLS_SHEET"], false, false);
						break;
					case("XLSX"):
						$scriptData = new CWebprostorImportXLSX;
						$fileData = $scriptData->ParseFile($IMPORT_FILE, $planParams["IMPORT_FILE_SHARSET"], $planParams["XLS_SHEET"], false, false);
						break;
					default:
						return false;
				}
				
				$entities = $scriptData->GetEntities($PLAN_ID);
				
				if($planParams["IMPORT_FILE_CHECK"] != "N")
				{
					if($planParams["IMPORT_FORMAT"] == "XML")
						$checkEntitiesResult = CWebprostorImport::CheckEntitiesNames($PLAN_ID, $entities["KEYS"]);
					else
						$checkEntitiesResult = CWebprostorImport::CheckEntitiesNames($PLAN_ID, $entities);
					
					if(count($checkEntitiesResult))
						$arErrors .= GetMessage("WEBPROSTOR_IMPORT_ENTITIES_AND_NAMES_NOT_IDENTICAL")."<br />";
				}
				
				$TOTAL = $fileData["ITEMS_COUNT"];
				
				$NS = array(
					"START_FROM" => intVal($START_FROM),
					//"START_FROM" => 0,
					"START_TIME" => microtime(true),
					"IMPORT_FILE" => $IMPORT_FILE,
					"TOTAL" => $TOTAL,
				);
			}
		}
	}
	
	if(!check_bitrix_sessid())
	{
		$arErrors .= GetMessage("ACCESS_DENIED")."<br />";
	}
	elseif(!$PLAN_ID)
	{
		$arErrors .= GetMessage("WEBPROSTOR_IMPORT_PLAN_ID_ERROR")."<br />";
	}
	?>
	<script>
		CloseWaitWindow();
	</script>
	<?
	CAdminMessage::ShowMessage($arErrors);
		
	if($arErrors == "")
	{
		$currentItem = $NS["START_FROM"];
		
		$compareText = "CWebprostorImport::Import({$PLAN_ID}, 0);";
		$import = CWebprostorImport::Import($PLAN_ID, $currentItem);
		
		preg_match('/CWebprostorImport::Import\([0-9]*\,\s(.*)\);/', $import, $matches);
		$NS["START_FROM"] = $matches[1];
		
		$progressTotal = intval($NS["TOTAL"]);
		$progressValue = intval($NS["START_FROM"]);
		
		if($NS["START_FROM"] > 0)
		{

			CAdminMessage::ShowMessage(array(
				"MESSAGE" => GetMessage("IMPORT_PROGRESS", array(
					"#DONE#" => number_format(intVal($NS["START_FROM"]), 0, "", " "),
					"#TOTAL#"=> number_format(intVal($NS["TOTAL"]), 0, "", " "),
				)),
				"DETAILS" => "#PROGRESS_BAR#",
				"HTML" => true,
				"TYPE" => "PROGRESS",
				"PROGRESS_TOTAL" => $progressTotal,
				"PROGRESS_VALUE" => $progressValue,
			));
			
			echo '<script>DoNext('.CUtil::PhpToJSObject(array("NS"=>$NS)).');</script>';
		}
		else
		{
			$arMessages[] = GetMessage("IMPORT_PROGRESS_FINISH", array(
				"#TOTAL#"=> number_format(intVal($NS["TOTAL"]), 0, "", " "),
				"#TIME#"=> round(microtime(true) - $NS["START_TIME"], 0),
			));
			echo '<script>EndImport();</script>';
		}
	}
	else
	{
		echo '<script>EndImport();</script>';
	}
	
	foreach($arMessages as $strMessage)
		CAdminMessage::ShowMessage(array("MESSAGE"=>$strMessage, "TYPE"=>"OK"));

	require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin_js.php");
}

$APPLICATION->SetTitle( GetMessage("WEBPROSTOR_IMPORT_MANUALLY_PAGE_TITLE") );
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
?>
<div id="import_result_div"></div>
<?
$aTabs = array(
	array(
		"DIV" => "FORM",
		"TAB" => GetMessage("WEBPROSTOR_IMPORT_MANUALLY_TAB_NAME"),
		"ICON" => "",
		"TITLE" => GetMessage("WEBPROSTOR_IMPORT_MANUALLY_TAB_DESCRIPTION")
	),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);
?>
<script language="JavaScript" type="text/javascript">
var running = false;
var oldNS = '';
function DoNext(NS)
{
	var PLAN_ID = parseInt(document.getElementById('PLAN_ID').value);
	var START_FROM = parseInt(document.getElementById('START_FROM').value);
	var LOAD_FILES = document.getElementById('LOAD_FILES').value;
	var queryString = 'Import=Y'
		+ '&lang=<?echo LANG?>'
		+ '&<?echo bitrix_sessid_get()?>'
		+ '&START_FROM=' + START_FROM
		+ '&LOAD_FILES=' + LOAD_FILES
		+ '&PLAN_ID=' + PLAN_ID;
	;

	if(running)
	{
		ShowWaitWindow();
		BX.ajax.post(
			'webprostor.import_manually.php?'+queryString,
			NS,
			function(result){
				document.getElementById('import_result_div').innerHTML = result;
			}
		);
	}
}
function StartImport()
{
	running = document.getElementById('start_button').disabled = true;
	DoNext();
}
function EndImport()
{
	running = document.getElementById('start_button').disabled = false;
	window.history.pushState("webprostor_import_manually", "", "webprostor.import_manually.php?lang=<?=LANG?>" );
}
</script>
<form id="webprostor_smtp_send" method="POST" action="<?=$APPLICATION->GetCurPage()?>?lang=<?echo LANG?>" ENCTYPE="multipart/form-data" name="webprostor_smtp_send">
<?
$queryObject = $cData->getList(Array("ID" => "DESC"), array());
$listPlans = array();
while($plan = $queryObject->getNext())
	$listPlans[$plan["ID"]] = htmlspecialcharsbx($plan["NAME"]).' ['.$plan["ID"].']';

$tabControl->Begin();
$tabControl->BeginNextTab();

$arFields["IMPORT"]["ITEMS"][] = Array(
	"CODE" => "PLAN_ID",
	"ID" => "PLAN_ID",
	"TYPE" => "SELECT",
	"LABEL" => GetMessage("WEBPROSTOR_IMPORT_PLAN_ID"),
	"ITEMS" => $listPlans,
	"VALUE" => intVal($PLAN_ID),
);

$arFields["IMPORT"]["ITEMS"][] = Array(
	"CODE" => "START_FROM",
	"ID" => "START_FROM",
	"TYPE" => "NUMBER",
	"LABEL" => GetMessage("WEBPROSTOR_IMPORT_START_FROM"),
	"VALUE" => intval($START_FROM),
	"PARAMS" => Array(
		"MIN" => 0
	),
);
$arFields["IMPORT"]["ITEMS"][] = Array(
	"CODE" => "LOAD_FILES",
	"ID" => "LOAD_FILES",
	"TYPE" => "CHECKBOX",
	"LABEL" => GetMessage("WEBPROSTOR_IMPORT_LOAD_FILES"),
	"DESCRIPTION" => GetMessage("WEBPROSTOR_IMPORT_LOAD_FILES_DESCRIPTION"),
);

CWebprostorCoreFunctions::ShowFormFields($arFields);

$tabControl->Buttons();
?>
	<input type="button" id="start_button" value="<?echo GetMessage("WEBPROSTOR_IMPORT_MANUALLY_START")?>" OnClick="StartImport();" class="adm-btn-save"<?=count($listPlans)?"":' disabled=""'?>>
	<input type="button" id="stop_button" value="<?echo GetMessage("WEBPROSTOR_IMPORT_MANUALLY_STOP")?>" OnClick="EndImport();"<?=count($listPlans)?"":' disabled=""'?>>
<?
$tabControl->End();

if(isset($_REQUEST['PLAN_ID']) && check_bitrix_sessid())
{
	$ID = intval($_REQUEST['PLAN_ID']);
	if($ID > 0)
	{
?>
<script>
BX.ready(BX.defer(function(){
	StartImport();
}));
</script>
<?
	}
}
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>