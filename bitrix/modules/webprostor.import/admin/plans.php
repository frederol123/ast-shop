<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/webprostor.import/prolog.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/webprostor.import/include.php");

IncludeModuleLangFile(__FILE__);

$module_id = 'webprostor.import';
$moduleAccessLevel = $APPLICATION->GetGroupRight($module_id);

if ($moduleAccessLevel == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$sTableID = "webprostor_import_plans";

$oSort = new CAdminSorting($sTableID, "ID", "desc");
$arOrder = (strtoupper($by) === "ID"? array($by => $order): array($by => $order, "ID" => "ASC"));
$lAdmin = new CAdminUiList($sTableID, $oSort);

$cData = new CWebprostorImportPlan;

$arFilterFields = Array(
	"find_id",
	"find_active",
	"find_name",
	"find_iblock_id",
	"find_highload_block",
	"find_import_format",
	"find_debug_events",
);

$lAdmin->InitFilter($arFilterFields);

$arFilter = Array(
	"ID" => $find_id,
	"ACTIVE" => $find_active,
	"NAME" => $find_name,
	"?NAME" => $find_name,
	"IBLOCK_ID" => $find_iblock_id,
	"HIGHLOAD_BLOCK" => $find_highload_block,
	"IMPORT_FORMAT" => $find_import_format,
	"DEBUG_EVENTS" => $find_debug_events,
);

/* Prepare data for new filter */
$queryObject = CIBlock::GetList(Array("ID"=>"ASC"), Array());
$listIblocks = array('' => GetMessage("BLOCK_ID_NO"));
$listIblocksEdit = array(0 => GetMessage("BLOCK_ID_NO"));
while($iblock = $queryObject->Fetch())
{
	$listIblocks[$iblock["ID"]] = htmlspecialcharsbx($iblock["NAME"]).' ['.$iblock["ID"].']';
	$listIblocksEdit[$iblock["ID"]] = '<a target="_blank" href="iblock_list_admin.php?IBLOCK_ID='.$iblock["ID"].'&type='.$iblock['IBLOCK_TYPE_ID'].'&lang='.LANG.'&find_section_section=0">'.htmlspecialcharsbx($iblock['NAME']).' ['.$iblock["ID"].']</a>';
}
$queryObject = \Bitrix\Highloadblock\HighloadBlockTable::getList();
$listHblocks = array('' => GetMessage("BLOCK_ID_NO"));
$listHblocksEdit = array(0 => GetMessage("BLOCK_ID_NO"));
while($hldata = $queryObject->Fetch())
{
	$listHblocks[$hldata["ID"]] = htmlspecialcharsbx($hldata["NAME"]).' ['.$hldata["TABLE_NAME"].']';
	$listHblocksEdit[$hldata["ID"]] = '<a target="_blank" href="highloadblock_rows_list.php?ENTITY_ID='.$hldata["ID"].'&lang='.LANG.'">'.htmlspecialcharsbx($hldata['NAME']).' ['.$hldata["TABLE_NAME"].']</a>';
}
$importFormats = Array(
	"CSV" => "CSV",
	"XML" => "XML",
	"XLS" => "XLS",
	"XLSX" => "XLSX",
);
$fileCharsets = Array(
	"UTF-8" => "UTF-8",
	"WINDOWS-1251" => "WINDOWS-1251",
);

$filterFields = array(
	array(
		"id" => "ID",
		"name" => "ID",
		"filterable" => "",
		"default" => true
	),
	array(
		"id" => "ACTIVE",
		"name" => GetMessage("ACTIVE"),
		"type" => "checkbox",
		"filterable" => "",
		"default" => true
	),
	array(
		"id" => "NAME",
		"name" => GetMessage("NAME"),
		"filterable" => "?",
		"quickSearch" => "?",
		"default" => true
	),
	array(
		"id" => "IBLOCK_ID",
		"name" => GetMessage("IBLOCK_ID"),
		"filterable" => "",
		"type" => "list",
		"items" => $listIblocks,
		"default" => true
	),
	array(
		"id" => "HIGHLOAD_BLOCK",
		"name" => GetMessage("HIGHLOAD_BLOCK"),
		"filterable" => "",
		"type" => "list",
		"items" => $listHblocks,
		"default" => true
	),
	array(
		"id" => "IMPORT_FORMAT",
		"name" => GetMessage("IMPORT_FORMAT"),
		"filterable" => "",
		"type" => "list",
		"items" => $importFormats,
		"default" => true
	),
	array(
		"id" => "IMPORT_FILE_SHARSET",
		"name" => GetMessage("IMPORT_FILE_SHARSET"),
		"filterable" => "",
		"type" => "list",
		"items" => $fileCharsets,
		"default" => true
	),
	array(
		"id" => "DEBUG_EVENTS",
		"name" => GetMessage("DEBUG_EVENTS"),
		"type" => "checkbox",
		"filterable" => "",
		"default" => true
	),
);

$lAdmin->AddFilter($filterFields, $arFilter);

if($lAdmin->EditAction())
{
	foreach($FIELDS as $ID=>$arFields)
	{
		$DB->StartTransaction();
		$ID = IntVal($ID);
		
		if(!$lAdmin->IsUpdated($ID))
			continue;

		if(($rsData = $cData->GetByID($ID)) && ($arData = $rsData->Fetch()))
		{
			foreach($arFields as $key=>$value)
				$arData[$key]=$value;
				
			if(!$cData->Update($ID, $arData))
			{
				$lAdmin->AddGroupError(GetMessage("SAVING_ERROR")." ".$cData->LAST_ERROR, $ID);
				$DB->Rollback();
			}
		}
		else
		{
			$lAdmin->AddGroupError(GetMessage("SAVING_ERROR")." ".GetMessage("ELEMENT_DOS_NOT_EXIST"), $ID);
			$DB->Rollback();
		}
		$DB->Commit();
	}
}

if(($arID = $lAdmin->GroupAction()))
{
	if (!empty($_REQUEST["action_all_rows_".$sTableID]) && $_REQUEST["action_all_rows_".$sTableID] === "Y")
	{
		$rsData = $cData->GetList(array($by=>$order), $arFilter);
		while($arRes = $rsData->Fetch())
			$arID[] = $arRes['ID'];
	}

	foreach($arID as $ID)
	{
		if(strlen($ID)<=0)
			continue;
		
		if(($rsData = $cData->GetByID($ID)) && ($arData = $rsData->Fetch()))
		{
			if(is_array($arFields))
			{
				foreach($arFields as $key=>$value)
					$arData[$key]=$value;
			}
		}
    
		switch($_REQUEST['action'])
		{
			case "delete":
				@set_time_limit(0);
				$DB->StartTransaction();
				if(!$cData->Delete($ID))
				{
					$DB->Rollback();
					$lAdmin->AddGroupError(GetMessage("DELETING_ERROR"), $ID);
				}
				$DB->Commit();
				break;
			case "activate":
			case "deactivate":
				$arData["ACTIVE"] = ($_REQUEST['action']=="activate"?"Y":"N");
				if(!$cData->Update($ID, $arData))
					$lAdmin->AddGroupError(GetMessage("UPDATING_ERROR").$cData->LAST_ERROR, $ID);

				break;
			case "debug":
			case "undebug":
				$arData["DEBUG_EVENTS"] = ($_REQUEST['action']=="debug"?"Y":"N");
				if(!$cData->Update($ID, $arData))
					$lAdmin->AddGroupError(GetMessage("UPDATING_ERROR").$cData->LAST_ERROR, $ID);

				break;
		}
	}
}

$arHeader = array(
	array(  
		"id"    =>	"ID",
		"content"  =>	"ID",
		"sort"    =>	"id",
		"align"    =>	"center",
		"default"  =>	true,
	),
	array(  
		"id"    =>	"ACTIVE",
		"content"  =>	GetMessage("ACTIVE"),
		"sort"    =>	"active",
		"default"  =>	true,
	),
	array(  
		"id"    =>	"NAME",
		"content"  =>	GetMessage("NAME"),
		"sort"    =>	"name",
		"default"  =>	true,
	),
	array(  
		"id"    =>	"SORT",
		"content"  =>	GetMessage("SORT"),
		"sort"    =>	"sort",
		"default"  =>	true,
	),
	array(  
		"id"    =>	"IBLOCK_ID",
		"content"  =>	GetMessage("IBLOCK_ID"),
		"sort"    =>	"iblock_id",
		"default"  =>	true,
	),
	array(  
		"id"    =>	"HIGHLOAD_BLOCK",
		"content"  =>	GetMessage("HIGHLOAD_BLOCK"),
		"sort"    =>	"highload_block",
		"default"  =>	true,
	),
	array(  
		"id"    =>	"IMPORT_FORMAT",
		"content"  =>	GetMessage("IMPORT_FORMAT"),
		"sort"    =>	"import_format",
		"default"  =>	true,
	),
	array(  
		"id"    =>	"ITEMS_PER_ROUND",
		"content"  =>	GetMessage("ITEMS_PER_ROUND"),
		"sort"    =>	"items_per_round",
		"default"  =>	true,
	),
	array(  
		"id"    =>	"IMPORT_FILE",
		"content"  =>	GetMessage("IMPORT_FILE"),
		"sort"    =>	"import_file",
		"default"  =>	true,
	),
	array(  
		"id"    =>	"PATH_TO_IMAGES",
		"content"  =>	GetMessage("PATH_TO_IMAGES"),
		"sort"    =>	"path_to_images",
		"default"  =>	false,
	),
	array(  
		"id"    =>	"PATH_TO_FILES",
		"content"  =>	GetMessage("PATH_TO_FILES"),
		"sort"    =>	"path_to_files",
		"default"  =>	false,
	),
	array(  
		"id"    =>	"DEBUG_EVENTS",
		"content"  =>	GetMessage("DEBUG_EVENTS"),
		"sort"    =>	"debug_events",
		"default"  =>	true,
	),
	array(  
		"id"    =>	"AGENT_INTERVAL",
		"content"  =>	GetMessage("AGENT_INTERVAL"),
		"sort"    =>	"agent_interval",
		"default"  =>	false,
	),
	array(  
		"id"    =>	"AGENT_ID",
		"content"  =>	GetMessage("AGENT_ID"),
		"sort"    =>	"agent_id",
		"default"  =>	true,
	),
	array(  
		"id"    =>	"LAST_IMPORT_DATE",
		"content"  =>	GetMessage("LAST_IMPORT_DATE"),
		"sort"    =>	"last_import_date",
		"default"  =>	false,
	),
);

$lAdmin->AddHeaders($arHeader);

$rsData = $cData->GetList(array($by=>$order), $arFilter);
$rsData = new CAdminUiResult($rsData, $sTableID);
$rsData->NavStart();

$lAdmin->SetNavigationParams($rsData);

while($arRes = $rsData->NavNext(true, "f_"))
{
	$plan_edit_link = "webprostor.import_plan_edit.php?ID=".$f_ID."&lang=".LANG;
	
	$row =& $lAdmin->AddRow($f_ID, $arRes, $plan_edit_link, GetMessage("EDIT_ELEMENT"));

	$row->AddCheckField("ACTIVE"); 
	$row->AddInputField("NAME", array("size"=>20));
	$row->AddInputField("SORT", array("size"=>5));
	$row->AddInputField("ITEMS_PER_ROUND", array("size"=>20));
	$row->AddSelectField("IBLOCK_ID", $listIblocks);
	$row->AddSelectField("HIGHLOAD_BLOCK", $listHblocks);
	$row->AddSelectField("IMPORT_FORMAT", $importFormats);
	$row->AddInputField("IMPORT_FILE", array("size"=>20));
	$row->AddInputField("PATH_TO_IMAGES", array("size"=>20));
	$row->AddInputField("PATH_TO_FILES", array("size"=>20));
	$row->AddCheckField("DEBUG_EVENTS"); 
	$row->AddInputField("AGENT_INTERVAL", array("size"=>5));
	
	$row->AddViewField("NAME", '<a href="'.$plan_edit_link.'">'.$f_NAME.'</a>');
	$row->AddViewField("IBLOCK_ID", $listIblocksEdit[$f_IBLOCK_ID]);
	$row->AddViewField("HIGHLOAD_BLOCK", $listHblocksEdit[$f_HIGHLOAD_BLOCK]);

	if($f_AGENT_ID>0)
		$row->AddViewField("AGENT_ID", '<a class="adm-btn" href="agent_list.php?set_filter=Y&adm_filter_applied=0&find='.$f_AGENT_ID.'&find_type=id&find_module_id=webprostor.import&lang='.LANG.'" target="_blank"><span>'.GetMessage("AGENT_OPEN").'</span></a>');
	else
		$row->AddViewField("AGENT_ID", GetMessage("NO_AGENT"));

	$arActions = Array();
	
	if($f_ACTIVE == "Y")
	{
		$arActions[] = array(
			"TEXT" => GetMessage("LIST_DEACTIVATE"),
			"ACTION" => $lAdmin->ActionDoGroup($f_ID, "deactivate"),
			"ONCLICK" => "",
		);
	}
	else
	{
		$arActions[] = array(
			"TEXT" => GetMessage("LIST_ACTIVATE"),
			"ACTION" => $lAdmin->ActionDoGroup($f_ID, "activate"),
			"ONCLICK" => "",
		);
	}
	
	if($f_DEBUG_EVENTS == "Y")
	{
		$arActions[] = array(
			"TEXT" => GetMessage("LIST_UNDEBUG"),
			"ACTION" => $lAdmin->ActionDoGroup($f_ID, "undebug"),
			"ONCLICK" => "",
		);
	}
	else
	{
		$arActions[] = array(
			"TEXT" => GetMessage("LIST_DEBUG"),
			"ACTION" => $lAdmin->ActionDoGroup($f_ID, "debug"),
			"ONCLICK" => "",
		);
	}
	
	$arActions[] = array(
		"ICON"=>"edit",
		"DEFAULT"=>true,
		"TEXT"=>	GetMessage("EDIT_ELEMENT"),
		"ACTION"=>$lAdmin->ActionRedirect("webprostor.import_plan_edit.php?ID=".$f_ID.'&lang='.LANG)
	);
	
	$arActions[] = array(
		"ICON"=>"copy",
		"DEFAULT"=>true,
		"TEXT"=>	GetMessage("COPY_ELEMENT"),
		"ACTION"=>$lAdmin->ActionRedirect("webprostor.import_plan_edit.php?COPY_ID=".$f_ID.'&lang='.LANG)
	);
	
	$arActions[] = array(
		"ICON"=>"rename",
		"DEFAULT"=>true,
		"TEXT"=>	GetMessage("EDIT_CONNECTIONS"),
		"LINK"=>"webprostor.import_plan_connections_edit.php?ID=".$f_ID.'&lang='.LANG,
	);
	
	$arActions[] = array(
		"ICON"=>"view",
		"DEFAULT"=>true,
		"TEXT"=>	GetMessage("CONNECTIONS_LIST"),
		"LINK"=>"webprostor.import_connections.php?PLAN_ID=".$f_ID."&find_plan_id=".$f_ID."&apply_filter=Y&lang=".LANG,
	);
	
	$arActions[] = array(
		"ICON"=>"pack",
		"DEFAULT"=>true,
		"TEXT"=>GetMessage("IMPORT_CONNECTIONS"),
		"LINK"=>"webprostor.import_connections_import.php?PLAN_ID=".$f_ID.'&lang='.LANG,
	);
	
	$arActions[] = array(
		"ICON"=>"delete",
		"TEXT"=>GetMessage("DELETE_ELEMENT"),
		"ACTION"=>"if(confirm('".GetMessageJS("CONFIRM_DELETING")."')) ".$lAdmin->ActionDoGroup($f_ID, "delete")
    );
  
	$row->AddActions($arActions);
}

$lAdmin->AddFooter(
	array(
		array(
			"title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"),
			"value"=>$rsData->SelectedRowsCount()
		),
		array(
			"counter"=>true,
			"title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"),
			"value"=>"0"
		),
	)
);

if ($moduleAccessLevel>="W")
{
	$aContext = array(
		array(
			"TEXT"=>GetMessage("ADD_ELEMENT"),
			"LINK"=>"webprostor.import_plan_edit.php?lang=".LANG,
			"TITLE"=>GetMessage("ADD_ELEMENT_TITLE"),
			"ICON"=>"btn_new",
		),
	);
	
	$lAdmin->AddAdminContextMenu($aContext);
	
	$lAdmin->AddGroupActionTable(
		Array(
			"edit"=>true,
			"delete"=>true,
			"for_all"=>true,
			"activate"=>GetMessage("LIST_ACTIVATE"),
			"deactivate"=>GetMessage("LIST_DEACTIVATE"),
			"debug"=>GetMessage("LIST_DEBUG"),
			"undebug"=>GetMessage("LIST_UNDEBUG"),
		)
	);
}
else
{
	$lAdmin->AddAdminContextMenu(array());
}

$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("PAGE_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$lAdmin->DisplayFilter($filterFields);
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");