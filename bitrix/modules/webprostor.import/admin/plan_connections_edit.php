<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/webprostor.import/prolog.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/webprostor.import/include.php");

IncludeModuleLangFile(__FILE__);

$module_id = 'webprostor.import';
$moduleAccessLevel = $APPLICATION->GetGroupRight($module_id);

if ($moduleAccessLevel == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$aTabs = array(
  array("DIV" => "main", "TAB" => GetMessage("ELEMENT_TAB_1"), "ICON"=>"", "TITLE"=>GetMessage("ELEMENT_TAB_1_TITLE")),
);

$sTableID = "webprostor_import_plans_connections";
$ID = intval($ID);
$message = null;
$strError = '';
$bVarsFromForm = false;

$arFields = array();

$tabControl = new CAdminTabControl("tabControl", $aTabs);

$CConnection = new CWebprostorImportPlanConnections;
$CConnectionFields = new CWebprostorImportPlanConnectionsFields;
$cProcessingData = new CWebprostorImportProcessingSettings;
$cProcessingTypesData = new CWebprostorImportProcessingSettingsTypes;

if($REQUEST_METHOD == "POST" && ($save!="" || $apply!="") && $moduleAccessLevel=="W" && check_bitrix_sessid()) 
{

	$arFields = Array(
		"ID" => $CONNECTIONS_ID,
		"ACTIVE" => $ACTIVE,
		"ENTITY" => $ENTITY,
		"NAME" => $NAME,
		"ENTITY_ATTRIBUTE" => $ENTITY_ATTRIBUTE,
		"SORT" => $SORT,
		"IBLOCK_SECTION_FIELD" => $IBLOCK_SECTION_FIELD,
		"IBLOCK_SECTION_DEPTH_LEVEL" => $IBLOCK_SECTION_DEPTH_LEVEL,
		"IBLOCK_SECTION_PARENT_FIELD" => $IBLOCK_SECTION_PARENT_FIELD,
		"IBLOCK_ELEMENT_FIELD" => $IBLOCK_ELEMENT_FIELD,
		"IBLOCK_ELEMENT_OFFER_FIELD" => $IBLOCK_ELEMENT_OFFER_FIELD,
		"IBLOCK_ELEMENT_PROPERTY" => $IBLOCK_ELEMENT_PROPERTY,
		"IBLOCK_ELEMENT_PROPERTY_E" => $IBLOCK_ELEMENT_PROPERTY_E,
		"IBLOCK_ELEMENT_PROPERTY_G" => $IBLOCK_ELEMENT_PROPERTY_G,
		"IBLOCK_ELEMENT_PROPERTY_M" => $IBLOCK_ELEMENT_PROPERTY_M,
		"IBLOCK_ELEMENT_OFFER_PROPERTY" => $IBLOCK_ELEMENT_OFFER_PROPERTY,
		"CATALOG_PRODUCT_FIELD" => $CATALOG_PRODUCT_FIELD,
		"CATALOG_PRODUCT_OFFER_FIELD" => $CATALOG_PRODUCT_OFFER_FIELD,
		"CATALOG_PRODUCT_PRICE" => $CATALOG_PRODUCT_PRICE,
		"CATALOG_PRODUCT_STORE_AMOUNT" => $CATALOG_PRODUCT_STORE_AMOUNT,
		"HIGHLOAD_BLOCK_ENTITY_FIELD" => $HIGHLOAD_BLOCK_ENTITY_FIELD,
		"IS_IMAGE" => $IS_IMAGE,
		"IS_FILE" => $IS_FILE,
		"IS_URL" => $IS_URL,
		"IS_REQUIRED" => $IS_REQUIRED,
		"USE_IN_SEARCH" => $USE_IN_SEARCH,
		"USE_IN_CODE" => $USE_IN_CODE,
		"ENTITY_NAME" => $ENTITY_NAME,
		"PROCESSING_TYPES" => $PROCESSING_TYPES,
	);
	
	if($ID > 0)
	{
		$res = $CConnection->UpdatePlanConnections($ID, $arFields);
	}
	else
	{
		$exception = new CApplicationException(GetMessage("ERROR_PLAN_ID_NO_SET"), WP_IMPORT_PLAN_ID_NO_SET);
		$APPLICATION->ThrowException($exception); 
	}
	
	if($res)
	{
		if ($apply != "")
			$message = new CAdminMessage(Array("MESSAGE" => GetMessage("CONNECTIONS_SAVED"), "TYPE" => "OK"));
		else
			LocalRedirect("/bitrix/admin/webprostor.import_plan_edit.php?ID=".$ID."&lang=".LANG);
	}
	else
	{
		if($e = $APPLICATION->GetException())
			$message = new CAdminMessage(GetMessage("MESSAGE_SAVE_ERROR"), $e);
		$bVarsFromForm = true;
	}
}

if($ID>0)
{
    $cData = new CWebprostorImportPlan;
	$element = $cData->GetById($ID);
	if(!$element->ExtractFields("plan_"))
		$ID=0;
	
	switch($plan_IMPORT_FORMAT)
	{
		case("CSV"):
			$scriptData = new CWebprostorImportCSV;
			break;
		case("XML"):
			$scriptData = new CWebprostorImportXML;
			break;
		case("XLS"):
			$scriptData = new CWebprostorImportXLS;
			break;
		case("XLSX"):
			$scriptData = new CWebprostorImportXLSX;
			break;
	}
	
	if($ID>0)
	{
		$entitiesArray = $scriptData->GetEntities($ID);
		if($plan_IMPORT_FORMAT == "XML")
		{
			$attributesArray = $entitiesArray["ATTRIBUTES"];
			$entitiesArray = $entitiesArray["KEYS"];
		}
		else
		{
			$dl = 1;
			$sectionDepthLevels = Array("" => GetMessage("MESSAGE_DO_NOT_USE"));
			while($dl <= intVal($plan_CSV_XLS_MAX_DEPTH_LEVEL))
			{
				$sectionDepthLevels[$dl] = $dl;
				$dl++;
			}
		}
		if(!is_array($entitiesArray) || count($entitiesArray) == 0)
		{
			//$element->LAST_ERROR = GetMessage("ERROR_NO_IMPORT_FILE_EXIST");
			$strError .= GetMessage("ERROR_NO_IMPORT_FILE_EXIST").'<br />';
		}
	}
}

if($bVarsFromForm)
	$DB->InitTableVarsForEdit($sTableID, "", "str_");

$APPLICATION->SetTitle(($ID>0? GetMessage("ELEMENT_EDIT_TITLE_1").': '.$plan_NAME.' ['.$ID.']' : GetMessage("ELEMENT_EDIT_TITLE_2")));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

if($ID>0)
{
	if($plan_IMPORT_IBLOCK_SECTIONS == "Y")
	{
		$connectionsRes = $CConnection->GetList(Array("SORT" => "ASC"), Array("PLAN_ID" => $ID, "!IBLOCK_SECTION_FIELD" => false, "USE_IN_SEARCH" => "Y", "ACTIVE" => "Y"));
		if (intval($connectionsRes->SelectedRowsCount()) == 0)
			$strError .= GetMessage("MESSAGE_ERROR_NO_SECTION_SEARCH_CONNECTION").'<br />';
	}
	if($plan_IMPORT_IBLOCK_ELEMENTS == "Y" && $plan_IMPORT_IBLOCK_PROPERTIES == "N")
	{
		$connectionsRes = $CConnection->GetList(Array("SORT" => "ASC"), Array("PLAN_ID" => $ID, "!IBLOCK_ELEMENT_FIELD" => false, "USE_IN_SEARCH" => "Y", "ACTIVE" => "Y"));
		if (intval($connectionsRes->SelectedRowsCount()) == 0)
			$strError .= GetMessage("MESSAGE_ERROR_NO_ELEMENT_SEARCH_CONNECTION").'<br />';
	}
	elseif($plan_IMPORT_IBLOCK_ELEMENTS == "Y" && $plan_IMPORT_IBLOCK_PROPERTIES == "Y")
	{
		$connectionsRes1 = $CConnection->GetList(Array("SORT" => "ASC"), Array("PLAN_ID" => $ID, "!IBLOCK_ELEMENT_FIELD" => false, "USE_IN_SEARCH" => "Y", "ACTIVE" => "Y"));
		$connectionsRes2 = $CConnection->GetList(Array("SORT" => "ASC"), Array("PLAN_ID" => $ID, "!IBLOCK_ELEMENT_PROPERTY" => false, "USE_IN_SEARCH" => "Y", "ACTIVE" => "Y"));
		if (intval($connectionsRes1->SelectedRowsCount()) == 0 && intval($connectionsRes2->SelectedRowsCount()) == 0)
			$strError .= GetMessage("MESSAGE_ERROR_NO_ELEMENT_OR_PROPERTY_SEARCH_CONNECTION").'<br />';
	}
	if($plan_IMPORT_HIGHLOAD_BLOCK_ENTITIES == "Y")
	{
		$connectionsRes = $CConnection->GetList(Array("SORT" => "ASC"), Array("PLAN_ID" => $ID, "!HIGHLOAD_BLOCK_ENTITY_FIELD" => false, "USE_IN_SEARCH" => "Y", "ACTIVE" => "Y"));
		if (intval($connectionsRes->SelectedRowsCount()) == 0)
			$strError .= GetMessage("MESSAGE_ERROR_NO_HIGHLOAD_SEARCH_CONNECTION").'<br />';
	}
	
	CAdminMessage::ShowOldStyleError($strError);
}
	
$aMenu = array(
	array(
		"TEXT"=>GetMessage("PLAN_EDIT"),
		"TITLE"=>GetMessage("PLAN_EDIT_TITLE"),
		"LINK"=>"webprostor.import_plan_edit.php?ID=".$ID."&lang=".LANG,
		"ICON"=>"btn_list",
	)
);

$aMenu[] = array("SEPARATOR"=>"Y");
$aSubMenu = array();

if($SHOW_ACTIVE != "Y")
{
	$aSubMenu[] = array(
		"TEXT"  => GetMessage("ONLY_ACTIVE"),
		"LINK"=>"webprostor.import_plan_connections_edit.php?ID={$ID}&lang=".LANG."&SHOW_ACTIVE=Y",
		"ICON"  => "",
	);
}

if($SHOW_ACTIVE != "N")
{
	$aSubMenu[] = array(
		"TEXT"  => GetMessage("ONLY_NOT_ACTIVE"),
		"LINK"=>"webprostor.import_plan_connections_edit.php?ID={$ID}&lang=".LANG."&SHOW_ACTIVE=N",
		"ICON"  => "",
	);
}

if($SHOW_REQUIRED != "Y")
{
	$aSubMenu[] = array(
		"TEXT"  => GetMessage("ONLY_REQUIRED"),
		"LINK"=>"webprostor.import_plan_connections_edit.php?ID={$ID}&lang=".LANG."&SHOW_REQUIRED=Y",
		"ICON"  => "",
	);
}

if($SHOW_REQUIRED != "N")
{
	$aSubMenu[] = array(
		"TEXT"  => GetMessage("ONLY_NOT_REQUIRED"),
		"LINK"=>"webprostor.import_plan_connections_edit.php?ID={$ID}&lang=".LANG."&SHOW_REQUIRED=N",
		"ICON"  => "",
	);
}

if($SHOW_IMAGE != "Y")
{
	$aSubMenu[] = array(
		"TEXT"  => GetMessage("ONLY_IMAGE"),
		"LINK"=>"webprostor.import_plan_connections_edit.php?ID={$ID}&lang=".LANG."&SHOW_IMAGE=Y",
		"ICON"  => "",
	);
}

if($SHOW_IMAGE != "N")
{
	$aSubMenu[] = array(
		"TEXT"  => GetMessage("ONLY_NOT_IMAGE"),
		"LINK"=>"webprostor.import_plan_connections_edit.php?ID={$ID}&lang=".LANG."&SHOW_IMAGE=N",
		"ICON"  => "",
	);
}

if($SHOW_FILE != "Y")
{
	$aSubMenu[] = array(
		"TEXT"  => GetMessage("ONLY_FILE"),
		"LINK"=>"webprostor.import_plan_connections_edit.php?ID={$ID}&lang=".LANG."&SHOW_FILE=Y",
		"ICON"  => "",
	);
}

if($SHOW_FILE != "N")
{
	$aSubMenu[] = array(
		"TEXT"  => GetMessage("ONLY_NOT_FILE"),
		"LINK"=>"webprostor.import_plan_connections_edit.php?ID={$ID}&lang=".LANG."&SHOW_FILE=N",
		"ICON"  => "",
	);
}

if($SHOW_URL != "Y")
{
	$aSubMenu[] = array(
		"TEXT"  => GetMessage("ONLY_URL"),
		"LINK"=>"webprostor.import_plan_connections_edit.php?ID={$ID}&lang=".LANG."&SHOW_URL=Y",
		"ICON"  => "",
	);
}

if($SHOW_URL != "N")
{
	$aSubMenu[] = array(
		"TEXT"  => GetMessage("ONLY_NOT_URL"),
		"LINK"=>"webprostor.import_plan_connections_edit.php?ID={$ID}&lang=".LANG."&SHOW_URL=N",
		"ICON"  => "",
	);
}

if($SHOW_SEARCH != "Y")
{
	$aSubMenu[] = array(
		"TEXT"  => GetMessage("ONLY_SEARCH"),
		"LINK"=>"webprostor.import_plan_connections_edit.php?ID={$ID}&lang=".LANG."&SHOW_SEARCH=Y",
		"ICON"  => "",
	);
}

if($SHOW_SEARCH != "N")
{
	$aSubMenu[] = array(
		"TEXT"  => GetMessage("ONLY_NOT_SEARCH"),
		"LINK"=>"webprostor.import_plan_connections_edit.php?ID={$ID}&lang=".LANG."&SHOW_SEARCH=N",
		"ICON"  => "",
	);
}

if($SHOW_ACTIVE || $SHOW_REQUIRED || $SHOW_IMAGE || $SHOW_FILE || $SHOW_URL || $SHOW_SEARCH)
{
	$aSubMenu[] = array(
		"TEXT"  => GetMessage("SHOW_ALL"),
		"LINK"=>"webprostor.import_plan_connections_edit.php?ID={$ID}&lang=".LANG,
		"ICON"  => "view",
	);
}

$aMenu[] = array(
	"TEXT"  => GetMessage("BTN_ACTIONS"),
	"ICON"  => "btn_new",
	"MENU"  => $aSubMenu,
);

$context = new CAdminContextMenu($aMenu);

$context->Show();

if($message)
	echo $message->Show();
/*elseif($element->LAST_ERROR!="")
	CAdminMessage::ShowMessage($element->LAST_ERROR);*/
	
if (($moduleAccessLevel < 'W' && $moduleAccessLevel <> 'D') || ($ID==0 || !$ID))
{
	echo BeginNote();
	if ($moduleAccessLevel < 'W' && $moduleAccessLevel <> 'D')
		echo GetMessage('MESSAGE_NOT_SAVE_ACCESS');
	if ($ID==0 || !$ID)
		echo GetMessage('MESSAGE_NO_PLAN_ID');
	echo EndNote();
}
?>
<form method="POST" name="PLAN_CONNECTIONS_EDIT" id="plan_connections_edit" Action="<?echo $APPLICATION->GetCurPage()?>" ENCTYPE="multipart/form-data">
<?echo bitrix_sessid_post();?>
<?
$tabControl->Begin();

$isCatalogSKU = false;
if(CModule::IncludeModule("catalog"))
{
	$isCatalogSKU = CCatalogSKU::GetInfoByProductIBlock($plan_IBLOCK_ID);
	$plan_OFFERS_IBLOCK_ID = $isCatalogSKU["IBLOCK_ID"];
	$plan_OFFERS_SKU_PROPERTY_ID = $isCatalogSKU["SKU_PROPERTY_ID"];
}
?>
<?
$tabControl->BeginNextTab();
?>
<?
if($ID>0)
{
?>
<tr>
	<td colspan="2" align="center">
	
		<?
		if(is_array($entitiesArray))
		{
			foreach($entitiesArray as $value => $label)
			{
			?>
			<input type="hidden" name="ENTITY_NAME[<?=$value;?>]" value="<?=htmlspecialcharsbx($label);?>" />
			<?
			}
		}
		?>
		
		<?
		if($plan_IMPORT_IBLOCK_SECTIONS=="Y" || $plan_IMPORT_IBLOCK_PROPERTIES=="Y")
			$iblockSectionFields = $CConnectionFields->GetFields("SECTION", $plan_IBLOCK_ID);
		
		if($plan_IMPORT_IBLOCK_ELEMENTS=="Y" || $plan_IMPORT_IBLOCK_PROPERTIES=="Y" || $plan_IMPORT_CATALOG_PRODUCT_OFFERS=="Y")
		{
			$iblockElementFields = $CConnectionFields->GetFields("ELEMENT");
		}
		
		if($plan_IMPORT_IBLOCK_PROPERTIES == "Y")
		{
			$propertiesArray = Array();
			$propRes = CIBlockProperty::GetList(Array("ID"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>$plan_IBLOCK_ID));
			while($propFields = $propRes->GetNext())
			{
				$propertiesArray[$propFields["ID"]] = htmlspecialcharsbx(addslashes($propFields["NAME"])).' ['.$propFields["ID"].']';
			}
			$coordinates = Array(
				"latitude" => GetMessage("MAP_LATITUDE"),
				"longitude" => GetMessage("MAP_LONGITUDE")
			);
		}
		
		if($plan_IMPORT_CATALOG_PRODUCT_OFFERS == "Y" && $isCatalogSKU)
		{
			$offersPropertiesArray = Array();
			$propRes = CIBlockProperty::GetList(Array("ID"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>$plan_OFFERS_IBLOCK_ID));
			while($propFields = $propRes->GetNext())
			{
				if($propFields["ID"] != $plan_OFFERS_SKU_PROPERTY_ID)
					$offersPropertiesArray[$propFields["ID"]] = htmlspecialcharsbx(addslashes($propFields["NAME"])).' ['.$propFields["ID"].']';
			}
		}
		
		if($plan_IMPORT_CATALOG_PRODUCTS=="Y")
			$catalogProductFields = $CConnectionFields->GetFields("PRODUCT");
		
		if($plan_IMPORT_CATALOG_PRICES=="Y" || $plan_IMPORT_CATALOG_PRODUCT_OFFERS == "Y")
			$catalogProductPrice = $CConnectionFields->GetFields("PRICE");
		
		if($plan_IMPORT_CATALOG_STORE_AMOUNT=="Y")
			$catalogProductStore = $CConnectionFields->GetFields("STORE");
		
		if($plan_IMPORT_HIGHLOAD_BLOCK_ENTITIES=="Y")
			$highloadBlockEntityFields = $CConnectionFields->GetFields("ENTITIES", false, $plan_HIGHLOAD_BLOCK);
		
		/*if($plan_IMPORT_FORMAT != "XML")
		{*/
			$useInCodeValues = $CConnectionFields->GetUseInCode([
				"SECTIONS" => ($plan_IMPORT_FORMAT != "XML"?$plan_IMPORT_IBLOCK_SECTIONS:"N"),
				"ELEMENTS" => $plan_IMPORT_IBLOCK_ELEMENTS,
				"ENTITIES" => $plan_IMPORT_HIGHLOAD_BLOCK_ENTITIES,
			]);
		//}
		
		$queryObject = $cProcessingData->getList(Array($b = "sort" => $o = "asc"), array());
		$listTypes = Array();
		while($type = $queryObject->getNext())
			$listTypes[$type["ID"]] = htmlspecialcharsbx($type["PROCESSING_TYPE"]).' ['.$type["ID"].'] '.$cProcessingTypesData->GetParamsValue($type["PARAMS"], '; ');
		?>
		<script type="text/javascript" language="JavaScript">
		<!--
		var EntitiesValues = [], EntitiesLabels = [], entitiesArray = [];
		<?foreach($entitiesArray as $value => $label):?>
		<?$label = str_replace(array("\r\n", "\r", "\n"), ' ', $label);?>
		EntitiesValues[EntitiesValues.length] = '<?=$value?>'; EntitiesLabels[EntitiesLabels.length] = '<?=htmlspecialcharsbx($label)?>'; entitiesArray[entitiesArray.length] = { id: '<?=$value?>', text: '<?=htmlspecialcharsbx($label)?> [<?=$value?>]' }; <?endforeach;?> 		
		$(document).ready(function() {
			$('.select-search-entities').select2({ 
				language: "ru",
				data: entitiesArray,
			}); 
		});
		
		<?if($plan_IMPORT_IBLOCK_SECTIONS=="Y" || $plan_IMPORT_IBLOCK_PROPERTIES=="Y") {?>
		<?if($plan_IMPORT_FORMAT != "XML") {?>
		var sectionDepthLevelsValues = [], sectionDepthLevelsLabels = [], sectionDepthLevels = [];
		<?foreach($sectionDepthLevels as $value => $label):?>
		sectionDepthLevelsValues[sectionDepthLevelsValues.length] = '<?=$value?>'; sectionDepthLevelsLabels[sectionDepthLevelsLabels.length] = '<?=htmlspecialcharsbx($label)?>'; sectionDepthLevels[sectionDepthLevels.length] = { id: '<?=$value?>', text: '<?=htmlspecialcharsbx($label)?>' }; <?endforeach;?>
		$(document).ready(function() {
			$('.select-search-section-depth').select2({ 
				language: "ru",
				data: sectionDepthLevels,
			}); 
		});
		<? } ?>
		var IblockSectionFieldsValues = [''], IblockSectionFieldsLabels = ['<?=GetMessage("MESSAGE_DO_NOT_USE");?>'], iblockSectionFields = [];
		<?foreach($iblockSectionFields as $value => $label):?>
		IblockSectionFieldsValues[IblockSectionFieldsValues.length] = '<?=$value?>'; IblockSectionFieldsLabels[IblockSectionFieldsLabels.length] = '<?=htmlspecialcharsbx($label)?>'; iblockSectionFields[iblockSectionFields.length] = { id: '<?=$value?>', text: '<?=htmlspecialcharsbx($label)?>' }; <?endforeach;?>
		$(document).ready(function() {
			$('.select-search-sections').select2({ 
				language: "ru",
				data: iblockSectionFields,
			}); 
		});
		<? } ?>
		
		<?if($plan_IMPORT_IBLOCK_ELEMENTS=="Y" || $plan_IMPORT_IBLOCK_PROPERTIES=="Y" || $plan_IMPORT_CATALOG_PRODUCT_OFFERS=="Y")	{?>
		var IblockFieldsValues = [''], IblockFieldsLabels = ['<?=GetMessage("MESSAGE_DO_NOT_USE");?>'], iblockElementFields = [];
		<?foreach($iblockElementFields as $value => $label):?>
		IblockFieldsValues[IblockFieldsValues.length] = '<?=$value?>'; IblockFieldsLabels[IblockFieldsLabels.length] = '<?=htmlspecialcharsbx($label)?>'; iblockElementFields[iblockElementFields.length] = { id: '<?=$value?>', text: '<?=htmlspecialcharsbx($label)?>' }; <?endforeach;?>
		$(document).ready(function() {
			$('.select-search-elements').select2({ 
				language: "ru",
				data: iblockElementFields,
			}); 
		});
		<? } ?>
		
		<? if($plan_IMPORT_IBLOCK_PROPERTIES == "Y") { ?>
		var IblockPropertiesValues = [''], IblockPropertiesLabels = ['<?=GetMessage("MESSAGE_DO_NOT_USE");?>'], propertiesArray = [];
		var IblockPropertiesMapValues = ['','latitude','longitude'], IblockPropertiesMapLabels = ['<?=GetMessage("MESSAGE_DO_NOT_USE");?>','<?=GetMessage("MAP_LATITUDE");?>','<?=GetMessage("MAP_LONGITUDE");?>'];
		<?foreach($propertiesArray as $value => $label):?>
		IblockPropertiesValues[IblockPropertiesValues.length] = '<?=$value?>'; IblockPropertiesLabels[IblockPropertiesLabels.length] = '<?=htmlspecialcharsbx($label)?>'; propertiesArray[propertiesArray.length] = { id: '<?=$value?>', text: '<?=htmlspecialcharsbx($label)?>' };
		<?endforeach;?>
		$(document).ready(function() {
			$('.select-search-properties').select2({ 
				language: "ru",
				data: propertiesArray,
			}); 
		});
		<? } ?>
		
		<? if($plan_IMPORT_CATALOG_PRODUCT_OFFERS == "Y" && $isCatalogSKU) { ?>
		var IblockOffersPropertiesValues = [''], IblockOffersPropertiesLabels = ['<?=GetMessage("MESSAGE_DO_NOT_USE");?>'], offersPropertiesArray = [];
		<?foreach($offersPropertiesArray as $value => $label):?>
		IblockOffersPropertiesValues[IblockOffersPropertiesValues.length] = '<?=$value?>'; IblockOffersPropertiesLabels[IblockOffersPropertiesLabels.length] = '<?=$label?>'; offersPropertiesArray[offersPropertiesArray.length] = { id: '<?=$value?>', text: '<?=htmlspecialcharsbx($label)?>' };
 		<?endforeach;?>
		$(document).ready(function() {
			$('.select-search-properties-offer').select2({ 
				language: "ru",
				data: offersPropertiesArray,
			}); 
		});
		<? } ?>
		
		<?if($plan_IMPORT_CATALOG_PRODUCTS=="Y")	{?>
		var CatalogFieldsValues = [''], CatalogFieldsLabels = ['<?=GetMessage("MESSAGE_DO_NOT_USE");?>'], catalogProductFields = [];
		<?foreach($catalogProductFields as $value => $label):?>
		CatalogFieldsValues[CatalogFieldsValues.length] = '<?=$value?>';
		CatalogFieldsLabels[CatalogFieldsLabels.length] = '<?=$label?>';
		catalogProductFields[catalogProductFields.length] = {
			id: '<?=$value?>',
			text: '<?=htmlspecialcharsbx($label)?>'
		};
		<?endforeach;?>
		$(document).ready(function() {
			$('.select-search-catalog-product').select2({ 
				language: "ru",
				data: catalogProductFields,
			}); 
		});
		<? } ?>
		
		<?if($plan_IMPORT_CATALOG_PRICES=="Y" || $plan_IMPORT_CATALOG_PRODUCT_OFFERS == "Y")	{?>
		var CatalogPricesValues = [''], CatalogPricesLabels = ['<?=GetMessage("MESSAGE_DO_NOT_USE");?>'], catalogProductPrice = [];
		<?foreach($catalogProductPrice as $value => $label):?>
		CatalogPricesValues[CatalogPricesValues.length] = '<?=$value?>'; CatalogPricesLabels[CatalogPricesLabels.length] = '<?=$label?>'; catalogProductPrice[catalogProductPrice.length] = { id: '<?=$value?>', text: '<?=htmlspecialcharsbx($label)?>' };
 		<?endforeach;?>
		$(document).ready(function() {
			$('.select-search-price').select2({ 
				language: "ru",
				data: catalogProductPrice,
			}); 
		});
		<? } ?>
		
		<?if($plan_IMPORT_CATALOG_STORE_AMOUNT=="Y") {?>
		var CatalogStoresValues = [''], CatalogStoresLabels = ['<?=GetMessage("MESSAGE_DO_NOT_USE");?>'], catalogProductStore = [];
		<?foreach($catalogProductStore as $value => $label):?>
		CatalogStoresValues[CatalogStoresValues.length] = '<?=$value?>'; CatalogStoresLabels[CatalogStoresLabels.length] = '<?=$label?>'; catalogProductStore[catalogProductStore.length] = { id: '<?=$value?>', text: '<?=htmlspecialcharsbx($label)?>' };
 		<?endforeach;?>
		$(document).ready(function() {
			$('.select-search-store').select2({ 
				language: "ru",
				data: catalogProductStore,
			}); 
		});
		<? } ?>
		
		<?if($plan_IMPORT_HIGHLOAD_BLOCK_ENTITIES=="Y")	{?>
		var HighloadEntitiesValues = [''], HighloadEntitiesLabels = ['<?=GetMessage("MESSAGE_DO_NOT_USE");?>'], highloadEntities = [];
		<?foreach($highloadBlockEntityFields as $value => $label):?>
		HighloadEntitiesValues[HighloadEntitiesValues.length] = '<?=$value?>'; HighloadEntitiesLabels[HighloadEntitiesLabels.length] = '<?=$label?>'; highloadEntities[highloadEntities.length] = { id: '<?=$value?>', text: '<?=htmlspecialcharsbx($label)?>' };
 		<?endforeach;?>
		$(document).ready(function() {
			$('.select-search-highload').select2({ 
				language: "ru",
				data: highloadEntities,
			}); 
		});
		<? } ?>
		
		<?//if($plan_IMPORT_FORMAT != "XML") {?>
		var UseInCodeValues = [], UseInCodeLabels = [], useInCode = [];
		<?foreach($useInCodeValues as $value => $label):?>
		UseInCodeValues[UseInCodeValues.length] = '<?=$value?>'; UseInCodeLabels[UseInCodeLabels.length] = '<?=$label?>'; useInCode[useInCode.length] = { id: '<?=$value?>', text: '<?=htmlspecialcharsbx($label)?>' };
 		<?endforeach;?>
		$(document).ready(function() {
			$('.select-search-code').select2({ 
				language: "ru",
				data: useInCode,
			}); 
		});
		<? //} ?>
		
		var CheckboxValues = ['N', 'Y'], CheckboxLabels = ['<?=GetMessage("OPTION_CHECKBOX_NO");?>', '<?=GetMessage("OPTION_CHECKBOX_YES");?>'];
		
		function CheckRowClass(rowEl)
		{
			var rowClass = rowEl.className;
			if(rowClass != 'data-row checked')
				rowEl.className = 'data-row checked';
			else
				rowEl.className = 'data-row';
		}
		
		function AddConnectionRow(tableID)
		{
			var tableRef = document.getElementById(tableID);
			var newRowID = WebprostorCoreGetNewRowId();
			
			var rows = tableRef.getElementsByTagName("tbody")[0].getElementsByTagName("tr").length;
			var newRow = tableRef.insertRow(rows);
			var newRowId = 'row_connection_'+newRowID;
			newRow.id = newRowId;
			newRow.className = "data-row";
			
			WebprostorCoreAddNewCellButton(newRow, 'center', 'button', function(){WebprostorCoreRemoveRow(newRowId)}, '<img src="/bitrix/themes/.default/images/buttons/delete.gif">');
			WebprostorCoreAddNewCellInput(newRow, 'center', 'hidden', 'PROCESSING_TYPES[]');
			<?if($plan_IMPORT_IBLOCK_PROPERTIES=="Y"){?>
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'IBLOCK_ELEMENT_PROPERTY_M[]', IblockPropertiesMapValues, IblockPropertiesMapLabels);
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'IBLOCK_ELEMENT_PROPERTY_G[]', IblockSectionFieldsValues, IblockSectionFieldsLabels);
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'IBLOCK_ELEMENT_PROPERTY_E[]', IblockFieldsValues, IblockFieldsLabels);
			<? } ?>
			<?//if($plan_IMPORT_FORMAT != "XML") {?>
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'USE_IN_CODE[]', UseInCodeValues, UseInCodeLabels);
			<?// } ?>
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'USE_IN_SEARCH[]', CheckboxValues, CheckboxLabels);
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'IS_REQUIRED[]', CheckboxValues, CheckboxLabels);
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'IS_FILE[]', CheckboxValues, CheckboxLabels);
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'IS_URL[]', CheckboxValues, CheckboxLabels);
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'IS_IMAGE[]', CheckboxValues, CheckboxLabels);
			<?if($plan_IMPORT_HIGHLOAD_BLOCK_ENTITIES=="Y"){?>
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'HIGHLOAD_BLOCK_ENTITY_FIELD[]', HighloadEntitiesValues, HighloadEntitiesLabels);
			<? } ?>
			<?if($plan_IMPORT_CATALOG_STORE_AMOUNT=="Y"){?>
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'CATALOG_PRODUCT_STORE_AMOUNT[]', CatalogStoresValues, CatalogStoresLabels);
			<? } ?>
			<?if($plan_IMPORT_CATALOG_PRICES=="Y"){?>
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'CATALOG_PRODUCT_PRICE[]', CatalogPricesValues, CatalogPricesLabels);
			<? } ?>
			<? if($plan_IMPORT_CATALOG_PRODUCT_OFFERS == "Y" && $isCatalogSKU) { ?>
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'CATALOG_PRODUCT_OFFER_FIELD[]', CatalogFieldsValues, CatalogFieldsLabels);
			<? } ?>
			<?if($plan_IMPORT_CATALOG_PRODUCTS=="Y"){?>
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'CATALOG_PRODUCT_FIELD[]', CatalogFieldsValues, CatalogFieldsLabels);
			<? } ?>
			<? if($plan_IMPORT_CATALOG_PRODUCT_OFFERS == "Y" && $isCatalogSKU) {?>
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'IBLOCK_ELEMENT_OFFER_PROPERTY[]', IblockOffersPropertiesValues, IblockOffersPropertiesLabels);
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'IBLOCK_ELEMENT_OFFER_FIELD[]', IblockFieldsValues, IblockFieldsLabels);
			<? } ?>
			<?if($plan_IMPORT_IBLOCK_PROPERTIES=="Y"){?>
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'IBLOCK_ELEMENT_PROPERTY[]', IblockPropertiesValues, IblockPropertiesLabels);
			<? } ?>
			<?if($plan_IMPORT_IBLOCK_ELEMENTS=="Y"){?>
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'IBLOCK_ELEMENT_FIELD[]', IblockFieldsValues, IblockFieldsLabels);
			<? } ?>
			<?if($plan_IMPORT_IBLOCK_SECTIONS=="Y"){?>
			<?if($plan_IMPORT_FORMAT == "XML") { ?>
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'IBLOCK_SECTION_PARENT_FIELD[]', IblockSectionFieldsValues, IblockSectionFieldsLabels);
			<? } else {	?>
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'IBLOCK_SECTION_DEPTH_LEVEL[]', sectionDepthLevelsValues, sectionDepthLevelsLabels);
			<? } ?>
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'IBLOCK_SECTION_FIELD[]', IblockSectionFieldsValues, IblockSectionFieldsLabels);
			<? } ?>
			WebprostorCoreAddNewCellInput(newRow, 'center', 'number', 'SORT[]', 500);
			<?if($plan_IMPORT_FORMAT == "XML") { ?>
			WebprostorCoreAddNewCellInput(newRow, 'center', 'text', 'ENTITY_ATTRIBUTE[]');
			<? } ?>
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'ACTIVE[]', CheckboxValues, CheckboxLabels);
			WebprostorCoreAddNewCellInput(newRow, 'center', 'hidden', 'NAME[]');
			WebprostorCoreAddNewCellSelect(newRow, 'center', 'ENTITY[]', EntitiesValues, EntitiesLabels);
			WebprostorCoreAddNewCellInput(newRow, 'center', 'hidden', 'CONNECTIONS_ID[]');
	
			$('#' + newRowId + ' select').select2({ 
				language: "ru",
			});
		}
		-->
		</script>
		<table class="internal" id="table_CONNECTIONS">
			<tbody>
				<tr class="heading">
					<td class="internal-left">ID</td>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_ENTITY");?></td>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_NAME");?></td>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_ACTIVE");?></td>
					<?if($plan_IMPORT_FORMAT == "XML") { ?>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_ENTITY_ATTRIBUTE");?> <span class="required" style="vertical-align: super; font-size: smaller;">1</span></td>
					<? } ?>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_ENTITY_SORT");?></td>
					<?if($plan_IMPORT_IBLOCK_SECTIONS=="Y"){?>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_IBLOCK_SECTION_FIELD");?></td>
					<?if($plan_IMPORT_FORMAT == "XML") { ?>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_IBLOCK_SECTION_PARENT_FIELD");?></td>
					<? } else {	?>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_IBLOCK_SECTION_DEPTH_LEVEL");?></td>
					<? } ?>
					<? } ?>
					<?if($plan_IMPORT_IBLOCK_ELEMENTS=="Y"){?>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_IBLOCK_ELEMENT_FIELD");?></td>
					<? } ?>
					<?if($plan_IMPORT_IBLOCK_PROPERTIES=="Y"){?>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_IBLOCK_ELEMENT_PROPERTY");?></td>
					<? } ?>
					<? if($plan_IMPORT_CATALOG_PRODUCT_OFFERS == "Y" && $isCatalogSKU) { ?>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_IBLOCK_ELEMENT_OFFER_FIELD");?></td>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_IBLOCK_ELEMENT_OFFER_PROPERTY");?></td>
					<? } ?>
					<?if($plan_IMPORT_CATALOG_PRODUCTS=="Y"){?>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_CATALOG_PRODUCT_FIELD");?></td>
					<? } ?>
					<? if($plan_IMPORT_CATALOG_PRODUCT_OFFERS == "Y" && $isCatalogSKU) { ?>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_CATALOG_PRODUCT_OFFER_FIELD");?></td>
					<? } ?>
					<?if($plan_IMPORT_CATALOG_PRICES=="Y"){?>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_CATALOG_PRODUCT_PRICE");?></td>
					<? } ?>
					<?if($plan_IMPORT_CATALOG_STORE_AMOUNT=="Y"){?>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_CATALOG_PRODUCT_STORE_AMOUNT");?></td>
					<? } ?>
					<?if($plan_IMPORT_HIGHLOAD_BLOCK_ENTITIES=="Y"){?>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_HIGHLOAD_BLOCK_ENTITY");?></td>
					<? } ?>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_IS_IMAGE");?></td>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_IS_URL");?></td>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_IS_FILE");?></td>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_IS_REQUIRED");?></td>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_USE_IN_SEARCH");?></td>
					<?//if($plan_IMPORT_FORMAT != "XML") {?>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_USE_IN_CODE");?></td>
					<?// } ?>
					<?if($plan_IMPORT_IBLOCK_PROPERTIES=="Y"){?>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_IBLOCK_ELEMENT_PROPERTY_E");?></td>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_IBLOCK_ELEMENT_PROPERTY_G");?></td>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_IBLOCK_ELEMENT_PROPERTY_M");?></td>
					<? } ?>
					<td><?=GetMessage("CONNECTIONS_TABLE_HEADING_PROCESSING_TYPES");?></td>
					<td class="internal-right">
					</td>
				</tr>
				<?
				function CheckDisplay($data)
				{
					$display = 'table-row';
					$arrayCheck = Array(
						"SHOW_ACTIVE" => "ACTIVE",
						"SHOW_REQUIRED" => "IS_REQUIRED",
						"SHOW_IMAGE" => "IS_IMAGE",
						"SHOW_FILE" => "IS_FILE",
						"SHOW_URL" => "IS_URL",
						"SHOW_SEARCH" => "USE_IN_SEARCH",
					);
					
					foreach($arrayCheck as $code => $value)
					{
						if($_REQUEST[$code] && $_REQUEST[$code] != $data[$value])
						{
							$display = "none";
							break;
						}
					}
					return $display;
				}
				$connectionsRes = $CConnection->GetList(Array("SORT" => "ASC"), Array("PLAN_ID" => $ID));
				while($connectionArr = $connectionsRes->GetNext())
				{
					$CONNECTIONS_ID = $connectionArr["ID"];
					$ACTIVE = $connectionArr["ACTIVE"];
					$ENTITY = $connectionArr["ENTITY"];
					$ENTITY_ATTRIBUTE = $connectionArr["ENTITY_ATTRIBUTE"];
					$NAME = $connectionArr["NAME"];
					$SORT = $connectionArr["SORT"];
					$IBLOCK_ELEMENT_FIELD = $connectionArr["IBLOCK_ELEMENT_FIELD"];
					$IBLOCK_ELEMENT_PROPERTY = $connectionArr["IBLOCK_ELEMENT_PROPERTY"];
					$IBLOCK_ELEMENT_OFFER_FIELD = $connectionArr["IBLOCK_ELEMENT_OFFER_FIELD"];
					$IBLOCK_ELEMENT_OFFER_PROPERTY = $connectionArr["IBLOCK_ELEMENT_OFFER_PROPERTY"];
					$IBLOCK_SECTION_FIELD = $connectionArr["IBLOCK_SECTION_FIELD"];
					$IBLOCK_SECTION_DEPTH_LEVEL = $connectionArr["IBLOCK_SECTION_DEPTH_LEVEL"];
					$IBLOCK_SECTION_PARENT_FIELD = $connectionArr["IBLOCK_SECTION_PARENT_FIELD"];
					$CATALOG_PRODUCT_FIELD = $connectionArr["CATALOG_PRODUCT_FIELD"];
					$CATALOG_PRODUCT_OFFER_FIELD = $connectionArr["CATALOG_PRODUCT_OFFER_FIELD"];
					$CATALOG_PRODUCT_PRICE = $connectionArr["CATALOG_PRODUCT_PRICE"];
					$CATALOG_PRODUCT_STORE_AMOUNT = $connectionArr["CATALOG_PRODUCT_STORE_AMOUNT"];
					$HIGHLOAD_BLOCK_ENTITY_FIELD = $connectionArr["HIGHLOAD_BLOCK_ENTITY_FIELD"];
					$IS_IMAGE = $connectionArr["IS_IMAGE"];
					$IS_FILE = $connectionArr["IS_FILE"];
					$IS_URL = $connectionArr["IS_URL"];
					$IS_REQUIRED = $connectionArr["IS_REQUIRED"];
					$USE_IN_SEARCH = $connectionArr["USE_IN_SEARCH"];
					$USE_IN_CODE = $connectionArr["USE_IN_CODE"];
					$IBLOCK_ELEMENT_PROPERTY_E = $connectionArr["IBLOCK_ELEMENT_PROPERTY_E"];
					$IBLOCK_ELEMENT_PROPERTY_G = $connectionArr["IBLOCK_ELEMENT_PROPERTY_G"];
					$IBLOCK_ELEMENT_PROPERTY_M = $connectionArr["IBLOCK_ELEMENT_PROPERTY_M"];
					$PROCESSING_TYPES = $connectionArr["PROCESSING_TYPES"];
				?>
				<tr class="data-row" id="row_connection_<?=$CONNECTIONS_ID;?>" onClick="javascript:CheckRowClass(this);" style="display: <?=CheckDisplay($connectionArr);?>">
					<td align="center">
						<input type="hidden" name="CONNECTIONS_ID[]" value="<?=$CONNECTIONS_ID;?>" />
						<?=$CONNECTIONS_ID;?>
					</td>
					<td align="center">
						<?if(is_array($entitiesArray) && count($entitiesArray)) { ?>
						<select name="ENTITY[]" class="select-search-entities">
							<?if($ENTITY) {?>
							<option value="<?=$ENTITY?>" selected><?=$entitiesArray[$ENTITY]?> [<?=$ENTITY?>]</option>
							<? } ?>
						</select>
						<?} else {?>
						<em><?=$NAME?> [<?=$ENTITY?>]</em>
						<? } ?>
					</td>
					<td align="center">
						<input type="text" name="NAME[]" class="adm-input" value="<?=$NAME;?>" disabled=""/>
					</td>
					<td align="center">
						<select name="ACTIVE[]" class="select-search">
							<option value="N"><?=GetMessage("OPTION_CHECKBOX_NO_ACTIVE");?></option>
							<option value="Y"<?if($ACTIVE == "Y") echo ' selected';?>><?=GetMessage("OPTION_CHECKBOX_YES");?></option>
						</select>
					</td>
					<?if($plan_IMPORT_FORMAT == "XML") { ?>
					<td align="center">
						<input type="text" name="ENTITY_ATTRIBUTE[]" class="adm-input" value="<?=$ENTITY_ATTRIBUTE;?>" />
					</td>
					<? } ?>
					<td align="center">
						<input type="number" name="SORT[]" class="adm-input" value="<?=$SORT;?>" />
					</td>
					<?if($plan_IMPORT_IBLOCK_SECTIONS=="Y"){?>
					<td align="center">
						<select name="IBLOCK_SECTION_FIELD[]" class="select-search-sections">
							<option value=""><?=GetMessage("MESSAGE_DO_NOT_USE");?></option>
							<?if($IBLOCK_SECTION_FIELD) {?>
							<option value="<?=$IBLOCK_SECTION_FIELD?>" selected><?=$iblockSectionFields[$IBLOCK_SECTION_FIELD]?></option>
							<? } ?>
						</select>
					</td>
					<?if($plan_IMPORT_FORMAT == "XML") { ?>
					<td align="center">
						<select name="IBLOCK_SECTION_PARENT_FIELD[]" class="select-search-sections">
							<option value=""><?=GetMessage("MESSAGE_DO_NOT_USE");?></option>
							<?if($IBLOCK_SECTION_PARENT_FIELD) {?>
							<option value="<?=$IBLOCK_SECTION_PARENT_FIELD?>" selected><?=$iblockSectionFields[$IBLOCK_SECTION_PARENT_FIELD]?></option>
							<? } ?>
						</select>
					</td>
					<? } else {	?>
					<td align="center">
						<select name="IBLOCK_SECTION_DEPTH_LEVEL[]" class="select-search-section-depth">
							<option value=""><?=GetMessage("MESSAGE_DO_NOT_USE");?></option>
							<?if($IBLOCK_SECTION_DEPTH_LEVEL) {?>
							<option value="<?=$IBLOCK_SECTION_DEPTH_LEVEL?>" selected><?=$sectionDepthLevels[$IBLOCK_SECTION_DEPTH_LEVEL]?></option>
							<? } ?>
						</select>
					</td>
					<? } ?>
					<? } ?>
					<?if($plan_IMPORT_IBLOCK_ELEMENTS=="Y"){?>
					<td align="center">
						<select name="IBLOCK_ELEMENT_FIELD[]" class="select-search-elements">
							<option value=""><?=GetMessage("MESSAGE_DO_NOT_USE");?></option>
							<?if($IBLOCK_ELEMENT_FIELD) {?>
							<option value="<?=$IBLOCK_ELEMENT_FIELD?>" selected><?=$iblockElementFields[$IBLOCK_ELEMENT_FIELD]?></option>
							<? } ?>
						</select>
					</td>
					<? } ?>
					<?if($plan_IMPORT_IBLOCK_PROPERTIES=="Y"){?>
					<td align="center">
						<select name="IBLOCK_ELEMENT_PROPERTY[]" class="select-search-properties">
							<option value=""><?=GetMessage("MESSAGE_DO_NOT_USE");?></option>
							<?if($IBLOCK_ELEMENT_PROPERTY) {?>
							<option value="<?=$IBLOCK_ELEMENT_PROPERTY?>" selected><?=$propertiesArray[$IBLOCK_ELEMENT_PROPERTY]?></option>
							<? } ?>
						</select>
					</td>
					<? } ?>
					<? if($plan_IMPORT_CATALOG_PRODUCT_OFFERS == "Y" && $isCatalogSKU) { ?>
					<td align="center">
						<select name="IBLOCK_ELEMENT_OFFER_FIELD[]" class="select-search-elements">
							<option value=""><?=GetMessage("MESSAGE_DO_NOT_USE");?></option>
							<?if($IBLOCK_ELEMENT_OFFER_FIELD) {?>
							<option value="<?=$IBLOCK_ELEMENT_OFFER_FIELD?>" selected><?=$iblockElementFields[$IBLOCK_ELEMENT_OFFER_FIELD]?></option>
							<? } ?>
						</select>
					</td>
					<td align="center">
						<select name="IBLOCK_ELEMENT_OFFER_PROPERTY[]" class="select-search-properties-offer">
							<option value=""><?=GetMessage("MESSAGE_DO_NOT_USE");?></option>
							<?if($IBLOCK_ELEMENT_OFFER_PROPERTY) {?>
							<option value="<?=$IBLOCK_ELEMENT_OFFER_PROPERTY?>" selected><?=$offersPropertiesArray[$IBLOCK_ELEMENT_OFFER_PROPERTY]?></option>
							<? } ?>
						</select>
					</td>
					<? } ?>
					<?if($plan_IMPORT_CATALOG_PRODUCTS=="Y"){?>
					<td align="center">
						<select name="CATALOG_PRODUCT_FIELD[]" class="select-search-catalog-product">
							<option value=""><?=GetMessage("MESSAGE_DO_NOT_USE");?></option>
							<?if($CATALOG_PRODUCT_FIELD) {?>
							<option value="<?=$CATALOG_PRODUCT_FIELD?>" selected><?=$catalogProductFields[$CATALOG_PRODUCT_FIELD]?></option>
							<? } ?>
						</select>
					</td>
					<? } ?>
					<? if($plan_IMPORT_CATALOG_PRODUCT_OFFERS == "Y" && $isCatalogSKU) { ?>
					<td align="center">
						<select name="CATALOG_PRODUCT_OFFER_FIELD[]" class="select-search-catalog-product">
							<option value=""><?=GetMessage("MESSAGE_DO_NOT_USE");?></option>
							<?if($CATALOG_PRODUCT_OFFER_FIELD) {?>
							<option value="<?=$CATALOG_PRODUCT_OFFER_FIELD?>" selected><?=$catalogProductFields[$CATALOG_PRODUCT_OFFER_FIELD]?></option>
							<? } ?>
						</select>
					</td>
					<? } ?>
					<?if($plan_IMPORT_CATALOG_PRICES=="Y"){?>
					<td align="center">
						<select name="CATALOG_PRODUCT_PRICE[]" class="select-search-price">
							<option value=""><?=GetMessage("MESSAGE_DO_NOT_USE");?></option>
							<?if($CATALOG_PRODUCT_PRICE) {?>
							<option value="<?=$CATALOG_PRODUCT_PRICE?>" selected><?=$catalogProductPrice[$CATALOG_PRODUCT_PRICE]?></option>
							<? } ?>
						</select>
					</td>
					<? } ?>
					<?if($plan_IMPORT_CATALOG_STORE_AMOUNT=="Y"){?>
					<td align="center">
						<select name="CATALOG_PRODUCT_STORE_AMOUNT[]" class="select-search-store">
							<option value=""><?=GetMessage("MESSAGE_DO_NOT_USE");?></option>
							<?if($CATALOG_PRODUCT_STORE_AMOUNT) {?>
							<option value="<?=$CATALOG_PRODUCT_STORE_AMOUNT?>" selected><?=$catalogProductStore[$CATALOG_PRODUCT_STORE_AMOUNT]?></option>
							<? } ?>
						</select>
					</td>
					<? } ?>
					<?if($plan_IMPORT_HIGHLOAD_BLOCK_ENTITIES=="Y"){?>
					<td align="center">
						<select name="HIGHLOAD_BLOCK_ENTITY_FIELD[]" class="select-search-highload">
							<option value=""><?=GetMessage("MESSAGE_DO_NOT_USE");?></option>
							<?if($HIGHLOAD_BLOCK_ENTITY_FIELD) {?>
							<option value="<?=$HIGHLOAD_BLOCK_ENTITY_FIELD?>" selected><?=$highloadBlockEntityFields[$HIGHLOAD_BLOCK_ENTITY_FIELD]?></option>
							<? } ?>
						</select>
					</td>
					<? } ?>
					<td align="center">
						<select name="IS_IMAGE[]" class="select-search">
							<option value="N"><?=GetMessage("OPTION_CHECKBOX_NO");?></option>
							<option value="Y"<?if($IS_IMAGE == "Y") echo ' selected';?>><?=GetMessage("OPTION_CHECKBOX_YES");?></option>
						</select>
					</td>
					<td align="center">
						<select name="IS_URL[]" class="select-search">
							<option value="N"><?=GetMessage("OPTION_CHECKBOX_NO");?></option>
							<option value="Y"<?if($IS_URL == "Y") echo ' selected';?>><?=GetMessage("OPTION_CHECKBOX_YES");?></option>
						</select>
					</td>
					<td align="center">
						<select name="IS_FILE[]" class="select-search">
							<option value="N"><?=GetMessage("OPTION_CHECKBOX_NO");?></option>
							<option value="Y"<?if($IS_FILE == "Y") echo ' selected';?>><?=GetMessage("OPTION_CHECKBOX_YES");?></option>
						</select>
					</td>
					<td align="center">
						<select name="IS_REQUIRED[]" class="select-search">
							<option value="N"><?=GetMessage("OPTION_CHECKBOX_NO");?></option>
							<option value="Y"<?if($IS_REQUIRED == "Y") echo ' selected';?>><?=GetMessage("OPTION_CHECKBOX_YES");?></option>
						</select>
					</td>
					<td align="center">
						<select name="USE_IN_SEARCH[]" class="select-search">
							<option value="N"><?=GetMessage("OPTION_CHECKBOX_NO");?></option>
							<option value="Y"<?if($USE_IN_SEARCH == "Y") echo ' selected';?>><?=GetMessage("OPTION_CHECKBOX_YES");?></option>
						</select>
					</td>
					<?//if($plan_IMPORT_FORMAT != "XML") {?>
					<td align="center">
						<select name="USE_IN_CODE[]" class="select-search-code">
							<?if($USE_IN_CODE) {?>
							<option value="<?=$USE_IN_CODE?>" selected><?=$useInCodeValues[$USE_IN_CODE]?></option>
							<? } ?>
						</select>
					</td>
					<?// } ?>
					<?if($plan_IMPORT_IBLOCK_PROPERTIES=="Y"){?>
					<td align="center">
						<select name="IBLOCK_ELEMENT_PROPERTY_E[]" class="select-search-elements">
							<option value=""><?=GetMessage("MESSAGE_DO_NOT_USE");?></option>
							<?if($IBLOCK_ELEMENT_PROPERTY_E) {?>
							<option value="<?=$IBLOCK_ELEMENT_PROPERTY_E?>" selected><?=$iblockElementFields[$IBLOCK_ELEMENT_PROPERTY_E]?></option>
							<? } ?>
						</select>
					</td>
					<td align="center">
						<select name="IBLOCK_ELEMENT_PROPERTY_G[]" class="select-search-sections">
							<option value=""><?=GetMessage("MESSAGE_DO_NOT_USE");?></option>
							<?if($IBLOCK_ELEMENT_PROPERTY_G) {?>
							<option value="<?=$IBLOCK_ELEMENT_PROPERTY_G?>" selected><?=$iblockSectionFields[$IBLOCK_ELEMENT_PROPERTY_G]?></option>
							<? } ?>
						</select>
					</td>
					<td align="center">
						<select name="IBLOCK_ELEMENT_PROPERTY_M[]" class="select-search">
							<option value=""><?=GetMessage("MESSAGE_DO_NOT_USE");?></option>
							<?
							foreach($coordinates as $k => $field)
							{
							?>
							<option value="<?=$k?>" <?if($IBLOCK_ELEMENT_PROPERTY_M==$k) echo 'selected';?>><?=$field?> [<?=$k;?>]</option>
							<?
							}
							?>
						</select>
					</td>
					<? } ?>
					<td align="center">
					<?
					if($PROCESSING_TYPES)
					{
						$processingTypesArr = unserialize(base64_decode($PROCESSING_TYPES));
						if(!is_array($processingTypesArr))
							$processingTypesArr = Array();
					}
					else
					{
						$processingTypesArr = array();
					}
					?>
						<select name="PROCESSING_TYPES[<?=$CONNECTIONS_ID?>][]" class="select-search" multiple>
							<option value=""><?=GetMessage("MESSAGE_DO_NOT_USE");?></option>
							<?
							foreach($listTypes as $k => $field)
							{
							?>
							<option value="<?=$k?>" <?if(in_array($k, $processingTypesArr)) echo 'selected';?>><?=$field?></option>
							<?
							}
							?>
						</select>
					</td>
					<td align="center">
						<button type="button" onClick="WebprostorCoreRemoveRow('row_connection_<?=$CONNECTIONS_ID;?>')" title="<?=GetMessage("BTN_DELETE_CONNECTION");?>" /><img src="/bitrix/themes/.default/images/buttons/delete.gif" /></button>
					</td>
				</tr>
				<?
				}
				?>
			</tbody>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2" align="left" style="padding-top: 10px;">
		<input class="adm-btn-copy adm-btn" type="button" onClick="javascript:AddConnectionRow('table_CONNECTIONS');" value="<?=GetMessage("BTN_ADD_CONNECTION");?>"<?if(!is_array($entitiesArray) || count($entitiesArray) == 0):?> disabled style="opacity: 0.5"<?else:?>style="position: sticky; left: 10px;"<?endif;?> />
	</td>
</tr>
<? } ?>
<?
$tabControl->Buttons(
	array(
		"disabled"=>(($moduleAccessLevel<"W" || $ID==0)),
		"btnSave"=>true,
		"btnApply"=>true,
		"back_url"=>"webprostor.import_plan_edit.php?ID=".$ID."&lang=".LANG,
	)
);
?>
<input type="hidden" name="lang" value="<?=LANG?>">
<input type="hidden" name="SHOW_ACTIVE" value="<?=$SHOW_ACTIVE?>">
<input type="hidden" name="SHOW_REQUIRED" value="<?=$SHOW_REQUIRED?>">
<input type="hidden" name="SHOW_IMAGE" value="<?=$SHOW_IMAGE?>">
<input type="hidden" name="SHOW_FILE" value="<?=$SHOW_FILE?>">
<input type="hidden" name="SHOW_URL" value="<?=$SHOW_URL?>">
<input type="hidden" name="SHOW_SEARCH" value="<?=$SHOW_SEARCH?>">
<?
if($ID>0) {
?>
  <input type="hidden" name="ID" value="<?=$ID?>">
<?
}

$tabControl->End();

if($ID>0 && $plan_IMPORT_FORMAT == "XML")
{
	echo BeginNote();
	echo GetMessage("ENTITY_ATTRIBUTE_NOTE");
	foreach($attributesArray as $k => $attributeGroup)
	{
		echo '<strong>'.$k.':</strong><br />';
		echo '<ul>';
		foreach($attributeGroup as $k2 => $attributeName)
		{
			echo '<li>'.$attributeName.'</li>';
		}
		echo '</ul>';
	}
	echo EndNote();
}
	
$tabControl->ShowWarnings("PLAN_EDIT", $message);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");