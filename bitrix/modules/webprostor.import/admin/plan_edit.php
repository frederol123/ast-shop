<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/webprostor.import/prolog.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/webprostor.import/include.php");

IncludeModuleLangFile(__FILE__);

$module_id = 'webprostor.import';
$moduleAccessLevel = $APPLICATION->GetGroupRight($module_id);

if ($moduleAccessLevel == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$aTabs = array(
  array("DIV" => "main", "TAB" => GetMessage("ELEMENT_TAB_1"), "ICON"=>"", "TITLE"=>GetMessage("ELEMENT_TAB_1_TITLE")),
  array("DIV" => "objects", "TAB" => GetMessage("ELEMENT_TAB_2"), "ICON"=>"", "TITLE"=>GetMessage("ELEMENT_TAB_2_TITLE")),
);

$sTableID = "webprostor_import_plans";
$ID = intval($ID);
$COPY_ID = intval($COPY_ID);
$message = null;
$bVarsFromForm = false;

$arFields = array();

$aTabs[] = array("DIV" => "setting", "TAB" => GetMessage("ELEMENT_TAB_3"), "ICON"=>"", "TITLE"=>GetMessage("ELEMENT_TAB_3_TITLE"));
$aTabs[] = array("DIV" => "debug", "TAB" => GetMessage("ELEMENT_TAB_4"), "ICON"=>"", "TITLE"=>GetMessage("ELEMENT_TAB_4_TITLE"));

$tabControl = new CAdminTabControl("tabControl", $aTabs);

if($REQUEST_METHOD == "POST" && ($save!="" || $apply!="") && $moduleAccessLevel=="W" && check_bitrix_sessid()) 
{
	$element = new CWebprostorImportPlan;

	$arFields = Array(
		"NAME" => $NAME,
		"SORT" => $SORT,
		"ACTIVE" => $ACTIVE,
		"SHOW_IN_MENU" => $SHOW_IN_MENU,
		"IBLOCK_ID" => $IBLOCK_ID,
		"HIGHLOAD_BLOCK" => $HIGHLOAD_BLOCK,
		"IMPORT_FORMAT" => $IMPORT_FORMAT,
		"ITEMS_PER_ROUND" => $ITEMS_PER_ROUND,
		"IMPORT_FILE" => $IMPORT_FILE,
		"IMPORT_FILE_SHARSET" => $IMPORT_FILE_SHARSET,
		"IMPORT_FILE_DELETE" => $IMPORT_FILE_DELETE,
		"IMPORT_FILE_CHECK" => $IMPORT_FILE_CHECK,
		"IMPORT_FILE_URL" => $IMPORT_FILE_URL,
		"IMPORT_IBLOCK_SECTIONS" => $IMPORT_IBLOCK_SECTIONS,
		"SECTIONS_ADD" => $SECTIONS_ADD,
		"SECTIONS_DEFAULT_ACTIVE" => $SECTIONS_DEFAULT_ACTIVE,
		"SECTIONS_DEFAULT_SECTION_ID" => $SECTIONS_DEFAULT_SECTION_ID,
		"SECTIONS_UPDATE" => $SECTIONS_UPDATE,
		"SECTIONS_OUT_ACTION" => $SECTIONS_OUT_ACTION,
		"SECTIONS_OUT_ACTION_FILTER" => $SECTIONS_OUT_ACTION_FILTER,
		"SECTIONS_IN_ACTION" => $SECTIONS_IN_ACTION,
		"SECTIONS_IN_ACTION_FILTER" => $SECTIONS_IN_ACTION_FILTER,
		"IMPORT_IBLOCK_ELEMENTS" => $IMPORT_IBLOCK_ELEMENTS,
		"ELEMENTS_2_STEP_SEARCH" => $ELEMENTS_2_STEP_SEARCH,
		"ELEMENTS_ADD" => $ELEMENTS_ADD,
		"ELEMENTS_DEFAULT_ACTIVE" => $ELEMENTS_DEFAULT_ACTIVE,
		"ELEMENTS_DEFAULT_SECTION_ID" => $ELEMENTS_DEFAULT_SECTION_ID,
		"ELEMENTS_UPDATE" => $ELEMENTS_UPDATE,
		"ELEMENTS_OUT_ACTION" => $ELEMENTS_OUT_ACTION,
		"ELEMENTS_OUT_ACTION_FILTER" => $ELEMENTS_OUT_ACTION_FILTER,
		"ELEMENTS_IN_ACTION" => $ELEMENTS_IN_ACTION,
		"ELEMENTS_IN_ACTION_FILTER" => $ELEMENTS_IN_ACTION_FILTER,
		"IMPORT_IBLOCK_PROPERTIES" => $IMPORT_IBLOCK_PROPERTIES,
		"PROPERTIES_ADD" => $PROPERTIES_ADD,
		"PROPERTIES_UPDATE" => $PROPERTIES_UPDATE,
		"PROPERTIES_RESET" => $PROPERTIES_RESET,
		"PROPERTIES_TRANSLATE_XML_ID" => $PROPERTIES_TRANSLATE_XML_ID,
		"PROPERTIES_SET_DEFAULT_VALUES" => $PROPERTIES_SET_DEFAULT_VALUES,
		"PROPERTIES_SKIP_DOWNLOAD_URL_UPDATE" => $PROPERTIES_SKIP_DOWNLOAD_URL_UPDATE,
		"PROPERTIES_WHATERMARK" => $PROPERTIES_WHATERMARK,
		"PROPERTIES_INCREMENT_TO_MULTIPLE" => $PROPERTIES_INCREMENT_TO_MULTIPLE,
		"IMPORT_CATALOG_PRODUCTS" => $IMPORT_CATALOG_PRODUCTS,
		"PRODUCTS_PARAMS" => base64_encode(serialize(Array(
			"PRODUCTS_QUANTITY_TRACE" => $PRODUCTS_QUANTITY_TRACE, 
			"PRODUCTS_USE_STORE" => $PRODUCTS_USE_STORE,
			"PRODUCTS_SUBSCRIBE" => $PRODUCTS_SUBSCRIBE
		))),
		"PRODUCTS_ADD" => $PRODUCTS_ADD,
		"PRODUCTS_UPDATE" => $PRODUCTS_UPDATE,
		"IMPORT_CATALOG_PRODUCT_OFFERS" => $IMPORT_CATALOG_PRODUCT_OFFERS,
		"OFFERS_ADD" => $OFFERS_ADD,
		"OFFERS_UPDATE" => $OFFERS_UPDATE,
		"OFFERS_SET_NAME_FROM_ELEMENT" => $OFFERS_SET_NAME_FROM_ELEMENT,
		"OFFERS_OUT_ACTION" => $OFFERS_OUT_ACTION,
		"OFFERS_OUT_ACTION_FILTER" => $OFFERS_OUT_ACTION_FILTER,
		"OFFERS_IN_ACTION" => $OFFERS_IN_ACTION,
		"OFFERS_IN_ACTION_FILTER" => $OFFERS_IN_ACTION_FILTER,
		"IMPORT_CATALOG_PRICES" => $IMPORT_CATALOG_PRICES,
		"PRICES_ADD" => $PRICES_ADD,
		"PRICES_UPDATE" => $PRICES_UPDATE,
		"IMPORT_CATALOG_STORE_AMOUNT" => $IMPORT_CATALOG_STORE_AMOUNT,
		"STORE_AMOUNT_ADD" => $STORE_AMOUNT_ADD,
		"STORE_AMOUNT_UPDATE" => $STORE_AMOUNT_UPDATE,
		"IMPORT_HIGHLOAD_BLOCK_ENTITIES" => $IMPORT_HIGHLOAD_BLOCK_ENTITIES,
		"ENTITIES_ADD" => $ENTITIES_ADD,
		"ENTITIES_UPDATE" => $ENTITIES_UPDATE,
		"ENTITIES_TRANSLATE_XML_ID" => $ENTITIES_TRANSLATE_XML_ID,
		"PATH_TO_IMAGES" => $PATH_TO_IMAGES,
		"CLEAR_IMAGES_DIR" => $CLEAR_IMAGES_DIR,
		"CLEAR_UPLOAD_TMP_DIR" => $CLEAR_UPLOAD_TMP_DIR,
		"PATH_TO_IMAGES_URL" => $PATH_TO_IMAGES_URL,
		"RESIZE_IMAGE" => $RESIZE_IMAGE,
		"PATH_TO_FILES" => $PATH_TO_FILES,
		"CLEAR_FILES_DIR" => $CLEAR_FILES_DIR,
		"PATH_TO_FILES_URL" => $PATH_TO_FILES_URL,
		"CURL_TIMEOUT" => $CURL_TIMEOUT,
		"CURL_FOLLOWLOCATION" => $CURL_FOLLOWLOCATION,
		"RAW_URL_DECODE" => $RAW_URL_DECODE,
		"AGENT_INTERVAL" => $AGENT_INTERVAL,
		"AGENT_INTERVAL_URL" => $AGENT_INTERVAL_URL,
		"CSV_DELIMITER" => $CSV_DELIMITER,
		"XLS_SHEET" => $XLS_SHEET,
		"CSV_XLS_NAME_LINE" => $CSV_XLS_NAME_LINE,
		"CSV_XLS_START_LINE" => $CSV_XLS_START_LINE,
		"CSV_XLS_MAX_DEPTH_LEVEL" => $CSV_XLS_MAX_DEPTH_LEVEL,
		"XML_ENTITY" => $XML_ENTITY,
		"XML_USE_ENTITY_NAME" => $XML_USE_ENTITY_NAME,
		"XML_PARSE_PARAMS_TO_PROPERTIES" => $XML_PARSE_PARAMS_TO_PROPERTIES,
		"XML_ADD_PROPERTIES_FOR_PARAMS" => $XML_ADD_PROPERTIES_FOR_PARAMS,
		"DEBUG_EVENTS" => $DEBUG_EVENTS,
		"DEBUG_IMAGES" => $DEBUG_IMAGES,
		"DEBUG_FILES" => $DEBUG_FILES,
		"DEBUG_URL" => $DEBUG_URL,
		"DEBUG_IMPORT_SECTION" => $DEBUG_IMPORT_SECTION,
		"DEBUG_IMPORT_ELEMENTS" => $DEBUG_IMPORT_ELEMENTS,
		"DEBUG_IMPORT_PROPERTIES" => $DEBUG_IMPORT_PROPERTIES,
		"DEBUG_IMPORT_PRODUCTS" => $DEBUG_IMPORT_PRODUCTS,
		"DEBUG_IMPORT_OFFERS" => $DEBUG_IMPORT_OFFERS,
		"DEBUG_IMPORT_PRICES" => $DEBUG_IMPORT_PRICES,
		"DEBUG_IMPORT_STORE_AMOUNT" => $DEBUG_IMPORT_STORE_AMOUNT,
		"DEBUG_IMPORT_ENTITIES" => $DEBUG_IMPORT_ENTITIES,
	);
	
	if($ID > 0)
	{
		$res = $element->Update($ID, $arFields);
	}
	else
	{
		$ID = $element->Add($arFields);
		$res = ($ID > 0);
	}
	
	if($ID > 0 && $COPY_ID > 0)
	{
		$CConnection = new CWebprostorImportPlanConnections;
		$connections = Array();
		$connectionsRes = $CConnection->GetList(Array("SORT" => "ASC"), Array("PLAN_ID" => $COPY_ID));
		while($connectionArr = $connectionsRes->GetNext())
		{
			$connections["ID"][] = "copy_".$connectionArr["ID"];
			
			$connections["ENTITY"][] = $connectionArr["ENTITY"];
			$connections["ENTITY_ATTRIBUTE"][] = $connectionArr["ENTITY_ATTRIBUTE"];
			$connections["ACTIVE"][] = $connectionArr["ACTIVE"];
			$connections["NAME"][] = $connectionArr["NAME"];
			$connections["SORT"][] = $connectionArr["SORT"];
			$connections["IBLOCK_SECTION_FIELD"][] = $connectionArr["IBLOCK_SECTION_FIELD"];
			$connections["IBLOCK_SECTION_DEPTH_LEVEL"][] = $connectionArr["IBLOCK_SECTION_DEPTH_LEVEL"];
			$connections["IBLOCK_SECTION_PARENT_FIELD"][] = $connectionArr["IBLOCK_SECTION_PARENT_FIELD"];
			$connections["IBLOCK_ELEMENT_FIELD"][] = $connectionArr["IBLOCK_ELEMENT_FIELD"];
			$connections["IBLOCK_ELEMENT_PROPERTY"][] = $connectionArr["IBLOCK_ELEMENT_PROPERTY"];
			$connections["IBLOCK_ELEMENT_PROPERTY_E"][] = $connectionArr["IBLOCK_ELEMENT_PROPERTY_E"];
			$connections["IBLOCK_ELEMENT_PROPERTY_G"][] = $connectionArr["IBLOCK_ELEMENT_PROPERTY_G"];
			$connections["IBLOCK_ELEMENT_PROPERTY_M"][] = $connectionArr["IBLOCK_ELEMENT_PROPERTY_M"];
			$connections["IBLOCK_ELEMENT_OFFER_FIELD"][] = $connectionArr["IBLOCK_ELEMENT_OFFER_FIELD"];
			$connections["IBLOCK_ELEMENT_OFFER_PROPERTY"][] = $connectionArr["IBLOCK_ELEMENT_OFFER_PROPERTY"];
			$connections["CATALOG_PRODUCT_FIELD"][] = $connectionArr["CATALOG_PRODUCT_FIELD"];
			$connections["CATALOG_PRODUCT_OFFER_FIELD"][] = $connectionArr["CATALOG_PRODUCT_OFFER_FIELD"];
			$connections["CATALOG_PRODUCT_PRICE"][] = $connectionArr["CATALOG_PRODUCT_PRICE"];
			$connections["CATALOG_PRODUCT_STORE_AMOUNT"][] = $connectionArr["CATALOG_PRODUCT_STORE_AMOUNT"];
			$connections["HIGHLOAD_BLOCK_ENTITY_FIELD"][] = $connectionArr["HIGHLOAD_BLOCK_ENTITY_FIELD"];
			$connections["IS_IMAGE"][] = $connectionArr["IS_IMAGE"];
			$connections["IS_FILE"][] = $connectionArr["IS_FILE"];
			$connections["IS_URL"][] = $connectionArr["IS_URL"];
			$connections["IS_REQUIRED"][] = $connectionArr["IS_REQUIRED"];
			$connections["USE_IN_SEARCH"][] = $connectionArr["USE_IN_SEARCH"];
			$connections["USE_IN_CODE"][] = $connectionArr["USE_IN_CODE"];
			$connections["PROCESSING_TYPES"][] = unserialize(base64_decode($connectionArr["PROCESSING_TYPES"]));
			
		}
		if(count($connections)>0)
		{
			$resCopyConnections = $CConnection->UpdatePlanConnections($ID, $connections);
		}
	}
	
	if($res)
	{
		if ($apply != "")
		LocalRedirect("/bitrix/admin/webprostor.import_plan_edit.php?ID=".$ID."&mess=ok&lang=".LANG."&".$tabControl->ActiveTabParam());
		else
		LocalRedirect("/bitrix/admin/webprostor.import_plans.php?lang=".LANG);
	}
	else
	{
		if($e = $APPLICATION->GetException())
			$message = new CAdminMessage(GetMessage("MESSAGE_SAVE_ERROR"), $e);
		$bVarsFromForm = true;
	}
}

$str_NAME = "";
$str_SORT = "500";
$str_IMPORT_FORMAT = "CSV";
$str_ITEMS_PER_ROUND = "100";
$str_RESIZE_IMAGE = "N";
$str_CURL_TIMEOUT = "5";
$str_AGENT_INTERVAL = "600";
$str_AGENT_INTERVAL_URL = "86400";
$str_XLS_SHEET = 0;
$str_IMPORT_FILE_DELETE = "N";
$str_IMPORT_FILE_CHECK = "Y";

if($ID>0 || $COPY_ID>0)
{
    $cData = new CWebprostorImportPlan;
	if($ID>0)
		$element = $cData->GetById($ID);
	else
		$element = $cData->GetById($COPY_ID);
	if(!$element->ExtractFields("str_"))
		$ID=0;
}
	
if(intVal($str_IBLOCK_ID)>0 && CModule::IncludeModule("iblock"))
{
	$arFilter = Array('IBLOCK_ID'=>$str_IBLOCK_ID);
	$iblockSectionDB = CIBlockSection::GetList(Array("ID"=>"ASC"), $arFilter, true);
	$iblockSectionsArray = Array(0 => GetMessage("FIELDS_BLOCK_ID_NO"));
	while($iblockSectionsResult = $iblockSectionDB->GetNext())
	{
		$iblockSectionsArray[$iblockSectionsResult["ID"]] = htmlspecialcharsbx($iblockSectionsResult['NAME']).' ['.$iblockSectionsResult["ID"].']';
	}
}
	
if(intVal($str_IBLOCK_ID)>0 && CModule::IncludeModule("catalog"))
{
	$catalogSKU = CCatalogSKU::GetInfoByProductIBlock($str_IBLOCK_ID);
	
	$str_OFFER_IBLOCK_ID = $catalogSKU["IBLOCK_ID"];
}

if($bVarsFromForm)
	$DB->InitTableVarsForEdit($sTableID, "", "str_");

$APPLICATION->SetTitle(($ID>0? GetMessage("ELEMENT_EDIT_TITLE").': '.$str_NAME : GetMessage("ELEMENT_ADD_TITLE")));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$aMenu = array(
	array(
		"TEXT"=>GetMessage("ELEMENTS_LIST"),
		"TITLE"=>GetMessage("ELEMENTS_LIST_TITLE"),
		"LINK"=>"webprostor.import_plans.php?lang=".LANG,
		"ICON"=>"btn_list",
	)
);

$aMenu[] = array("SEPARATOR"=>"Y");

if($ID>0)
{

	$aMenu[] = array(
		"TEXT"  => GetMessage("EDIT_CONNECTIONS_BTN"),
		"TITLE" => GetMessage("EDIT_CONNECTIONS_BTN_TITLE"),
		"LINK"  => "webprostor.import_plan_connections_edit.php?ID=".$ID."&lang=".LANG,
		"ICON"  => "btn_green",
	);

	$aMenu[] = array(
		"TEXT"  => GetMessage("IMPORT_CONNECTIONS"),
		"TITLE" => GetMessage("IMPORT_CONNECTIONS_TITLE"),
		"LINK"  => "webprostor.import_connections_import.php?lang=".LANGUAGE_ID."&PLAN_ID=".$ID,
	);

	$aMenu[] = array(
		"TEXT"  => GetMessage("BTN_ACTIONS"),
		"TITLE" => GetMessage("BTN_ACTIONS_TITLE"),
		"ICON"  => "btn_new",
		"MENU"  => Array(
			array(
				"TEXT"  => GetMessage("START_IMPORT"),
				"LINK"  => "webprostor.import_manually.php?PLAN_ID=".$ID."&lang=".LANG."&".bitrix_sessid_get(),
				"LINK_PARAM"  => "target=\"_blank\"",
			),
			array(
				"TEXT"  => GetMessage("OPEN_LOGS"),
				"LINK"  => "webprostor.import_logs.php?PLAN_ID=".$ID."&find_plan_id=".$ID."&apply_filter=Y&lang=".LANG,
				"LINK_PARAM"  => "target=\"_blank\"",
			),
			array(
				"TEXT"  => GetMessage("ELEMENT_ADD_BTN"),
				"TITLE" => GetMessage("ELEMENT_ADD_BTN_TITLE"),
				"LINK"  => "webprostor.import_plan_edit.php?lang=".LANG,
				"ICON"  => "edit",
			),
			array(
				"TEXT"  => GetMessage("COPY_BTN"),
				"TITLE" => GetMessage("COPY_BTN_TITLE"),
				"LINK"  => "webprostor.import_plan_edit.php?COPY_ID={$ID}&lang=".LANG,
				"ICON"  => "copy",
			),
			array(
				"TEXT"  => GetMessage("EXPORT_BTN"),
				"TITLE" => GetMessage("EXPORT_BTN_TITLE"),
				"LINK"  => "javascript:(new BX.CDialog({
							width: 310,
							height: 170,
							resizable: false,
							title: '".GetMessage('EXPORT_TITLE')."',
							buttons: [BX.CAdminDialog.btnSave, BX.CAdminDialog.btnCancel],
							content: '<form action=\"".CUtil::JSEscape($GLOBALS['APPLICATION']->GetCurPageParam('', array('action')))."\" method=\"post\" enctype=\"multipart/form-data\">"
										.bitrix_sessid_post()
										."<input type=\"hidden\" name=\"action\" value=\"plan_export\" />"
										."<input type=\"hidden\" name=\"ID\" value=\"".$ID."\" />"
										.GetMessage('EXPORT_PLAN_SETTINGS_NOTE')
										."<input type=\"checkbox\" name=\"plan_connections\" id=\"plan_connections\" value=\"Y\" checked=\"checked\" /><label for=\"plan_connections\">".GetMessage('EXPORT_PLAN_CONNECTIONS')."</label><br />"
										."<input type=\"checkbox\" name=\"plan_properties\" id=\"plan_properties\" value=\"Y\" checked=\"checked\" /><label for=\"plan_properties\">".GetMessage('EXPORT_PLAN_PROPERTIES')."</label><br />"
										."<input type=\"checkbox\" name=\"plan_processing_settings\" id=\"plan_processing_settings\" value=\"Y\" checked=\"checked\" /><label for=\"plan_processing_settings\">".GetMessage('EXPORT_PLAN_PROCESSING_SETTINGS')."</label><br />"
									."</form>'
						})).Show()",
				"ICON"  => "export",
			),
			array(
				"TEXT"  => GetMessage("ELEMENT_DELETE_BTN"),
				"TITLE" => GetMessage("ELEMENT_DELETE_BTN_TITLE"),
				"LINK"  => "javascript:if(confirm('".GetMessage("ELEMENT_DELETE_BTN_MESSAGE")."')) ".
				  "window.location='webprostor.import_plans.php?action=delete&ID[]=".CUtil::JSEscape($ID)."&lang=".LANGUAGE_ID."&".bitrix_sessid_get()."';",
				"ICON"  => "delete",
			)
		),
	);
}
else
{
	$importAction = "javascript:(new BX.CDialog({
					width: 410,
					height: 210,
					resizable: true,
					title: '".GetMessage('IMPORT_TITLE')."',
					content: '<form action=\"".CUtil::JSEscape($GLOBALS['APPLICATION']->GetCurPageParam('', array('action')))."\" method=\"post\" enctype=\"multipart/form-data\">"
								.bitrix_sessid_post()
								."<input type=\"hidden\" name=\"action\" value=\"plan_import\" />"
								."<input type=\"file\" name=\"xml_file\" id=\"xml_file\" /><br/><br/>"
								."<label for=\"import_iblock_id\">".GetMessage('FIELDS_IBLOCK_ID')."</label><br /><select id=\"import_iblock_id\" name=\"import_iblock_id\">";
	$resIblocks = CIBlock::GetList(Array("NAME"=>"ASC"), Array());
	$importAction .= "<option value=\"0\">".htmlspecialcharsbx(GetMessage("FIELDS_BLOCK_ID_NO"))."</option>";
	while($iblock = $resIblocks->Fetch()){
		$importAction .= "<option value=\"".$iblock["ID"]."\">".htmlspecialcharsbx($iblock["NAME"])." [".$iblock["ID"]."]</option>";
	}
	$importAction .= "</select><br/><br/>"
								."<label for=\"import_highload_block\">".GetMessage('FIELDS_HIGHLOAD_BLOCK')."</label><br /><select id=\"import_highload_block\" name=\"import_highload_block\">";
	$rsData = \Bitrix\Highloadblock\HighloadBlockTable::getList();
	$importAction .= "<option value=\"0\">".htmlspecialcharsbx(GetMessage("FIELDS_BLOCK_ID_NO"))."</option>";
	while($hldata = $rsData->Fetch())
	{
		$importAction .= "<option value=\"".$hldata["ID"]."\">".htmlspecialcharsbx($hldata["NAME"])." [".$hldata["TABLE_NAME"]."]</option>";
	}
	$importAction .= "</select><br/><br/>"
								."<center><input type=\"submit\" onclick=\"if(document.getElementById(\'xml_file\').value.length == \'0\' || (document.getElementById(\'import_iblock_id\').value == \'0\' && document.getElementById(\'import_highload_block\').value == \'0\')) return false;\" value=\"".GetMessage('IMPORT_SUBMIT')."\" /></center>"
							."</form>'
				})).Show()";
	$aMenu[] = array(
		"TEXT"  => GetMessage("IMPORT_BTN"),
		"TITLE" => GetMessage("IMPORT_BTN_TITLE"),
		"LINK"  => $importAction,
		"ICON"  => "btn_copy",
	);
}

$context = new CAdminContextMenu($aMenu);

$context->Show();

if($_REQUEST["mess"] == "ok" && $ID>0)
	CAdminMessage::ShowMessage(array("MESSAGE"=>GetMessage("ELEMENT_SAVED"), "TYPE"=>"OK"));

if($message)
	echo $message->Show();
elseif($element->LAST_ERROR!="")
	CAdminMessage::ShowMessage($element->LAST_ERROR);
	
if ($moduleAccessLevel < 'W' && $moduleAccessLevel <> 'D')
{
	echo BeginNote();
	echo GetMessage('MESSAGE_NOT_SAVE_ACCESS');
	echo EndNote();
}
?>
<form method="POST" name="PLAN_EDIT" id="plan_edit" Action="<?echo $APPLICATION->GetCurPage()?>" ENCTYPE="multipart/form-data">
<?echo bitrix_sessid_post();?>
<?
$tabControl->Begin();
?>
<?
$tabControl->BeginNextTab();

$IMPORT_FORMATS = Array(
	"CSV" => GetMessage("FIELDS_IMPORT_FORMAT_CSV").' [CSV]',
	"XML" => GetMessage("FIELDS_IMPORT_FORMAT_XML").' [XML]',
	"XLS" => GetMessage("FIELDS_IMPORT_FORMAT_XLS").' [XLS]',
	"XLSX" => GetMessage("FIELDS_IMPORT_FORMAT_XLSX").' [XLSX]',
);
$IMPORT_FILE_SHARSETS = Array(
	"UTF-8" => GetMessage("FIELDS_IMPORT_FILE_SHARSET_UTF_8"),
	"WINDOWS-1251" => GetMessage("FIELDS_IMPORT_FILE_SHARSET_WINDOWS_1251"),
);

$arFormFields = Array();
$arFormFields["MAIN"]["LABEL"] = GetMessage("GROUP_MAIN");

if($ID>0)
{
	$arFormFields["MAIN"]["ITEMS"][] = Array(
		"CODE" => "OBJECT_ID",
		"REQUIRED" => "Y",
		"LABEL" => "ID",
		"VALUE" => $str_ID,
	);
	
	$arFormFields["MAIN"]["ITEMS"][] = Array(
		"CODE" => "OBJECT_LAST_IMPORT_DATE",
		"LABEL" => "ID",
		"LABEL" => GetMessage("FIELDS_LAST_IMPORT_DATE"),
		"DESCRIPTION" => GetMessage("FIELDS_LAST_IMPORT_DATE_NOTE"),
		"VALUE" => $str_LAST_IMPORT_DATE,
	);
	
	$arFormFields["MAIN"]["ITEMS"][] = Array(
		"CODE" => "OBJECT_LAST_FINISH_IMPORT_DATE",
		"LABEL" => "ID",
		"LABEL" => GetMessage("FIELDS_LAST_FINISH_IMPORT_DATE"),
		"DESCRIPTION" => GetMessage("FIELDS_LAST_FINISH_IMPORT_DATE_NOTE"),
		"VALUE" => $str_LAST_FINISH_IMPORT_DATE,
	);
}
$arFormFields["MAIN"]["ITEMS"][] = Array(
	"CODE" => "ACTIVE",
	"REQUIRED" => "N",
	"TYPE" => "CHECKBOX",
	"LABEL" => GetMessage("FIELDS_ACTIVE"),
	"DESCRIPTION" => GetMessage("FIELDS_ACTIVE_NOTE"),
	"VALUE" => (!$ID && !$COPY_ID)?'N':$str_ACTIVE,
);
$arFormFields["MAIN"]["ITEMS"][] = Array(
	"CODE" => "SHOW_IN_MENU",
	"REQUIRED" => "N",
	"TYPE" => "CHECKBOX",
	"LABEL" => GetMessage("FIELDS_SHOW_IN_MENU"),
	"VALUE" => (!$ID && !$COPY_ID)?'N':$str_SHOW_IN_MENU,
);
$arFormFields["MAIN"]["ITEMS"][] = Array(
	"CODE" => "NAME",
	"REQUIRED" => "Y",
	"TYPE" => "TEXT",
	"LABEL" => GetMessage("FIELDS_NAME"),
	"VALUE" => $str_NAME,
	"PARAMS" => Array(
		"SIZE" => 30,
		"MAXLENGTH" => 255,
	),
);
$arFormFields["MAIN"]["ITEMS"][] = Array(
	"CODE" => "SORT",
	"REQUIRED" => "N",
	"TYPE" => "NUMBER",
	"LABEL" => GetMessage("FIELDS_SORT"),
	"VALUE" => $str_SORT,
	"PARAMS" => Array(
		"SIZE" => 8,
		"MAXLENGTH" => 11,
		"MIN" => 0,
	),
);
$arFormFields["MAIN"]["ITEMS"][] = Array(
	"CODE" => "IMPORT_FORMAT",
	"REQUIRED" => "Y",
	"TYPE" => "SELECT",
	"LABEL" => GetMessage("FIELDS_IMPORT_FORMAT"),
	"VALUE" => $str_IMPORT_FORMAT,
	"ITEMS" => $IMPORT_FORMATS,
);
$arFormFields["MAIN"]["ITEMS"][] = Array(
	"CODE" => "ITEMS_PER_ROUND",
	"REQUIRED" => "Y",
	"TYPE" => "NUMBER",
	"LABEL" => GetMessage("FIELDS_ITEMS_PER_ROUND"),
	"VALUE" => $str_ITEMS_PER_ROUND,
	"PARAMS" => Array(
		"SIZE" => 8,
		"MAXLENGTH" => 11,
		"MIN" => 1,
	),
);

$arFormFields["IMPORT_TYPE"]["LABEL"] = GetMessage("GROUP_IMPORT_TYPE");
$arFormFields["IMPORT_TYPE"]["ITEMS"][] = Array(
	"CODE" => "IBLOCK_ID",
	"REQUIRED" => "N",
	"TYPE" => "IBLOCK_TREE",
	"LABEL" => GetMessage("FIELDS_IBLOCK_ID"),
	"VALUE" => $str_IBLOCK_ID,
	"PARAMS" => Array(
		"MIN_PERMISSION" => "W",
		"ADD_ZERO" => "Y",
		"ZERO_LABEL" => GetMessage("FIELDS_BLOCK_ID_NO"),
	),
);
$arFormFields["IMPORT_TYPE"]["ITEMS"][] = Array(
	"CODE" => "HIGHLOAD_BLOCK",
	"REQUIRED" => "N",
	"TYPE" => "HIGHLOAD_BLOCK",
	"LABEL" => GetMessage("FIELDS_HIGHLOAD_BLOCK"),
	"VALUE" => $str_HIGHLOAD_BLOCK,
	"PARAMS" => Array(
		"ADD_ZERO" => "Y",
		"ZERO_LABEL" => GetMessage("FIELDS_BLOCK_ID_NO"),
	),
);

$arFormFields["FILES"]["LABEL"] = GetMessage("GROUP_FILES");
$arFormFields["FILES"]["ITEMS"][] = Array(
	"CODE" => "IMPORT_FILE",
	"REQUIRED" => "Y",
	"TYPE" => "FILE_DIALOG",
	"LABEL" => GetMessage("FIELDS_IMPORT_FILE"),
	"VALUE" => $str_IMPORT_FILE,
	"PARAMS" => Array(
		"SIZE" => 30,
		"MAXLENGTH" => 255,
		"FORM_NAME" => "PLAN_EDIT",
		"SELECT" => "F",
		"ALLOW_UPLOAD" => "Y",
		"ALLOW_FILE_FORMATS" => "csv,xml,yml,xls,xlsx",
		"PATH" => "/".COption::GetOptionString("main", "upload_dir", "upload"),
	)
);
$arFormFields["FILES"]["ITEMS"][] = Array(
	"CODE" => "IMPORT_FILE_SHARSET",
	"REQUIRED" => "N",
	"TYPE" => "SELECT",
	"LABEL" => GetMessage("FIELDS_IMPORT_FILE_SHARSET"),
	"DESCRIPTION" => GetMessage("FIELDS_IMPORT_FILE_SHARSET_NOTE"),
	"VALUE" => $str_IMPORT_FILE_SHARSET,
	"ITEMS" => $IMPORT_FILE_SHARSETS,
);
$arFormFields["FILES"]["ITEMS"][] = Array(
	"CODE" => "IMPORT_FILE_DELETE",
	"REQUIRED" => "N",
	"TYPE" => "CHECKBOX",
	"LABEL" => GetMessage("FIELDS_IMPORT_FILE_DELETE"),
	"VALUE" => $str_IMPORT_FILE_DELETE,
);
$arFormFields["FILES"]["ITEMS"][] = Array(
	"CODE" => "IMPORT_FILE_CHECK",
	"REQUIRED" => "N",
	"TYPE" => "CHECKBOX",
	"LABEL" => GetMessage("FIELDS_IMPORT_FILE_CHECK"),
	"DESCRIPTION" => GetMessage("FIELDS_IMPORT_FILE_CHECK_NOTE"),
	"VALUE" => $str_IMPORT_FILE_CHECK,
);
$arFormFields["FILES"]["ITEMS"][] = Array(
	"CODE" => "PATH_TO_IMAGES",
	"REQUIRED" => "N",
	"TYPE" => "FILE_DIALOG",
	"LABEL" => GetMessage("FIELDS_PATH_TO_IMAGES"),
	"VALUE" => $str_PATH_TO_IMAGES,
	"PARAMS" => Array(
		"SIZE" => 30,
		"MAXLENGTH" => 255,
		"FORM_NAME" => "PLAN_EDIT",
		"SELECT" => "D",
		"ALLOW_UPLOAD" => "N",
		"PATH" => "/".COption::GetOptionString("main", "upload_dir", "upload")."/webprostor.import/data/images/",
		"PLACEHOLDER" => "/upload/webprostor.import/data/images/",
	)
);
$arFormFields["FILES"]["ITEMS"][] = Array(
	"CODE" => "RESIZE_IMAGE",
	"REQUIRED" => "N",
	"TYPE" => "CHECKBOX",
	"LABEL" => GetMessage("FIELDS_RESIZE_IMAGE"),
	"VALUE" => (!$ID && !$COPY_ID)?'N':$str_RESIZE_IMAGE,
);
$arFormFields["FILES"]["ITEMS"][] = Array(
	"CODE" => "CLEAR_IMAGES_DIR",
	"REQUIRED" => "N",
	"TYPE" => "CHECKBOX",
	"LABEL" => GetMessage("FIELDS_CLEAR_IMAGES_DIR"),
	"VALUE" => (!$ID && !$COPY_ID)?'N':$str_CLEAR_IMAGES_DIR,
);
$arFormFields["FILES"]["ITEMS"][] = Array(
	"CODE" => "CLEAR_UPLOAD_TMP_DIR",
	"REQUIRED" => "N",
	"TYPE" => "CHECKBOX",
	"LABEL" => GetMessage("FIELDS_CLEAR_UPLOAD_TMP_DIR"),
	"DESCRIPTION" => GetMessage("FIELDS_CLEAR_UPLOAD_TMP_DIR_NOTE"),
	"VALUE" => (!$ID && !$COPY_ID)?'N':$str_CLEAR_UPLOAD_TMP_DIR,
);
$arFormFields["FILES"]["ITEMS"][] = Array(
	"CODE" => "PATH_TO_FILES",
	"REQUIRED" => "N",
	"TYPE" => "FILE_DIALOG",
	"LABEL" => GetMessage("FIELDS_PATH_TO_FILES"),
	"VALUE" => $str_PATH_TO_FILES,
	"PARAMS" => Array(
		"SIZE" => 30,
		"MAXLENGTH" => 255,
		"FORM_NAME" => "PLAN_EDIT",
		"SELECT" => "D",
		"ALLOW_UPLOAD" => "N",
		"PATH" => "/".COption::GetOptionString("main", "upload_dir", "upload")."/webprostor.import/data/files/",
		"PLACEHOLDER" => "/upload/webprostor.import/data/files/",
	)
);
$arFormFields["FILES"]["ITEMS"][] = Array(
	"CODE" => "CLEAR_FILES_DIR",
	"REQUIRED" => "N",
	"TYPE" => "CHECKBOX",
	"LABEL" => GetMessage("FIELDS_CLEAR_FILES_DIR"),
	"VALUE" => (!$ID && !$COPY_ID)?'N':$str_CLEAR_FILES_DIR,
);

$arFormFields["FILES_URL"]["LABEL"] = GetMessage("GROUP_FILES_URL");
$arFormFields["FILES_URL"]["ITEMS"][] = Array(
	"CODE" => "IMPORT_FILE_URL",
	"REQUIRED" => "N",
	"TYPE" => "TEXT",
	"LABEL" => GetMessage("FIELDS_IMPORT_FILE_URL"),
	"VALUE" => $str_IMPORT_FILE_URL,
	"PARAMS" => Array(
		"SIZE" => 60,
		"MAXLENGTH" => 255,
		"PLACEHOLDER" => "https://example.com/import.".strtolower($str_IMPORT_FORMAT),
	),
);
$arFormFields["FILES_URL"]["ITEMS"][] = Array(
	"CODE" => "PATH_TO_IMAGES_URL",
	"REQUIRED" => "N",
	"TYPE" => "TEXT",
	"LABEL" => GetMessage("FIELDS_PATH_TO_IMAGES_URL"),
	"VALUE" => $str_PATH_TO_IMAGES_URL,
	"PARAMS" => Array(
		"SIZE" => 60,
		"MAXLENGTH" => 255,
		"PLACEHOLDER" => "https://example.com/images.zip",
	),
);
$arFormFields["FILES_URL"]["ITEMS"][] = Array(
	"CODE" => "PATH_TO_FILES_URL",
	"REQUIRED" => "N",
	"TYPE" => "TEXT",
	"LABEL" => GetMessage("FIELDS_PATH_TO_FILES_URL"),
	"VALUE" => $str_PATH_TO_FILES_URL,
	"PARAMS" => Array(
		"SIZE" => 60,
		"MAXLENGTH" => 255,
		"PLACEHOLDER" => "https://example.com/files.zip",
	),
);
$arFormFields["FILES_URL"]["ITEMS"][] = Array(
	"CODE" => "CURL_TIMEOUT",
	"REQUIRED" => "N",
	"TYPE" => "NUMBER",
	"LABEL" => GetMessage("FIELDS_CURL_TIMEOUT"),
	"VALUE" => $str_CURL_TIMEOUT,
	"PARAMS" => Array(
		"SIZE" => 30,
		"MIN" => 1,
	),
);
$arFormFields["FILES_URL"]["ITEMS"][] = Array(
	"CODE" => "CURL_FOLLOWLOCATION",
	"REQUIRED" => "N",
	"TYPE" => "CHECKBOX",
	"LABEL" => GetMessage("FIELDS_CURL_FOLLOWLOCATION"),
	"DESCRIPTION" => GetMessage("FIELDS_CURL_FOLLOWLOCATION_DESCRIPTION"),
	"VALUE" => $str_CURL_FOLLOWLOCATION,
);
$arFormFields["FILES_URL"]["ITEMS"][] = Array(
	"CODE" => "RAW_URL_DECODE",
	"REQUIRED" => "N",
	"TYPE" => "CHECKBOX",
	"LABEL" => GetMessage("FIELDS_RAW_URL_DECODE"),
	"VALUE" => $str_RAW_URL_DECODE,
);
$arFormFields["FILES_URL"]["ITEMS"][] = Array(
	"CODE" => "AGENT_INTERVAL_URL",
	"REQUIRED" => "N",
	"TYPE" => "NUMBER",
	"LABEL" => GetMessage("FIELDS_AGENT_INTERVAL_URL"),
	"VALUE" => $str_AGENT_INTERVAL_URL,
	"PARAMS" => Array(
		"SIZE" => 30,
		"MAXLENGTH" => 11,
		"MIN" => 60,
	),
);

$arFormFields["AGENT"]["LABEL"] = GetMessage("GROUP_AGENT");

	if($str_AGENT_ID)
	{
		$arFormFields["AGENT"]["ITEMS"][] = Array(
			"CODE" => "AGENT_ID",
			"REQUIRED" => "N",
			"TYPE" => "BUTTON",
			"LABEL" => GetMessage("FIELDS_AGENT_ID"),
			"VALUE" => 'agent_list.php?set_filter=Y&adm_filter_applied=0&find='.$str_AGENT_ID.'&find_type=id&find_module_id='.$module_id.'&lang='.LANG,
			"PARAMS" => Array(
				"TARGET" => "_blank",
				"TEXT" => GetMessage("FIELDS_AGENT_OPEN"),
			),
		);
	}
	else
	{
		$arFormFields["AGENT"]["ITEMS"][] = Array(
			"CODE" => "AGENT_ID",
			"REQUIRED" => "N",
			"LABEL" => GetMessage("FIELDS_AGENT_ID"),
			"VALUE" => GetMessage("FIELDS_AGENT_NO"),
			//"DESCRIPTION" => GetMessage("FIELDS_AGENT_NO_NOTE"),
		);
	}
$arFormFields["AGENT"]["ITEMS"][] = Array(
	"CODE" => "AGENT_INTERVAL",
	"REQUIRED" => "N",
	"TYPE" => "NUMBER",
	"LABEL" => GetMessage("FIELDS_AGENT_INTERVAL"),
	"VALUE" => $str_AGENT_INTERVAL,
	"PARAMS" => Array(
		"SIZE" => 30,
		"MAXLENGTH" => 11,
		"MIN" => 60,
	),
);

CWebprostorCoreFunctions::ShowFormFields($arFormFields);

$tabControl->BeginNextTab();

$arFormFields = [];

$DEFAULT_ACTIVE = [
	"Y" => GetMessage("FIELDS_ACTION_A"),
	"N" => GetMessage("FIELDS_ACTION_H")
];
$OFFER_OUT_ACTION = [
	"N" => GetMessage("FIELDS_ACTION_N"),
	"H" => GetMessage("FIELDS_ACTION_H"),
	"Q" => GetMessage("FIELDS_ACTION_Q"),
	"D" => GetMessage("FIELDS_ACTION_D")
];
if(CModule::IncludeModule('catalog'))
{
	$OUT_ACTION = $OFFER_OUT_ACTION;
}
else
{
	$OUT_ACTION = [
		"N" => GetMessage("FIELDS_ACTION_N"),
		"H" => GetMessage("FIELDS_ACTION_H"),
		"D" => GetMessage("FIELDS_ACTION_D")
	];
}
$SECTIONS_OUT_ACTION = [
	"N" => GetMessage("FIELDS_ACTION_N"),
	"H" => GetMessage("FIELDS_ACTION_H"),
	"D" => GetMessage("FIELDS_ACTION_D")
];
$IN_ACTION = [
	"N" => GetMessage("FIELDS_ACTION_N"),
	"A" => GetMessage("FIELDS_ACTION_A")
];
$PRODUCT_DEFAULT_FIELDS = [
	"D" => GetMessage("FIELDS_D"),
	"Y" => GetMessage("FIELDS_Y"),
	"N" => GetMessage("FIELDS_N")
];

if(CModule::IncludeModule("iblock"))
{
	$arFormFields["SECTIONS"]["LABEL"] = GetMessage("GROUP_IMPORT_SECTIONS");
	$arFormFields["SECTIONS"]["ITEMS"][] = [
		"CODE" => "IMPORT_IBLOCK_SECTIONS",
		"TYPE" => "CHECKBOX",
		"REQUIRED" => "Y",
		"LABEL" => GetMessage("FIELDS_IMPORT_IBLOCK_SECTIONS"),
		"VALUE" => $str_IMPORT_IBLOCK_SECTIONS,
	];
	$arFormFields["SECTIONS"]["ITEMS"][] = [
		"CODE" => "SECTIONS_ADD",
		"TYPE" => "CHECKBOX",
		"REQUIRED" => "N",
		"LABEL" => GetMessage("FIELDS_SECTIONS_ADD"),
		"VALUE" => $str_SECTIONS_ADD,
	];
	$arFormFields["SECTIONS"]["ITEMS"][] = [
		"CODE" => "SECTIONS_DEFAULT_ACTIVE",
		"TYPE" => "SELECT",
		"LABEL" => GetMessage("FIELDS_SECTIONS_DEFAULT_ACTIVE"),
		"VALUE" => $str_SECTIONS_DEFAULT_ACTIVE,
		"ITEMS" => $DEFAULT_ACTIVE,
	];
	$arFormFields["SECTIONS"]["ITEMS"][] = [
		"CODE" => "SECTIONS_DEFAULT_SECTION_ID",
		"TYPE" => "SELECT",
		"LABEL" => GetMessage("FIELDS_SECTIONS_DEFAULT_SECTION_ID"),
		"DESCRIPTION" => GetMessage("FIELDS_SECTIONS_DEFAULT_SECTION_ID_NOTE"),
		"VALUE" => $str_SECTIONS_DEFAULT_SECTION_ID,
		"ITEMS" => $iblockSectionsArray,
	];
	$arFormFields["SECTIONS"]["ITEMS"][] = [
		"CODE" => "SECTIONS_UPDATE",
		"TYPE" => "CHECKBOX",
		"REQUIRED" => "N",
		"LABEL" => GetMessage("FIELDS_SECTIONS_UPDATE"),
		"VALUE" => $str_SECTIONS_UPDATE,
	];
	$arFormFields["SECTIONS"]["ITEMS"][] = [
		"CODE" => "SECTIONS_OUT_ACTION",
		"TYPE" => "SELECT",
		"LABEL" => GetMessage("FIELDS_SECTIONS_OUT_ACTION"),
		"VALUE" => $str_SECTIONS_OUT_ACTION,
		"ITEMS" => $SECTIONS_OUT_ACTION,
	];
	$arFormFields["SECTIONS"]["ITEMS"][] = [
		"CODE" => "SECTIONS_OUT_ACTION_FILTER",
		"ID" => "sections_out_action_filter",
		"TYPE" => "WINDOW_DIALOG",
		"LABEL" => GetMessage("FIELDS_SECTIONS_OUT_ACTION_FILTER"),
		"VALUE" => $str_SECTIONS_OUT_ACTION_FILTER,
		"DATA" => [
			"IBLOCK_ID" => $str_IBLOCK_ID,
			"OBJECTS" => ["SECTIONS"],
			"CODE" => "SECTIONS_OUT_ACTION_FILTER",
		],
		"PARAMS" => [
			"AJAX_URL" => "/bitrix/admin/".$module_id."_window_filter.php",
			"MODAL" => "Y",
			"DRAGGABLE" => "Y",
			"WIDTH" => 600,
			"HEIGHT" => 500,
		]
	];
	$arFormFields["SECTIONS"]["ITEMS"][] = [
		"CODE" => "SECTIONS_IN_ACTION",
		"TYPE" => "SELECT",
		"LABEL" => GetMessage("FIELDS_SECTIONS_IN_ACTION"),
		"VALUE" => $str_SECTIONS_IN_ACTION,
		"ITEMS" => $IN_ACTION,
	];
	$arFormFields["SECTIONS"]["ITEMS"][] = [
		"CODE" => "SECTIONS_IN_ACTION_FILTER",
		"ID" => "sections_in_action_filter",
		"TYPE" => "WINDOW_DIALOG",
		"LABEL" => GetMessage("FIELDS_SECTIONS_IN_ACTION_FILTER"),
		"VALUE" => $str_SECTIONS_IN_ACTION_FILTER,
		"DATA" => [
			"IBLOCK_ID" => $str_IBLOCK_ID,
			"OBJECTS" => ["SECTIONS"],
			"CODE" => "SECTIONS_IN_ACTION_FILTER",
		],
		"PARAMS" => [
			"AJAX_URL" => "/bitrix/admin/".$module_id."_window_filter.php",
			"MODAL" => "Y",
			"DRAGGABLE" => "Y",
			"WIDTH" => 600,
			"HEIGHT" => 500,
		]
	];
	
	$arFormFields["ELEMENTS"]["LABEL"] = GetMessage("GROUP_IMPORT_ELEMENTS");
	$arFormFields["ELEMENTS"]["ITEMS"][] = [
		"CODE" => "IMPORT_IBLOCK_ELEMENTS",
		"TYPE" => "CHECKBOX",
		"REQUIRED" => "Y",
		"LABEL" => GetMessage("FIELDS_IMPORT_IBLOCK_ELEMENTS"),
		"VALUE" => $str_IMPORT_IBLOCK_ELEMENTS,
	];
	$arFormFields["ELEMENTS"]["ITEMS"][] = [
		"CODE" => "ELEMENTS_2_STEP_SEARCH",
		"TYPE" => "CHECKBOX",
		"REQUIRED" => "N",
		"LABEL" => GetMessage("FIELDS_ELEMENTS_2_STEP_SEARCH"),
		"DESCRIPTION" => GetMessage("FIELDS_ELEMENTS_2_STEP_SEARCH_DESCRIPTION"),
		"VALUE" => $str_ELEMENTS_2_STEP_SEARCH,
	];
	$arFormFields["ELEMENTS"]["ITEMS"][] = [
		"CODE" => "ELEMENTS_ADD",
		"TYPE" => "CHECKBOX",
		"REQUIRED" => "N",
		"LABEL" => GetMessage("FIELDS_ELEMENTS_ADD"),
		"VALUE" => $str_ELEMENTS_ADD,
	];
	$arFormFields["ELEMENTS"]["ITEMS"][] = [
		"CODE" => "ELEMENTS_DEFAULT_ACTIVE",
		"TYPE" => "SELECT",
		"LABEL" => GetMessage("FIELDS_ELEMENTS_DEFAULT_ACTIVE"),
		"VALUE" => $str_ELEMENTS_DEFAULT_ACTIVE,
		"ITEMS" => $DEFAULT_ACTIVE,
	];
	$arFormFields["ELEMENTS"]["ITEMS"][] = [
		"CODE" => "ELEMENTS_DEFAULT_SECTION_ID",
		"TYPE" => "SELECT",
		"LABEL" => GetMessage("FIELDS_ELEMENTS_DEFAULT_SECTION_ID"),
		"DESCRIPTION" => GetMessage("FIELDS_ELEMENTS_DEFAULT_SECTION_ID_NOTE"),
		"VALUE" => $str_ELEMENTS_DEFAULT_SECTION_ID,
		"ITEMS" => $iblockSectionsArray,
	];
	$arFormFields["ELEMENTS"]["ITEMS"][] = [
		"CODE" => "ELEMENTS_UPDATE",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_ELEMENTS_UPDATE"),
		"VALUE" => $str_ELEMENTS_UPDATE,
	];
	$arFormFields["ELEMENTS"]["ITEMS"][] = [
		"CODE" => "ELEMENTS_OUT_ACTION",
		"TYPE" => "SELECT",
		"LABEL" => GetMessage("FIELDS_ELEMENTS_OUT_ACTION"),
		"VALUE" => $str_ELEMENTS_OUT_ACTION,
		"ITEMS" => $OUT_ACTION,
	];
	$arFormFields["ELEMENTS"]["ITEMS"][] = [
		"CODE" => "ELEMENTS_OUT_ACTION_FILTER",
		"ID" => "elements_out_action_filter",
		"TYPE" => "WINDOW_DIALOG",
		"LABEL" => GetMessage("FIELDS_ELEMENTS_OUT_ACTION_FILTER"),
		"VALUE" => $str_ELEMENTS_OUT_ACTION_FILTER,
		"DATA" => [
			"IBLOCK_ID" => $str_IBLOCK_ID,
			"OBJECTS" => ["ELEMENTS", "PROPERTIES", "PRODUCTS", "PRICES"],
			"CODE" => "ELEMENTS_OUT_ACTION_FILTER",
		],
		"PARAMS" => [
			"AJAX_URL" => "/bitrix/admin/".$module_id."_window_filter.php",
			"MODAL" => "Y",
			"DRAGGABLE" => "Y",
			"WIDTH" => 600,
			"HEIGHT" => 500,
		]
	];
	$arFormFields["ELEMENTS"]["ITEMS"][] = [
		"CODE" => "ELEMENTS_IN_ACTION",
		"TYPE" => "SELECT",
		"LABEL" => GetMessage("FIELDS_ELEMENTS_IN_ACTION"),
		"VALUE" => $str_ELEMENTS_IN_ACTION,
		"ITEMS" => $IN_ACTION,
	];
	$arFormFields["ELEMENTS"]["ITEMS"][] = [
		"CODE" => "ELEMENTS_IN_ACTION_FILTER",
		"ID" => "elements_in_action_filter",
		"TYPE" => "WINDOW_DIALOG",
		"LABEL" => GetMessage("FIELDS_ELEMENTS_IN_ACTION_FILTER"),
		"VALUE" => $str_ELEMENTS_IN_ACTION_FILTER,
		"DATA" => [
			"IBLOCK_ID" => $str_IBLOCK_ID,
			"OBJECTS" => ["ELEMENTS", "PROPERTIES", "PRODUCTS", "PRICES"],
			"CODE" => "ELEMENTS_IN_ACTION_FILTER",
		],
		"PARAMS" => [
			"AJAX_URL" => "/bitrix/admin/".$module_id."_window_filter.php",
			"MODAL" => "Y",
			"DRAGGABLE" => "Y",
			"WIDTH" => 600,
			"HEIGHT" => 500,
		]
	];
	
	$arFormFields["PROPERTIES"]["LABEL"] = GetMessage("GROUP_IMPORT_ELEMENT_PROPERTIES");
	$arFormFields["PROPERTIES"]["ITEMS"][] = [
		"CODE" => "IMPORT_IBLOCK_PROPERTIES",
		"TYPE" => "CHECKBOX",
		"REQUIRED" => "N",
		"LABEL" => GetMessage("FIELDS_IMPORT_IBLOCK_PROPERTIES"),
		"VALUE" => $str_IMPORT_IBLOCK_PROPERTIES,
	];
	$arFormFields["PROPERTIES"]["ITEMS"][] = [
		"CODE" => "PROPERTIES_UPDATE",
		"TYPE" => "CHECKBOX",
		"REQUIRED" => "N",
		"LABEL" => GetMessage("FIELDS_PROPERTIES_UPDATE"),
		"VALUE" => $str_PROPERTIES_UPDATE,
	];
	$arFormFields["PROPERTIES"]["ITEMS"][] = [
		"CODE" => "PROPERTIES_RESET",
		"TYPE" => "CHECKBOX",
		"REQUIRED" => "N",
		"LABEL" => GetMessage("FIELDS_PROPERTIES_RESET"),
		"VALUE" => $str_PROPERTIES_RESET,
	];
	$arFormFields["PROPERTIES"]["ITEMS"][] = [
		"CODE" => "PROPERTIES_TRANSLATE_XML_ID",
		"TYPE" => "CHECKBOX",
		"REQUIRED" => "N",
		"LABEL" => GetMessage("FIELDS_PROPERTIES_TRANSLATE_XML_ID"),
		"VALUE" => $str_PROPERTIES_TRANSLATE_XML_ID,
	];
	$arFormFields["PROPERTIES"]["ITEMS"][] = [
		"CODE" => "PROPERTIES_SET_DEFAULT_VALUES",
		"TYPE" => "CHECKBOX",
		"REQUIRED" => "N",
		"LABEL" => GetMessage("FIELDS_PROPERTIES_SET_DEFAULT_VALUES"),
		"VALUE" => $str_PROPERTIES_SET_DEFAULT_VALUES,
	];
	$arFormFields["PROPERTIES"]["ITEMS"][] = [
		"CODE" => "PROPERTIES_SKIP_DOWNLOAD_URL_UPDATE",
		"TYPE" => "CHECKBOX",
		"REQUIRED" => "N",
		"LABEL" => GetMessage("FIELDS_PROPERTIES_SKIP_DOWNLOAD_URL_UPDATE"),
		"DESCRIPTION" => GetMessage("FIELDS_PROPERTIES_SKIP_DOWNLOAD_URL_UPDATE_NOTE"),
		"VALUE" => $str_PROPERTIES_SKIP_DOWNLOAD_URL_UPDATE,
	];
	$arFormFields["PROPERTIES"]["ITEMS"][] = [
		"CODE" => "PROPERTIES_WHATERMARK",
		"TYPE" => "SELECT",
		"LABEL" => GetMessage("FIELDS_PROPERTIES_WHATERMARK"),
		"VALUE" => $str_PROPERTIES_WHATERMARK,
		"ITEMS" => [
			"N" => GetMessage("FIELDS_NO_W"),
			"P" => GetMessage("FIELDS_W_FROM_PREVIEW"),
			"D" => GetMessage("FIELDS_W_FROM_DETAIL")
		],
	];
	$arFormFields["PROPERTIES"]["ITEMS"][] = [
		"CODE" => "PROPERTIES_INCREMENT_TO_MULTIPLE",
		"TYPE" => "CHECKBOX",
		"REQUIRED" => "N",
		"LABEL" => GetMessage("FIELDS_PROPERTIES_INCREMENT_TO_MULTIPLE"),
		"DESCRIPTION" => GetMessage("FIELDS_PROPERTIES_INCREMENT_TO_MULTIPLE_NOTE"),
		"VALUE" => $str_PROPERTIES_INCREMENT_TO_MULTIPLE,
	];
}

if(CModule::IncludeModule("catalog"))
{

	if($str_PRODUCTS_PARAMS)
	{
		$productsParamsArr = unserialize(base64_decode($str_PRODUCTS_PARAMS));
		if(!is_array($productsParamsArr))
			$productsParamsArr = Array();
	}
	else
	{
		$productsParamsArr = array();
	}
	
	$arFormFields["PRODUCTS"]["LABEL"] = GetMessage("GROUP_IMPORT_PRODUCTS");
	$arFormFields["PRODUCTS"]["ITEMS"][] = [
		"CODE" => "IMPORT_CATALOG_PRODUCTS",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_IMPORT_CATALOG_PRODUCTS"),
		"VALUE" => $str_IMPORT_CATALOG_PRODUCTS,
	];
	$arFormFields["PRODUCTS"]["ITEMS"][] = [
		"CODE" => "PRODUCTS_QUANTITY_TRACE",
		"TYPE" => "SELECT",
		"LABEL" => GetMessage("FIELDS_PRODUCTS_QUANTITY_TRACE"),
		"VALUE" => $productsParamsArr["PRODUCTS_QUANTITY_TRACE"],
		"ITEMS" => $PRODUCT_DEFAULT_FIELDS,
	];
	$arFormFields["PRODUCTS"]["ITEMS"][] = [
		"CODE" => "PRODUCTS_USE_STORE",
		"TYPE" => "SELECT",
		"LABEL" => GetMessage("FIELDS_PRODUCTS_USE_STORE"),
		"VALUE" => $productsParamsArr["PRODUCTS_USE_STORE"],
		"ITEMS" => $PRODUCT_DEFAULT_FIELDS,
	];
	$arFormFields["PRODUCTS"]["ITEMS"][] = [
		"CODE" => "PRODUCTS_SUBSCRIBE",
		"TYPE" => "SELECT",
		"LABEL" => GetMessage("FIELDS_PRODUCTS_SUBSCRIBE"),
		"VALUE" => $productsParamsArr["PRODUCTS_SUBSCRIBE"],
		"ITEMS" => $PRODUCT_DEFAULT_FIELDS,
	];
	$arFormFields["PRODUCTS"]["ITEMS"][] = [
		"CODE" => "PRODUCTS_ADD",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_PRODUCTS_ADD"),
		"VALUE" => $str_PRODUCTS_ADD,
	];
	$arFormFields["PRODUCTS"]["ITEMS"][] = [
		"CODE" => "PRODUCTS_UPDATE",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_PRODUCTS_UPDATE"),
		"VALUE" => $str_PRODUCTS_UPDATE,
	];
	
	$arFormFields["OFFERS"]["LABEL"] = GetMessage("GROUP_IMPORT_PRODUCT_OFFERS");
	$arFormFields["OFFERS"]["ITEMS"][] = [
		"CODE" => "IMPORT_CATALOG_PRODUCT_OFFERS",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_IMPORT_CATALOG_PRODUCT_OFFERS"),
		"VALUE" => $str_IMPORT_CATALOG_PRODUCT_OFFERS,
	];
	$arFormFields["OFFERS"]["ITEMS"][] = [
		"CODE" => "OFFERS_ADD",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_OFFERS_ADD"),
		"VALUE" => $str_OFFERS_ADD,
	];
	$arFormFields["OFFERS"]["ITEMS"][] = [
		"CODE" => "OFFERS_UPDATE",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_OFFERS_UPDATE"),
		"VALUE" => $str_OFFERS_UPDATE,
	];
	$arFormFields["OFFERS"]["ITEMS"][] = [
		"CODE" => "OFFERS_SET_NAME_FROM_ELEMENT",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_OFFERS_SET_NAME_FROM_ELEMENT"),
		"VALUE" => $str_OFFERS_SET_NAME_FROM_ELEMENT,
	];
	$arFormFields["OFFERS"]["ITEMS"][] = [
		"CODE" => "OFFERS_OUT_ACTION",
		"TYPE" => "SELECT",
		"LABEL" => GetMessage("FIELDS_OFFERS_OUT_ACTION"),
		"VALUE" => $str_OFFERS_OUT_ACTION,
		"ITEMS" => $OFFER_OUT_ACTION,
	];
	$arFormFields["OFFERS"]["ITEMS"][] = [
		"CODE" => "OFFERS_OUT_ACTION_FILTER",
		"ID" => "offers_out_action_filter",
		"TYPE" => "WINDOW_DIALOG",
		"LABEL" => GetMessage("FIELDS_OFFERS_OUT_ACTION_FILTER"),
		"VALUE" => $str_OFFERS_OUT_ACTION_FILTER,
		"DATA" => [
			"IBLOCK_ID" => $str_OFFER_IBLOCK_ID,
			"OBJECTS" => ["ELEMENTS", "PROPERTIES", "PRODUCTS", "PRICES"],
			"CODE" => "OFFERS_OUT_ACTION_FILTER",
		],
		"PARAMS" => [
			"AJAX_URL" => "/bitrix/admin/".$module_id."_window_filter.php",
			"MODAL" => "Y",
			"DRAGGABLE" => "Y",
			"WIDTH" => 600,
			"HEIGHT" => 500,
		]
	];
	$arFormFields["OFFERS"]["ITEMS"][] = [
		"CODE" => "OFFERS_IN_ACTION",
		"TYPE" => "SELECT",
		"LABEL" => GetMessage("FIELDS_OFFERS_IN_ACTION"),
		"VALUE" => $str_OFFERS_IN_ACTION,
		"ITEMS" => $IN_ACTION,
	];
	$arFormFields["OFFERS"]["ITEMS"][] = [
		"CODE" => "OFFERS_IN_ACTION_FILTER",
		"ID" => "offers_in_action_filter",
		"TYPE" => "WINDOW_DIALOG",
		"LABEL" => GetMessage("FIELDS_OFFERS_IN_ACTION_FILTER"),
		"VALUE" => $str_OFFERS_IN_ACTION_FILTER,
		"DATA" => [
			"IBLOCK_ID" => $str_OFFER_IBLOCK_ID,
			"OBJECTS" => ["ELEMENTS", "PROPERTIES", "PRODUCTS", "PRICES"],
			"CODE" => "OFFERS_IN_ACTION_FILTER",
		],
		"PARAMS" => [
			"AJAX_URL" => "/bitrix/admin/".$module_id."_window_filter.php",
			"MODAL" => "Y",
			"DRAGGABLE" => "Y",
			"WIDTH" => 600,
			"HEIGHT" => 500,
		]
	];
	
	$arFormFields["PRICES"]["LABEL"] = GetMessage("GROUP_IMPORT_PRICES");
	$arFormFields["PRICES"]["ITEMS"][] = [
		"CODE" => "IMPORT_CATALOG_PRICES",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_IMPORT_CATALOG_PRICES"),
		"VALUE" => $str_IMPORT_CATALOG_PRICES,
	];
	$arFormFields["PRICES"]["ITEMS"][] = [
		"CODE" => "PRICES_ADD",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_PRICES_ADD"),
		"VALUE" => $str_PRICES_ADD,
	];
	$arFormFields["PRICES"]["ITEMS"][] = [
		"CODE" => "PRICES_UPDATE",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_PRICES_UPDATE"),
		"VALUE" => $str_PRICES_UPDATE,
	];
	
	$arFormFields["STORE_AMOUNT"]["LABEL"] = GetMessage("GROUP_IMPORT_STORE_AMOUNT");
	$arFormFields["STORE_AMOUNT"]["ITEMS"][] = [
		"CODE" => "IMPORT_CATALOG_STORE_AMOUNT",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_IMPORT_CATALOG_STORE_AMOUNT"),
		"VALUE" => $str_IMPORT_CATALOG_STORE_AMOUNT,
	];
	$arFormFields["STORE_AMOUNT"]["ITEMS"][] = [
		"CODE" => "STORE_AMOUNT_ADD",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_STORE_AMOUNT_ADD"),
		"VALUE" => $str_STORE_AMOUNT_ADD,
	];
	$arFormFields["STORE_AMOUNT"]["ITEMS"][] = [
		"CODE" => "STORE_AMOUNT_UPDATE",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_STORE_AMOUNT_UPDATE"),
		"VALUE" => $str_STORE_AMOUNT_UPDATE,
	];
}

if(CModule::IncludeModule("highloadblock"))
{
	$arFormFields["ENTITIES"]["LABEL"] = GetMessage("GROUP_IMPORT_ENTITIES");
	$arFormFields["ENTITIES"]["ITEMS"][] = [
		"CODE" => "IMPORT_HIGHLOAD_BLOCK_ENTITIES",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_IMPORT_HIGHLOAD_BLOCK_ENTITIES"),
		"VALUE" => $str_IMPORT_HIGHLOAD_BLOCK_ENTITIES,
	];
	$arFormFields["ENTITIES"]["ITEMS"][] = [
		"CODE" => "ENTITIES_ADD",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_ENTITIES_ADD"),
		"VALUE" => $str_ENTITIES_ADD,
	];
	$arFormFields["ENTITIES"]["ITEMS"][] = [
		"CODE" => "ENTITIES_UPDATE",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_ENTITIES_UPDATE"),
		"VALUE" => $str_ENTITIES_UPDATE,
	];
	$arFormFields["ENTITIES"]["ITEMS"][] = [
		"CODE" => "ENTITIES_TRANSLATE_XML_ID",
		"TYPE" => "CHECKBOX",
		"REQUIRED" => "N",
		"LABEL" => GetMessage("FIELDS_ENTITIES_TRANSLATE_XML_ID"),
		"VALUE" => $str_ENTITIES_TRANSLATE_XML_ID,
	];
}

CWebprostorCoreFunctions::ShowFormFields($arFormFields);

$tabControl->BeginNextTab();

if($ID>0 || $COPY_ID>0)
{
	
	$arFormFields = [];
	
$GLOBALS["PLAN_ID"] = $ID;

switch($str_IMPORT_FORMAT)
{
	case("CSV"):

		$CSV_DELIMITERS = Array(
			"TZP" => GetMessage("FIELDS_CSV_DELIMITER_TZP"),
			"ZPT" => GetMessage("FIELDS_CSV_DELIMITER_ZPT"),
			"TAB" => GetMessage("FIELDS_CSV_DELIMITER_TAB"),
			"SPS" => GetMessage("FIELDS_CSV_DELIMITER_SPS"),
		);
		$arFormFields["FORMAT"]["ITEMS"][] = [
			"CODE" => "CSV_DELIMITER",
			"TYPE" => "SELECT",
			"LABEL" => GetMessage("FIELDS_CSV_DELIMITER"),
			"VALUE" => $str_CSV_DELIMITER,
			"ITEMS" => $CSV_DELIMITERS,
		];
		$arFormFields["FORMAT"]["ITEMS"][] = Array(
			"CODE" => "CSV_XLS_NAME_LINE",
			"TYPE" => "NUMBER",
			"LABEL" => GetMessage("FIELDS_CSV_XLS_NAME_LINE"),
			"DESCRIPTION" => GetMessage("FIELDS_CSV_XLS_FIRST_LINE_ZERO"),
			"VALUE" => $str_CSV_XLS_NAME_LINE,
			"PARAMS" => Array(
				"SIZE" => 5,
				"MAXLENGTH" => 3,
				"MIN" => 0,
			),
		);
		$arFormFields["FORMAT"]["ITEMS"][] = Array(
			"CODE" => "CSV_XLS_START_LINE",
			"TYPE" => "NUMBER",
			"LABEL" => GetMessage("FIELDS_CSV_XLS_START_LINE"),
			"DESCRIPTION" => GetMessage("FIELDS_CSV_XLS_FIRST_LINE_ZERO"),
			"VALUE" => $str_CSV_XLS_START_LINE,
			"PARAMS" => Array(
				"SIZE" => 5,
				"MAXLENGTH" => 3,
				"MIN" => 0,
			),
		);
		$arFormFields["FORMAT"]["ITEMS"][] = Array(
			"CODE" => "CSV_XLS_MAX_DEPTH_LEVEL",
			"TYPE" => "NUMBER",
			"LABEL" => GetMessage("FIELDS_CSV_XLS_MAX_DEPTH_LEVEL"),
			"VALUE" => $str_CSV_XLS_MAX_DEPTH_LEVEL,
			"PARAMS" => Array(
				"SIZE" => 5,
				"MAXLENGTH" => 3,
				"MIN" => 1,
			),
		);
		break;
		
	case("XML"):
		$arFormFields["FORMAT"]["ITEMS"][] = Array(
			"CODE" => "XML_ENTITY",
			"TYPE" => "TEXT",
			"LABEL" => GetMessage("FIELDS_XML_ENTITY"),
			"VALUE" => $str_XML_ENTITY,
			"PARAMS" => Array(
				"SIZE" => 30,
				"MAXLENGTH" => 255,
			),
		);
		$arFormFields["FORMAT"]["ITEMS"][] = Array(
			"CODE" => "XML_USE_ENTITY_NAME",
			"TYPE" => "CHECKBOX",
			"LABEL" => GetMessage("FIELDS_XML_USE_ENTITY_NAME"),
			"VALUE" => (!$ID && !$COPY_ID)?'Y':$str_XML_USE_ENTITY_NAME,
		);
		$arFormFields["FORMAT"]["ITEMS"][] = Array(
			"CODE" => "XML_PARSE_PARAMS_TO_PROPERTIES",
			"TYPE" => "CHECKBOX",
			"LABEL" => GetMessage("FIELDS_XML_PARSE_PARAMS_TO_PROPERTIES"),
			"DESCRIPTION" => GetMessage("FIELDS_XML_PARSE_PARAMS_TO_PROPERTIES_NOTE"),
			"VALUE" => (!$ID && !$COPY_ID)?'Y':$str_XML_PARSE_PARAMS_TO_PROPERTIES,
		);
		$arFormFields["FORMAT"]["ITEMS"][] = Array(
			"CODE" => "XML_ADD_PROPERTIES_FOR_PARAMS",
			"TYPE" => "CHECKBOX",
			"LABEL" => GetMessage("FIELDS_XML_ADD_PROPERTIES_FOR_PARAMS"),
			"VALUE" => (!$ID && !$COPY_ID)?'Y':$str_XML_ADD_PROPERTIES_FOR_PARAMS,
		);
		break;
		
	case("XLS"):
	case("XLSX"):
		if($str_IMPORT_FORMAT == "XLSX")
		{
			$scriptData = new CWebprostorImportXLSX;
			$sheets = $scriptData->GetSheets($str_IMPORT_FILE, $str_IMPORT_FILE_SHARSET);
		}
		if($sheets)
		{
			$arFormFields["FORMAT"]["ITEMS"][] = [
				"CODE" => "XLS_SHEET",
				"TYPE" => "SELECT",
				"LABEL" => GetMessage("FIELDS_XLS_SHEET"),
				"VALUE" => $str_XLS_SHEET,
				"ITEMS" => $sheets,
			];
		}
		else
		{
			$arFormFields["FORMAT"]["ITEMS"][] = Array(
				"CODE" => "XLS_SHEET",
				"TYPE" => "NUMBER",
				"LABEL" => GetMessage("FIELDS_XLS_SHEET_NUMBER"),
				"VALUE" => $str_XLS_SHEET,
				"PARAMS" => Array(
					"SIZE" => 10,
					"MAXLENGTH" => 11,
					"MIN" => 0,
				),
			);
		}
		$arFormFields["FORMAT"]["ITEMS"][] = Array(
			"CODE" => "CSV_XLS_NAME_LINE",
			"TYPE" => "NUMBER",
			"LABEL" => GetMessage("FIELDS_CSV_XLS_NAME_LINE"),
			"DESCRIPTION" => GetMessage("FIELDS_CSV_XLS_FIRST_LINE_ZERO"),
			"VALUE" => $str_CSV_XLS_NAME_LINE,
			"PARAMS" => Array(
				"SIZE" => 5,
				"MAXLENGTH" => 3,
				"MIN" => 0,
			),
		);
		$arFormFields["FORMAT"]["ITEMS"][] = Array(
			"CODE" => "CSV_XLS_START_LINE",
			"TYPE" => "NUMBER",
			"LABEL" => GetMessage("FIELDS_CSV_XLS_START_LINE"),
			"DESCRIPTION" => GetMessage("FIELDS_CSV_XLS_FIRST_LINE_ZERO"),
			"VALUE" => $str_CSV_XLS_START_LINE,
			"PARAMS" => Array(
				"SIZE" => 5,
				"MAXLENGTH" => 3,
				"MIN" => 0,
			),
		);
		$arFormFields["FORMAT"]["ITEMS"][] = Array(
			"CODE" => "CSV_XLS_MAX_DEPTH_LEVEL",
			"TYPE" => "NUMBER",
			"LABEL" => GetMessage("FIELDS_CSV_XLS_MAX_DEPTH_LEVEL"),
			"VALUE" => $str_CSV_XLS_MAX_DEPTH_LEVEL,
			"PARAMS" => Array(
				"SIZE" => 5,
				"MAXLENGTH" => 3,
				"MIN" => 1,
			),
		);
		break;
}
	
	CWebprostorCoreFunctions::ShowFormFields($arFormFields);
}

$tabControl->BeginNextTab();

$arFormFields = Array();
$arFormFields["MAIN"]["LABEL"] = GetMessage("GROUP_DEBUG_MAIN");

$arFormFields["MAIN"]["ITEMS"][] = Array(
	"CODE" => "DEBUG_EVENTS",
	"TYPE" => "CHECKBOX",
	"LABEL" => GetMessage("FIELDS_DEBUG_EVENTS"),
	"VALUE" => $str_DEBUG_EVENTS,
);

$arFormFields["FILES"]["LABEL"] = GetMessage("GROUP_DEBUG_FILES");
$arFormFields["FILES"]["ITEMS"][] = Array(
	"CODE" => "DEBUG_IMAGES",
	"TYPE" => "CHECKBOX",
	"LABEL" => GetMessage("FIELDS_DEBUG_IMAGES"),
	"VALUE" => $str_DEBUG_IMAGES,
);
$arFormFields["FILES"]["ITEMS"][] = Array(
	"CODE" => "DEBUG_FILES",
	"TYPE" => "CHECKBOX",
	"LABEL" => GetMessage("FIELDS_DEBUG_FILES"),
	"VALUE" => $str_DEBUG_FILES,
);
$arFormFields["FILES"]["ITEMS"][] = Array(
	"CODE" => "DEBUG_URL",
	"TYPE" => "CHECKBOX",
	"LABEL" => GetMessage("FIELDS_DEBUG_URL"),
	"VALUE" => $str_DEBUG_URL,
);

$arFormFields["AREAS"]["LABEL"] = GetMessage("GROUP_DEBUG_AREAS");
if(CModule::IncludeModule("iblock"))
{
	$arFormFields["AREAS"]["ITEMS"][] = Array(
		"CODE" => "DEBUG_IMPORT_SECTION",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_DEBUG_IMPORT_SECTION"),
		"VALUE" => $str_DEBUG_IMPORT_SECTION,
	);
	$arFormFields["AREAS"]["ITEMS"][] = Array(
		"CODE" => "DEBUG_IMPORT_ELEMENTS",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_DEBUG_IMPORT_ELEMENTS"),
		"VALUE" => $str_DEBUG_IMPORT_ELEMENTS,
	);
	$arFormFields["AREAS"]["ITEMS"][] = Array(
		"CODE" => "DEBUG_IMPORT_PROPERTIES",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_DEBUG_IMPORT_PROPERTIES"),
		"VALUE" => $str_DEBUG_IMPORT_PROPERTIES,
	);
}
if(CModule::IncludeModule("catalog"))
{
	$arFormFields["AREAS"]["ITEMS"][] = Array(
		"CODE" => "DEBUG_IMPORT_PRODUCTS",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_DEBUG_IMPORT_PRODUCTS"),
		"VALUE" => $str_DEBUG_IMPORT_PRODUCTS,
	);
	$arFormFields["AREAS"]["ITEMS"][] = Array(
		"CODE" => "DEBUG_IMPORT_OFFERS",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_DEBUG_IMPORT_OFFERS"),
		"VALUE" => $str_DEBUG_IMPORT_OFFERS,
	);
	$arFormFields["AREAS"]["ITEMS"][] = Array(
		"CODE" => "DEBUG_IMPORT_PRICES",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_DEBUG_IMPORT_PRICES"),
		"VALUE" => $str_DEBUG_IMPORT_PRICES,
	);
	$arFormFields["AREAS"]["ITEMS"][] = Array(
		"CODE" => "DEBUG_IMPORT_STORE_AMOUNT",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_DEBUG_IMPORT_STORE_AMOUNT"),
		"VALUE" => $str_DEBUG_IMPORT_STORE_AMOUNT,
	);
}
if(CModule::IncludeModule("highloadblock"))
{
	$arFormFields["AREAS"]["ITEMS"][] = Array(
		"CODE" => "DEBUG_IMPORT_ENTITIES",
		"TYPE" => "CHECKBOX",
		"LABEL" => GetMessage("FIELDS_DEBUG_IMPORT_ENTITIES"),
		"VALUE" => $str_DEBUG_IMPORT_ENTITIES,
	);
}

CWebprostorCoreFunctions::ShowFormFields($arFormFields);

$tabControl->Buttons(
	array(
		"disabled"=>($moduleAccessLevel<"W"),
		"back_url"=>"webprostor.import_plans.php?lang=".LANG,
	)
);
?>
<input type="hidden" name="lang" value="<?=LANG?>">
<?
if($ID>0 && !$COPY_ID) {
?>
  <input type="hidden" name="ID" value="<?=$ID?>">
<?
}
elseif($COPY_ID>0)
{
?>
  <input type="hidden" name="COPY_ID" value="<?=$COPY_ID?>">
<?
}

$tabControl->End();
?>
<script type="text/javaScript">
<!--
BX.ready(function() {
<?if(!$ID){?>
	tabControl.DisableTab("setting");
<? } ?>
});
//-->
</script>
<?
	
$tabControl->ShowWarnings("PLAN_EDIT", $message);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");