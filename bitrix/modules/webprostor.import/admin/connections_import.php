<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/webprostor.import/prolog.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/webprostor.import/include.php");

IncludeModuleLangFile(__FILE__);

$module_id = 'webprostor.import';
$moduleAccessLevel = $APPLICATION->GetGroupRight($module_id);

if ($moduleAccessLevel == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

if($back_url=='')
	$back_url = '/bitrix/admin/webprostor.import_connections.php?lang='.$lang;

$strWarning = "";

$aTabs = array(
  array("DIV" => "main", "TAB" => GetMessage("ELEMENT_TAB"), "ICON"=>"", "TITLE"=>GetMessage("ELEMENT_TAB_TITLE")),
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);

$PLAN_ID = intval($PLAN_ID);
$pData = new CWebprostorImportPlan;
$cData = new CWebprostorImportPlanConnections;

if($_SERVER["REQUEST_METHOD"] == "POST" && strlen($Update)>0 && check_bitrix_sessid())
{
	if($PLAN_ID>0)
	{
		$plan = $pData->GetById($PLAN_ID);
		if(!$plan->ExtractFields("plan_"))
			$PLAN_ID=0;
		
		switch($plan_IMPORT_FORMAT)
		{
			case("CSV"):
				$scriptData = new CWebprostorImportCSV;
				break;
			case("XML"):
				$scriptData = new CWebprostorImportXML;
				break;
			case("XLS"):
				$scriptData = new CWebprostorImportXLS;
				break;
			case("XLSX"):
				$scriptData = new CWebprostorImportXLSX;
				break;
		}
		
		if($PLAN_ID>0)
		{
			$entitiesArray = $scriptData->GetEntities($PLAN_ID);
			if($plan_IMPORT_FORMAT == "XML")
			{
				$attributesArray = $entitiesArray["ATTRIBUTES"];
				$entitiesArray = $entitiesArray["KEYS"];
			}
			
			$CConnectionFields = new CWebprostorImportPlanConnectionsFields;
			
			if($plan_IMPORT_IBLOCK_SECTIONS=="Y" || $plan_IMPORT_IBLOCK_PROPERTIES=="Y")
				$iblockSectionFields = $CConnectionFields->GetFields("SECTION", $plan_IBLOCK_ID);
			
			if($plan_IMPORT_IBLOCK_ELEMENTS=="Y" || $plan_IMPORT_IBLOCK_PROPERTIES=="Y" || $plan_IMPORT_CATALOG_PRODUCT_OFFERS=="Y")
				$iblockElementFields = $CConnectionFields->GetFields("ELEMENT");
		
			if($plan_IMPORT_IBLOCK_PROPERTIES == "Y")
			{
				$propertiesArray = Array();
				$propRes = CIBlockProperty::GetList(Array("ID"=>"asc", "name"=>"asc"), Array("IBLOCK_ID"=>$plan_IBLOCK_ID));
				while($propFields = $propRes->GetNext())
				{
					$propertiesArray[$propFields["ID"]] = htmlspecialcharsbx($propFields["NAME"]);
				}
			}
		
			if($plan_IMPORT_CATALOG_PRODUCTS=="Y")
				$catalogProductFields = $CConnectionFields->GetFields("PRODUCT");
			
			if($plan_IMPORT_CATALOG_PRICES=="Y")
				$catalogProductPrice = $CConnectionFields->GetFields("PRICE");
			
			if($plan_IMPORT_HIGHLOAD_BLOCK_ENTITIES=="Y")
				$highloadBlockEntityFields = $CConnectionFields->GetFields("ENTITIES", false, $plan_HIGHLOAD_BLOCK);
		
			if(count($entitiesArray))
			{
				$sort = 10;
				foreach($entitiesArray as $ENTITY => $NAME)
				{
					$connectionFields = Array();
					$connectionFields["ACTIVE"] = $ACTIVE;
					$connectionFields["ENTITY"] = $ENTITY;
					$connectionFields["NAME"] = $NAME;
					$connectionFields["PLAN_ID"] = $PLAN_ID;
					$connectionFields["SORT"] = $sort;
					
					$ENTITY_REAL_NAME = substr($NAME, 3, strlen($NAME));
					switch(substr($NAME, 0, 3))
					{
						case("IC_"):
							$GROUP_DEPTH_LEVEL = intVal(substr($ENTITY_REAL_NAME, 5, strlen($ENTITY_REAL_NAME)));
							$connectionFields["IBLOCK_SECTION_FIELD"] = "NAME";
							$connectionFields["IBLOCK_SECTION_DEPTH_LEVEL"] = ++$GROUP_DEPTH_LEVEL;
							$connectionFields["USE_IN_SEARCH"] = "Y";
							break;
						case("IE_"):
							if(is_set($iblockElementFields[$ENTITY_REAL_NAME]))
								$connectionFields["IBLOCK_ELEMENT_FIELD"] = $ENTITY_REAL_NAME;
							
							if($ENTITY_REAL_NAME == "PREVIEW_PICTURE" || $ENTITY_REAL_NAME == "DETAIL_PICTURE")
								$connectionFields["IS_IMAGE"] = "Y";
							elseif($ENTITY_REAL_NAME == "NAME")
								$connectionFields["IS_REQUIRED"] = "Y";
							break;
						case("IP_"):
							$ENTITY_PROP_ID = substr($NAME, 7, strlen($NAME));
							if(is_set($propertiesArray[$ENTITY_PROP_ID]))
								$connectionFields["IBLOCK_ELEMENT_PROPERTY"] = $ENTITY_PROP_ID;
							break;
						case("CP_"):
							if(is_set($catalogProductFields[$ENTITY_REAL_NAME]))
								$connectionFields["CATALOG_PRODUCT_FIELD"] = $ENTITY_REAL_NAME;
							break;
						case("CV_"):
							if($ENTITY_REAL_NAME == "PRICE_1")
								$connectionFields["CATALOG_PRODUCT_PRICE"] = "PRICE";
							elseif($ENTITY_REAL_NAME == "CURRENCY_1")
								$connectionFields["CATALOG_PRODUCT_PRICE"] = "CURRENCY";
							elseif(is_set($catalogProductPrice[$ENTITY_REAL_NAME]))
								$connectionFields["CATALOG_PRODUCT_PRICE"] = $ENTITY_REAL_NAME;
							break;
						case("UF_"):
							if(isset($iblockSectionFields[$NAME]))
								$connectionFields["IBLOCK_SECTION_FIELD"] = $NAME;
							elseif(isset($highloadBlockEntityFields[$NAME]))
							{
								$connectionFields["HIGHLOAD_BLOCK_ENTITY_FIELD"] = $NAME;
								if($NAME == "UF_FILE")
									$connectionFields["IS_IMAGE"] = "Y";
							}
								
							break;
					}
					var_dump($connectionFields);
					
					$connectionName = $ENTITY_NAME.' ['.$ENTITY.']<br />';
					
					$ID = $cData->Add($connectionFields);
					$res = ($ID>0);
					
					if(!$res)
					{
						if(strlen($strWarning)>0)
							$strWarning.= $connectionName.' '.$cData->LAST_ERROR;
						else
							$strWarning.= GetMessage("MESSAGE_IMPORT_ERROR").":<br />".$connectionName.' '.$cData->LAST_ERROR."";
						$DB->Rollback();
					}
					else
					{
						$DB->Commit();
						$sort += 10;
					}
				}
			}
			else
				$strWarning.= GetMessage("ERROR_NO_CONNECTIONS");
			
			if(strlen($apply)<=0 && strlen($strWarning)==0)
			{
				if(strlen($back_url)>0)
					//LocalRedirect("/".ltrim($back_url, "/"));
					LocalRedirect('/bitrix/admin/webprostor.import_connections.php?PLAN_ID='.$PLAN_ID.'&apply_filter=Y&find_plan_id='.$PLAN_ID.'&lang='.$lang);
			}
			/*else
			{
				LocalRedirect($APPLICATION->GetCurPage()."?lang=".$lang."&PLAN_ID=".UrlEncode($PLAN_ID)."&".$tabControl->ActiveTabParam());
			}*/
		}
	}
}

$queryObject = $pData->getList(Array($b = "sort" => $o = "asc"), array());
$listPlans = array();
while($plan = $queryObject->getNext())
	$listPlans[$plan["ID"]] = htmlspecialcharsbx($plan["NAME"]).' ['.$plan["ID"].']';

$APPLICATION->SetTitle(GetMessage("IMPORT_PAGE_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

$aMenu = array(
	array(
		"TEXT" => GetMessage("CONNECTIONS_LIST"),
		"TITLE" => GetMessage("CONNECTIONS_LIST_TITLE"),
		"LINK" => "webprostor.import_connections.php?lang=".LANGUAGE_ID,
		"ICON" => "btn_list"
	)
);

$context = new CAdminContextMenu($aMenu);
$context->Show();
?>
<?CAdminMessage::ShowOldStyleError($strWarning);?>
<form method="POST" id="form" name="form" action="webprostor.import_connections_import.php?lang=<?echo LANG?>">
<?=bitrix_sessid_post()?>
<input type="hidden" name="Update" value="Y">
<input type="hidden" name="PLAN_ID" value="<?echo $PLAN_ID?>">
<?if(strlen($back_url)>0):?><input type="hidden" name="back_url" value="<?=htmlspecialcharsbx($back_url)?>"><?endif?>
<?
$tabControl->Begin();
$tabControl->BeginNextTab();

$arFormFields = [];

$arFormFields["MAIN"]["ITEMS"][] = Array(
	"CODE" => "PLAN_ID",
	"REQUIRED" => "Y",
	"TYPE" => "SELECT",
	"LABEL" => GetMessage("TABLE_HEADING_PLAN_ID"),
	"VALUE" => $PLAN_ID,
	"ITEMS" => $listPlans,
);
$arFormFields["MAIN"]["ITEMS"][] = Array(
	"CODE" => "ACTIVE",
	"TYPE" => "CHECKBOX",
	"LABEL" => GetMessage("TABLE_HEADING_ACTIVE"),
	"VALUE" => $ACTIVE?$ACTIVE:"Y",
);

CWebprostorCoreFunctions::ShowFormFields($arFormFields);

$tabControl->Buttons(
	array(
		"disabled"=>($moduleAccessLevel<"W"),
		"back_url"=>$back_url,
		"btnApply" => false,
	)
);
?>
<?
$tabControl->End();
?>
</form>
<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
?>