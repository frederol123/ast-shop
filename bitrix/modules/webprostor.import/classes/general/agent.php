<?
class CWebprostorImportAgent
{
	public function Add($PLAN_ID = false, $AGENT_INTERVAL = 600, $TYPE = "Import")
	{
		if($TYPE == "Import")
			$next_exec = date("d.m.Y H:i:s", time() + $AGENT_INTERVAL);
		else
			$next_exec = date("d.m.Y H:i:s");
		$AGENT_ID = CAgent::AddAgent(
			"CWebprostorImport::{$TYPE}({$PLAN_ID});",
			"webprostor.import",
			"N",
			$AGENT_INTERVAL,
			date("d.m.Y H:i:s", time() + $AGENT_INTERVAL),
			"Y",
			$next_exec, 
			30
		);
		
		return $AGENT_ID;
	}
	
	public function Update($PLAN_ID = false, $AGENT_INTERVAL = 600, $TYPE = "Import")
	{
		$searchAndDelete = self::Delete($PLAN_ID, $TYPE);
		
		$AGENT_ID = self::Add($PLAN_ID, $AGENT_INTERVAL, $TYPE);
		
		return $AGENT_ID;
	}
	
	public function Delete($PLAN_ID = false, $TYPE = "Import")
	{
		$agents = self::SearchAgent($PLAN_ID, $TYPE);
		
		if(is_array($agents))
			CAgent::Delete($agents[0]["ID"]);
		
		return true;
	}
	
	private function SearchAgent($PLAN_ID = false, $TYPE = "Import")
	{
		global $DB;
		
		if($PLAN_ID>0)
		{
			$query = "SELECT * FROM `b_agent` WHERE `NAME` LIKE '%CWebprostorImport::{$TYPE}({$PLAN_ID}%'";
			
			$cAgentResults = $DB->Query($query);
			$cAgentArray = array();
			
			while ($cAgentRow = $cAgentResults->Fetch()) {
				array_push($cAgentArray, $cAgentRow);
			}
			
			if(count($cAgentArray)>0)
				return $cAgentArray;
		}
		
		return false;
	}	
}
?>