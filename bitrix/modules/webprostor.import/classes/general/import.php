<?
IncludeModuleLangFile(__FILE__);

class CWebprostorImport extends CWebprostorImportLog
{
	CONST MODULE_ID = "webprostor.import";
	CONST ITEMS_PER_ROUND = 50;
	CONST DEFAULT_CURRENCY = 'RUB';
	CONST DELETE_OLD_PROPERTY_FILE_VALUE = true;
	CONST CURL_TIMEOUT = 5;
	
	public function CWebprostorImport()
	{
		
	}
	
	public function CheckEntitiesNames($PLAN_ID = false, $entities = Array())
	{
		$errors = [];
		
		$planCData = new CWebprostorImportPlanConnections;
		$planCRes = $planCData->GetList(Array("SORT"=>"ASC"), Array("PLAN_ID" => $PLAN_ID), ["ENTITY", "NAME"]);
		while($connect = $planCRes->GetNext(true, false))
		{
			if(!(
				isset($entities[$connect["ENTITY"]]) && 
				htmlspecialcharsbx($entities[$connect["ENTITY"]]) == $connect["NAME"]
			))
			{
				$errors[$connect["NAME"]] = $entities[$connect["ENTITY"]];
			}
		}
		
		return $errors;
	}
	
	private function GetPlanEntityConnection($PLAN_ID = false, $entities = Array(), $USE_ENTITY_NAME = "N")
	{
		$result = Array();
		
		foreach($entities as $code => $entity)
		{
			$result[$code] = Array(
				"NAME" => $entity,
				"RULES" => Array(),
			);
		}
		
		$planCData = new CWebprostorImportPlanConnections;
		$planCRes = $planCData->GetList(Array("SORT"=>"ASC"), Array("PLAN_ID" => $PLAN_ID, "ACTIVE" => "Y"));
		while($planConnection = $planCRes->GetNext(true, false))
		{
			if(is_array($result[$planConnection["ENTITY"]]))
			{
				$ruleFields = Array(
					"IBLOCK_SECTION_FIELD" => $planConnection["IBLOCK_SECTION_FIELD"],
					"IBLOCK_SECTION_DEPTH_LEVEL" => $planConnection["IBLOCK_SECTION_DEPTH_LEVEL"],
					"IBLOCK_SECTION_PARENT_FIELD" => $planConnection["IBLOCK_SECTION_PARENT_FIELD"],
					"IBLOCK_ELEMENT_FIELD" => $planConnection["IBLOCK_ELEMENT_FIELD"],
					"IBLOCK_ELEMENT_PROPERTY" => $planConnection["IBLOCK_ELEMENT_PROPERTY"],
					"IBLOCK_ELEMENT_PROPERTY_E" => $planConnection["IBLOCK_ELEMENT_PROPERTY_E"],
					"IBLOCK_ELEMENT_PROPERTY_G" => $planConnection["IBLOCK_ELEMENT_PROPERTY_G"],
					"IBLOCK_ELEMENT_PROPERTY_M" => $planConnection["IBLOCK_ELEMENT_PROPERTY_M"],
					"IBLOCK_ELEMENT_OFFER_FIELD" => $planConnection["IBLOCK_ELEMENT_OFFER_FIELD"],
					"IBLOCK_ELEMENT_OFFER_PROPERTY" => $planConnection["IBLOCK_ELEMENT_OFFER_PROPERTY"],
					"CATALOG_PRODUCT_FIELD" => $planConnection["CATALOG_PRODUCT_FIELD"],
					"CATALOG_PRODUCT_OFFER_FIELD" => $planConnection["CATALOG_PRODUCT_OFFER_FIELD"],
					"CATALOG_PRODUCT_PRICE" => $planConnection["CATALOG_PRODUCT_PRICE"],
					"CATALOG_PRODUCT_STORE_AMOUNT" => $planConnection["CATALOG_PRODUCT_STORE_AMOUNT"],
					"HIGHLOAD_BLOCK_ENTITY_FIELD" => $planConnection["HIGHLOAD_BLOCK_ENTITY_FIELD"],
					"IS_IMAGE" => $planConnection["IS_IMAGE"],
					"IS_FILE" => $planConnection["IS_FILE"],
					"IS_URL" => $planConnection["IS_URL"],
					"IS_REQUIRED" => $planConnection["IS_REQUIRED"],
					"USE_IN_SEARCH" => $planConnection["USE_IN_SEARCH"],
					"USE_IN_CODE" => $planConnection["USE_IN_CODE"],
					"SORT" => $planConnection["SORT"],
				);
				if($planConnection["PROCESSING_TYPES"])
				{
					$ruleFields["PROCESSING_TYPES"] = unserialize(base64_decode($planConnection["PROCESSING_TYPES"]));
					if(!is_array($ruleFields["PROCESSING_TYPES"]))
						$ruleFields["PROCESSING_TYPES"] = Array();
				}
				else
				{
					$ruleFields["PROCESSING_TYPES"] = array();
				}
				if(isset($planConnection["ENTITY_ATTRIBUTE"]))
					$ruleFields["ENTITY_ATTRIBUTE"] = $planConnection["ENTITY_ATTRIBUTE"];
				$result[$planConnection["ENTITY"]]["RULES"][] = $ruleFields;
			}
		}
		foreach($result as $k => $item)
		{
			if(empty($item["RULES"]))
				unset($result[$k]);
		}
		
		if($USE_ENTITY_NAME == "Y")
		{
			foreach($result as $k => $item)
			{
				$result[$item["NAME"]] = $item;
				unset($result[$item["NAME"]]["NAME"]);
				unset($result[$k]);
			}
		}
		return $result;
	}
	
	private function ExtractArchiveTo($url, $path_to)
	{
		$result = false;
		$archive = filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED);
		if($archive && is_dir($_SERVER["DOCUMENT_ROOT"].$path_to))
		{
			$temp = CFile::MakeFileArray($archive);
			
			if(is_array($temp))
			{
				switch($temp["type"])
				{
					case("application/x-rar"):
						if(extension_loaded('rar'))
						{
							$rar_file = rar_open($temp["tmp_name"]);
							$list = rar_list($rar_file);
							foreach($list as $file)
							{
								$entry = rar_entry_get($rar_file, $file);
								$entry->extract($_SERVER["DOCUMENT_ROOT"].$path_to);
							}
							rar_close($rar_file);
						}
						else
						{
							$errorArray = Array(
								"MESSAGE" => GetMessage("RAR_NOT_INCLUDED"),
								"TAG" => "RAR_NOT_INCLUDED",
								"MODULE_ID" => "WEBPROSTOR.IMPORT",
								"ENABLE_CLOSE" => "Y"
							);
							$notifyID = CAdminNotify::Add($errorArray);
						}
						break;
					case("application/zip"):
						if(extension_loaded('zip'))
						{
							$zip = new ZipArchive;
							if($zip->open($temp["tmp_name"]) === TRUE)
							{
								$result = $zip->extractTo($_SERVER["DOCUMENT_ROOT"].$path_to);
								$zip->close();
							}
						}
						else
						{
							$errorArray = Array(
								"MESSAGE" => GetMessage("ZIP_NOT_INCLUDED"),
								"TAG" => "ZIP_NOT_INCLUDED",
								"MODULE_ID" => "WEBPROSTOR.IMPORT",
								"ENABLE_CLOSE" => "Y"
							);
							$notifyID = CAdminNotify::Add($errorArray);
						}
						break;
					case("application/x-gzip"):
						if(extension_loaded('zlib'))
						{
							$buffer_size = 4096;
							$out_file_name = $_SERVER["DOCUMENT_ROOT"].$path_to.str_replace('.gz', '', $temp["name"]); 

							$file = gzopen($temp["tmp_name"], 'rb');
							$out_file = fopen($out_file_name, 'wb'); 

							while (!gzeof($file))
							{
								fwrite($out_file, gzread($file, $buffer_size));
							}

							fclose($out_file);
							gzclose($file);
						}
						else
						{
							$errorArray = Array(
								"MESSAGE" => GetMessage("ZLIB_NOT_INCLUDED"),
								"TAG" => "ZLIB_NOT_INCLUDED",
								"MODULE_ID" => "WEBPROSTOR.IMPORT",
								"ENABLE_CLOSE" => "Y"
							);
							$notifyID = CAdminNotify::Add($errorArray);
						}
						break;
				}
			}
		}
		return $result;
	}
	
	public function Load($PLAN_ID = false)
	{
		$resultText = "CWebprostorImport::Load({$PLAN_ID});";
		
		$planData = new CWebprostorImportPlan;
		$planRes = $planData->GetByID($PLAN_ID);
		$planParams = $planRes->Fetch();
		
		if($planParams["DEBUG_EVENTS"] == "Y")
			$DEBUG_EVENTS = true;
		
		if($planParams["DEBUG_URL"] == "Y")
			$DEBUG_URL = true;
		
		if($planParams["CURL_TIMEOUT"] > 0)
			$CURL_TIMEOUT = $planParams["CURL_TIMEOUT"];
		else
			$CURL_TIMEOUT = self::CURL_TIMEOUT;
		
		if($DEBUG_EVENTS && $DEBUG_URL)
		{
			$logLoad = new CWebprostorImportLog;
			$logLoadData = Array("PLAN_ID" => $PLAN_ID);
		}
		
		$IMPORT_FILE = $planParams["IMPORT_FILE"];
		$IMPORT_FILE_URL = $planParams["IMPORT_FILE_URL"];
		
		$PATH_TO_IMAGES = $planParams["PATH_TO_IMAGES"];
		$PATH_TO_IMAGES_URL = $planParams["PATH_TO_IMAGES_URL"];
		
		$PATH_TO_FILES = $planParams["PATH_TO_FILES"];
		$PATH_TO_FILES_URL = $planParams["PATH_TO_FILES_URL"];
			
		if(extension_loaded('curl'))
		{

			$urlF = filter_var($IMPORT_FILE_URL, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED);
				
			if($DEBUG_EVENTS && $DEBUG_URL)
			{
				$logLoadData["EVENT"] = "LOAD_IMPORT_FILE";
				$logLoadData["MESSAGE"] = GetMessage("EVENT_GET_URL_FILE");
				$logLoadData["DATA"] = $urlF;
				$logLoad->Add($logLoadData);
			}
		
			if($IMPORT_FILE && $urlF && !is_file($_SERVER["DOCUMENT_ROOT"].$IMPORT_FILE))
			{
				$urlParams = parse_url($urlF);
				
				$handle = curl_init();
				
				curl_setopt($handle, CURLOPT_URL, $urlF);
				curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
				curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, $CURL_TIMEOUT);
				if($planParams["CURL_FOLLOWLOCATION"] == "Y")
				{
					curl_setopt($handle, CURLOPT_FOLLOWLOCATION, TRUE);
				}
				
				$response = curl_exec($handle);
				
				$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
				
				//if($httpCode == 200)
				if($httpCode == 200 || ($httpCode == 226 && strtolower($urlParams["scheme"]) == 'ftp'))
				{
					$temp = CFile::MakeFileArray($urlF);
					if(is_array($temp))
					{
						if($DEBUG_EVENTS && $DEBUG_URL)
						{
							$logLoadData["EVENT"] = "LOAD_IMPORT_FILE";
							$logLoadData["MESSAGE"] = GetMessage("MESSAGE_URL_OK");
							$logLoadData["DATA"] = base64_encode(serialize(array_merge(array_keys($temp), $temp)));
							$logLoad->Add($logLoadData);
						}
						rename($temp["tmp_name"], $_SERVER["DOCUMENT_ROOT"].$IMPORT_FILE);
					}
				}
				elseif($DEBUG_EVENTS && $DEBUG_URL)
				{
					$logLoadData["EVENT"] = "LOAD_IMPORT_FILE";
					$logLoadData["MESSAGE"] = GetMessage("ERROR_GET_URL_FILE", Array("#ERROR_CODE#" => $httpCode));
					$logLoadData["DATA"] = "";
					$logLoad->Add($logLoadData);
				}
			}
			
			if($PATH_TO_IMAGES && $PATH_TO_IMAGES_URL)
			{
				$result = self::ExtractArchiveTo($PATH_TO_IMAGES_URL, $PATH_TO_IMAGES);
			}
			
			if($PATH_TO_FILES && $PATH_TO_FILES_URL)
			{
				$result = self::ExtractArchiveTo($PATH_TO_FILES_URL, $PATH_TO_FILES);
			}
		}
		else
		{
			$errorArray = Array(
				"MESSAGE" => GetMessage("CURL_NOT_INCLUDED"),
				"TAG" => "CURL_NOT_INCLUDED",
				"MODULE_ID" => "WEBPROSTOR.IMPORT",
				"ENABLE_CLOSE" => "Y"
			);
			$notifyID = CAdminNotify::Add($errorArray);
		}
		
		return $resultText;
	}
	
	public function Import($PLAN_ID = false, $startFrom = 0, $endTo = false)
	{
		$rsModule = CModule::IncludeModuleEx("WEBPROSTOR.IMPORT");
		if($rsModule == MODULE_DEMO_EXPIRED)
		{
			$errorArray = Array(
				"MESSAGE" => GetMessage("MODULE_DEMO_EXPIRED"),
				"TAG" => "WEBPROSTOR_IMPORT_MODULE_DEMO_EXPIRED",
				"MODULE_ID" => "WEBPROSTOR.IMPORT",
				"ENABLE_CLOSE" => "Y"
			);
			$notifyID = CAdminNotify::Add($errorArray);
			return;
		}
		elseif($rsModule == MODULE_NOT_FOUND)
		{
			return;
		}

		if(!$rsModule) {
			return;
		}
		
		$logData = new CWebprostorImportLog;
		$logFields = Array("PLAN_ID" => $PLAN_ID);
		
		$resultText = "CWebprostorImport::Import({$PLAN_ID});";
		
		if(!$PLAN_ID)
		{
			$logFields["EVENT"] = "IMPORT_PLAN";
			$logFields["MESSAGE"] = GetMessage("ERROR_NO_PLAN");
			unset($logFields["DATA"]);
			$logData->Add($logFields);
			
			return $resultText;
		}
		
		$planData = new CWebprostorImportPlan;
		$planRes = $planData->GetByID($PLAN_ID);
		$planParams = $planRes->Fetch();
		
		if(!is_array($planParams))
			return false;
		
		if($planParams["IMPORT_CATALOG_PRODUCT_OFFERS"] == "Y" && CModule::IncludeModule("catalog"))
		{
			$catalogSKU = CCatalogSKU::GetInfoByProductIBlock($planParams["IBLOCK_ID"]);
			
			$planParams["OFFERS_IBLOCK_ID"] = $catalogSKU["IBLOCK_ID"];
			$planParams["OFFERS_SKU_PROPERTY_ID"] = $catalogSKU["SKU_PROPERTY_ID"];
		}
		
		global $DEBUG_PLAN_ID, $DEBUG_IMAGES, $DEBUG_FILES, $DEBUG_URL, $DEBUG_EVENTS, $DEBUG_IMPORT_SECTION, $DEBUG_IMPORT_ELEMENTS, $DEBUG_IMPORT_PROPERTIES, $DEBUG_IMPORT_PRODUCTS, $DEBUG_IMPORT_OFFERS, $DEBUG_IMPORT_PRICES, $DEBUG_IMPORT_STORE_AMOUNT, $DEBUG_IMPORT_ENTITIES, $CURL_TIMEOUT, $CURL_FOLLOWLOCATION;
		
		$DEBUG_PLAN_ID = $PLAN_ID;
		$DEBUG_EVENTS = false;
		$DEBUG_IMAGES = false;
		$DEBUG_FILES = false;
		$DEBUG_URL = false;
		$DEBUG_IMPORT_SECTION = false;
		$DEBUG_IMPORT_ELEMENTS = false;
		$DEBUG_IMPORT_PROPERTIES = false;
		$DEBUG_IMPORT_PRODUCTS = false;
		$DEBUG_IMPORT_OFFERS = false;
		$DEBUG_IMPORT_PRICES = false;
		$DEBUG_IMPORT_STORE_AMOUNT = false;
		$DEBUG_IMPORT_ENTITIES = false;
		
		$CURL_TIMEOUT = self::CURL_TIMEOUT;
		$CURL_FOLLOWLOCATION = ($planParams["CURL_FOLLOWLOCATION"] == "Y"?true:false);
		
		if($planParams["DEBUG_EVENTS"] == "Y")
			$DEBUG_EVENTS = true;
		
		if($planParams["DEBUG_IMAGES"] == "Y")
			$DEBUG_IMAGES = true;
		
		if($planParams["DEBUG_FILES"] == "Y")
			$DEBUG_FILES = true;
		
		if($planParams["DEBUG_URL"] == "Y")
			$DEBUG_URL = true;
		
		if($planParams["DEBUG_IMPORT_SECTION"] == "Y")
			$DEBUG_IMPORT_SECTION = true;
		
		if($planParams["DEBUG_IMPORT_ELEMENTS"] == "Y")
			$DEBUG_IMPORT_ELEMENTS = true;
		
		if($planParams["DEBUG_IMPORT_PROPERTIES"] == "Y")
			$DEBUG_IMPORT_PROPERTIES = true;
		
		if($planParams["DEBUG_IMPORT_PRODUCTS"] == "Y")
			$DEBUG_IMPORT_PRODUCTS = true;
		
		if($planParams["DEBUG_IMPORT_OFFERS"] == "Y")
			$DEBUG_IMPORT_OFFERS = true;
		
		if($planParams["DEBUG_IMPORT_PRICES"] == "Y")
			$DEBUG_IMPORT_PRICES = true;
		
		if($planParams["DEBUG_IMPORT_STORE_AMOUNT"] == "Y")
			$DEBUG_IMPORT_STORE_AMOUNT = true;
		
		if($planParams["DEBUG_IMPORT_ENTITIES"] == "Y")
			$DEBUG_IMPORT_ENTITIES = true;
		
		if(!$planParams["ITEMS_PER_ROUND"]>0)
			$planParams["ITEMS_PER_ROUND"] = self::ITEMS_PER_ROUND;
		
		if($planParams["CURL_TIMEOUT"] > $CURL_TIMEOUT)
			$CURL_TIMEOUT = $planParams["CURL_TIMEOUT"];
		
		$rsHandlers = GetModuleEvents(self::MODULE_ID, "onBeforeImport");
		while($arHandler = $rsHandlers->Fetch())
		{
			ExecuteModuleEvent($arHandler, $PLAN_ID, $planParams);
		}
		
		if($DEBUG_EVENTS)
		{
			$logFields["EVENT"] = "START_PLAN_CYCLE";
			$logFields["MESSAGE"] = GetMessage("EVENT_START_PLAN");
			$logFields["DATA"] = "--STEP START--";
			$logData->Add($logFields);
		}
		
		$IMPORT_FILE = $_SERVER["DOCUMENT_ROOT"].$planParams["IMPORT_FILE"];
		if(!is_file($IMPORT_FILE))
		{
			if($DEBUG_EVENTS)
			{
				$logFields["EVENT"] = "GET_IMPORT_FILE";
				$logFields["MESSAGE"] = GetMessage("ERROR_NO_IMPORT_FILE");
				$logFields["DATA"] = $planParams["IMPORT_FILE"];
				$logData->Add($logFields);
			}
			return $resultText;
		}
		elseif($DEBUG_EVENTS)
		{
			$logFields["EVENT"] = "START_PARSE_FILE";
			$logFields["MESSAGE"] = GetMessage("EVENT_START_PARSE_FILE");
			$logFields["DATA"] = $planParams["IMPORT_FILE"];
			$logData->Add($logFields);
		}
		
		$GLOBALS["PLAN_ID"] = $PLAN_ID;
		
		switch($planParams["IMPORT_FORMAT"])
		{
			case("CSV"):
				$scriptData = new CWebprostorImportCSV;
				$fileData = $scriptData->ParseFile($IMPORT_FILE, $planParams["IMPORT_FILE_SHARSET"], false, $planParams["CSV_DELIMITER"]);
				break;
			case("XML"):
				$scriptData = new CWebprostorImportXML;
				$fileData = $scriptData->ParseFile($IMPORT_FILE, $planParams["IMPORT_FILE_SHARSET"], $planParams["XML_ENTITY"], $planParams["XML_PARSE_PARAMS_TO_PROPERTIES"], $planParams["ITEMS_PER_ROUND"], $startFrom);
				break;
			case("XLS"):
				$scriptData = new CWebprostorImportXLS;
				$fileData = $scriptData->ParseFile($IMPORT_FILE, $planParams["IMPORT_FILE_SHARSET"], $planParams["XLS_SHEET"], false, false);
				break;
			case("XLSX"):
				$scriptData = new CWebprostorImportXLSX;
				$fileData = $scriptData->ParseFile($IMPORT_FILE, $planParams["IMPORT_FILE_SHARSET"], $planParams["XLS_SHEET"], false, false);
				break;
			default:
				return false;
		}
		
		$entities = $scriptData->GetEntities($PLAN_ID);
		
		if($planParams["IMPORT_FILE_CHECK"] != "N")
		{
			if($planParams["IMPORT_FORMAT"] == "XML")
				$checkEntitiesResult = self::CheckEntitiesNames($PLAN_ID, $entities["KEYS"]);
			else
				$checkEntitiesResult = self::CheckEntitiesNames($PLAN_ID, $entities);
			
			if(count($checkEntitiesResult))
			{
				if($DEBUG_EVENTS)
				{
					$logFields["EVENT"] = "CHECK_ENTITIES_NAMES";
					$logFields["MESSAGE"] = GetMessage("ERROR_ENTITIES_AND_NAMES_NOT_IDENTICAL");
					$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($checkEntitiesResult), $checkEntitiesResult)));
					$logData->Add($logFields);
				}
				return $resultText;
			}
			elseif($DEBUG_EVENTS)
			{
				$logFields["EVENT"] = "CHECK_ENTITIES_NAMES_OK";
				$logFields["MESSAGE"] = GetMessage("MESSAGE_CHECK_ENTITIES_NAMES_OK");
				$logFields["DATA"] = "";
				$logData->Add($logFields);
			}
		}
		
		if(!$fileData)
		{
			if($DEBUG_EVENTS)
			{
				$logFields["EVENT"] = "GET_FILE_DATA";
				$logFields["MESSAGE"] = GetMessage("ERROR_NO_FILE_DATA");
				unset($logFields["DATA"]);
				$logData->Add($logFields);
			}
			return $resultText;
		}
		elseif($DEBUG_EVENTS)
		{
			$logFields["EVENT"] = "PARSE_FILE";
			$logFields["MESSAGE"] = GetMessage("EVENT_PARSE_FILE", Array("#COUNT#" => $fileData["ITEMS_COUNT"]));
			$logFields["DATA"] = "";
			$logData->Add($logFields);
		}
		
		if($planParams["IMPORT_FORMAT"] == "XML")
		{
			$fileDataArrayRes = $scriptData->GetDataArray($fileData["DATA"], $planParams, $startFrom, $fileData["NEXT_NODE"]);
		}
		else
		{
			$fileDataArrayRes = $scriptData->GetDataArray($fileData["DATA"], $planParams, $startFrom);
		}
		$fileDataArray = $fileDataArrayRes["DATA_ARRAY"];
		if(!$fileDataArray && !is_array($fileDataArray))
		{
			if($DEBUG_EVENTS)
			{
				$logFields["EVENT"] = "GET_DATA_ARRAY";
				$logFields["MESSAGE"] = GetMessage("ERROR_CANNOT_GET_DATA_ARRAY");
				unset($logFields["DATA"]);
				$logData->Add($logFields);
			}
			return $resultText;
		}
		elseif($DEBUG_EVENTS)
		{
			$logFields["EVENT"] = "GET_DATA_ARRAY";
			$logFields["MESSAGE"] = GetMessage("EVENT_GET_DATA_ARRAY", Array("#COUNT#" => count($fileDataArray)));
			$logFields["DATA"] = "";
			$logData->Add($logFields);
		}
		if($fileDataArrayRes["START_FROM"]>0)
		{
			$resultText = "CWebprostorImport::Import({$PLAN_ID}, {$fileDataArrayRes["START_FROM"]});";
		}
		
		if($startFrom == 0)
		{
			$UpdateLastImportDate = $planData -> UpdateLastImportDate($PLAN_ID, "LAST_IMPORT_DATE", date("d.m.Y H:i:s"));
		}
		
		//$entities = $scriptData->GetEntities($PLAN_ID);
		if($planParams["IMPORT_FORMAT"] == "XML")
		{
			$entities = $entities["KEYS"];
			$planEntityConnection = self::GetPlanEntityConnection($PLAN_ID, $entities, $planParams["XML_USE_ENTITY_NAME"]);
		}
		else
		{
			$planEntityConnection = self::GetPlanEntityConnection($PLAN_ID, $entities);
		}
		
		if(!$planEntityConnection && !is_array($planEntityConnection))
		{
			if($DEBUG_EVENTS)
			{
				$logFields["EVENT"] = "GET_PLAN_ENTITY_CONNECTION";
				$logFields["MESSAGE"] = GetMessage("ERROR_CANNOT_GET_PLAN_ENTITY_CONNECTION");
				unset($logFields["DATA"]);
				$logData->Add($logFields);
			}
			return $resultText;
		}
		elseif($DEBUG_EVENTS)
		{
			$logFields["EVENT"] = "GET_PLAN_ENTITY_CONNECTION";
			$logFields["MESSAGE"] = GetMessage("EVENT_GET_PLAN_ENTITY_CONNECTION", Array("#COUNT#" => count($planEntityConnection)));
			$logFields["DATA"] = '';
			$logData->Add($logFields);
		}
		
		$importParams = Array(
			"IMPORT_SECTIONS" => $planParams["IMPORT_IBLOCK_SECTIONS"],
			"SECTIONS_ADD" => $planParams["SECTIONS_ADD"],
			"SECTIONS_DEFAULT_ACTIVE" => $planParams["SECTIONS_DEFAULT_ACTIVE"],
			"SECTIONS_DEFAULT_SECTION_ID" => intVal($planParams["SECTIONS_DEFAULT_SECTION_ID"]),
			"SECTIONS_UPDATE" => $planParams["SECTIONS_UPDATE"],
			
			"IMPORT_ELEMENTS" => $planParams["IMPORT_IBLOCK_ELEMENTS"],
			"ELEMENTS_2_STEP_SEARCH" => $planParams["ELEMENTS_2_STEP_SEARCH"],
			"ELEMENTS_ADD" => $planParams["ELEMENTS_ADD"],
			"ELEMENTS_DEFAULT_ACTIVE" => $planParams["ELEMENTS_DEFAULT_ACTIVE"],
			"ELEMENTS_DEFAULT_SECTION_ID" => intVal($planParams["ELEMENTS_DEFAULT_SECTION_ID"]),
			"ELEMENTS_UPDATE" => $planParams["ELEMENTS_UPDATE"],
			
			"IMPORT_PROPERTIES" => $planParams["IMPORT_IBLOCK_PROPERTIES"],
			"PROPERTIES_ADD" => $planParams["PROPERTIES_ADD"],
			"PROPERTIES_UPDATE" => $planParams["PROPERTIES_UPDATE"],
			"PROPERTIES_RESET" => $planParams["PROPERTIES_RESET"],
			"PROPERTIES_TRANSLATE_XML_ID" => $planParams["PROPERTIES_TRANSLATE_XML_ID"],
			"PROPERTIES_SET_DEFAULT_VALUES" => $planParams["PROPERTIES_SET_DEFAULT_VALUES"],
			"PROPERTIES_SKIP_DOWNLOAD_URL_UPDATE" => $planParams["PROPERTIES_SKIP_DOWNLOAD_URL_UPDATE"],
			"PROPERTIES_WHATERMARK" => $planParams["PROPERTIES_WHATERMARK"],
			"PROPERTIES_INCREMENT_TO_MULTIPLE" => $planParams["PROPERTIES_INCREMENT_TO_MULTIPLE"],
			
			"IMPORT_PRODUCTS" => $planParams["IMPORT_CATALOG_PRODUCTS"],
			"PRODUCTS_ADD" => $planParams["PRODUCTS_ADD"],
			"PRODUCTS_UPDATE" => $planParams["PRODUCTS_UPDATE"],
			
			"IMPORT_OFFERS" => $planParams["IMPORT_CATALOG_PRODUCT_OFFERS"],
			"OFFERS_ADD" => $planParams["OFFERS_ADD"],
			"OFFERS_UPDATE" => $planParams["OFFERS_UPDATE"],
			"OFFERS_SET_NAME_FROM_ELEMENT" => $planParams["OFFERS_SET_NAME_FROM_ELEMENT"],
			
			"IMPORT_PRICES" => $planParams["IMPORT_CATALOG_PRICES"],
			"PRICES_ADD" => $planParams["PRICES_ADD"],
			"PRICES_UPDATE" => $planParams["PRICES_UPDATE"],
			
			"IMPORT_STORE_AMOUNT" => $planParams["IMPORT_CATALOG_STORE_AMOUNT"],
			"STORE_AMOUNT_ADD" => $planParams["STORE_AMOUNT_ADD"],
			"STORE_AMOUNT_UPDATE" => $planParams["STORE_AMOUNT_UPDATE"],
			
			"IMPORT_HIGHLOAD_BLOCK_ENTITIES" => $planParams["IMPORT_HIGHLOAD_BLOCK_ENTITIES"],
			"ENTITIES_ADD" => $planParams["ENTITIES_ADD"],
			"ENTITIES_UPDATE" => $planParams["ENTITIES_UPDATE"],
			"ENTITIES_TRANSLATE_XML_ID" => $planParams["ENTITIES_TRANSLATE_XML_ID"],
			
			"IBLOCK_ID" => $planParams["IBLOCK_ID"],
			"HIGHLOAD_BLOCK" => $planParams["HIGHLOAD_BLOCK"],
			
			"OFFERS_IBLOCK_ID" => $planParams["OFFERS_IBLOCK_ID"],
			"OFFERS_SKU_PROPERTY_ID" => $planParams["OFFERS_SKU_PROPERTY_ID"],
			
			"PATH_TO_IMAGES" => $planParams["PATH_TO_IMAGES"],
			"RESIZE_IMAGE" => $planParams["RESIZE_IMAGE"],
			
			"PATH_TO_FILES" => $planParams["PATH_TO_FILES"],
			"RAW_URL_DECODE" => $planParams["RAW_URL_DECODE"],
		);
		if($planParams["PRODUCTS_PARAMS"])
		{
			$importParams["PRODUCTS_PARAMS"] = unserialize(base64_decode($planParams["PRODUCTS_PARAMS"]));
			if(!is_array($importParams["PRODUCTS_PARAMS"]))
				$importParams["PRODUCTS_PARAMS"] = Array();
		}
		else
		{
			$importParams["PRODUCTS_PARAMS"] = array();
		}
		
		if($planParams["IMPORT_FORMAT"] != "XML")
		{
			$importParams["SECTIONS_MAX_DEPTH_LEVEL"] = $planParams["CSV_XLS_MAX_DEPTH_LEVEL"];
		}
		else
		{
			$importParams["XML_PARSE_PARAMS_TO_PROPERTIES"] = $planParams["XML_PARSE_PARAMS_TO_PROPERTIES"];
			$importParams["XML_ADD_PROPERTIES_FOR_PARAMS"] = $planParams["XML_ADD_PROPERTIES_FOR_PARAMS"];
		}
		
		$importData = self::ImportData(
			$PLAN_ID,
			$fileDataArray, 
			$planEntityConnection,
			$importParams
		);
		
		if($DEBUG_EVENTS)
		{
			if($planParams["IMPORT_IBLOCK_SECTIONS"] == "Y" && $DEBUG_IMPORT_SECTION == "Y")
			{
				$logFields["EVENT"] = "SECTIONS_IS_IMPORTED";
				$logFields["MESSAGE"] = GetMessage("EVENT_SECTIONS_IS_IMPORTED");
				$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importData["SECTIONS"]), $importData["SECTIONS"])));
				$logData->Add($logFields);
			}
			
			if($planParams["IMPORT_IBLOCK_ELEMENTS"] == "Y" && $DEBUG_IMPORT_ELEMENTS == "Y")
			{
				$logFields["EVENT"] = "ELEMENTS_IS_IMPORTED";
				$logFields["MESSAGE"] = GetMessage("EVENT_ELEMENTS_IS_IMPORTED");
				$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importData["ELEMENTS"]), $importData["ELEMENTS"])));
				$logData->Add($logFields);
			}
			
			if($planParams["IMPORT_IBLOCK_PROPERTIES"] == "Y" && $DEBUG_IMPORT_PROPERTIES == "Y")
			{
				$logFields["EVENT"] = "PROPERTIES_IS_IMPORTED";
				$logFields["MESSAGE"] = GetMessage("EVENT_PROPERTIES_IS_IMPORTED");
				$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importData["PROPERTIES"]), $importData["PROPERTIES"])));
				$logData->Add($logFields);
			}
			
			if($planParams["IMPORT_CATALOG_PRODUCTS"] == "Y" && $DEBUG_IMPORT_PRODUCTS == "Y")
			{
				$logFields["EVENT"] = "PRODUCTS_IS_IMPORTED";
				$logFields["MESSAGE"] = GetMessage("EVENT_PRODUCTS_IS_IMPORTED");
				$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importData["PRODUCTS"]), $importData["PRODUCTS"])));
				$logData->Add($logFields);
			}
			if($planParams["IMPORT_CATALOG_PRODUCT_OFFERS"] == "Y" && $DEBUG_IMPORT_OFFERS == "Y")
			{
				$logFields["EVENT"] = "OFFERS_IS_IMPORTED";
				$logFields["MESSAGE"] = GetMessage("EVENT_OFFERS_IS_IMPORTED");
				$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importData["OFFERS"]), $importData["OFFERS"])));
				$logData->Add($logFields);
			}
			
			if($planParams["IMPORT_CATALOG_PRICES"] == "Y" && $DEBUG_IMPORT_PRICES == "Y")
			{
				$logFields["EVENT"] = "PRICES_IS_IMPORTED";
				$logFields["MESSAGE"] = GetMessage("EVENT_PRICES_IS_IMPORTED");
				$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importData["PRICES"]), $importData["PRICES"])));
				$logData->Add($logFields);
			}
			
			if($planParams["IMPORT_CATALOG_STORE_AMOUNT"] == "Y" && $DEBUG_IMPORT_STORE_AMOUNT == "Y")
			{
				$logFields["EVENT"] = "STORE_AMOUNT_IS_IMPORTED";
				$logFields["MESSAGE"] = GetMessage("EVENT_STORE_AMOUNT_IS_IMPORTED");
				$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importData["STORE_AMOUNTS"]), $importData["STORE_AMOUNTS"])));
				$logData->Add($logFields);
			}
			
			if($planParams["IMPORT_HIGHLOAD_BLOCK_ENTITIES"] == "Y" && $DEBUG_IMPORT_ENTITIES == "Y")
			{
				$logFields["EVENT"] = "ENTITIES_IS_IMPORTED";
				$logFields["MESSAGE"] = GetMessage("EVENT_ENTITIES_IS_IMPORTED");
				$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importData["ENTITIES"]), $importData["ENTITIES"])));
				$logData->Add($logFields);
			}
		}
		
		if($DEBUG_EVENTS)
		{
			$logFields["EVENT"] = "FINISH_PLAN_CYCLE";
			$logFields["MESSAGE"] = GetMessage("EVENT_FINISH_PLAN");
			$logFields["DATA"] = "--STEP FINISH--";
			$logData->Add($logFields);
		}
		
		if($fileDataArrayRes["FINISHED"])
		{
		
			$lastImportDateRes = $planData -> GetLastImportDate($PLAN_ID);
			$lastImportDateArr = $lastImportDateRes -> Fetch();
			$lastImportDate = $lastImportDateArr["LAST_IMPORT_DATE"];
			
			$rsHandlers = GetModuleEvents(self::MODULE_ID, "onBeforeFinishedImport");
			while($arHandler = $rsHandlers->Fetch())
			{
				ExecuteModuleEvent($arHandler, $PLAN_ID, $planParams, $lastImportDate);
			}
			
			DeleteDirFilesEx('/bitrix/tmp/'.self::MODULE_ID);
			
			if($planParams["IMPORT_FILE_DELETE"] == "Y")
			{
				$deleteFile = unlink($IMPORT_FILE);
				if($DEBUG_EVENTS)
				{
					$logFields["EVENT"] = "IMPORT_FILE_DELETE";
					$logFields["DATA"] = $IMPORT_FILE;
					$logFields["MESSAGE"] = ($deleteFile?GetMessage("EVENT_IMPORT_FILE_DELETE_OK"):GetMessage("EVENT_IMPORT_FILE_DELETE_NO"));
					$logData->Add($logFields);
				}
			}
			if($planParams["CLEAR_IMAGES_DIR"] == "Y")
			{
				$clearImagesDir = self::DeleteDirFilesOnly($planParams["PATH_TO_IMAGES"]);
				if($DEBUG_EVENTS)
				{
					$logFields["EVENT"] = "CLEAR_IMAGES_DIR";
					$logFields["DATA"] = $planParams["PATH_TO_IMAGES"];
					$logFields["MESSAGE"] = ($clearImagesDir?GetMessage("EVENT_CLEAR_DIR_OK"):GetMessage("EVENT_CLEAR_DIR_NO"));
					$logData->Add($logFields);
				}
			}
			if($planParams["CLEAR_UPLOAD_TMP_DIR"] == "Y")
			{
				$clearUploadTmpDir = self::DeleteDirFilesOnly("/upload/tmp/");
				if($DEBUG_EVENTS)
				{
					$logFields["EVENT"] = "CLEAR_UPLOAD_TMP_DIR";
					$logFields["DATA"] = "/upload/tmp/";
					$logFields["MESSAGE"] = ($clearUploadTmpDir?GetMessage("EVENT_CLEAR_DIR_OK"):GetMessage("EVENT_CLEAR_DIR_NO"));
					$logData->Add($logFields);
				}
			}
			if($planParams["CLEAR_FILES_DIR"] == "Y")
			{
				$clearFilesDir = self::DeleteDirFilesOnly($planParams["PATH_TO_FILES"]);
				if($DEBUG_EVENTS)
				{
					$logFields["EVENT"] = "CLEAR_FILES_DIR";
					$logFields["DATA"] = $planParams["PATH_TO_FILES"];
					$logFields["MESSAGE"] = ($clearFilesDir?GetMessage("EVENT_CLEAR_DIR_OK"):GetMessage("EVENT_CLEAR_DIR_NO"));
					$logData->Add($logFields);
				}
			}
			
			if($planParams["SECTIONS_IN_ACTION"] != "N" && $planParams["IMPORT_IBLOCK_SECTIONS"] == "Y")
			{
				$changedInSections = self::ChangeInSections($lastImportDate, $planParams["SECTIONS_IN_ACTION"], $planParams["IBLOCK_ID"], $planParams["SECTIONS_IN_ACTION_FILTER"]);
				
				if($DEBUG_EVENTS)
				{
					$logFields["EVENT"] = "CHANGE_IN_SECTIONS";
					$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($changedInSections), $changedInSections)));
					$logFields["MESSAGE"] = GetMessage("EVENT_CHANGE_IN_SECTIONS");
					$logData->Add($logFields);
				}
			}
			
			if($planParams["ELEMENTS_IN_ACTION"] != "N" && $planParams["IMPORT_IBLOCK_ELEMENTS"] == "Y")
			{
				$changedInElements = self::ChangeInElements($lastImportDate, $planParams["ELEMENTS_IN_ACTION"], $planParams["IBLOCK_ID"], $planParams["ELEMENTS_IN_ACTION_FILTER"]);
				
				if($DEBUG_EVENTS)
				{
					$logFields["EVENT"] = "CHANGE_IN_ELEMENTS";
					$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($changedInElements), $changedInElements)));
					$logFields["MESSAGE"] = GetMessage("EVENT_CHANGE_IN_ELEMENTS");
					$logData->Add($logFields);
				}
			}

			if($planParams["OFFERS_IN_ACTION"] != "N" && $planParams["IMPORT_CATALOG_PRODUCT_OFFERS"] == "Y")
			{
				$changedInOffers = self::ChangeInElements($lastImportDate, $planParams["OFFERS_IN_ACTION"], $planParams["OFFERS_IBLOCK_ID"], $planParams["OFFERS_IN_ACTION_FILTER"]);
				
				if($DEBUG_EVENTS)
				{
					$logFields["EVENT"] = "CHANGE_IN_OFFERS";
					$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($changedInOffers), $changedInOffers)));
					$logFields["MESSAGE"] = GetMessage("EVENT_CHANGE_IN_OFFERS");
					$logData->Add($logFields);
				}
			}
			
			if($planParams["SECTIONS_OUT_ACTION"] != "N" && $planParams["IMPORT_IBLOCK_SECTIONS"] == "Y")
			{
				$changedOutSections = self::ChangeOutSections($lastImportDate, $planParams["SECTIONS_OUT_ACTION"], $planParams["IBLOCK_ID"], $planParams["SECTIONS_OUT_ACTION_FILTER"]);
				
				if($DEBUG_EVENTS)
				{
					$logFields["EVENT"] = "CHANGE_OUT_SECTIONS";
					$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($changedOutSections), $changedOutSections)));
					$logFields["MESSAGE"] = GetMessage("EVENT_CHANGE_OUT_SECTIONS");
					$logData->Add($logFields);
				}
			}
			
			if($planParams["ELEMENTS_OUT_ACTION"] != "N" && $planParams["IMPORT_IBLOCK_ELEMENTS"] == "Y")
			{
				$changedOutElements = self::ChangeOutElements($lastImportDate, $planParams["ELEMENTS_OUT_ACTION"], $planParams["IBLOCK_ID"], $planParams["ELEMENTS_OUT_ACTION_FILTER"]);
				
				if($DEBUG_EVENTS)
				{
					$logFields["EVENT"] = "CHANGE_OUT_ELEMENTS";
					$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($changedOutElements), $changedOutElements)));
					$logFields["MESSAGE"] = GetMessage("EVENT_CHANGE_OUT_ELEMENTS");
					$logData->Add($logFields);
				}
			}

			if($planParams["OFFERS_OUT_ACTION"] != "N" && $planParams["IMPORT_CATALOG_PRODUCT_OFFERS"] == "Y")
			{
				$changedOutOffers = self::ChangeOutElements($lastImportDate, $planParams["OFFERS_OUT_ACTION"], $planParams["OFFERS_IBLOCK_ID"], $planParams["OFFERS_OUT_ACTION_FILTER"]);
				
				if($DEBUG_EVENTS)
				{
					$logFields["EVENT"] = "CHANGE_OUT_OFFERS";
					$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($changedOutOffers), $changedOutOffers)));
					$logFields["MESSAGE"] = GetMessage("EVENT_CHANGE_OUT_OFFERS");
					$logData->Add($logFields);
				}
			}
		
			if($DEBUG_EVENTS)
			{
				$logListRes = $logData->GetList();
				if(intVal($logListRes->SelectedRowsCount())>=1000)
				{
					$errorArray = Array(
						"MESSAGE" => GetMessage("LOGS_ARE_TOO_BIG"),
						"TAG" => "LOGS_ARE_TOO_BIG",
						"MODULE_ID" => "WEBPROSTOR.IMPORT",
						"ENABLE_CLOSE" => "Y"
					);
					$notifyID = CAdminNotify::Add($errorArray);
				}
			}
			
			$rsHandlers = GetModuleEvents(self::MODULE_ID, "onAfterFinishedImport");
			while($arHandler = $rsHandlers->Fetch())
			{
				ExecuteModuleEvent($arHandler, $PLAN_ID, $planParams, $lastImportDate);
			}
			
			$UpdateLastFinishImportDate = $planData -> UpdateLastImportDate($PLAN_ID, "LAST_FINISH_IMPORT_DATE", date("d.m.Y H:i:s"));
		}
		
		return $resultText;
	}
	
	private function ChangeOutSections($startDate, $action, $IBLOCK_ID, $arPreFilter)
	{
		global $DB;
		
		$arPreFilter = CWebprostorImportFilter::SerializePreFilter($arPreFilter);
		
		$hideCount = 0;
		$deleteCount = 0;
		$format = "YYYY-MM-DD HH:MI:SS";
		$bs = new CIBlockSection;
		
		$lastImportDate = new DateTime($startDate);
		
		$dateArr = ParseDateTime($startDate, $format);
		
		$arSelect = Array("IBLOCK_ID", "ID", "ACTIVE", "NAME", "TIMESTAMP_X");
		$arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, "<TIMESTAMP_X"=>date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")), mktime($dateArr["HH"], $dateArr["MI"], $dateArr["SS"], $dateArr["MM"], $dateArr["DD"], $dateArr["YYYY"])));
		if($action == "H")
		{
			$arFilter["ACTIVE"] = "Y";
		}
		
		if(is_array($arPreFilter))
		{
			$arFilter = array_merge($arPreFilter, $arFilter);
		}
		
		$db_list = CIBlockSection::GetList(Array("TIMESTAMP_X"=>"DESC"), $arFilter, false, $arSelect, true);
		while($sectionArr = $db_list->GetNext())
		{
			switch($action)
			{
				case("H"):
					$arFields = Array(
						"ACTIVE" => "N",
					);
					$updateResult = $bs->Update($sectionArr['ID'], $arFields);
					$hideCount ++;
					
					break;
				case("D"):
					$deleteResult = $bs->Delete($sectionArr['ID']);
					$deleteCount ++;
					
					break;
			}
		}
		
		$result = Array(
			"HIDEN" => $hideCount,
			"DELETED" => $deleteCount,
		);
		
		return $result;
	}
	
	private function ChangeInSections($startDate, $action, $IBLOCK_ID, $arPreFilter)
	{
		global $DB;
		
		$arPreFilter = CWebprostorImportFilter::SerializePreFilter($arPreFilter);
		
		$activateCount = 0;
		$format = "YYYY-MM-DD HH:MI:SS";
		$bs = new CIBlockSection;
		
		$lastImportDate = new DateTime($startDate);
		
		$dateArr = ParseDateTime($startDate, $format);
		
		$arSelect = Array("IBLOCK_ID", "ID", "ACTIVE", "NAME", "TIMESTAMP_X");
		$arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, "ACTIVE"=>"N", ">TIMESTAMP_X"=>date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")), mktime($dateArr["HH"], $dateArr["MI"], $dateArr["SS"], $dateArr["MM"], $dateArr["DD"], $dateArr["YYYY"])));
		
		if(is_array($arPreFilter))
		{
			$arFilter = array_merge($arPreFilter, $arFilter);
		}

		$db_list = CIBlockSection::GetList(Array("TIMESTAMP_X"=>"DESC"), $arFilter, false, $arSelect, true);
		while($sectionArr = $db_list->GetNext())
		{
			switch($action)
			{
				case("A"):
					$arFields = Array(
						"ACTIVE" => "Y",
					);
					$updateResult = $bs->Update($sectionArr['ID'], $arFields);
					$activateCount ++;
					
					break;
			}
		}
		
		$result = Array(
			"ACTIVATED" => $activateCount,
		);
		
		return $result;
	}
	
	private function ChangeOutElements($startDate, $action, $IBLOCK_ID, $arPreFilter)
	{
		global $DB;
		
		$arPreFilter = CWebprostorImportFilter::SerializePreFilter($arPreFilter);
		
		$hideCount = 0;
		$deleteCount = 0;
		$format = "YYYY-MM-DD HH:MI:SS";
		$el = new CIBlockElement;
		
		$lastImportDate = new DateTime($startDate);
		
		$dateArr = ParseDateTime($startDate, $format);
		
		$arSelect = Array("IBLOCK_ID", "ID", "ACTIVE", "TIMESTAMP_X");
		$arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, "<TIMESTAMP_X"=>date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")), mktime($dateArr["HH"], $dateArr["MI"], $dateArr["SS"], $dateArr["MM"], $dateArr["DD"], $dateArr["YYYY"])));
		if($action == "H")
		{
			$arFilter["ACTIVE"] = "Y";
		}
		if(is_array($arPreFilter))
		{
			$arFilter = array_merge($arPreFilter, $arFilter);
		}
		$elementsRes = CIBlockElement::GetList(Array("TIMESTAMP_X"=>"DESC"), $arFilter, false, Array(), $arSelect);
		while($element = $elementsRes->GetNextElement()) {
			$arFields = $element->GetFields();
			
			switch($action)
			{
				case("H"):
					$arLoadProductArray = Array(
						"ACTIVE" => "N",
					);
					$updateResult = $el->Update($arFields['ID'], $arLoadProductArray);
					$hideCount ++;
					
					break;
				case("D"):
					$deleteResult = $el->Delete($arFields['ID']);
					$deleteCount ++;
					
					break;
				case("Q"):
					if(CModule::IncludeModule('catalog'))
					{
						$arProductFields = ['QUANTITY' => 0];
						CCatalogProduct::Update($arFields['ID'], $arProductFields);
						$resetQuantityCount ++;
					}
					
					break;
			}
		}
		
		$result = Array(
			"HIDEN" => $hideCount,
			"DELETED" => $deleteCount,
			"RESET_QUANTITY" => $resetQuantityCount,
		);
		
		return $result;
	}
	
	private function ChangeInElements($startDate, $action, $IBLOCK_ID, $arPreFilter)
	{
		global $DB;
		
		$arPreFilter = CWebprostorImportFilter::SerializePreFilter($arPreFilter);
		
		$activateCount = 0;
		$format = "YYYY-MM-DD HH:MI:SS";
		$el = new CIBlockElement;
		
		$lastImportDate = new DateTime($startDate);
		
		$dateArr = ParseDateTime($startDate, $format);
		
		$arSelect = Array("IBLOCK_ID", "ID", "ACTIVE", "TIMESTAMP_X");
		$arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID, "ACTIVE"=>"N", ">TIMESTAMP_X"=>date($DB->DateFormatToPHP(CLang::GetDateFormat("FULL")), mktime($dateArr["HH"], $dateArr["MI"], $dateArr["SS"], $dateArr["MM"], $dateArr["DD"], $dateArr["YYYY"])));

		if(is_array($arPreFilter))
		{
			$arFilter = array_merge($arPreFilter, $arFilter);
		}
		$elementsRes = CIBlockElement::GetList(Array("TIMESTAMP_X"=>"DESC"), $arFilter, false, Array(), $arSelect);
		while($element = $elementsRes->GetNextElement()) {
			$arFields = $element->GetFields();
			
			switch($action)
			{
				case("A"):
					$arLoadProductArray = Array(
						"ACTIVE" => "Y",
					);
					$updateResult = $el->Update($arFields['ID'], $arLoadProductArray);
					$activateCount ++;
					
					break;
			}
		}
		
		$result = Array(
			"ACTIVATED" => $activateCount,
		);
		
		return $result;
	}
	
	private function DeleteDirFilesOnly($fileDir)
	{
		
		$fileFullDir = $_SERVER["DOCUMENT_ROOT"].$fileDir;
		
		if(file_exists($fileFullDir))
		{
			$dir = opendir($fileFullDir);
				
			while(($file = readdir($dir)) !== false)
			{
				if ($file == '..' || $file == '.')
					continue;
				if(is_dir($fileFullDir.$file))
				{
					DeleteDirFilesEx($fileDir.$file);
				}
				elseif(is_file($fileFullDir.$file))
				{
					unlink($fileFullDir.$file);
				}
			}
			
			closedir($dir);
			return true;
		}
		
		return false;
	}
	
	private function GetImportResultCount(&$result, $code, $event)
	{
		switch($event)
		{
			case("ADD"):
				$result[$code]["ADD"]++;
				break;
			case("UPDATE"):
				$result[$code]["UPDATE"]++;
				break;
			case("ERROR"):
				$result[$code]["ERROR"]++;
				break;
		}
	}
	
	private function GetImportResultCountPrice(&$result, $code, $events)
	{
		foreach($events as $event => $count)
		{
			$result[$code][$event] = $result[$code][$event]+$count;
		}
	}
	
	private function ImportData($PLAN_ID, $data, $rules, $params = Array())
	{
		$result = Array(
			"SECTIONS" => Array(
				"ADD" => 0,
				"UPDATE" => 0,
				"ERROR" => 0,
			),
			"ELEMENTS" => Array(
				"ADD" => 0,
				"UPDATE" => 0,
				"ERROR" => 0,
			),
			"PROPERTIES" => Array(
				"ADD" => 0,
				"UPDATE" => 0,
				"ERROR" => 0,
			),
			"PRODUCTS" => Array(
				"ADD" => 0,
				"UPDATE" => 0,
				"ERROR" => 0,
			),
			"OFFERS" => Array(
				"ADD" => 0,
				"UPDATE" => 0,
				"ERROR" => 0,
			),
			"PRICES" => Array(
				"ADD" => 0,
				"UPDATE" => 0,
				"ERROR" => 0,
			),
			"STORE_AMOUNTS" => Array(
				"ADD" => 0,
				"UPDATE" => 0,
				"ERROR" => 0,
			),
			"ENTITIES" => Array(
				"ADD" => 0,
				"UPDATE" => 0,
				"ERROR" => 0,
			),
		);
		
		global $DEBUG_EVENTS, $DEBUG_IMPORT_SECTION, $DEBUG_IMPORT_ELEMENTS, $DEBUG_IMPORT_PROPERTIES, $DEBUG_IMPORT_PRODUCTS, $DEBUG_IMPORT_OFFERS, $DEBUG_IMPORT_PRICES, $DEBUG_IMPORT_STORE_AMOUNT, $DEBUG_IMPORT_ENTITIES;
		
		if($DEBUG_EVENTS)
		{
			$logData = new CWebprostorImportLog;
			$logFields = Array("PLAN_ID" => $PLAN_ID);
		}
		
		if($params["IMPORT_SECTIONS"] == "Y" || $params["IMPORT_ELEMENTS"] == "Y" || $params["IMPORT_PROPERTIES"] == "Y" || $params["IMPORT_OFFERS"] == "Y")
		{
			CModule::IncludeModule("iblock");
			
			$iblockParams = CIBlock::GetFields($params["IBLOCK_ID"]);
			
			if($params["IMPORT_SECTIONS"] == "Y")
			{
				$iblockSectionParams = Array(
					"NAME" => $iblockParams["SECTION_NAME"],
					"PICTURE" => $iblockParams["SECTION_PICTURE"],
					"DESCRIPTION_TYPE" => $iblockParams["SECTION_DESCRIPTION_TYPE"],
					"DESCRIPTION" => $iblockParams["SECTION_DESCRIPTION"],
					"DETAIL_PICTURE" => $iblockParams["SECTION_DETAIL_PICTURE"],
					"CODE" => $iblockParams["SECTION_CODE"],
				);
			}
			if($params["IMPORT_ELEMENTS"] == "Y")
			{
				$iblockElementParams = Array(
					"IBLOCK_SECTION" => $iblockParams["IBLOCK_SECTION"],
					"ACTIVE" => $iblockParams["ACTIVE"],
					"ACTIVE_FROM" => $iblockParams["ACTIVE_FROM"],
					"ACTIVE_TO" => $iblockParams["ACTIVE_TO"],
					"SORT" => $iblockParams["SORT"],
					"NAME" => $iblockParams["NAME"],
					"PREVIEW_PICTURE" => $iblockParams["PREVIEW_PICTURE"],
					"PREVIEW_TEXT_TYPE" => $iblockParams["PREVIEW_TEXT_TYPE"],
					"PREVIEW_TEXT" => $iblockParams["PREVIEW_TEXT"],
					"DETAIL_PICTURE" => $iblockParams["DETAIL_PICTURE"],
					"DETAIL_TEXT_TYPE" => $iblockParams["DETAIL_TEXT_TYPE"],
					"DETAIL_TEXT" => $iblockParams["DETAIL_TEXT"],
					"CODE" => $iblockParams["CODE"],
					"TAGS" => $iblockParams["TAGS"],
				);
			}
			if($params["IMPORT_PROPERTIES"] == "Y")
			{
				$iblockPropertiesParams = Array(
					"PREVIEW_PICTURE" => Array(
						"USE_WATERMARK_TEXT" => $iblockParams["PREVIEW_PICTURE"]["DEFAULT_VALUE"]["USE_WATERMARK_TEXT"],
						"WATERMARK_TEXT" => $iblockParams["PREVIEW_PICTURE"]["DEFAULT_VALUE"]["WATERMARK_TEXT"],
						"WATERMARK_TEXT_FONT" => $iblockParams["PREVIEW_PICTURE"]["DEFAULT_VALUE"]["WATERMARK_TEXT_FONT"],
						"WATERMARK_TEXT_COLOR" => $iblockParams["PREVIEW_PICTURE"]["DEFAULT_VALUE"]["WATERMARK_TEXT_COLOR"],
						"WATERMARK_TEXT_SIZE" => $iblockParams["PREVIEW_PICTURE"]["DEFAULT_VALUE"]["WATERMARK_TEXT_SIZE"],
						"WATERMARK_TEXT_POSITION" => $iblockParams["PREVIEW_PICTURE"]["DEFAULT_VALUE"]["WATERMARK_TEXT_POSITION"],
						"USE_WATERMARK_FILE" => $iblockParams["PREVIEW_PICTURE"]["DEFAULT_VALUE"]["USE_WATERMARK_FILE"],
						"WATERMARK_FILE" => $iblockParams["PREVIEW_PICTURE"]["DEFAULT_VALUE"]["WATERMARK_FILE"],
						"WATERMARK_FILE_ALPHA" => $iblockParams["PREVIEW_PICTURE"]["DEFAULT_VALUE"]["WATERMARK_FILE_ALPHA"],
						"WATERMARK_FILE_POSITION" => $iblockParams["PREVIEW_PICTURE"]["DEFAULT_VALUE"]["WATERMARK_FILE_POSITION"],
					),
					"DETAIL_PICTURE" => Array(
						"USE_WATERMARK_TEXT" => $iblockParams["DETAIL_PICTURE"]["DEFAULT_VALUE"]["USE_WATERMARK_TEXT"],
						"WATERMARK_TEXT" => $iblockParams["DETAIL_PICTURE"]["DEFAULT_VALUE"]["WATERMARK_TEXT"],
						"WATERMARK_TEXT_FONT" => $iblockParams["DETAIL_PICTURE"]["DEFAULT_VALUE"]["WATERMARK_TEXT_FONT"],
						"WATERMARK_TEXT_COLOR" => $iblockParams["DETAIL_PICTURE"]["DEFAULT_VALUE"]["WATERMARK_TEXT_COLOR"],
						"WATERMARK_TEXT_SIZE" => $iblockParams["DETAIL_PICTURE"]["DEFAULT_VALUE"]["WATERMARK_TEXT_SIZE"],
						"WATERMARK_TEXT_POSITION" => $iblockParams["DETAIL_PICTURE"]["DEFAULT_VALUE"]["WATERMARK_TEXT_POSITION"],
						"USE_WATERMARK_FILE" => $iblockParams["DETAIL_PICTURE"]["DEFAULT_VALUE"]["USE_WATERMARK_FILE"],
						"WATERMARK_FILE" => $iblockParams["DETAIL_PICTURE"]["DEFAULT_VALUE"]["WATERMARK_FILE"],
						"WATERMARK_FILE_ALPHA" => $iblockParams["DETAIL_PICTURE"]["DEFAULT_VALUE"]["WATERMARK_FILE_ALPHA"],
						"WATERMARK_FILE_POSITION" => $iblockParams["DETAIL_PICTURE"]["DEFAULT_VALUE"]["WATERMARK_FILE_POSITION"],
					)
				);
			}
			if($params["IMPORT_OFFERS"] == "Y")
			{
				$offersIblockParams = CIBlock::GetFields($params["OFFERS_IBLOCK_ID"]);
				
				$iblockOfferParams = Array(
					"IBLOCK_SECTION" => $offersIblockParams["IBLOCK_SECTION"],
					"ACTIVE" => $offersIblockParams["ACTIVE"],
					"ACTIVE_FROM" => $offersIblockParams["ACTIVE_FROM"],
					"ACTIVE_TO" => $offersIblockParams["ACTIVE_TO"],
					"SORT" => $offersIblockParams["SORT"],
					"NAME" => $offersIblockParams["NAME"],
					"PREVIEW_PICTURE" => $offersIblockParams["PREVIEW_PICTURE"],
					"PREVIEW_TEXT_TYPE" => $offersIblockParams["PREVIEW_TEXT_TYPE"],
					"PREVIEW_TEXT" => $offersIblockParams["PREVIEW_TEXT"],
					"DETAIL_PICTURE" => $offersIblockParams["DETAIL_PICTURE"],
					"DETAIL_TEXT_TYPE" => $offersIblockParams["DETAIL_TEXT_TYPE"],
					"DETAIL_TEXT" => $offersIblockParams["DETAIL_TEXT"],
					"CODE" => $offersIblockParams["CODE"],
					"TAGS" => $offersIblockParams["TAGS"],
				);
			}
		}
		if($params["IMPORT_PRODUCTS"] == "Y" || $params["IMPORT_PRICES"] == "Y" || $params["IMPORT_STORE_AMOUNT"] == "Y")
		{
			CModule::IncludeModule("catalog");
		}
		if($params["IMPORT_HIGHLOAD_BLOCK_ENTITIES"] == "Y")
		{
			CModule::IncludeModule("highloadblock");
		}
		
		//if($params["IMPORT_ELEMENTS"] == "Y")
		if(
			$params["IMPORT_ELEMENTS"] == "Y" && 
			(
				//($params["ELEMENTS_ADD"] == "Y" || $params["ELEMENTS_UPDATE"] == "Y") || 
				$params["PRODUCTS_ADD"] == "Y" || 
				$params["PRODUCTS_UPDATE"] == "Y"
			)
		)
			$isImportElementProduct = true;
		else
			$isImportElementProduct = false;
		
		//if($params["IMPORT_OFFERS"] == "Y")
		if(
			$params["IMPORT_OFFERS"] == "Y" && 
			(
				//($params["OFFERS_ADD"] == "Y" || $params["OFFERS_UPDATE"] == "Y")
				$params["PRODUCTS_ADD"] == "Y" || 
				$params["PRODUCTS_UPDATE"] == "Y"
			)
		)
			$isImportOfferProduct = true;
		else
			$isImportOfferProduct = false;
		
		foreach($data as $item)
		{
			if(!is_array($item))
				continue;
			
			if($params["IMPORT_SECTIONS"] == "Y")
			{
				$sectionData = self::GetSectionArray($item, $rules, $params, $iblockSectionParams);
				
				if($sectionData && is_array($sectionData["FIELDS_PARENT"]))
				{
					if($params["SECTIONS_DEFAULT_SECTION_ID"]>0)
					{
						$sectionData["FIELDS_PARENT"][1]["SECTION_ID"] = $params["SECTIONS_DEFAULT_SECTION_ID"];
						$sectionData["FIELDS_PARENT"][1]["IBLOCK_SECTION_ID"] = $params["SECTIONS_DEFAULT_SECTION_ID"];
						$sectionData["SEARCH_PARENT"][1][] = "SECTION_ID";
					}
					$importParentSection = self::ImportSection($sectionData["FIELDS_PARENT"], $sectionData["SEARCH_PARENT"], $params, false); /*onlySearch why true?*/
				}
				
				if(is_array($importParentSection))
				{
					if($DEBUG_EVENTS && $DEBUG_IMPORT_SECTION)
					{
						$logFields["EVENT"] = "IMPORT_OBJECT_NEW";
						$logFields["MESSAGE"] = GetMessage("MESSAGE_IMPORT_OBJECT_NEW", Array("#OBJECT#" => "Parent Section"));
						$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importParentSection), $importParentSection)));
						$logData->Add($logFields);
					}
					
					self::GetImportResultCount($result, "SECTIONS", $importParentSection["EVENT"]);
				}
				
				if($importParentSection["ID"] > 0)
				{
					$sectionData["FIELDS"][1]["SECTION_ID"] = $importParentSection["ID"];
					$sectionData["FIELDS"][1]["IBLOCK_SECTION_ID"] = $importParentSection["ID"];
					$sectionData["SEARCH"][1][] = "SECTION_ID";
				}
				elseif($params["SECTIONS_DEFAULT_SECTION_ID"]>0)
				{
					$sectionData["FIELDS"][1]["SECTION_ID"] = $params["SECTIONS_DEFAULT_SECTION_ID"];
					$sectionData["FIELDS"][1]["IBLOCK_SECTION_ID"] = $params["SECTIONS_DEFAULT_SECTION_ID"];
					$sectionData["SEARCH"][1][] = "SECTION_ID";
				}
				
				if($sectionData && is_array($sectionData["FIELDS"]))
				{
					$importSection = self::ImportSection($sectionData["FIELDS"], $sectionData["SEARCH"], $params);
				}
				
				if(is_array($importSection))
				{
					if($DEBUG_EVENTS && $DEBUG_IMPORT_SECTION)
					{
						$logFields["EVENT"] = "IMPORT_OBJECT_NEW";
						$logFields["MESSAGE"] = GetMessage("MESSAGE_IMPORT_OBJECT_NEW", Array("#OBJECT#" => "Section"));
						$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importSection), $importSection)));
						$logData->Add($logFields);
					}
					
					self::GetImportResultCount($result, "SECTIONS", $importSection["EVENT"]);
				}
			}
			if($params["IMPORT_ELEMENTS"] == "Y")
			{
				$elementData = self::GetElementArray($item, $rules, $params, $importSection["ID"], $iblockElementParams);

				if($elementData)
				{
					$importElement = self::ImportElement($elementData, $params);
				}

				if(is_array($importElement))
				{
					if($DEBUG_EVENTS && $DEBUG_IMPORT_ELEMENTS)
					{
						$logFields["EVENT"] = "IMPORT_OBJECT_NEW";
						$logFields["MESSAGE"] = GetMessage("MESSAGE_IMPORT_OBJECT_NEW", Array("#OBJECT#" => "Element"));
						$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importElement), $importElement)));
						$logData->Add($logFields);
					}
					
					self::GetImportResultCount($result, "ELEMENTS", $importElement["EVENT"]);
				}
			}
			if($params["IMPORT_PROPERTIES"] == "Y" && $params["PROPERTIES_UPDATE"] == "Y" && $params["IMPORT_OFFERS"] != "Y")
			{
				$propertyData = self::GetPropertiesArray($item, $rules, $params, $iblockPropertiesParams, $params["IBLOCK_ID"], "IBLOCK_ELEMENT_PROPERTY", $importElement["EVENT"]);
				
				if($propertyData && $importElement["ID"]>0)
				{
					$importProperty = self::ImportProperties($propertyData, $params, $importElement["ID"], $params["IBLOCK_ID"]);
				}
				else
				{
					if($DEBUG_EVENTS && $DEBUG_IMPORT_PROPERTIES)
					{
						if(!$propertyData)
						{
							$logFields["EVENT"] = "IMPORT_PROPERTIES";
							$logFields["MESSAGE"] = GetMessage("ERROR_REQUIRED_FIELDS_NOT_FILLED", Array("#OBJECT#" => "Properties"));
							$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($item), $item)));
						}
						elseif(!$importElement["ID"]>0)
						{
							$logFields["EVENT"] = "IMPORT_PROPERTIES";
							$logFields["MESSAGE"] = GetMessage("ERROR_NO_ELEMENT_ID");
							$logFields["DATA"] = "";
						}
						$logData->Add($logFields);
					}
				}
				
				if(is_array($importProperty))
				{
					if($DEBUG_EVENTS && $DEBUG_IMPORT_PROPERTIES)
					{
						$logFields["EVENT"] = "IMPORT_OBJECT_NEW";
						$logFields["MESSAGE"] = GetMessage("MESSAGE_IMPORT_OBJECT_NEW", Array("#OBJECT#" => "Property"));
						$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importProperty), $importProperty)));
						$logData->Add($logFields);
					}
					
					self::GetImportResultCount($result, "PROPERTIES", $importProperty["EVENT"]);
				}
			}
			if($isImportElementProduct == true && $params["IMPORT_PRODUCTS"] == "Y")
			{
				if($importElement["ID"]>0)
				{
					$productData = self::GetProductArray($item, $rules, $params, $importElement["ID"], "CATALOG_PRODUCT_FIELD");
					
					if($productData)
					{
						$importProduct = self::ImportProduct($productData, $params, "PRODUCTS");
					}
					else
					{
						if($DEBUG_EVENTS && $DEBUG_IMPORT_PRODUCTS)
						{
							$logFields["EVENT"] = "IMPORT_PRODUCTS";
							$logFields["MESSAGE"] = GetMessage("ERROR_REQUIRED_FIELDS_NOT_FILLED", Array("#OBJECT#" => "Product"));
							$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($item), $item)));
							$logData->Add($logFields);
						}
					}
					
					if(is_array($importProduct))
					{
						if($DEBUG_EVENTS && $DEBUG_IMPORT_PRODUCTS)
						{
							$logFields["EVENT"] = "IMPORT_OBJECT_NEW";
							$logFields["MESSAGE"] = GetMessage("MESSAGE_IMPORT_OBJECT_NEW", Array("#OBJECT#" => "Product"));
							$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importProduct), $importProduct)));
							$logData->Add($logFields);
						}
						
						self::GetImportResultCount($result, "PRODUCTS", $importProduct["EVENT"]);
					}
				}
				else
				{
					if($DEBUG_EVENTS && $DEBUG_IMPORT_PRODUCTS)
					{
						$logFields["EVENT"] = "IMPORT_PRODUCTS";
						$logFields["MESSAGE"] = GetMessage("ERROR_NO_ELEMENT_ID_FOR_PRODUCT");
						$logFields["DATA"] = "";
						$logData->Add($logFields);
					}
				}
			}
			if($params["IMPORT_ELEMENTS"] == "Y" && $params["IMPORT_OFFERS"] == "Y")
			{
				if($importElement["ID"]>0)
				{
					$elementOfferData = self::GetElementOfferArray($item, $rules, $params, $importElement["ID"], $iblockOfferParams, $iblockPropertiesParams);
				
					if($elementOfferData)
					{
						$importElementOffer = self::ImportElementOffer($elementOfferData, $params);
					}
					else
					{
						if($DEBUG_EVENTS && $DEBUG_IMPORT_OFFERS)
						{
							$logFields["EVENT"] = "IMPORT_OFFERS";
							$logFields["MESSAGE"] = GetMessage("ERROR_REQUIRED_FIELDS_NOT_FILLED", Array("#OBJECT#" => "Offer"));
							$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($item), $item)));
							$logData->Add($logFields);
						}
					}
				}
				else
				{
					if($DEBUG_EVENTS && $DEBUG_IMPORT_OFFERS)
					{
						$logFields["EVENT"] = "IMPORT_OFFERS";
						$logFields["MESSAGE"] = GetMessage("ERROR_NO_ELEMENT_ID_FOR_OFFERS");
						$logFields["DATA"] = "";
						$logData->Add($logFields);
					}
				}
				
				if(is_array($importElementOffer))
				{
					if($DEBUG_EVENTS && $DEBUG_IMPORT_OFFERS)
					{
						$logFields["EVENT"] = "IMPORT_OBJECT_NEW";
						$logFields["MESSAGE"] = GetMessage("MESSAGE_IMPORT_OBJECT_NEW", Array("#OBJECT#" => "ElementOffer"));
						$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importElementOffer), $importElementOffer)));
						$logData->Add($logFields);
					}
					self::GetImportResultCount($result, "OFFERS", $importElementOffer["EVENT"]);
				}
			}
			if($params["IMPORT_PROPERTIES"] == "Y" && $params["PROPERTIES_UPDATE"] == "Y" && $params["IMPORT_OFFERS"] == "Y")
			{
				$propertyOfferData = self::GetPropertiesArray($item, $rules, $params, $iblockPropertiesParams, $params["OFFERS_IBLOCK_ID"], "IBLOCK_ELEMENT_OFFER_PROPERTY", $importElementOffer["EVENT"]);
				
				if($propertyOfferData && $importElementOffer["ID"]>0)
				{
					$importOfferProperty = self::ImportProperties($propertyOfferData, $params, $importElementOffer["ID"], $params["OFFERS_IBLOCK_ID"]);
				}
				else
				{
					if($DEBUG_EVENTS && $DEBUG_IMPORT_PROPERTIES)
					{
						if(!$propertyOfferData)
						{
							$logFields["EVENT"] = "IMPORT_PROPERTIES";
							$logFields["MESSAGE"] = GetMessage("ERROR_REQUIRED_FIELDS_NOT_FILLED", Array("#OBJECT#" => "Properties"));
							$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($item), $item)));
						}
						elseif(!$importElementOffer["ID"]>0)
						{
							$logFields["EVENT"] = "IMPORT_PROPERTIES";
							$logFields["MESSAGE"] = GetMessage("ERROR_NO_ELEMENT_ID");
							$logFields["DATA"] = "";
						}
						$logData->Add($logFields);
					}
				}
				
				if(is_array($importOfferProperty))
				{
					if($DEBUG_EVENTS && $DEBUG_IMPORT_PROPERTIES)
					{
						$logFields["EVENT"] = "IMPORT_OBJECT_NEW";
						$logFields["MESSAGE"] = GetMessage("MESSAGE_IMPORT_OBJECT_NEW", Array("#OBJECT#" => "Property"));
						$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importOfferProperty), $importOfferProperty)));
						$logData->Add($logFields);
					}
					
					self::GetImportResultCount($result, "PROPERTIES", $importOfferProperty["EVENT"]);
				}
			}
			if($isImportOfferProduct == true && $params["IMPORT_PRODUCTS"] == "Y")
			{
				
				if($importElementOffer["ID"]>0)
				{
					$productOfferData = self::GetProductArray($item, $rules, $params, $importElementOffer["ID"], "CATALOG_PRODUCT_OFFER_FIELD");
				}
				else
				{
					if($DEBUG_EVENTS && $DEBUG_IMPORT_OFFERS)
					{
						$logFields["EVENT"] = "IMPORT_OFFERS";
						$logFields["MESSAGE"] = GetMessage("ERROR_NO_OFFER_ID_FOR_PRODUCT");
						$logFields["DATA"] = "";
						$logData->Add($logFields);
					}
				}
				
				if($productOfferData)
				{
					$importProductOffer = self::ImportProduct($productOfferData, $params, "OFFERS");
				}
				else
				{
					if($DEBUG_EVENTS && $DEBUG_IMPORT_OFFERS)
					{
						$logFields["EVENT"] = "IMPORT_OFFERS";
						$logFields["MESSAGE"] = GetMessage("ERROR_REQUIRED_FIELDS_NOT_FILLED", Array("#OBJECT#" => "ProductOffer"));
						$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($item), $item)));
						$logData->Add($logFields);
					}
				}
				
				if(is_array($importProductOffer))
				{
					if($DEBUG_EVENTS && $DEBUG_IMPORT_OFFERS)
					{
						$logFields["EVENT"] = "IMPORT_OBJECT_NEW";
						$logFields["MESSAGE"] = GetMessage("MESSAGE_IMPORT_OBJECT_NEW", Array("#OBJECT#" => "ProductOffer"));
						$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importProductOffer), $importProductOffer)));
						$logData->Add($logFields);
					}
					
					self::GetImportResultCount($result, "PRODUCTS", $importProductOffer["EVENT"]);
				}
			}
			if($params["IMPORT_PRODUCTS"] == "Y" && $params["IMPORT_PRICES"] == "Y")
			{
				if($importElement["ID"]>0 || $importElementOffer["ID"]>0)
				{
					$basePrice = CCatalogGroup::GetBaseGroup();
					
					if($isImportElementProduct == true && $importElement["ID"] && !$importElementOffer["ID"])
					{
						$priceData = self::GetPriceArray($item, $rules, $params, $importElement["ID"], $basePrice["ID"]);
					}
					elseif($isImportOfferProduct == true && $importElementOffer["ID"])
					{
						$priceData = self::GetPriceArray($item, $rules, $params, $importElementOffer["ID"], $basePrice["ID"]);
					}
					
					if($priceData)
					{
						$importPrice = self::ImportPrice($priceData, $params);
					}
					else
					{
						if($DEBUG_EVENTS && $DEBUG_IMPORT_PRICES)
						{
							$logFields["EVENT"] = "IMPORT_PRICES";
							$logFields["MESSAGE"] = GetMessage("ERROR_REQUIRED_FIELDS_NOT_FILLED", Array("#OBJECT#" => "Price"));
							$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($item), $item)));
							$logData->Add($logFields);
						}
					}
					
					if(is_array($importPrice["LOGS"]))
					{
						foreach($importPrice["LOGS"] as $singlePrice)
						{
							if($DEBUG_EVENTS && $DEBUG_IMPORT_PRICES)
							{
								$logFields["EVENT"] = "IMPORT_OBJECT_NEW";
								$logFields["MESSAGE"] = GetMessage("MESSAGE_IMPORT_OBJECT_NEW", Array("#OBJECT#" => "Price"));
								$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($singlePrice), $singlePrice)));
								$logData->Add($logFields);
							}
						}
						
						self::GetImportResultCountPrice($result, "PRICES", $importPrice["SYSTEM"]["EVENT"]);
					}
				}
				else
				{
					if($DEBUG_EVENTS && $DEBUG_IMPORT_PRICES)
					{
						$logFields["EVENT"] = "IMPORT_PRICES";
						$logFields["MESSAGE"] = GetMessage("ERROR_NO_PRODUCT_ID_FOR_PRICE");
						$logFields["DATA"] = "";
						$logData->Add($logFields);
					}
				}
			}
			if($params["IMPORT_PRODUCTS"] == "Y" && $params["IMPORT_STORE_AMOUNT"] == "Y")
			{
				if($importElement["ID"]>0 || $importElementOffer["ID"]>0)
				{
					if($isImportElementProduct == true && $importElement["ID"] && !$importElementOffer["ID"])
					{
						$storeAmountData = self::GetStoreAmountArray($item, $rules, $params, $importElement["ID"]);
					}
					elseif($isImportOfferProduct == true && $importElementOffer["ID"])
					{
						$storeAmountData = self::GetStoreAmountArray($item, $rules, $params, $importElementOffer["ID"]);
					}
					
					if($storeAmountData)
					{
						$importStoreAmount = self::ImportStoreAmount($storeAmountData, $params);
					}
					else
					{
						if($DEBUG_EVENTS && $DEBUG_IMPORT_STORE_AMOUNT)
						{
							$logFields["EVENT"] = "IMPORT_STORE_AMOUNT";
							$logFields["MESSAGE"] = GetMessage("ERROR_REQUIRED_FIELDS_NOT_FILLED", Array("#OBJECT#" => "StoreAmount"));
							$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($item), $item)));
							$logData->Add($logFields);
						}
					}
					
					/*if(is_array($importStoreAmount))
					{
						if($DEBUG_EVENTS && $DEBUG_IMPORT_STORE_AMOUNT)
						{
							$logFields["EVENT"] = "IMPORT_OBJECT_NEW";
							$logFields["MESSAGE"] = GetMessage("MESSAGE_IMPORT_OBJECT_NEW", Array("#OBJECT#" => "StoreAmount"));
							$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importStoreAmount), $importStoreAmount)));
							$logData->Add($logFields);
						}
						
						self::GetImportResultCountPrice($result, "STORE_AMOUNTS", $importStoreAmount["EVENT"]);
					}*/
					
					if(is_array($importStoreAmount["LOGS"]))
					{
						foreach($importStoreAmount["LOGS"] as $singleStoreAmount)
						{
							if($DEBUG_EVENTS && $DEBUG_IMPORT_STORE_AMOUNT)
							{
								$logFields["EVENT"] = "IMPORT_OBJECT_NEW";
								$logFields["MESSAGE"] = GetMessage("MESSAGE_IMPORT_OBJECT_NEW", Array("#OBJECT#" => "StoreAmount"));
								$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($singleStoreAmount), $singleStoreAmount)));
								$logData->Add($logFields);
							}
						}
						
						self::GetImportResultCountPrice($result, "STORE_AMOUNTS", $importStoreAmount["SYSTEM"]["EVENT"]);
					}
				}
				else
				{
					if($DEBUG_EVENTS && $DEBUG_IMPORT_STORE_AMOUNT)
					{
						$logFields["EVENT"] = "IMPORT_STORE_AMOUNT";
						$logFields["MESSAGE"] = GetMessage("ERROR_NO_PRODUCT_ID_FOR_STORE_AMOUNT");
						$logFields["DATA"] = "";
						$logData->Add($logFields);
					}
				}
			}
			if($params["IMPORT_HIGHLOAD_BLOCK_ENTITIES"] == "Y")
			{
				$entityData = self::GetEntityArray($item, $rules, $params);
				
				if($entityData)
				{
					$importEntity = self::ImportEntity($entityData, $params);
				}
				
				if(is_array($importEntity))
				{
					if($DEBUG_EVENTS && $DEBUG_IMPORT_ENTITIES)
					{
						$logFields["EVENT"] = "IMPORT_OBJECT_NEW";
						$logFields["MESSAGE"] = GetMessage("MESSAGE_IMPORT_OBJECT_NEW", Array("#OBJECT#" => "Entity"));
						$logFields["DATA"] = base64_encode(serialize(array_merge(array_keys($importEntity), $importEntity)));
						$logData->Add($logFields);
					}
					
					self::GetImportResultCount($result, "ENTITIES", $importEntity["EVENT"]);
				}
			}
			unset($importParentSection);
			unset($importSection);
			unset($importElement);
			unset($importProperty);
			unset($importElementOffer);
			unset($importProduct);
			unset($importProductOffer);
			unset($importPrice);
			unset($importStoreAmount);
			
			unset($sectionData);
			unset($elementData);
			unset($propertyData);
			unset($elementOfferData);
			unset($productData);
			unset($productOfferData);
			unset($priceData);
			unset($storeAmountData);
			unset($entityData);
			
		}
		
		return $result;
	}
	
	private function GetPropertyMultiply($PROPERTY_ID)
	{
		$result = false;
		
		$res = CIBlockProperty::GetByID($PROPERTY_ID);
		if($arProperty = $res->GetNext())
		{
			if($arProperty['MULTIPLE'] == "Y")
				$result = true;
		}

		return $result;
	}
	
	private function DeleteOldFilesByProperty($ELEMENT_ID, $IBLOCK_ID, $PROPERTY_ID)
	{
		$res = CIBlockElement::SetPropertyValuesEx(
			$ELEMENT_ID, 
			$IBLOCK_ID, 
			Array(
				$PROPERTY_ID => array(
					"del" => "Y"
				)
			)
		);
		
		return $res;
	}
	
	private function AddWhatermarkToFiles(&$files = Array(), $multiple = true, $rule, $defaultParams, $curl = false)
	{
		if(!$multiple)
		{
			$temp[] = $files;
			$files = $temp;
		}
		
		if(count($files))
		{
			switch($rule)
			{
				case("P"):
					$params = $defaultParams["PREVIEW_PICTURE"];
					break;
				case("D"):
					$params = $defaultParams["DETAIL_PICTURE"];
					break;
			}
			foreach($files as $id => $file)
			{
				$is_image = CFile::IsImage($file["tmp_name"]);
				$resizeTo = $_SERVER['DOCUMENT_ROOT'].'/upload/tmp/'.$file["name"];
				if($is_image)
				{
					if($params["USE_WATERMARK_TEXT"] === "Y")
					{
						if($curl)
						{
							$arWatermark = array(
								"position" => $params["WATERMARK_FILE_POSITION"],
								'type' => 'text',
								"font" => $_SERVER["DOCUMENT_ROOT"].Rel2Abs("/", $params["WATERMARK_TEXT_FONT"]),
								"coefficient" => $params["WATERMARK_TEXT_SIZE"],
								"text" => $params["WATERMARK_TEXT"],
								"color" => $params["WATERMARK_TEXT_COLOR"],
							);
						}
						else
						{
							$filterPicture = CIBlock::FilterPicture($file["tmp_name"], array(
								"name" => "watermark",
								"position" => $params["WATERMARK_TEXT_POSITION"],
								"type" => "text",
								"coefficient" => $params["WATERMARK_TEXT_SIZE"],
								"text" => $params["WATERMARK_TEXT"],
								"font" => $_SERVER["DOCUMENT_ROOT"].Rel2Abs("/", $params["WATERMARK_TEXT_FONT"]),
								"color" => $params["WATERMARK_TEXT_COLOR"],
							));
						}
					}
					elseif($params["USE_WATERMARK_FILE"] === "Y")
					{
						if($curl)
						{
							$arWatermark = array(
								"position" => $params["WATERMARK_FILE_POSITION"],
								'type' => 'file',
								'size' => 'real',
								"alpha_level" => 100 - min(max($params["WATERMARK_FILE_ALPHA"], 0), 100),
								"file" => $_SERVER["DOCUMENT_ROOT"].Rel2Abs("/", $params["WATERMARK_FILE"]),
							);
						}
						else
						{
							$filterPicture = CIBlock::FilterPicture($file["tmp_name"], array(
								"name" => "watermark",
								"position" => $params["WATERMARK_FILE_POSITION"],
								"type" => "file",
								"size" => "real",
								"alpha_level" => 100 - min(max($params["WATERMARK_FILE_ALPHA"], 0), 100),
								"file" => $_SERVER["DOCUMENT_ROOT"].Rel2Abs("/", $params["WATERMARK_FILE"]),
							));
						}
					}
					
					if($curl)
					{
						$imageSize = getimagesize($file["tmp_name"]);
						$filterPicture = CFile::ResizeImageFile(
							$file["tmp_name"],
							$resizeTo,
							Array("width" => $imageSize[0], "height" => $imageSize[1]),
							BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
							$arWatermark,
							false,
							false
						);
						if($filterPicture)
						{
							$file = CFile::MakeFileArray($resizeTo);
							$files[$id] = $file;
						}
					}
				}
			}
		}
	}
	
	private function GetFileArrayByFileName($fileName = '', $fileDir = '', $multiply = false, $value = false, $useWhatermark = false, $whaterMarkParams = Array(), $DEBUG_THIS = false, &$params)
	{
		$fileFullDir = $_SERVER["DOCUMENT_ROOT"].$fileDir;
		$result = false;
		
		global $DEBUG_EVENTS, $DEBUG_PLAN_ID;
		if($DEBUG_EVENTS && $DEBUG_THIS)
		{
			$logFile = new CWebprostorImportLog;
			$logFileData = Array("PLAN_ID" => $DEBUG_PLAN_ID);
		}
		
		if(!is_array($fileName))
		{
			if($params["RAW_URL_DECODE"] != "N")
				$fileName = rawurldecode($fileName);
			$fileName = Array($fileName);
		}
		
		foreach($fileName as $fileSingleName)
		{
			$fileFullPath = $_SERVER["DOCUMENT_ROOT"].$fileDir.$fileSingleName;
			
			$fileSingleName = strval($fileSingleName);
			
			if($DEBUG_EVENTS && $DEBUG_THIS)
			{
				$logFileData["EVENT"] = "GET_FILE";
				$logFileData["MESSAGE"] = GetMessage("EVENT_GET_FILE");
				$logFileData["DATA"] = $fileFullPath;
				$logFile->Add($logFileData);
			}
			
			if(file_exists($fileFullPath))
			{
				$result[] = CFile::MakeFileArray($fileFullPath);
			}
			elseif(file_exists($fileFullDir) && strlen($fileDir)>0)
			{
			
				if($DEBUG_EVENTS && $DEBUG_THIS)
				{
					$logFileData["EVENT"] = "GET_FILE_SCAN";
					$logFileData["MESSAGE"] = GetMessage("EVENT_GET_FILE_SCAN");
					$logFileData["DATA"] = $fileSingleName;
					$logFile->Add($logFileData);
				}
				$files = scandir($fileFullDir, 0);
				
				foreach($files as $file)
				{
					if ($file == '..' || $file == '.')
						continue;
					$fileFounded = strpos($file, $fileSingleName);
					if(is_int($fileFounded))
					{
						if(is_file($fileFullDir.$file))
							$result[] = CFile::MakeFileArray($fileFullDir.$file);
						if(!$multiply)
							break 1;
					}
				}
			}
		}

		if($result && count($result))
		{
			if($DEBUG_EVENTS && $DEBUG_THIS)
			{
				$logFileData["MESSAGE"] = GetMessage("MESSAGE_FILE_OK");
				$logFileData["DATA"] = base64_encode(serialize(array_merge(array_keys($result[0]), $result[0])));
			}
			
			if($useWhatermark && $useWhatermark != "N")
				self::AddWhatermarkToFiles($result, true, $useWhatermark, $whaterMarkParams, false);
			
			if($multiply && count($result) > 1)
			{
				$newArray = Array();
				
				if($DEBUG_EVENTS && $DEBUG_THIS)
				{
					$logFileData["MESSAGE"] = GetMessage("MESSAGE_FILES_OK", Array("#COUNT#" => count($result)));
					$logFileData["DATA"] = '';
				}
				foreach($result as $key => $array)
				{
					$newArray[] = Array(
						"VALUE" => $array,
						"DESCRIPTION" => $array["name"],
					);
				}
				
				$result = $newArray;
			}
			elseif($multiply && is_array($value) && count($result) == 1)
			{
				$temp = $result[0];
				unset($result[0]);
				if(isset($value["VALUE"]["name"]))
				{
					$result["n0"] = $value;
					$result["n1"] = Array("VALUE" => $temp, "DESCRIPTION" => $temp["name"]);
				}
				elseif(!isset($value["name"]))
				{
					$result = $value;
					$result["n".count($value)] = Array("VALUE" => $temp, "DESCRIPTION" => $temp["name"]);
				}
				unset($temp);
			}
			else
			{
				if($multiply)
					$result = Array(
						"VALUE" => $result[0],
						"DESCRIPTION" => $result[0]["name"],
					);
				else
					$result = $result[0];
			}
			
			if($DEBUG_EVENTS && $DEBUG_THIS)
			{
				$logFileData["EVENT"] = "GET_FILE";
				$logFile->Add($logFileData);
			}
		}
		elseif($DEBUG_EVENTS && $DEBUG_THIS)
		{
			$logFileData["EVENT"] = "GET_FILE";
			$logFileData["MESSAGE"] = GetMessage("ERROR_GET_FILE");
			$logFileData["DATA"] = "";
			$logFile->Add($logFileData);
		}
		
		return $result;
	}
	
	private function GetFileArrayByUrl($url = '', $property = false, $value = false, $useWhatermark = false, $whaterMarkParams = Array(), &$params)
	{
		$result = false;
		
		if(!is_string($url))
			return $value?$value:$result;
		
		$url = ltrim($url);
		$url = rtrim($url);
		$url = str_replace(" ", "%20", $url);
		$url = htmlspecialchars_decode($url);
		
		global $DEBUG_EVENTS, $DEBUG_PLAN_ID, $DEBUG_URL, $CURL_TIMEOUT, $CURL_FOLLOWLOCATION;
		
		if($DEBUG_EVENTS && $DEBUG_URL)
		{
			$logUrl = new CWebprostorImportLog;
			$logUrlData = Array("PLAN_ID" => $DEBUG_PLAN_ID);
		}
		
		$urlF = filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED);
		
		if($urlF && $DEBUG_EVENTS && $DEBUG_URL)
		{
			$logUrlData["EVENT"] = "GET_URL_FILE";
			$logUrlData["MESSAGE"] = GetMessage("EVENT_GET_URL_FILE");
			$logUrlData["DATA"] = $urlF;
			$logUrl->Add($logUrlData);
		}
		
		if(!extension_loaded('curl'))
		{
			$errorArray = Array(
				"MESSAGE" => GetMessage("CURL_NOT_INCLUDED"),
				"TAG" => "CURL_NOT_INCLUDED",
				"MODULE_ID" => "WEBPROSTOR.IMPORT",
				"ENABLE_CLOSE" => "Y"
			);
			$notifyID = CAdminNotify::Add($errorArray);
		}
		
		if($params["RAW_URL_DECODE"] != "N")
			$urlF = rawurldecode($urlF);
		
		if($urlF && extension_loaded('curl'))
		{
			$urlParams = parse_url($urlF);
			
			$handle = curl_init();
			
			curl_setopt($handle, CURLOPT_URL, $urlF);
			curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, $CURL_TIMEOUT);
			if($CURL_FOLLOWLOCATION)
			{
				curl_setopt($handle, CURLOPT_FOLLOWLOCATION, true);
			}
			
			$response = curl_exec($handle);
			$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
			
			if($httpCode == 200 || ($httpCode == 226 && strtolower($urlParams["scheme"]) == 'ftp'))
			{
				$temp = CFile::MakeFileArray($urlF);
			}
			elseif($DEBUG_EVENTS && $DEBUG_URL)
			{
				$logUrlData["EVENT"] = "GET_URL_FILE";
				$logUrlData["MESSAGE"] = GetMessage("ERROR_GET_URL_FILE", Array("#ERROR_CODE#" => $httpCode));
				$logUrlData["DATA"] = "";
				$logUrl->Add($logUrlData);
			}
			
			curl_close($handle);
			
			if(is_array($temp))
			{
				if(GetFileType($temp["tmp_name"]) == "UNKNOWN")
				{
					switch($temp["type"])
					{
						case("image/gif"):
							$new_tmp_name_format = '.gif';
							break;
						case("image/jpeg"):
							$new_tmp_name_format = '.jpg';
							break;
						case("image/png"):
							$new_tmp_name_format = '.png';
							break;
					}
					if($new_tmp_name_format)
					{
						rename($temp["tmp_name"], $temp["tmp_name"].$new_tmp_name_format);
						$temp["tmp_name"] = $temp["tmp_name"].$new_tmp_name_format;
						$temp["name"] = $temp["name"].$new_tmp_name_format;
					}
					unset($new_tmp_name_format);
				}
				if($DEBUG_EVENTS && $DEBUG_URL)
				{
					$logUrlData["EVENT"] = "GET_URL_FILE";
					$logUrlData["MESSAGE"] = GetMessage("MESSAGE_URL_OK");
					$logUrlData["DATA"] = base64_encode(serialize(array_merge(array_keys($temp), $temp)));
					$logUrl->Add($logUrlData);
				}
				if($property)
				{
					if($useWhatermark && $useWhatermark != "N")
						self::AddWhatermarkToFiles($temp, false, $useWhatermark, $whaterMarkParams, true);
					
					if(is_array($value) && isset($value["VALUE"]["name"]))
					{
						$result["n0"] = $value;
						$result["n1"] = Array("VALUE" => $temp, "DESCRIPTION" => $temp["name"]);
					}
					elseif(is_array($value) && !isset($value["name"]))
					{
						$result = $value;
						$result["n".count($value)] = Array("VALUE" => $temp, "DESCRIPTION" => $temp["name"]);
					}
					else
					{
						if(is_array($temp[0]) && isset($temp[0]["name"]))
							$result = Array("VALUE" => $temp[0], "DESCRIPTION" => $temp[0]["name"]);
						else
							$result = Array("VALUE" => $temp, "DESCRIPTION" => $temp["name"]);
					}
				}
				else
				{
					$result = $temp;
				}
			}
		}
		if(!$result && $value)
			$result = $value;
		return $result;
	}
	
	private function ClearField($string)
	{
		$result = $string;
		
		if(is_string($result))
		{
			$result = trim($result, '""');
			$result = trim($result);
			$result = str_replace('""', '"', $result);
		}
		
		return $result;
	}
	
	private function ClearPrice($string)
	{
		$result = $string;
		
		$result = trim($result, '');
		$result = str_replace(' ', '', $result);
		$result = str_replace(',', '.', $result);
		
		return $result;
	}
	
	private function CompareCodeSort($a, $b)
	{
		if ($a == $b) {
			return 0;
		}
		return ($a < $b) ? -1 : 1;
	}
	
	private function CheckFields(&$fields, $params, &$additionalCode = false)
	{
		foreach($params as $code => $param)
		{
			if(
				(!isset($fields[$code]) || empty($fields[$code]))
				&& ($param["IS_REQUIRED"] == "Y" && is_string($param["DEFAULT_VALUE"]) && strlen($param["DEFAULT_VALUE"])>0)
			)
			{
				$fields[$code] = $param["DEFAULT_VALUE"];
			}
		}
		
		$previewImageParams = $params["PREVIEW_PICTURE"]["DEFAULT_VALUE"];
		/*if($previewImageParams["FROM_DETAIL"] == "Y" && !empty($fields["DETAIL_PICTURE"]) && !isset($fields["PREVIEW_PICTURE"]))
		{
			$fields["PREVIEW_PICTURE"] = $fields["DETAIL_PICTURE"];
		}*/
		
		$codeParams = $params["CODE"]["DEFAULT_VALUE"];
		if($codeParams["TRANSLITERATION"] == "Y")
		{
			if(is_array($additionalCode) && count($additionalCode))
			{
				uasort($additionalCode, 'self::CompareCodeSort');
				foreach($additionalCode as $value => $sort)
				{
					$translitName .= ' '.$value;
				}
				$translitName = trim($translitName);
			}
			elseif(!empty($fields["CODE"]))
				$translitName = $fields["CODE"];
			elseif(!empty($fields["NAME"]))
				$translitName = $fields["NAME"];
			
			if($translitName)
			{
				$translitParams = array(
					"max_len" => $codeParams["TRANS_LEN"],
					"change_case" => $codeParams["TRANS_CASE"],
					"replace_space" => $codeParams["TRANS_SPACE"],
					"replace_other" => $codeParams["TRANS_OTHER"],
				);
				$fields["CODE"] = Cutil::translit($translitName, LANGUAGE_ID, $translitParams);
			}
		}
	}
	
	private function CheckAttribute(&$rule, &$temp_field, &$field)
	{
		if(isset($rule["ENTITY_ATTRIBUTE"]))
		{
			if(is_array($temp_field) && isset($temp_field[$rule["ENTITY_ATTRIBUTE"]]))
			{
				$field = $temp_field[$rule["ENTITY_ATTRIBUTE"]];
			} 
			else
			{
				$field = $temp_field;
			}
		}
	}
	
	private function CheckArrayContinue(&$field)
	{		
		if(is_array($field))
			return true;
	}
	
	private function CheckRequired(&$rule, &$field)
	{		
		if($rule["IS_REQUIRED"] == "Y" && empty($field))
		{
			return false;
		}
	}
	
	private function CheckImageFileUrl(&$rule, &$field, &$params, &$resultFields)
	{
		global $DEBUG_IMAGES, $DEBUG_FILES;
		if(($rule["IS_IMAGE"] == "Y" || $rule["IS_FILE"] == "Y") && $rule["IS_URL"] != "Y")
		{
			if($rule["IS_IMAGE"] == "Y")
			{
				$fileArr = self::GetFileArrayByFileName($field, $params["PATH_TO_IMAGES"], false, false, false, Array(), $DEBUG_IMAGES, $params);
				if($fileArr)
					$resultFields = $fileArr;
			}
			elseif($rule["IS_FILE"] == "Y")
			{
				$fileArr = self::GetFileArrayByFileName($field, $params["PATH_TO_FILES"], false, false, false, Array(), $DEBUG_FILES, $params);
				if($fileArr)
					$resultFields = $fileArr;
			}
		}
		elseif($rule["IS_URL"] == "Y")
		{
			$fileArr = self::GetFileArrayByUrl($field, false, false, false, [], $params);
			if($fileArr)
				$resultFields = $fileArr;
		}
		else
			return false;
	}
	
	private function CheckImageFileUrlForProperty(&$rule, &$field, &$params, &$resultFields, &$elementPropertyImages, &$elementPropertyFiles, $checkRule = "IBLOCK_ELEMENT_OFFER_PROPERTY", $whaterMarkParams, $objectEvent)
	{
		global $DEBUG_IMAGES, $DEBUG_FILES;
			
		if(($rule["IS_IMAGE"] == "Y" || $rule["IS_FILE"] == "Y") && $rule["IS_URL"] != "Y")
		{
			if($rule["IS_IMAGE"] == "Y")
			{
				if(is_array($field) && count($field))
				{
					foreach($field as $value)
					{
						$fileArr = self::GetFileArrayByFileName($value, $params["PATH_TO_IMAGES"], self::GetPropertyMultiply($rule[$checkRule]), $fileArr, $params["PROPERTIES_WHATERMARK"], $whaterMarkParams, $DEBUG_IMAGES, $params);
					}
				}
				else
				{
					$fileArr = self::GetFileArrayByFileName($field, $params["PATH_TO_IMAGES"], self::GetPropertyMultiply($rule[$checkRule]), $resultFields[$rule[$checkRule]], $params["PROPERTIES_WHATERMARK"], $whaterMarkParams, $DEBUG_IMAGES, $params);
				}
				
				if($fileArr)
					$resultFields[$rule[$checkRule]] = $fileArr;
				
				if(self::DELETE_OLD_PROPERTY_FILE_VALUE)
					$elementPropertyImages[] = $rule[$checkRule];
			}
			elseif($rule["IS_FILE"] == "Y")
			{
				if(is_array($field) && count($field))
				{
					foreach($field as $value)
					{
						$fileArr = self::GetFileArrayByFileName($value, $params["PATH_TO_FILES"], self::GetPropertyMultiply($rule[$checkRule]), $params["PROPERTIES_WHATERMARK"], Array(), $DEBUG_FILES, $params);
					}
				}
				else
				{
					$fileArr = self::GetFileArrayByFileName($field, $params["PATH_TO_FILES"], self::GetPropertyMultiply($rule[$checkRule]), $params["PROPERTIES_WHATERMARK"], Array(), $DEBUG_FILES, $params);
				}
				
				if($fileArr)
					$resultFields[$rule[$checkRule]] = $fileArr;

				if(self::DELETE_OLD_PROPERTY_FILE_VALUE)
					$elementPropertyFiles[] = $rule[$checkRule];
			}
		}
		elseif($rule["IS_URL"] == "Y")
		{
			if(!($checkRule == "IBLOCK_ELEMENT_PROPERTY" && $objectEvent == "UPDATE" && $params["PROPERTIES_SKIP_DOWNLOAD_URL_UPDATE"] == "Y"))
			{
				if(is_array($field) && count($field))
				{
					foreach($field as $value)
					{
						$fileArr = self::GetFileArrayByUrl($value, true, $fileArr, $params["PROPERTIES_WHATERMARK"], $whaterMarkParams, $params);
					}
				}
				else
				{
					$fileArr = self::GetFileArrayByUrl($field, true, $resultFields[$rule[$checkRule]], $params["PROPERTIES_WHATERMARK"], $whaterMarkParams, $params);
				}
				
				if($fileArr)
					$resultFields[$rule[$checkRule]]  = $fileArr;
			}
		}
		else
			return false;
	}
	
	private function CheckMap(&$rule, &$field, &$resultFields)
	{
		if($rule["IBLOCK_ELEMENT_PROPERTY_M"] == "latitude" || $rule["IBLOCK_ELEMENT_PROPERTY_M"] == "longitude")
		{
			$resultFields[strtoupper($rule["IBLOCK_ELEMENT_PROPERTY_M"])] = $field;
			return true;
		}
		else
			return false;
	}
	
	private function GetSectionArray($item, &$rules, &$params, $defaultParams)
	{
		$sectionFields = Array();
		$sectionSearch = Array();
		
		foreach($item as $entity => $field)
		{
			$temp_field = $field;
			if(isset($rules[$entity]))
			{
				foreach($rules[$entity]["RULES"] as $rule)
				{
					self::CheckAttribute($rule, $temp_field, $field);
					
					CWebprostorImportProcessingSettingsTypes::ApplyProcessingRules($field, $rule["PROCESSING_TYPES"]);
					
					$CheckArrayContinue = self::CheckArrayContinue($field);
					if($CheckArrayContinue === true)
						continue;
					
					$CheckRequired = self::CheckRequired($rule, $field);
					if($CheckRequired === false)
						return false;
					
					if(
						!empty($rule["IBLOCK_SECTION_PARENT_FIELD"]) && 
						(
							empty($rule["IBLOCK_SECTION_DEPTH_LEVEL"]) || 
							$rule["IBLOCK_SECTION_DEPTH_LEVEL"] === "0"
						)
					)
					{
						if(!isset($rule["IBLOCK_SECTION_DEPTH_LEVEL"]) || $rule["IBLOCK_SECTION_DEPTH_LEVEL"] === "0")
						{
							$rule["IBLOCK_SECTION_DEPTH_LEVEL"] = 1;
						}
						if(($rule["IS_REQUIRED"] == "N" && !empty($field)) || ($rule["IS_REQUIRED"] == "Y" || !empty($field)))
						{
							
							$sectionParentFields[$rule["IBLOCK_SECTION_DEPTH_LEVEL"]][$rule["IBLOCK_SECTION_PARENT_FIELD"]] = self::ClearField($field);
							
							if($rule["USE_IN_SEARCH"] == "Y")
								$sectionParentSearch[$rule["IBLOCK_SECTION_DEPTH_LEVEL"]][] = $rule["IBLOCK_SECTION_PARENT_FIELD"];
							
							/*if($rule["USE_IN_CODE"] == "SECTION")
								$sectionParentCode[$rule["IBLOCK_SECTION_DEPTH_LEVEL"]][$field] = $rule["SORT"];*/
						}
					}
					elseif
					(
						!empty($rule["IBLOCK_SECTION_FIELD"]) && 
						(
							(
								!is_null($params["SECTIONS_MAX_DEPTH_LEVEL"]) && 
								$rule["IBLOCK_SECTION_DEPTH_LEVEL"] <= $params["SECTIONS_MAX_DEPTH_LEVEL"]
							) || 
							(
								is_null($params["SECTIONS_MAX_DEPTH_LEVEL"]) &&
								(
									empty($rule["IBLOCK_SECTION_DEPTH_LEVEL"]) || 
									$rule["IBLOCK_SECTION_DEPTH_LEVEL"] === "0"
								)
							)
						)
					)
					{
						if(!isset($rule["IBLOCK_SECTION_DEPTH_LEVEL"]) || $rule["IBLOCK_SECTION_DEPTH_LEVEL"] === "0")
						{
							$rule["IBLOCK_SECTION_DEPTH_LEVEL"] = 1;
						}
						if(($rule["IS_REQUIRED"] == "N" && !empty($field)) || ($rule["IS_REQUIRED"] == "Y" || !empty($field)))
						{
							
							$CheckImageFileUrl = self::CheckImageFileUrl($rule, $field, $params, $sectionFields[$rule["IBLOCK_SECTION_DEPTH_LEVEL"]][$rule["IBLOCK_SECTION_FIELD"]]);
							
							if($CheckImageFileUrl === false)
							{
								if(strpos($rule["IBLOCK_SECTION_FIELD"], "SECTION_META") !== false)
								{
									$sectionFields[$rule["IBLOCK_SECTION_DEPTH_LEVEL"]]["IPROPERTY_TEMPLATES"][$rule["IBLOCK_SECTION_FIELD"]] = self::ClearField($field);
								}
								$sectionFields[$rule["IBLOCK_SECTION_DEPTH_LEVEL"]][$rule["IBLOCK_SECTION_FIELD"]] = self::ClearField($field);
							}
							
							if($rule["USE_IN_SEARCH"] == "Y")
								$sectionSearch[$rule["IBLOCK_SECTION_DEPTH_LEVEL"]][] = $rule["IBLOCK_SECTION_FIELD"];
							
							if($rule["USE_IN_CODE"] == "SECTION")
								$sectionCode[$rule["IBLOCK_SECTION_DEPTH_LEVEL"]][$field] = $rule["SORT"];
						}
					}
				}
			}
		}
		
		$result = Array(
			"FIELDS" => $sectionFields,
			"SEARCH" => $sectionSearch,
			"ADDITIONAL_CODE" => $sectionCode,
			"FIELDS_PARENT" => $sectionParentFields,
			"SEARCH_PARENT" => $sectionParentSearch,
			//"ADDITIONAL_CODE_PARENT" => $sectionParentCode,
		);
		
		if(is_array($sectionFields) && count($sectionFields)>0)
		{
			asort($sectionFields);
			foreach($sectionFields as $dl => $section)
			{
				$addCode = [];
				for($i = $dl; $i > 0; $i--)
				{
					if(is_array($result["ADDITIONAL_CODE"][$i]))
						$addCode = array_merge($addCode, $result["ADDITIONAL_CODE"][$i]);
				}
				$result["FIELDS"][$dl]["IBLOCK_ID"] = $params["IBLOCK_ID"];
				$fieldsIsChecked = self::CheckFields($result["FIELDS"][$dl], $defaultParams, $addCode);
				unset($addCode);
			}
		}
		
		if(is_array($sectionParentFields) && count($sectionParentFields)>0)
		{
			asort($sectionParentFields);
			foreach($sectionParentFields as $pdl => $parentSection)
			{
				$result["FIELDS_PARENT"][$pdl]["IBLOCK_ID"] = $params["IBLOCK_ID"];
				//$fieldsIsChecked = self::CheckFields($result["FIELDS_PARENT"][$pdl], $defaultParams, $result["ADDITIONAL_CODE_PARENT"][$dl]);
				$fieldsIsChecked = self::CheckFields($result["FIELDS_PARENT"][$pdl], $defaultParams);
			}
		}
		
		return $result;
	}
	
	private function ImportSection($arFields = Array(), $arSearchBy = Array(), $params = Array(), $onlySearch = false)
	{
		
		global $DEBUG_PLAN_ID, $DEBUG_EVENTS, $DEBUG_IMPORT_SECTION;
		
		if(!is_array($arFields))
			return false;
		
		if($DEBUG_EVENTS && $DEBUG_IMPORT_SECTION)
		{
			$logSection = new CWebprostorImportLog;
			$logSectionData = Array("PLAN_ID" => $DEBUG_PLAN_ID);
		}
		
		$bs = new CIBlockSection;
		ksort($arFields);
		
		foreach($arFields as $DEPTH_LEVEL => $arSection)
		{
			$arFilter = Array();
			$arFilter['IBLOCK_ID'] = $arSection["IBLOCK_ID"];
			if($DEPTH_LEVEL > 1 && $PARENT_SECTION_ID)
			{
				$arFilter['SECTION_ID'] = $PARENT_SECTION_ID;
				$arSection['IBLOCK_SECTION_ID'] = $PARENT_SECTION_ID;
			}
			
			$arSelect = Array("IBLOCK_ID", "ID", "UF_*", "IBLOCK_SECTION_ID", "DEPTH_LEVEL");
			if(is_array($arSearchBy[$DEPTH_LEVEL]) && count($arSearchBy[$DEPTH_LEVEL])>0)
			{
				foreach($arSearchBy[$DEPTH_LEVEL] as $code)
				{
					$arFilter[$code] = $arSection[$code];
					if(!in_array($code, $arSelect))
						$arSelect[] = $code;
				}
			}
			if(
				is_array($arFilter) && 
				count($arFilter)>0 && 
				!(count($arFilter) == 1 && array_key_exists("IBLOCK_ID", $arFilter)) && 
				!(count($arFilter) == 2 && array_key_exists("IBLOCK_ID", $arFilter) && array_key_exists("SECTION_ID", $arFilter))
			)
			{
				if($DEBUG_EVENTS && $DEBUG_IMPORT_SECTION)
				{
					$logSectionData["EVENT"] = "IMPORT_ITEM_FILTER";
					$logSectionData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER", Array("#OBJECT#" => "Section"));
					$logSectionData["DATA"] = base64_encode(serialize(array_merge(array_keys($arFilter), $arFilter)));
					$logSection->Add($logSectionData);
				}
				
				$sectionsRes = CIBlockSection::GetList(Array("SORT" => "ASC"), $arFilter, false, $arSelect, Array("nPageSize"=>1));
				$findSection = false;
				while($findSection2 = $sectionsRes->GetNext(true, false))
				{
					$ID = $findSection2['ID'];
					$event = "SEARCH";
					$findSection = $findSection2;
				}
			}
			else
			{
				if($DEBUG_EVENTS && $DEBUG_IMPORT_SECTION)
				{
					$logSectionData["EVENT"] = "IMPORT_ITEM_FILTER_NO_SEARCH";
					$logSectionData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER_NO_SEARCH", Array("#OBJECT#" => "Section"));
					$logSection->Add($logSectionData);
				}
			}
			
			if($DEBUG_EVENTS && $DEBUG_IMPORT_SECTION)
			{
				if($ID > 0)
				{
					$logSectionData["EVENT"] = "IMPORT_ITEM_FILTER_RESUL";
					$logSectionData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER_RESULT", Array("#OBJECT#" => "Section"));
					$logSectionData["DATA"] = base64_encode(serialize(array_merge(array_keys($findSection), $findSection)));
					$logSection->Add($logSectionData);
				}
				elseif(!$ID && !$onlySearch)
				{
					$logSectionData["EVENT"] = "IMPORT_ITEM_FILTER_NO_RESULT";
					$logSectionData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER_NO_RESULT", Array("#OBJECT#" => "Section"));
					$logElementData["DATA"] = base64_encode(serialize(array_merge(array_keys($arSection), $arSection)));
					$logSection->Add($logSectionData);
				}
			}
			
			if(!$onlySearch)
			{
				if($ID > 0)
				{
					if($params["SECTIONS_UPDATE"] == "Y")
					{
						if(!isset($arSection["TIMESTAMP_X"]))
							$arSection["TIMESTAMP_X"] = date("d.m.Y H:i:s");
						$res = $bs->Update($ID, $arSection, true, true, ($params["RESIZE_IMAGE"] == "Y"?true:false));
						if($res)
							$event = "UPDATE";
					}
				}
				else
				{
					if($params["SECTIONS_ADD"] == "Y")
					{
						if($params["SECTIONS_DEFAULT_ACTIVE"] == "Y")
						{
							$arSection["ACTIVE"] = "Y";
						}
						else
						{
							$arSection["ACTIVE"] = "N";
						}
						
						$ID = $bs->Add($arSection, true, true, ($params["RESIZE_IMAGE"] == "Y"?true:false));
						$res = ($ID>0);
						if($res)
							$event = "ADD";
					}
				}
			}
			
			if($ID>0)
				$PARENT_SECTION_ID = $ID;
			else
				unset($PARENT_SECTION_ID);
			
			if(isset($res) && !$res)
			{
				$event = "ERROR";
				$error = $bs->LAST_ERROR;
			}
		
			$result = Array(
				"ID" => $ID,
				"EVENT" => $event,
				"ERROR" => $error,
			);
			
			unset($ID);
			unset($event);
			unset($error);
		}
		
		return $result;
	}
	
	private function GetElementArray($item, &$rules, &$params, $SECTION_ID = false, $defaultParams)
	{
		$elementFields = Array();
		$elementSearch = Array();
		$elementSearchProperty = Array();
		$elementSearchPropertyLinkFields = Array();
		$elementPropertyImages = Array();
		$elementPropertyFiles = Array();
		global $DEBUG_IMAGES, $DEBUG_FILES;
		
		foreach($item as $entity => $field)
		{
			$temp_field = $field;
			if(isset($rules[$entity]))
			{
				foreach($rules[$entity]["RULES"] as $rule)
				{
					
					self::CheckAttribute($rule, $temp_field, $field);
					
					CWebprostorImportProcessingSettingsTypes::ApplyProcessingRules($field, $rule["PROCESSING_TYPES"]);
					
					$CheckArrayContinue = self::CheckArrayContinue($field);
					if($CheckArrayContinue === true)
						continue;
					
					$CheckRequired = self::CheckRequired($rule, $field);
					if($CheckRequired === false)
						return false;
					
					if(!empty($rule["IBLOCK_ELEMENT_FIELD"]))
					{
						if($rule["IS_REQUIRED"] == "N" || ($rule["IS_REQUIRED"] == "Y" || !empty($field)))
						{
							
							$CheckImageFileUrl = self::CheckImageFileUrl($rule, $field, $params, $elementFields[$rule["IBLOCK_ELEMENT_FIELD"]]);
							
							if($CheckImageFileUrl === false)
							{
								if(strpos($rule["IBLOCK_ELEMENT_FIELD"], "ELEMENT_META") !== false)
								{
									$elementFields["IPROPERTY_TEMPLATES"][$rule["IBLOCK_ELEMENT_FIELD"]] = self::ClearField($field);
								}
								$elementFields[$rule["IBLOCK_ELEMENT_FIELD"]] = self::ClearField($field);
							}
							
							if($rule["USE_IN_SEARCH"] == "Y")
								$elementSearch[] = $rule["IBLOCK_ELEMENT_FIELD"];
						}
					}
					if(!empty($rule["IBLOCK_ELEMENT_PROPERTY"]))
					{
						if($rule["IS_REQUIRED"] == "N" || ($rule["IS_REQUIRED"] == "Y" || !empty($field)))
						{
							
							$elementFields["PROPERTY_VALUES"][$rule["IBLOCK_ELEMENT_PROPERTY"]] = self::ClearField($field);
							
							if($rule["USE_IN_SEARCH"] == "Y")
							{
								$elementSearchProperty[] = $rule["IBLOCK_ELEMENT_PROPERTY"];
								if($rule["IBLOCK_ELEMENT_PROPERTY_E"] != "")
								{
									$elementSearchPropertyLinkFields[$rule["IBLOCK_ELEMENT_PROPERTY"]] = $rule["IBLOCK_ELEMENT_PROPERTY_E"];
								}
								/*elseif($rule["IBLOCK_ELEMENT_PROPERTY_G"] != "")
								{
									$elementSearchPropertyLinkFields[$rule["IBLOCK_ELEMENT_PROPERTY"]] = $rule["IBLOCK_ELEMENT_PROPERTY_G"];
								}*/
							}
						}
					}
					if(($rule["IS_REQUIRED"] == "N" || ($rule["IS_REQUIRED"] == "Y" || !empty($field))) && $rule["USE_IN_CODE"] == "ELEMENT")
					{
						$elementCode[$field] = $rule["SORT"];
					}
				}
			}
		}
		
		if(is_array($elementFields) && count($elementFields)>0)
		{
			$elementFields["IBLOCK_ID"] = $params["IBLOCK_ID"];
		}
		if($SECTION_ID>0)
		{
			$elementFields["IBLOCK_SECTION_ID"] = $SECTION_ID;
		}
		
		$result = Array(
			"FIELDS" => $elementFields,
			"ADDITIONAL_CODE" => $elementCode,
			"SEARCH" => $elementSearch,
			"SEARCH_PROPERTY" => $elementSearchProperty,
			"SEARCH_PROPERTY_LINK_FIELDS" => $elementSearchPropertyLinkFields,
			"DELETE_PROPERTY_IMAGES" => $elementPropertyImages,
			"DELETE_PROPERTY_FILES" => $elementPropertyFiles,
		);
		
		$fieldsIsChecked = self::CheckFields($result["FIELDS"], $defaultParams, $result["ADDITIONAL_CODE"]);
		
		return $result;
	}
	
	private function GetElementOfferArray($item, &$rules, &$params, $ELEMENT_ID = false, $defaultParams, $whaterMarkParams)
	{
		$elementFields = Array();
		if($params["PROPERTIES_SET_DEFAULT_VALUES"] == "Y" && CModule::IncludeModule("webprostor.core"))
			$elementFields["PROPERTY_VALUES"] = CWebprostorCoreIblock::GetDefaultProperties($params["OFFERS_IBLOCK_ID"], ["USE_LIST_VALUE" => "Y", "USE_HTML_TEXT" => "Y"]);
		$elementSearch = Array();
		$elementSearchProperty = Array();
		$elementPropertyImages = Array();
		$elementPropertyFiles = Array();
		global $DEBUG_IMAGES, $DEBUG_FILES;
		
		foreach($item as $entity => $field)
		{
			$temp_field = $field;
			if(isset($rules[$entity]))
			{
				foreach($rules[$entity]["RULES"] as $rule)
				{
					
					self::CheckAttribute($rule, $temp_field, $field);
					
					CWebprostorImportProcessingSettingsTypes::ApplyProcessingRules($field, $rule["PROCESSING_TYPES"]);
					
					$CheckRequired = self::CheckRequired($rule, $field);
					if($CheckRequired === false)
						return false;
					
					if(!empty($rule["IBLOCK_ELEMENT_OFFER_FIELD"]))
					{
					
						$CheckArrayContinue = self::CheckArrayContinue($field);
						if($CheckArrayContinue === true)
							continue;
						
						if($rule["IS_REQUIRED"] == "N" || ($rule["IS_REQUIRED"] == "Y" || !empty($field)))
						{
							
							$CheckImageFileUrl = self::CheckImageFileUrl($rule, $field, $params, $elementFields[$rule["IBLOCK_ELEMENT_OFFER_FIELD"]]);
							
							if($CheckImageFileUrl === false)
							{
								$elementFields[$rule["IBLOCK_ELEMENT_OFFER_FIELD"]] = self::ClearField($field);
							}
							
							if($rule["USE_IN_SEARCH"] == "Y")
								$elementSearch[] = $rule["IBLOCK_ELEMENT_OFFER_FIELD"];
						}
					}
					
					if(!empty($rule["IBLOCK_ELEMENT_OFFER_PROPERTY"]))
					{
						
						if($rule["IS_REQUIRED"] == "N" || ($rule["IS_REQUIRED"] == "Y" || !empty($field)))
						{
							
							$CheckMap = self::CheckMap($rule, $field, $elementFields["PROPERTY_VALUES"][$rule["IBLOCK_ELEMENT_OFFER_PROPERTY"]]);
							
							$CheckImageFileUrlForProperty = self::CheckImageFileUrlForProperty($rule, $field, $params, $elementFields["PROPERTY_VALUES"], $elementPropertyImages, $elementPropertyFiles, "IBLOCK_ELEMENT_OFFER_PROPERTY", $whaterMarkParams, '');
							
							if($CheckMap === false && $CheckImageFileUrlForProperty === false)
							{
								self::LinkPropertyValue($rule["IBLOCK_ELEMENT_OFFER_PROPERTY"], $field, $ID, $params["OFFERS_IBLOCK_ID"], $params, $data, $elementFields["PROPERTY_VALUES"][$rule["IBLOCK_ELEMENT_OFFER_PROPERTY"]]);
							}
							
							if($rule["USE_IN_SEARCH"] == "Y")
								$elementSearchProperty[] = $rule["IBLOCK_ELEMENT_OFFER_PROPERTY"];
						}
					}
				}
			}
		}
		
		if(count($elementFields)>0)
		{
			$elementFields["IBLOCK_ID"] = $params["OFFERS_IBLOCK_ID"];
			
			if($ELEMENT_ID>0)
			{
				$elementFields["PROPERTY_VALUES"][$params["OFFERS_SKU_PROPERTY_ID"]] = $ELEMENT_ID;
				$elementSearchProperty[] = $params["OFFERS_SKU_PROPERTY_ID"];
				
				if($params["OFFERS_SET_NAME_FROM_ELEMENT"] == "Y")
				{
					$elementFields["NAME"] = self::GetElementNameByID($ELEMENT_ID);
				}
			}
		}
		
		$result = Array(
			"FIELDS" => $elementFields,
			"SEARCH" => $elementSearch,
			"SEARCH_PROPERTY" => $elementSearchProperty,
			"DELETE_PROPERTY_IMAGES" => $elementPropertyImages,
			"DELETE_PROPERTY_FILES" => $elementPropertyFiles,
		);
		
		$fieldsIsChecked = self::CheckFields($result["FIELDS"], $defaultParams);
		
		return $result;
	}
	
	private function ImportElement($data, $params = Array())
	{
		global $DEBUG_PLAN_ID, $DEBUG_EVENTS, $DEBUG_IMPORT_ELEMENTS;

		if($DEBUG_EVENTS && $DEBUG_IMPORT_ELEMENTS)
		{
			$logElement = new CWebprostorImportLog;
			$logElementData = Array("PLAN_ID" => $DEBUG_PLAN_ID);
		}
		
		$el = new CIBlockElement;
		$arFields = $data["FIELDS"];
		$arSearchBy = $data["SEARCH"];
		$arSearchByProperty = $data["SEARCH_PROPERTY"];
		$arDeleteValueProperty = array_merge($data["DELETE_PROPERTY_IMAGES"], $data["DELETE_PROPERTY_FILES"]);
		
		if($arFields["IBLOCK_ID"]>0)
			$arFilter = Array('IBLOCK_ID'=>$arFields["IBLOCK_ID"]);
		$arSelect = Array("IBLOCK_ID", "ID");
		/*if(isset($arFields["IBLOCK_SECTION_ID"]))
		{
			$arFilter["SECTION_ID"] = $arFields["IBLOCK_SECTION_ID"];
			$arSelect[] = "IBLOCK_SECTION_ID";
		}*/
		if($params["ELEMENTS_DEFAULT_SECTION_ID"]>0)
		{
			$arFilter['SECTION_ID'] = $params["ELEMENTS_DEFAULT_SECTION_ID"];
			$arFields['IBLOCK_SECTION_ID'] = $params["ELEMENTS_DEFAULT_SECTION_ID"];
			$arSelect[] = "IBLOCK_SECTION_ID";
		}
		foreach($arSearchBy as $code)
		{
			//if($arFields["PROPERTY_VALUES"][$code] != "")
			if($arFields[$code] != "")
			{
				$arFilter[$code] = $arFields[$code];
				if(!in_array($code, $arSelect))
					$arSelect[] = $code;
			}
		}
		foreach($arSearchByProperty as $code)
		{
			$propRes = CIBlockProperty::GetById($code);
			$propFields = $propRes->Fetch();
			if($propFields["PROPERTY_TYPE"] == "E" && isset($data["SEARCH_PROPERTY_LINK_FIELDS"][$code]))
				$PROPERTY_CODE = "PROPERTY_".$code.".".$data["SEARCH_PROPERTY_LINK_FIELDS"][$code];
			elseif($propFields["PROPERTY_TYPE"] == "G" && isset($data["SEARCH_PROPERTY_LINK_FIELDS"][$code]))
				$PROPERTY_CODE = "PROPERTY_".$code.".".$data["SEARCH_PROPERTY_LINK_FIELDS"][$code];
			else
				$PROPERTY_CODE = "PROPERTY_".$code;
			
			if($arFields["PROPERTY_VALUES"][$code] != "")
			{
				$arFilter[$PROPERTY_CODE] = $arFields["PROPERTY_VALUES"][$code];
				if(!in_array($code, $arSelect))
					$arSelect[] = $PROPERTY_CODE;
			}
		}
		
		if($params["ELEMENTS_2_STEP_SEARCH"] == "Y")
		{
			unset($arFilter['SECTION_ID']);
			$tempSectionID = $arFields['IBLOCK_SECTION_ID'];
			unset($arFields['IBLOCK_SECTION_ID']);
		}
		
		if(
			is_array($arFilter) && 
			count($arFilter)>0 && 
			!(count($arFilter) == 1 && array_key_exists("IBLOCK_ID", $arFilter))
		)
		{
			if($DEBUG_EVENTS && $DEBUG_IMPORT_ELEMENTS)
			{
				$logElementData["EVENT"] = "IMPORT_ITEM_FILTER";
				$logElementData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER", Array("#OBJECT#" => "Element"));
				$logElementData["DATA"] = base64_encode(serialize(array_merge(array_keys($arFilter), $arFilter)));
				$logElement->Add($logElementData);
			}
			
			$elementRes = $el->GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
			$findElement = false;
			while($findElement2 = $elementRes->GetNext(true, false))
			{
				$ID = $findElement2['ID'];
				$event = "SEARCH";
				$findElement = $findElement2;
			}
		}
		else
		{
			if($DEBUG_EVENTS && $DEBUG_IMPORT_ELEMENTS)
			{
				$logElementData["EVENT"] = "IMPORT_ITEM_FILTER_NO_SEARCH";
				$logElementData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER_NO_SEARCH", Array("#OBJECT#" => "Element"));
				$logElement->Add($logElementData);
			}
		}
		
		unset($arFields["PROPERTY_VALUES"]);
		
		if($ID > 0)
		{
			if($params["ELEMENTS_UPDATE"] == "Y")
			{
				if($DEBUG_EVENTS && $DEBUG_IMPORT_ELEMENTS)
				{
					$logElementData["EVENT"] = "IMPORT_ITEM_FILTER_RESULT";
					$logElementData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER_RESULT", Array("#OBJECT#" => "Element"));
					$logElementData["DATA"] = base64_encode(serialize(array_merge(array_keys($findElement), $findElement)));
					$logElement->Add($logElementData);
				}
				
				$res = $el->Update($ID, $arFields, false, false, ($params["RESIZE_IMAGE"] == "Y"?true:false));
				if($res)
					$event = "UPDATE";
			}
		}
		else
		{
		
			if($params["ELEMENTS_2_STEP_SEARCH"] == "Y" && isset($tempSectionID))
			{
				$arFields['IBLOCK_SECTION_ID'] = $tempSectionID;
			}
			if($params["ELEMENTS_ADD"] == "Y" && is_array($arFields) && count($arFields)>0)
			{
				if($DEBUG_EVENTS && $DEBUG_IMPORT_ELEMENTS)
				{
					$logElementData["EVENT"] = "IMPORT_ITEM_FILTER_NO_RESULT";
					$logElementData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER_NO_RESULT", Array("#OBJECT#" => "Element"));
					$logElementData["DATA"] = base64_encode(serialize(array_merge(array_keys($arFields), $arFields)));
					$logElement->Add($logElementData);
				}
				
				if($params["ELEMENTS_DEFAULT_ACTIVE"] == "Y")
					$arFields["ACTIVE"] = "Y";
				else
					$arFields["ACTIVE"] = "N";
				
				$ID = $el->Add($arFields, false, false, ($params["RESIZE_IMAGE"] == "Y"?true:false));
				$res = ($ID>0);
				if($res)
					$event = "ADD";
			}
		}
		unset($tempSectionID);
		
		if(isset($res) && !$res)
		{
			$event = "ERROR";
			$error = $el->LAST_ERROR;
			
			if($DEBUG_EVENTS && $DEBUG_IMPORT_ELEMENTS)
			{
				$logElementData["EVENT"] = "ADD_UPDATE_ITEM";
				$logElementData["MESSAGE"] = GetMessage("ERROR_CANNOT_ADD_UPDATE_ITEM");
				$logElementData["DATA"] = $error;
				$logElement->Add($logElementData);
			}
		}
		
		if(!is_null($ID) || !is_null($event) || !is_null($error))
		{
			$result = Array(
				"ID" => $ID,
				"EVENT" => $event,
				"ERROR" => $error,
			);
		}
		
		return $result;
	}
	
	private function ImportElementOffer($data, $params = Array())
	{
		global $DEBUG_PLAN_ID, $DEBUG_EVENTS, $DEBUG_IMPORT_ELEMENTS;
		
		if($DEBUG_EVENTS && $DEBUG_IMPORT_ELEMENTS)
		{
			$logElement = new CWebprostorImportLog;
			$logElementData = Array("PLAN_ID" => $DEBUG_PLAN_ID);
		}
		
		$el = new CIBlockElement;
		$arFields = $data["FIELDS"];
		$arSearchBy = $data["SEARCH"];
		$arSearchByProperty = $data["SEARCH_PROPERTY"];
		$arDeleteValueProperty = array_merge($data["DELETE_PROPERTY_IMAGES"], $data["DELETE_PROPERTY_FILES"]);
		
		$arFilter = Array('IBLOCK_ID'=>$arFields["IBLOCK_ID"]);
		$arSelect = Array("IBLOCK_ID", "ID");
		foreach($arSearchBy as $code)
		{
			if($arFields[$code] != "")
			{
				$arFilter[$code] = $arFields[$code];
				if(!in_array($code, $arSelect))
					$arSelect[] = $code;
			}
		}
		foreach($arSearchByProperty as $code)
		{
			if($arFields["PROPERTY_VALUES"][$code] != "")
			{
				$arFilter["PROPERTY_".$code] = $arFields["PROPERTY_VALUES"][$code];
				if(!in_array($code, $arSelect))
					$arSelect[] = "PROPERTY_".$code;
			}
		}
		
		if(
			is_array($arFilter) && 
			count($arFilter)>0 && 
			!(count($arFilter) == 1 && array_key_exists("IBLOCK_ID", $arFilter))
		)
		{
			if($DEBUG_EVENTS && $DEBUG_IMPORT_ELEMENTS)
			{
				$logElementData["EVENT"] = "IMPORT_ITEM_FILTER";
				$logElementData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER", Array("#OBJECT#" => "ElementOffer"));
				$logElementData["DATA"] = base64_encode(serialize(array_merge(array_keys($arFilter), $arFilter)));
				$logElement->Add($logElementData);
			}
			
			$elementRes = $el->GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
			$findElement = false;
			while($findElement2 = $elementRes->GetNext(true, false))
			{
				$ID = $findElement2['ID'];
				$event = "SEARCH";
				$findElement = $findElement2;
			}
		}
		else
		{
			if($DEBUG_EVENTS && $DEBUG_IMPORT_ELEMENTS)
			{
				$logElementData["EVENT"] = "IMPORT_ITEM_FILTER_NO_SEARCH";
				$logElementData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER_NO_SEARCH", Array("#OBJECT#" => "ElementOffer"));
				$logElement->Add($logElementData);
			}
		}
		
		if($DEBUG_EVENTS && $DEBUG_IMPORT_ELEMENTS)
		{
			if($ID > 0)
			{
				$logElementData["EVENT"] = "IMPORT_ITEM_FILTER_RESULT";
				$logElementData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER_RESULT", Array("#OBJECT#" => "ElementOffer"));
				$logElementData["DATA"] = base64_encode(serialize(array_merge(array_keys($findElement), $findElement)));
				$logElement->Add($logElementData);
			}
			elseif(!$ID)
			{
				$logElementData["EVENT"] = "IMPORT_ITEM_FILTER_NO_RESULT";
				$logElementData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER_NO_RESULT", Array("#OBJECT#" => "ElementOffer"));
				$logElementData["DATA"] = base64_encode(serialize(array_merge(array_keys($arFields), $arFields)));
				$logElement->Add($logElementData);
			}
		}
		
		if($ID > 0)
		{
			if($params["OFFERS_UPDATE"] == "Y")
			{
				unset($arFields["PROPERTY_VALUES"]);
				
				$res = $el->Update($ID, $arFields, false, false, ($params["RESIZE_IMAGE"] == "Y"?true:false));
				if($res)
					$event = "UPDATE";
			}
		}
		else
		{
			if($params["OFFERS_ADD"] == "Y")
			{
				
				$ID = $el->Add($arFields, false, false, ($params["RESIZE_IMAGE"] == "Y")?true:false);
				$res = ($ID>0);
				if($res)
					$event = "ADD";
			}
		}
		
		if(isset($res) && !$res)
		{
			$event = "ERROR";
			$error = $el->LAST_ERROR;
			
			if($DEBUG_EVENTS && $DEBUG_IMPORT_ELEMENTS)
			{
				$logElementData["EVENT"] = "ADD_UPDATE_ITEM";
				$logElementData["MESSAGE"] = GetMessage("ERROR_CANNOT_ADD_UPDATE_ITEM");
				$logElementData["DATA"] = $error;
				$logElement->Add($logElementData);
			}
		}
		
		$result = Array(
			"ID" => $ID,
			"EVENT" => $event,
			"ERROR" => $error,
		);
		
		return $result;
	}
	
	private function GetPropertiesArray($item, &$rules, &$params, $whaterMarkParams, $IBLOCK_ID = false, $CONNECTION_TYPE = "IBLOCK_ELEMENT_PROPERTY", $objectEvent = '')
	{
		if($params["PROPERTIES_SET_DEFAULT_VALUES"] == "Y" && CModule::IncludeModule("webprostor.core"))
			$elementProperties = CWebprostorCoreIblock::GetDefaultProperties($IBLOCK_ID, ["USE_LIST_VALUE" => "Y", "SKIP_HTML" => "Y", "USE_HTML_TEXT" => "Y"]);
		else
			$elementProperties = Array();
		$elementSearchProperty = Array();
		$elementSearchPropertyE = Array();
		$elementSearchPropertyG = Array();
		$elementPropertyImages = Array();
		$elementPropertyFiles = Array();
		
		global $DEBUG_IMAGES, $DEBUG_FILES;

		foreach($item as $entity => $field)
		{
			$temp_field = $field;
			if(isset($rules[$entity]))
			{
				foreach($rules[$entity]["RULES"] as $rule)
				{
					self::CheckAttribute($rule, $temp_field, $field);
					
					CWebprostorImportProcessingSettingsTypes::ApplyProcessingRules($field, $rule["PROCESSING_TYPES"]);
					
					$CheckRequired = self::CheckRequired($rule, $field);
					if($CheckRequired === false)
						return false;
					
					if(!empty($rule[$CONNECTION_TYPE]))
					{
						
						if($rule["IS_REQUIRED"] == "N" || ($rule["IS_REQUIRED"] == "Y" && !empty($field)))
						{
							
							$CheckMap = self::CheckMap($rule, $field, $elementProperties[$rule[$CONNECTION_TYPE]]);
							
							$CheckImageFileUrlForProperty = self::CheckImageFileUrlForProperty($rule, $field, $params, $elementProperties, $elementPropertyImages, $elementPropertyFiles, $CONNECTION_TYPE, $whaterMarkParams, $objectEvent);
							
							if($CheckMap === false && $CheckImageFileUrlForProperty === false)
							{
								$elementProperties[$rule[$CONNECTION_TYPE]] = self::ClearField($field);
							}
							
							if($rule["USE_IN_SEARCH"] == "Y")
								$elementSearchProperty[] = $rule[$CONNECTION_TYPE];
							
							if($rule["IBLOCK_ELEMENT_PROPERTY_E"])
								$elementSearchPropertyE = $rule["IBLOCK_ELEMENT_PROPERTY_E"];
							
							if($rule["IBLOCK_ELEMENT_PROPERTY_G"])
								$elementSearchPropertyG = $rule["IBLOCK_ELEMENT_PROPERTY_G"];
						}
					}
				}
			}
		}
		if(isset($params["XML_PARSE_PARAMS_TO_PROPERTIES"]) && $params["XML_PARSE_PARAMS_TO_PROPERTIES"] == "Y" && is_array($item["param_array"]) && count($item["param_array"])>0)
		{
			$presetProperties = self::PresetPropertiesByParam($item["param_array"], $params, $IBLOCK_ID);
			
			if(is_array($presetProperties) && count($presetProperties)>0)
			{
				$elementProperties = array_replace_recursive($presetProperties, $elementProperties);
			}
		}
		
		$result = Array(
			"PROPERTIES" => $elementProperties,
			"SEARCH_PROPERTY" => $elementSearchProperty,
			"SEARCH_PROPERTY_E" => $elementSearchPropertyE,
			"SEARCH_PROPERTY_G" => $elementSearchPropertyG,
			"DELETE_PROPERTY_IMAGES" => $elementPropertyImages,
			"DELETE_PROPERTY_FILES" => $elementPropertyFiles,
		);
		
		return $result;
	}
	
	private function PresetPropertiesByParam($data, &$params, $IBLOCK_ID = false)
	{
		$result = array();
		foreach($data as $item)
		{
			$PROPERTY_ID = false;
			
			$propertySearchFilter = Array("IBLOCK_ID"=>$IBLOCK_ID, "NAME" => $item["NAME"]);
			$properties = CIBlockProperty::GetList(Array("sort"=>"asc"), $propertySearchFilter);
			while ($prop_fields = $properties->GetNext())
			{
				$PROPERTY_ID = $prop_fields["ID"];
			}
			
			if(!$PROPERTY_ID && $params["XML_ADD_PROPERTIES_FOR_PARAMS"] == "Y")
			{
				$transParams = Array(
					"replace_space" => "_",
					"replace_other" => ""
				);
				$CODE = Cutil::translit($item["NAME"], LANGUAGE_ID, $transParams);
				$CODE = strtoupper($CODE);
				$CODE = trim($CODE, "_");
				
				$arFields = Array(
					"NAME" => $item["NAME"],
					"ACTIVE" => "Y",
					"CODE" => $CODE,
					"PROPERTY_TYPE" => "S",
					"IBLOCK_ID" => $IBLOCK_ID,
					"WITH_DESCRIPTION" => $item["UNIT"]!=""?"Y":"N",
				);
				
				$ibp = new CIBlockProperty;
				$PROPERTY_ID = $ibp->Add($arFields);
			}
			
			if($PROPERTY_ID)
			{
				if($item["UNIT"] != "")
				{
					$result[$PROPERTY_ID] = Array(
						"VALUE" => $item["VALUE"],
						"DESCRIPTION" => $item["UNIT"],
					);
				}
				else
				{
					$result[$PROPERTY_ID] = $item["VALUE"];
				}
				
			}
		}
		
		return $result;
	}
	
	private function GetPropertyIdByValue($IBLOCK_ID, $code, $value, $translateXmlId = false)
	{
		
		if(is_array($value))
			$result = Array();
		else
			$result = false;
		
		if(!is_array($value))
			$value = Array($value);
		
		if(!empty($value))
		{
			foreach($value as $sValue)
			{
				$ID = false;
				$propertyRes = CIBlockProperty::GetPropertyEnum($code, Array(), Array("IBLOCK_ID"=>$IBLOCK_ID, "VALUE"=>$sValue));
				if($propertyValueArr = $propertyRes->GetNext())
				{
					$ID = $propertyValueArr["ID"];
				}
					
				if($translateXmlId == "Y")
				{
					$arTransParams = array("replace_space"=>"-","replace_other"=>"-");
					$xml_id = Cutil::translit($sValue, LANGUAGE_ID, $arTransParams);
				}
				
				if(!$ID && $xml_id)
				{
					$propertyRes = CIBlockProperty::GetPropertyEnum($code, Array(), Array("IBLOCK_ID"=>$IBLOCK_ID, "EXTERNAL_ID"=>$xml_id));
					if($propertyValueArr = $propertyRes->GetNext())
					{
						$ID = $propertyValueArr["ID"];
					}
				}
				
				if(!$ID)
				{
					$ibpenum = new CIBlockPropertyEnum;
					$ibpenumParams = Array(
						'PROPERTY_ID'=>$code,
						'VALUE'=>$sValue,
						"SORT" => intVal($sValue),
					);
					
					if($translateXmlId == "Y")
					{
						if($xml_id)
							$ibpenumParams["XML_ID"] = $xml_id;
					}
					
					$ID = $ibpenum->Add($ibpenumParams);
				}
					
				if($ID)
				{
					if(!is_array($result))
						$result = $ID;
					else
						$result[] = $ID;
				}
			}
		}
		
		return $result;
	}
	
	private function GetHighloadblockIdByValue($table, $value)
	{
		
		CModule::IncludeModule('highloadblock');
		
		if(is_array($value))
			$result = Array();
		else
			$result = false;
		
		if(!is_array($value))
			$value = Array($value);
		
		if(!empty($table) && !empty($value))
		{
			$rsData = \Bitrix\Highloadblock\HighloadBlockTable::getList(array('filter'=>array('TABLE_NAME'=>$table)));
			if($hldata = $rsData->Fetch())
			{
				$hlentity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
				$hlDataClass = $hlentity->getDataClass(); 
				
				foreach($value as $sValue)
				{
					$sValue = trim($sValue);
					
					$ID = false;
					$res = $hlDataClass::getList(
						array(
							'filter' => array(
								'UF_XML_ID' => $sValue,
							), 
							'select' => array("*"), 
							'order' => array(
								'UF_XML_ID' => 'asc'
							),
							"limit" => 1,
						)
					);
					
					if ($row = $res->fetch()) {
						$ID = $row["UF_XML_ID"];
					} 
					
					if(!$ID)
					{
						$res = $hlDataClass::getList(
							array(
								'filter' => array(
									'UF_NAME' => $sValue,
								), 
								'select' => array("*"), 
								'order' => array(
									'UF_NAME' => 'asc'
								),
								"limit" => 1,
							)
						);
						
						if ($row = $res->fetch()) {
							$ID = $row["UF_XML_ID"];
						} 
					}
					
					if(!$ID)
					{
						$newEntityParams = Array(
							'UF_NAME'=> $sValue,
							'UF_SORT'=> 500,
						);
						
						$arTransParams = array("replace_space"=>"","replace_other"=>"");
						$xml_id = Cutil::translit($sValue, LANGUAGE_ID, $arTransParams);
						if(!is_numeric($xml_id))
							$newEntityParams["UF_XML_ID"] = $xml_id;
						else
							$newEntityParams["UF_XML_ID"] = $table.'_'.$xml_id;
						
						$entityResult = $hlDataClass::Add($newEntityParams);
						if($entityResult->isSuccess())
						{
							$ID = $newEntityParams["UF_XML_ID"];
						}
					}
					
					if($ID)
					{
						if(!is_array($result))
							$result = $ID;
						else
							$result[] = $ID;
					}
				}
			}
		}
		
		return $result;
	}
	
	private function GetElementIdByField($IBLOCK_ID, $SEARCH_BY, $value)
	{
		if(is_array($value))
			$result = Array();
		else
			$result = false;
		
		if(!is_array($value))
			$value = Array($value);
		
		if(!empty($value))
		{
			
			$el = new CIBlockElement;
			foreach($value as $sValue)
			{
				$arFilter = Array(
					"IBLOCK_ID" => $IBLOCK_ID, 
					$SEARCH_BY => $sValue
				);
				$arSelect = Array("IBLOCK_ID", "ID", $SEARCH_BY);
				
				$elementRes = $el->GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
				
				$findElement = false;
				$ID = false;
				
				while($findElement = $elementRes->GetNext())
				{
					$ID = $findElement['ID'];
				}
					
				if($ID)
				{
					if(!is_array($result))
						$result = $ID;
					else
						$result[] = $ID;
				}
			}
		}
		
		return $result;
	}
	
	private function GetElementNameByID($ELEMENT_ID = false)
	{
		$res = CIBlockElement::GetByID($ELEMENT_ID);
		if($ar_res = $res->GetNext())
		{
			return $ar_res['NAME'];
		}
		
		return false;
	}
	
	private function GetSectionIdByField($arFilter, $arSelect)
	{
		$el = new CIBlockSection;
		
		$sectionRes = $el->GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
		$findSection = false;
		while($findSection2 = $sectionRes->GetNext())
		{
			$ID = $findSection2['ID'];
			$findSection = $findSection2;
		}
		
		return $ID;
	}
	
	private function LinkPropertyValue($code, $value, $ID, $IBLOCK_ID, $params, $data, &$result)
	{
		$propertyRes = CIBlockProperty::GetByID($code, $IBLOCK_ID);
		if($propertyArray = $propertyRes->GetNext())
		{
			switch($propertyArray["PROPERTY_TYPE"])
			{
				case("S"):
					if($propertyArray["USER_TYPE"] == "directory")
					{
						$highloadblockTable = $propertyArray["USER_TYPE_SETTINGS"]["TABLE_NAME"];
						$value = self::GetHighloadblockIdByValue($highloadblockTable, $value);
					}
					elseif($propertyArray["USER_TYPE"] == "map_yandex" || $propertyArray["USER_TYPE"] == "map_google")
					{
						$value = Array("VALUE" => "{$value["LATITUDE"]},{$value["LONGITUDE"]}");
					}
					break;
				case("L"):
					$value = self::GetPropertyIdByValue($IBLOCK_ID, $code, $value, $params["PROPERTIES_TRANSLATE_XML_ID"]);
					break;
				case("E"):
					$value = self::GetElementIdByField($propertyArray["LINK_IBLOCK_ID"], $data["SEARCH_PROPERTY_E"], $value);
					break;
				case("G"):
					if($value)
					{
						$value = self::GetSectionIdByField(
							Array(
								"IBLOCK_ID" => $propertyArray["LINK_IBLOCK_ID"], 
								$data["SEARCH_PROPERTY_G"] => $value
							),
							Array("IBLOCK_ID", "ID", $data["SEARCH_PROPERTY_G"])
						);
					}
					break;
				default:
					break;
			}
			
			if($propertyArray["MULTIPLE"] == "Y" && $propertyArray["PROPERTY_TYPE"] != "F" && $params["PROPERTIES_INCREMENT_TO_MULTIPLE"] == "Y")
			{
				$propertyValueRes = CIBlockElement::GetProperty($IBLOCK_ID, $ID, array("sort" => "asc"), array("ID" => $code));
				
				while ($propertyValue = $propertyValueRes->GetNext())
				{
					if($propertyValue['VALUE'])
						$VALUES[] = $propertyValue['VALUE'];
				}
				
				if(is_array($VALUES))
				{
					if(!is_array($value))
					{
						if(!in_array($value, $VALUES))
							$VALUES[] = $value;
					}
					else
					{
						foreach($value as $sValue)
						{
							if(!in_array($sValue, $VALUES))
								$VALUES[] = $sValue;
						}
					}
				}
				else
					$VALUES = $value;
				
				$result = $VALUES;
				unset($VALUES);
			}
			else
			{
				$result = $value;
			}
		}
	}
	
	private function ImportProperties($data, $params = Array(), $ELEMENT_ID = false, $IBLOCK_ID = false)
	{
		global $DEBUG_PLAN_ID, $DEBUG_EVENTS, $DEBUG_IMPORT_PROPERTIES;
		
		if($DEBUG_EVENTS && $DEBUG_IMPORT_PROPERTIES)
		{
			$logProperties = new CWebprostorImportLog;
			$logPropertiesData = Array("PLAN_ID" => $DEBUG_PLAN_ID);
		}
		
		$el = new CIBlockElement;
		$arProperties = $data["PROPERTIES"];
		$arPropertyValues = Array();
		$arSearchByProperty = $data["SEARCH_PROPERTY"];
		$arDeleteValueProperty = array_merge($data["DELETE_PROPERTY_IMAGES"], $data["DELETE_PROPERTY_FILES"]);
		
		$arFilter = Array('IBLOCK_ID'=>$IBLOCK_ID);
		$arSelect = Array("IBLOCK_ID", "ID");
		foreach($arSearchByProperty as $code)
		{
			$arFilter["PROPERTY_".$code] = $arProperties[$code];
			if(!in_array($code, $arSelect))
				$arSelect[] = "PROPERTY_".$code;
		}
		
		$findElement = false;
			
		if(!$ELEMENT_ID)
		{
			
			if($DEBUG_EVENTS && $DEBUG_IMPORT_PROPERTIES)
			{
				$logPropertiesData["EVENT"] = "IMPORT_ITEM_FILTER";
				$logPropertiesData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER", Array("#OBJECT#" => "Properties"));
				$logPropertiesData["DATA"] = base64_encode(serialize(array_merge(array_keys($arFilter), $arFilter)));
				$logProperties->Add($logPropertiesData);
			}
			
			$elementRes = $el->GetList(Array("SORT" => "ASC"), $arFilter, false, Array("nPageSize"=>1), $arSelect);
			while($findElement2 = $elementRes->GetNextElement())
			{
				$ID = $findElement2['ID'];
				$findElement = $findElement2;
				$findProperties = $findElement2->GetProperties();
			}
		
		}
		else
		{
			$ID = $ELEMENT_ID;
			$elementRes = CIBlockElement::GetByID($ID);
			if($findElement2 = $elementRes->GetNextElement())
			{
				$findElement = $findElement2;
				$findProperties = $findElement2->GetProperties();
			}
		}
		
		if($DEBUG_EVENTS && $DEBUG_IMPORT_PROPERTIES)
		{
			if($ID > 0)
			{
				$logPropertiesData["EVENT"] = "IMPORT_ITEM_FILTER_RESULT_COUNT";
				$logPropertiesData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER_RESULT_COUNT", Array("#OBJECT#" => "Properties", "#COUNT#" => count($findProperties)));
				$logPropertiesData["DATA"] = "";
				$logProperties->Add($logPropertiesData);
			}
			elseif(!$ID)
			{
				$logPropertiesData["EVENT"] = "IMPORT_ITEM_FILTER_NO_RESULT";
				$logPropertiesData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER_NO_RESULT", Array("#OBJECT#" => "Properties"));
				unset($logPropertiesData["DATA"]);
				$logProperties->Add($logPropertiesData);
			}
		}
		
		if($ID > 0)
		{
			if(count($arDeleteValueProperty)>0)
			{
				foreach($arDeleteValueProperty as $propertyId)
				{
					self::DeleteOldFilesByProperty($ID, $IBLOCK_ID, $propertyId);
				}
			}
			
			foreach($arProperties as $code => $value)
			{
				self::LinkPropertyValue($code, $value, $ID, $IBLOCK_ID, $params, $data, $arPropertyValues[$code]);
			}
		
			if($DEBUG_EVENTS && $DEBUG_IMPORT_PROPERTIES)
			{
				$logPropertiesData["EVENT"] = "IMPORT_OBJECT_UPDATE_COUNT";
				$logPropertiesData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_OBJECT_UPDATE_COUNT", Array("#OBJECT#" => "Properties", "#COUNT#" => count($arPropertyValues)));
				$logPropertiesData["DATA"] = '';
				$logProperties->Add($logPropertiesData);
			}
			
			if($params["PROPERTIES_RESET"] != "Y")
			{
				$res = CIBlockElement::SetPropertyValuesEx(
					$ID, 
					$IBLOCK_ID, 
					$arPropertyValues
				);
			}
			else
			{
				$res = CIBlockElement::SetPropertyValues(
					$ID, 
					$IBLOCK_ID, 
					$arPropertyValues
				);
			}
			
			if($res == NULL)
				$event = "UPDATE";
		}
		
		if(isset($res) && $res != NULL)
		{
			$event = "ERROR";
			$error = $el->LAST_ERROR;
		}
		
		$result = Array(
			"ID" => $ID,
			"EVENT" => $event,
			"ERROR" => $error,
		);
		
		return $result;
	}
	
	private function GetProductArray($item, &$rules, &$params, $ELEMENT_ID = false, $checkRule = "CATALOG_PRODUCT_FIELD")
	{
		
		$productFields = Array();
		
		foreach($item as $entity => $field)
		{
			$temp_field = $field;
			if(isset($rules[$entity]))
			{
				foreach($rules[$entity]["RULES"] as $rule)
				{
					self::CheckAttribute($rule, $temp_field, $field);
					
					CWebprostorImportProcessingSettingsTypes::ApplyProcessingRules($field, $rule["PROCESSING_TYPES"]);
					
					$CheckArrayContinue = self::CheckArrayContinue($field);
					if($CheckArrayContinue === true)
						continue;
					
					$CheckRequired = self::CheckRequired($rule, $field);
					if($CheckRequired === false)
						return false;
					
					if(!empty($rule[$checkRule]))
					{
						if($rule["IS_REQUIRED"] == "N" || ($rule["IS_REQUIRED"] == "Y" || !empty($field)))
						{
							
							$productFields[$rule[$checkRule]] = $field;
						}
					}
				}
			}
		}
		
		if($ELEMENT_ID>0)
		{
			$productFields["ID"] = $ELEMENT_ID;
		}
		
		if(!$productFields["PURCHASING_CURRENCY"] && $productFields["PURCHASING_PRICE"])
		{
			$productFields["PURCHASING_CURRENCY"] = self::DEFAULT_CURRENCY;
		}
		
		if(!$productFields["QUANTITY_TRACE"] && $params["PRODUCTS_PARAMS"]["PRODUCTS_QUANTITY_TRACE"])
		{
			$productFields["QUANTITY_TRACE"] = $params["PRODUCTS_PARAMS"]["PRODUCTS_QUANTITY_TRACE"];
		}
		
		if(!$productFields["CAN_BUY_ZERO"] && $params["PRODUCTS_PARAMS"]["PRODUCTS_USE_STORE"])
		{
			$productFields["CAN_BUY_ZERO"] = $params["PRODUCTS_PARAMS"]["PRODUCTS_USE_STORE"];
		}
		
		if(!$productFields["SUBSCRIBE"] && $params["PRODUCTS_PARAMS"]["PRODUCTS_SUBSCRIBE"])
		{
			$productFields["SUBSCRIBE"] = $params["PRODUCTS_PARAMS"]["PRODUCTS_SUBSCRIBE"];
		}
		
		$result = Array(
			"FIELDS" => $productFields,
		);
		
		return $result;
	}
	
	private function ImportProduct($data, $params, $type = "PRODUCTS")
	{
		
		$pd = new CCatalogProduct;
		$arFields = $data["FIELDS"];
		
		$preRes = $pd->GetList(
			array(),
			array(
					"ID" => $arFields["ID"],
				)
		);
		
		if(!$arFields["PURCHASING_CURRENCY"] && isset($arFields["PURCHASING_PRICE"]))
		{
			$arFields["PURCHASING_CURRENCY"] = self::DEFAULT_CURRENCY;
		}

		if ($arProduct = $preRes->Fetch())
		{
			$event = "SEARCH";
			if ($params[$type."_UPDATE"] == "Y")
			{
				$res = $pd->Update($arProduct["ID"], $arFields);
				if($res)
				{
					$ID = $arProduct["ID"];
					$event = "UPDATE";
				}
			}
		}
		else
		{
			if($params[$type."_ADD"] == "Y")
			{
				$ID = $pd->Add($arFields);
				$res = ($ID>0);
				if($res)
					$event = "ADD";
			}
		}
		
		if(isset($res) && !$res)
		{
			$event = "ERROR";
			$error = $pd->LAST_ERROR;
		}
		
		$result = Array(
			"ID" => $ID,
			"EVENT" => $event,
			"ERROR" => $error,
		);
		
		return $result;
	}
	
	private function GetPriceArray($item, &$rules, &$params, $ELEMENT_ID = false, $BASE_PRICE = false)
	{
		
		$priceFields = Array();
		
		foreach($item as $entity => $field)
		{
			$temp_field = $field;
			if(isset($rules[$entity]))
			{
				foreach($rules[$entity]["RULES"] as $rule)
				{
					self::CheckAttribute($rule, $temp_field, $field);
					
					CWebprostorImportProcessingSettingsTypes::ApplyProcessingRules($field, $rule["PROCESSING_TYPES"]);
					
					$CheckArrayContinue = self::CheckArrayContinue($field);
					if($CheckArrayContinue === true)
						continue;
					
					$CheckRequired = self::CheckRequired($rule, $field);
					if($CheckRequired === false)
						return false;
					
					if(!empty($rule["CATALOG_PRODUCT_PRICE"]))
					{
						if($rule["IS_REQUIRED"] == "N" || ($rule["IS_REQUIRED"] == "Y" || !empty($field)))
						{
							
							$priceFields[$rule["CATALOG_PRODUCT_PRICE"]] = $field;
						}
					}
				}
			}
		}
		
		if(isset($priceFields["PRICE"]))
		{
			$prices[$BASE_PRICE]["PRICE"] = self::ClearPrice($priceFields["PRICE"]);
			unset($priceFields["PRICE"]);
		}
		
		if(isset($priceFields["CURRENCY"]))
		{
			$prices[$BASE_PRICE]["CURRENCY"] = $priceFields["CURRENCY"];
			unset($priceFields["CURRENCY"]);
		}
		elseif(!$priceFields["CURRENCY"] && isset($prices[$BASE_PRICE]["PRICE"]))
		{
			$prices[$BASE_PRICE]["CURRENCY"] = self::DEFAULT_CURRENCY;
		}
		
		$dbPriceType = CCatalogGroup::GetList(
			array("SORT" => "ASC"),
			array("BASE" => "N")
		);
		while ($arPriceType = $dbPriceType->Fetch())
		{
			$PRICE_ID = $arPriceType["ID"];
			if(isset($priceFields["PRICE_".$PRICE_ID]) || isset($priceFields["EXTRA_ID_".$PRICE_ID]))
			{
				if(isset($priceFields["PRICE_".$PRICE_ID]))
				{
					$prices[$PRICE_ID]["PRICE"] = self::ClearPrice($priceFields["PRICE_".$PRICE_ID]);
					unset($priceFields["PRICE_".$PRICE_ID]);
				
					if(isset($priceFields["CURRENCY_".$PRICE_ID]))
					{
						$prices[$PRICE_ID]["CURRENCY"] = $priceFields["CURRENCY_".$PRICE_ID];
						unset($priceFields["CURRENCY_".$PRICE_ID]);
					}
					elseif(!$priceFields["CURRENCY_".$PRICE_ID])
					{
						$prices[$PRICE_ID]["CURRENCY"] = self::DEFAULT_CURRENCY;
					}
				}
				
				if(isset($priceFields["EXTRA_ID_".$PRICE_ID]))
				{
					$prices[$PRICE_ID]["EXTRA_ID"] = $priceFields["EXTRA_ID_".$PRICE_ID];
					unset($priceFields["EXTRA_ID_".$PRICE_ID]);
				}
			}
		}
		
		if($ELEMENT_ID>0)
		{
			$priceFields["PRODUCT_ID"] = $ELEMENT_ID;
		}
		
		$result = Array(
			"FIELDS" => $priceFields,
		);
		
		if(is_array($prices) && count($prices)>0)
		{
			$result["PRICES"] = $prices;
		}
		
		return $result;
	}
	
	private function ImportPrice($data, $params)
	{
		$pr = new CPrice;
		$arFields = $data["FIELDS"];
		$arPrices = $data["PRICES"];
		if(is_array($arPrices) && count($arPrices)>0)
		{
			foreach($arPrices as $PRICE_ID => $PRICE_DATA)
			{
				$PRICE_DATA["PRODUCT_ID"] = $arFields["PRODUCT_ID"];
				
				$preRes = $pr->GetList(
					array(),
					array(
							"PRODUCT_ID" => $arFields["PRODUCT_ID"],
							"CATALOG_GROUP_ID" => $PRICE_ID
						)
				);

				if ($arPrice = $preRes->Fetch())
				{
					$event = "SEARCH";
					if ($params["PRICES_UPDATE"] == "Y")
					{
						$res = $pr->Update($arPrice["ID"], $PRICE_DATA);
						if($res)
						{
							$ID = $arPrice["ID"];
							$event = "UPDATE";
						}
					}
				}
				else
				{
					if($params["PRICES_ADD"] == "Y")
					{
						$PRICE_DATA["CATALOG_GROUP_ID"] = $PRICE_ID;
						$ID = $pr->Add($PRICE_DATA);
						$res = ($ID>0);
						if($res)
							$event = "ADD";
					}
				}
				
				if(isset($res) && !$res)
				{
					$event = "ERROR";
					$error = $pr->LAST_ERROR;
				}
				
				$result["SYSTEM"]["ID"][] = $ID;
				++$result["SYSTEM"]["EVENT"][$event];
				$result["SYSTEM"]["ERROR"][] = $error;
				
				$result["LOGS"][] = [
					"ID" => $ID,
					"EVENT" => $event,
					"ERROR" => $error,
				];
				
			}
		}
		
		return $result;
	}
	
	private function GetStoreAmountArray($item, &$rules, &$params, $ELEMENT_ID = false)
	{
		
		$storeAmountFields = Array();
		
		foreach($item as $entity => $field)
		{
			$temp_field = $field;
			if(isset($rules[$entity]))
			{
				foreach($rules[$entity]["RULES"] as $rule)
				{
					self::CheckAttribute($rule, $temp_field, $field);
					
					CWebprostorImportProcessingSettingsTypes::ApplyProcessingRules($field, $rule["PROCESSING_TYPES"]);
					
					$CheckArrayContinue = self::CheckArrayContinue($field);
					if($CheckArrayContinue === true)
						continue;
					
					$CheckRequired = self::CheckRequired($rule, $field);
					if($CheckRequired === false)
						return false;
					
					if(!empty($rule["CATALOG_PRODUCT_STORE_AMOUNT"]))
					{
						if($rule["IS_REQUIRED"] == "N" || ($rule["IS_REQUIRED"] == "Y" || !empty($field)))
						{
							
							$storeAmountFields[$rule["CATALOG_PRODUCT_STORE_AMOUNT"]] = $field;
						}
					}
				}
			}
		}
		
		$dbStore = CCatalogStore::GetList(
			array("SORT" => "ASC"),
			array("BASE" => "N")
		);
		while ($arStore = $dbStore->Fetch())
		{
			$STORE_ID = $arStore["ID"];
			if(isset($storeAmountFields["STORE_".$STORE_ID]))
			{
				$stores[$STORE_ID]["AMOUNT"] = self::ClearPrice($storeAmountFields["STORE_".$STORE_ID]);
				unset($storeAmountFields["STORE_".$STORE_ID]);
			}
		}
		
		if($ELEMENT_ID>0)
		{
			$storeAmountFields["PRODUCT_ID"] = $ELEMENT_ID;
		}
		
		$result = Array(
			"FIELDS" => $storeAmountFields,
		);
		
		if(is_array($stores) && count($stores)>0)
		{
			$result["STORES"] = $stores;
		}
		
		return $result;
	}
	
	private function ImportStoreAmount($data, $params)
	{
		
		$sa = new CCatalogStoreProduct;
		$arFields = $data["FIELDS"];
		$arStores = $data["STORES"];
		if(is_array($arStores) && count($arStores)>0)
		{
			foreach($arStores as $STORE_ID => $STORE_DATA)
			{
				$STORE_DATA["PRODUCT_ID"] = $arFields["PRODUCT_ID"];
				
				$preRes = $sa->GetList(
					array(),
					array(
							"PRODUCT_ID" => $arFields["PRODUCT_ID"],
							"STORE_ID" => $STORE_ID
						)
				);

				if ($arPrice = $preRes->Fetch())
				{
					$event = "SEARCH";
					if ($params["STORE_AMOUNT_UPDATE"] == "Y")
					{
						$res = $sa->Update($arPrice["ID"], $STORE_DATA);
						if($res)
						{
							$ID = $arPrice["ID"];
							$event = "UPDATE";
						}
					}
				}
				else
				{
					if($params["STORE_AMOUNT_ADD"] == "Y")
					{
						$STORE_DATA["STORE_ID"] = $STORE_ID;
						$ID = $sa->Add($STORE_DATA);
						$res = ($ID>0);
						if($res)
							$event = "ADD";
					}
				}
				
				if(isset($res) && !$res)
				{
					$event = "ERROR";
					$error = $sa->LAST_ERROR;
				}
				/*$result["ID"][] = $ID;
				++$result["EVENT"][$event];
				$result["ERROR"][] = $error;*/
				
				$result["SYSTEM"]["ID"][] = $ID;
				++$result["SYSTEM"]["EVENT"][$event];
				$result["SYSTEM"]["ERROR"][] = $error;
				
				$result["LOGS"][] = [
					"ID" => $ID,
					"EVENT" => $event,
					"ERROR" => $error,
				];
				
			}
		}
		
		return $result;
	}
	
	private function GetEntityArray($item, &$rules, &$params)
	{
		$entityFields = Array();
		$entitySearch = Array();
		global $DEBUG_IMAGES, $DEBUG_FILES;
		
		foreach($item as $entity => $field)
		{
			$temp_field = $field;
			if(isset($rules[$entity]))
			{
				foreach($rules[$entity]["RULES"] as $rule)
				{
					self::CheckAttribute($rule, $temp_field, $field);
					
					CWebprostorImportProcessingSettingsTypes::ApplyProcessingRules($field, $rule["PROCESSING_TYPES"]);
					
					$CheckArrayContinue = self::CheckArrayContinue($field);
					if($CheckArrayContinue === true)
						continue;
					
					$CheckRequired = self::CheckRequired($rule, $field);
					if($CheckRequired === false)
						return false;
					
					if(!empty($rule["HIGHLOAD_BLOCK_ENTITY_FIELD"]))
					{
						if($rule["IS_REQUIRED"] == "N" || ($rule["IS_REQUIRED"] == "Y" || !empty($field)))
						{
							
							$CheckImageFileUrl = self::CheckImageFileUrl($rule, $field, $params, $entityFields[$rule["HIGHLOAD_BLOCK_ENTITY_FIELD"]]);
							
							if($CheckImageFileUrl === false)
							{
								$entityFields[$rule["HIGHLOAD_BLOCK_ENTITY_FIELD"]] = self::ClearField($field);
							}
							
							if($rule["USE_IN_SEARCH"] == "Y")
								$entitySearch[] = $rule["HIGHLOAD_BLOCK_ENTITY_FIELD"];
							
							if($rule["USE_IN_CODE"] == "ENTITY")
							{
								$entityCode[$field] = $rule["SORT"];
							}
						}
					}
				}
			}
		}
		
		if(is_array($entityCode) && count($entityCode))
		{
			uasort($entityCode, 'self::CompareCodeSort');
			foreach($entityCode as $value => $sort)
			{
				$translitName .= ' '.$value;
			}
			$translitName = trim($translitName);
		}
		elseif(!empty($entityFields["UF_XML_ID"]))
		{
			$translitName = $entityFields["UF_XML_ID"];
		}
		elseif(!empty($entityFields["UF_NAME"]) && $params["ENTITIES_TRANSLATE_XML_ID"] != "N")
		{
			$translitName = $entityFields["UF_NAME"];
		}
		
		if($translitName)
		{
			$translitParams = array("replace_space"=>"","replace_other"=>"");
			$xml_id = Cutil::translit($translitName, LANGUAGE_ID, $translitParams);
			if(!is_numeric($xml_id))
				$entityFields["UF_XML_ID"] = $xml_id;
			else
				$entityFields["UF_XML_ID"] = $params["HIGHLOAD_BLOCK"].'_'.$xml_id;
		}
		
		$result = Array(
			"FIELDS" => $entityFields,
			"SEARCH" => $entitySearch,
		);
		
		return $result;
	}
	
	private function ImportEntity($data, $params = Array())
	{
		global $DEBUG_PLAN_ID, $DEBUG_EVENTS, $DEBUG_IMPORT_ENTITIES;
		
		if($DEBUG_EVENTS && $DEBUG_IMPORT_ENTITIES)
		{
			$logElement = new CWebprostorImportLog;
			$logElementData = Array("PLAN_ID" => $DEBUG_PLAN_ID);
		}
		
		$arFields = $data["FIELDS"];
		$arSearchBy = $data["SEARCH"];
		
		$arFilter = Array();
		$arSelect = Array("ID");
		
		foreach($arSearchBy as $code)
		{
			$arFilter[$code] = $arFields[$code];
			if(!in_array($code, $arSelect))
				$arSelect[] = $code;
		}
		
		if($DEBUG_EVENTS && $DEBUG_IMPORT_ENTITIES)
		{
			$logElementData["EVENT"] = "IMPORT_ITEM_FILTER";
			$logElementData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER", Array("#OBJECT#" => "Entity"));
			$logElementData["DATA"] = base64_encode(serialize(array_merge(array_keys($arFilter), $arFilter)));
			$logElement->Add($logElementData);
		}
		
		$rsData = \Bitrix\Highloadblock\HighloadBlockTable::getList(array('filter'=>array('ID'=>$params["HIGHLOAD_BLOCK"])));
		if($hldata = $rsData->Fetch())
		{
			$hlentity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);
			$hlDataClass = $hlentity->getDataClass(); 
			$res = $hlDataClass::getList(
				array(
					'filter' => $arFilter, 
					'select' => $arSelect, 
					'order' => array(),
				)
			);
				
			$findEntity = false;
			if ($row = $res->fetch()) {
				$findEntity = $row;
				$ID = $row["ID"];
				$event = "SEARCH";
			} 
		
			if($DEBUG_EVENTS && $DEBUG_IMPORT_ENTITIES)
			{
				if($ID > 0)
				{
					$logElementData["EVENT"] = "IMPORT_ITEM_FILTER_RESULT";
					$logElementData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER_RESULT", Array("#OBJECT#" => "Entity"));
					$logElementData["DATA"] = base64_encode(serialize(array_merge(array_keys($findEntity), $findEntity)));
					$logElement->Add($logElementData);
				}
				elseif(!$ID)
				{
					$logElementData["EVENT"] = "IMPORT_ITEM_FILTER_NO_RESULT";
					$logElementData["MESSAGE"] = GetMessage("MESSAGE_IMPORT_ITEM_FILTER_NO_RESULT", Array("#OBJECT#" => "Entity"));
					$logElementData["DATA"] = base64_encode(serialize(array_merge(array_keys($arFields), $arFields)));
					$logElement->Add($logElementData);
				}
			}
		
			if($ID > 0)
			{
				if($params["ENTITIES_UPDATE"] == "Y")
				{
					$res = $hlDataClass::Update($ID, $arFields);
					if($res->isSuccess())
						$event = "UPDATE";
				}
			}
			else
			{
				if($params["ENTITIES_ADD"] == "Y")
				{
					$res = $hlDataClass::Add($arFields);
					if($res->isSuccess())
					{
						$ID = $res->getId();
						$event = "ADD";
					}
				}
			}
			
			if(($params["ENTITIES_ADD"] == "Y" || $params["ENTITIES_ADD"] == "Y") && $event != "SEARCH" && $res->isSuccess())
			{
				$event = "ERROR";
				$error = $res->getErrorMessages();
				
				if($DEBUG_EVENTS && $DEBUG_IMPORT_ENTITIES)
				{
					$logElementData["EVENT"] = "ADD_UPDATE_ITEM";
					$logElementData["MESSAGE"] = GetMessage("ERROR_CANNOT_ADD_UPDATE_ITEM");
					$logElementData["DATA"] = $error;
					$logElement->Add($logElementData);
				}
			}
		}
		
		$result = Array(
			"ID" => $ID,
			"EVENT" => $event,
			"ERROR" => $error,
		);
		
		return $result;
	}
}
?>