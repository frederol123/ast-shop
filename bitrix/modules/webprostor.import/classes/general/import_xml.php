<?
class CWebprostorImportXML
{
	CONST MODULE_ID = "webprostor.import";
	
	function CWebprostorImportXML()
	{
		if(!extension_loaded('xmlreader'))
		{
			$errorArray = Array(
				"MESSAGE" => GetMessage("XMLREADER_NOT_INCLUDED"),
				"TAG" => "XMLREADER_NOT_INCLUDED",
				"MODULE_ID" => "WEBPROSTOR.IMPORT",
				"ENABLE_CLOSE" => "Y"
			);
			$notifyID = CAdminNotify::Add($errorArray);
		}
	}
	
	private function convArray($array, $from_char, $in_char = "UTF-8")
	{
		if(count($array))
		{
			foreach ($array as $key => $value)
			{
				if(is_array($value))
					$array[$key] = self::convArray($value, $from_char, $in_char);
				else
				{
					$array[$key] = iconv($from_char, $in_char, $value);
				}
			}
		}
		return $array;
	}
	
	public function ParseFile($file, $char = "UTF-8", $entity, $parse_params = "N", $limit = false, $start_element = 0)
	{
		$result = Array();
		$has_next_node = false;
		
		if(!extension_loaded('xmlreader'))
		{
			$xml = simplexml_load_file($file, 'SimpleXMLElement', LIBXML_NOCDATA);
			$all_count = count($xml->$entity);
			$json_array = json_decode(json_encode($xml), 1);
			$data = $json_array;
		}
		else
		{
			$xml = new XMLReader;
			$xml->open($file, null, LIBXML_NOCDATA);
			
			$doc = new DOMDocument;
			$doc->load($file);
			
			$data = Array();
			
			$all_count = 0;
			$counter = 0;
			
			while($xml->read())
			{
				
				if($limit && $all_count >= $limit)
				{
					$has_next_node = true;
					break 1;
				}
				
				if($xml->nodeType == XMLReader::ELEMENT)
				{
					if($xml->localName == $entity)
					{
						
						if($start_element > $counter)
						{
							$counter++;
							continue;
						}
						
						$node_dom = $doc->importNode($xml->expand(), true);
						
						$node = simplexml_import_dom($node_dom);
						
						$node_data = json_decode(json_encode($node, JSON_OBJECT_AS_ARRAY), 1);
						
						if($parse_params === "Y")
						{
							$xml2 = simplexml_load_string($xml->readOuterXml());
							$param_count = count($xml2->param);
							if($param_count>0)
							{
								for($i = 0; $i < $param_count; $i ++)
								{
									$param_data = json_decode(json_encode($xml2->param[$i], JSON_OBJECT_AS_ARRAY), 1);
									if(isset($param_data["@attributes"]["name"]) && isset($param_data[0]))
									{
										$node_data["param_array"][] = Array(
											"NAME" => $param_data["@attributes"]["name"],
											"UNIT" => $param_data["@attributes"]["unit"],
											"VALUE" => $param_data[0],
										);
									}
								}
							}
						}
						
						if(($xml->hasAttributes && count($node_data) == 2) || count($node_data) == 1)
						{
							$node_data[$entity] = $node_data[0];
							unset($node_data[0]);
						}
						$data[] = $node_data;
						$all_count++;
					}
				}
			}
			
			$xml->close();
		}
		
		if((defined("BX_UTF") && BX_UTF) && $char != "UTF-8")
		{
			$result["DATA"] = self::convArray($data, $char, "UTF-8");
		}
		elseif(((defined("BX_UTF") && !BX_UTF) || !defined("BX_UTF")) && $char != "WINDOWS-1251")
		{
			$result["DATA"] = self::convArray($data, $char, "WINDOWS-1251");
		}
		else
			$result["DATA"] = $data;
		
		$result["ITEMS_COUNT"] = $all_count;
		$result["NEXT_NODE"] = $has_next_node;
		
		return $result;
	}
	
	public function GetTotalCount($file, $entity)
	{
		$doc = new DOMDocument;
		$doc->load($file);
		
		$total_count = $doc->getElementsByTagName($entity)->length;
		
		return $total_count;
	}
	
	public function GetDataArray($data, $params, $startFrom = 0, $next_node = false)
	{
		$importFinished = false;
		
		if(!extension_loaded('xmlreader'))
		{
			$temp = $data[$params["XML_ENTITY"]];
		}
		else
		{
			$temp = $data;
		}
		
		foreach($temp as $key => $values)
		{
			foreach($values as $code => $value)
			{
				$new_data[$key][$code] = $value;
			}
		}
		
		$endTo = $startFrom + $params["ITEMS_PER_ROUND"];

		if($next_node === false)
		{
			$endTo = 0;
			$importFinished = true;
		}
		
		$result = Array(
			"DATA_ARRAY" => $new_data,
			"START_FROM" => $endTo,
			"FINISHED" => $importFinished,
		);
		
		return $result;
	}
	
	public function GetEntities($PLAN_ID = false)
	{
		$CPlan = new CWebprostorImportPlan;
		$planRes = $CPlan->GetById($PLAN_ID);
		$planInfo = $planRes->Fetch();
		
		$entities = Array();
		
		$IMPORT_FILE = $_SERVER["DOCUMENT_ROOT"].$planInfo["IMPORT_FILE"];
		
		if(is_file($IMPORT_FILE))
		{
			CheckDirPath($_SERVER["DOCUMENT_ROOT"].'/bitrix/tmp/'.self::MODULE_ID.'/');
			$tempFile = $_SERVER["DOCUMENT_ROOT"].'/bitrix/tmp/'.self::MODULE_ID.'/'.$PLAN_ID.'_entities.txt';
		}
		
		if($tempFile && is_file($tempFile) && (filemtime($IMPORT_FILE) > filemtime($tempFile)))
		{
			unlink($tempFile);
		}
		
		if($tempFile && is_file($tempFile) && (filemtime($IMPORT_FILE) < filemtime($tempFile)))
		{
			$tempData = file_get_contents($tempFile);
			$entities = unserialize($tempData);
		}
		else
		{
			$fileInfo = self::ParseFile($IMPORT_FILE, $planInfo["IMPORT_FILE_SHARSET"], $planInfo["XML_ENTITY"], "N", 1);
			
			if(!extension_loaded('xmlreader'))
			{
				$entities = array_slice($fileInfo["DATA"][$planInfo["XML_ENTITY"]], 0, 1);
			}
			else
			{
				$entities = array_slice($fileInfo["DATA"], 0, 1);
			}
			
			if($tempFile && !is_file($tempFile))
				file_put_contents($tempFile, serialize($entities));
		}
		
		if(is_array($entities[0]))
		{
			$entities_keys = array_keys($entities[0]);
			$entities_attributes = false;
		
			foreach($entities[0] as $key => $attribute)
			{
				if(is_array($attribute))
					$entities_attributes[$key] = array_keys($attribute);
			}
		}
		
		$entities = Array(
			"KEYS" => $entities_keys, 
			"ATTRIBUTES" => $entities_attributes
		);
		
		return $entities;
	}
	
}