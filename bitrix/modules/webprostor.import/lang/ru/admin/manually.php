<?
$MESS["WEBPROSTOR_IMPORT_MANUALLY_PAGE_TITLE"] = "Импортировать вручную";

$MESS["WEBPROSTOR_IMPORT_MANUALLY_TAB_NAME"] = "Настройки";
$MESS["WEBPROSTOR_IMPORT_MANUALLY_TAB_DESCRIPTION"] = "Настройки для ручного импорта";

$MESS["WEBPROSTOR_IMPORT_PLAN_ID"] = "План импорта";
$MESS["WEBPROSTOR_IMPORT_PLAN_ID_ERROR"] = "Не указан план импорта";
$MESS["WEBPROSTOR_IMPORT_IMPORT_FILE_ERROR"] = "Не доступен файл для импорта";
$MESS["WEBPROSTOR_IMPORT_ENTITIES_AND_NAMES_NOT_IDENTICAL"] = "Названия сущностей не совпадают. Проверьте настройки сопоставлений";

$MESS["WEBPROSTOR_IMPORT_START_FROM"] = "Начинать с импорта записи";
$MESS["WEBPROSTOR_IMPORT_LOAD_FILES"] = "Загрузить все необходимые файлы";
$MESS["WEBPROSTOR_IMPORT_LOAD_FILES_DESCRIPTION"] = "Файл для импорта будет заменен на файл по ссылке для скачивания";

$MESS["IMPORT_PROGRESS"] = "Обработано #DONE# из #TOTAL# записей";
$MESS["IMPORT_PROGRESS_FINISH"] = "Обработано #TOTAL# записей за #TIME# сек.";

$MESS["WEBPROSTOR_IMPORT_MANUALLY_START"] = "Импортировать";
$MESS["WEBPROSTOR_IMPORT_MANUALLY_STOP"] = "Остановить импорт";
?>