<?
$MESS["ACCESS_DENIED"] = "Доступ запрещен";

$MESS["ELEMENT_EDIT_TITLE"] = "Редактирование плана импорта";
$MESS["ELEMENT_ADD_TITLE"] = "Добавление плана импорта";

$MESS["ELEMENTS_LIST"] = "Список";
$MESS["ELEMENTS_LIST_TITLE"] = "Список планов импорта";

$MESS["BTN_ACTIONS"] = "Действия";
$MESS["BTN_ACTIONS_TITLE"] = "Действия с планом импорта";

$MESS["START_IMPORT"] = "Запустить импорт";
$MESS["OPEN_LOGS"] = "Открыть журнал";

$MESS["ELEMENT_ADD_BTN"] = "Добавить";
$MESS["ELEMENT_ADD_BTN_TITLE"] = "Добавить план";
$MESS["EDIT_CONNECTIONS_BTN"] = "Редактировать сопоставления";
$MESS["EDIT_CONNECTIONS_BTN_TITLE"] = "Установить сопоставление значений элементов файла к сущностям импорта";
$MESS["IMPORT_CONNECTIONS"] = "Импортировать сущности";
$MESS["IMPORT_CONNECTIONS_TITLE"] = "Импортировать все сущности в сопоставления";
$MESS["COPY_BTN"] = "Копировать";
$MESS["COPY_BTN_TITLE"] = "Копировать план импорта вместе с сопоставлениями";
$MESS["EXPORT_BTN"] = "Экспортировать";
$MESS["EXPORT_BTN_TITLE"] = "Экспортировать план импорта вместе с сопоставлениями";
$MESS["EXPORT_TITLE"] = "Экспортировать";
//$MESS["EXPORT_PLAN_SETTINGS"] = "Настройки импорта";
$MESS["EXPORT_PLAN_SETTINGS_NOTE"] = "<p>Будут выгружены основные настройки плана импорта</p><p>Выгрузить дополнительно:</p>";
$MESS["EXPORT_PLAN_CONNECTIONS"] = "Настройки сопоставлений";
$MESS["EXPORT_PLAN_PROPERTIES"] = "Свойства для сопоставлений";
$MESS["EXPORT_PLAN_PROCESSING_SETTINGS"] = "Настройки обработки";
$MESS["IMPORT_BTN"] = "Импортировать настройки";
$MESS["IMPORT_BTN_TITLE"] = "Импортировать план импорта вместе с сопоставлениями";
$MESS["IMPORT_TITLE"] = "Импортировать";
$MESS["IMPORT_SUBMIT"] = "Начать импорт";
$MESS["ELEMENT_DELETE_BTN"] = "Удалить";
$MESS["ELEMENT_DELETE_BTN_TITLE"] = "Удалить план импорта";
$MESS["ELEMENT_DELETE_BTN_MESSAGE"] = "Действительно удалить план импорта";
$MESS["ELEMENT_SAVED"] = "План импорта сохранен";

$MESS["MESSAGE_SAVE_ERROR"] = "Произошли ошибки при сохранении плана импорта";
$MESS["MESSAGE_NOT_SAVE_ACCESS"] = "У Вас нет доступа на изменение плана импорта";

$MESS["ELEMENT_TAB_1"] = "План импорта";
$MESS["ELEMENT_TAB_1_TITLE"] = "Основные настройки плана импорта";
$MESS["ELEMENT_TAB_2"] = "Объекты импорта";
$MESS["ELEMENT_TAB_2_TITLE"] = "Настройки объектов для импортирования";
$MESS["ELEMENT_TAB_3"] = "Формат импорта";
$MESS["ELEMENT_TAB_3_TITLE"] = "Настройка формата импорта";
$MESS["ELEMENT_TAB_4"] = "Отладка";
$MESS["ELEMENT_TAB_4_TITLE"] = "Журналирование событий и ошибок";

$MESS["GROUP_MAIN"] = "Основные";
$MESS["GROUP_IMPORT_TYPE"] = "Тип импорта";
$MESS["GROUP_FILES"] = "Файлы";
$MESS["GROUP_FILES_URL"] = "Загрузка файлов";
$MESS["GROUP_AGENT"] = "Агент";
$MESS["GROUP_IMPORT_SECTIONS"] = "Разделы";
$MESS["GROUP_IMPORT_ELEMENTS"] = "Элементы";
$MESS["GROUP_IMPORT_ELEMENT_PROPERTIES"] = "Свойства элементов и торговых предложений";
$MESS["GROUP_IMPORT_PRODUCTS"] = "Товары элементов и торговых предложений";
$MESS["GROUP_IMPORT_PRODUCT_OFFERS"] = "Торговые предложения";
$MESS["GROUP_IMPORT_PRICES"] = "Цены товаров и торговых предложений";
$MESS["GROUP_IMPORT_STORE_AMOUNT"] = "Остатки на складах";
$MESS["GROUP_IMPORT_ENTITIES"] = "Сущности Highload-блоков";
$MESS["GROUP_DEBUG_MAIN"] = "Основные";
$MESS["GROUP_DEBUG_FILES"] = "Файлы и изображения";
$MESS["GROUP_DEBUG_AREAS"] = "Объекты";

$MESS["FIELDS_LAST_IMPORT_DATE"] = "Дата последнего запуска";
$MESS["FIELDS_LAST_IMPORT_DATE_NOTE"] = "Указывается дата общего начала импорта";
$MESS["FIELDS_LAST_FINISH_IMPORT_DATE"] = "Дата последнего завершения";
$MESS["FIELDS_LAST_FINISH_IMPORT_DATE_NOTE"] = "Указывается дата общего окончания импорта";
$MESS["FIELDS_ACTIVE"] = "Активность";
$MESS["FIELDS_ACTIVE_NOTE"] = "Будет создан агент для выполнения импорта по расписанию";
$MESS["FIELDS_NAME"] = "Название";
$MESS["FIELDS_SORT"] = "Сортировка";
$MESS["FIELDS_SHOW_IN_MENU"] = "Показывать в меню";

$MESS["FIELDS_IBLOCK_ID"] = "Инфоблок";
$MESS["FIELDS_HIGHLOAD_BLOCK"] = "Highload-блок";

$MESS["FIELDS_BLOCK_ID_NO"] = "-- Не использовать --";

$MESS["FIELDS_IMPORT_FORMAT"] = "Формат импорта";
$MESS["FIELDS_IMPORT_FORMAT_CSV"] = "Текстовый формат";
$MESS["FIELDS_IMPORT_FORMAT_XML"] = "Язык разметки";
$MESS["FIELDS_IMPORT_FORMAT_XLS"] = "Формат Microsoft Excel";
$MESS["FIELDS_IMPORT_FORMAT_XLSX"] = "Формат Microsoft Excel 2007";
$MESS["FIELDS_ITEMS_PER_ROUND"] = "Количество объектов за шаг";

$MESS["FIELDS_ACTION_N"] = "оставить как есть";
$MESS["FIELDS_ACTION_A"] = "активировать";
$MESS["FIELDS_ACTION_H"] = "деактивировать";
$MESS["FIELDS_ACTION_Q"] = "обнулить остатки";
$MESS["FIELDS_ACTION_D"] = "удалить";

$MESS["FIELDS_IMPORT_IBLOCK_SECTIONS"] = "Обрабатывать и искать разделы";
$MESS["FIELDS_SECTIONS_ADD"] = "Добавлять новые разделы";
$MESS["FIELDS_SECTIONS_DEFAULT_ACTIVE"] = "Добавляемые разделы";
$MESS["FIELDS_SECTIONS_DEFAULT_SECTION_ID"] = "Начальный раздел";
$MESS["FIELDS_SECTIONS_DEFAULT_SECTION_ID_NOTE"] = "Начиная с этого раздела будет производиться последующее построение цепочки разделов. Применимо в случаях, когда прайсы от разных поставщиков имеют структуру от корня, а на вашем сайте их структура начинается с подгруппы.";
$MESS["FIELDS_SECTIONS_UPDATE"] = "Обновлять разделы";
//$MESS["FIELDS_SECTIONS_UPDATE_ACTIVE"] = "Обновляемые разделы";
$MESS["FIELDS_SECTIONS_OUT_ACTION"] = "Разделы, которых не было в файле";
$MESS["FIELDS_SECTIONS_OUT_ACTION_FILTER"] = "Условия для обработки отсутствующих разделов";
$MESS["FIELDS_SECTIONS_IN_ACTION"] = "Неактивные разделы, которые есть в файле";
$MESS["FIELDS_SECTIONS_IN_ACTION_FILTER"] = "Условия для обработки неактивных разделов";

$MESS["FIELDS_IMPORT_IBLOCK_ELEMENTS"] = "Обрабатывать и искать элементы";
$MESS["FIELDS_ELEMENTS_2_STEP_SEARCH"] = "Не учитывать раздел при поиске элемента";
$MESS["FIELDS_ELEMENTS_2_STEP_SEARCH_DESCRIPTION"] = "Данная опция подойдет в тех случаях, когда привязка к разделу в файле импорта и в структуре инфоблока отличаются. Новые элементы будут созданы с учетом раздела из файла импорта.";
$MESS["FIELDS_ELEMENTS_ADD"] = "Добавлять новые элементы";
$MESS["FIELDS_ELEMENTS_DEFAULT_ACTIVE"] = "Добавляемые элементы";
$MESS["FIELDS_ELEMENTS_DEFAULT_SECTION_ID"] = "Родительский раздел";
$MESS["FIELDS_ELEMENTS_DEFAULT_SECTION_ID_NOTE"] = "Если все товары лежат в одном разделе и не требуется обработка и поиск разделов, то можно напрямую указать раздел, в который добавлять все элементы";
$MESS["FIELDS_ELEMENTS_UPDATE"] = "Обновлять элементы";
//$MESS["FIELDS_ELEMENTS_UPDATE_ACTIVE"] = "Обновляемые элементы";
$MESS["FIELDS_ELEMENTS_OUT_ACTION"] = "Элементы, которых не было в файле";
$MESS["FIELDS_ELEMENTS_OUT_ACTION_FILTER"] = "Условия для обработки отсутствующих элементов";
$MESS["FIELDS_ELEMENTS_IN_ACTION"] = "Неактивные элементы, которые есть в файле";
$MESS["FIELDS_ELEMENTS_IN_ACTION_FILTER"] = "Условия для обработки неактивных элементов";

$MESS["FIELDS_IMPORT_IBLOCK_PROPERTIES"] = "<strong>Обрабатывать свойства</strong>";
$MESS["FIELDS_PROPERTIES_UPDATE"] = "Добавлять/обновлять значения свойств для созданных элементов";
$MESS["FIELDS_PROPERTIES_RESET"] = "Удалять незаполненные свойства элемента";
$MESS["FIELDS_PROPERTIES_TRANSLATE_XML_ID"] = "Транслитерировать внешний код для значений свойства типа Список";
$MESS["FIELDS_PROPERTIES_SET_DEFAULT_VALUES"] = "Устанавливать значения по умолчанию для свойств";
$MESS["FIELDS_PROPERTIES_SKIP_DOWNLOAD_URL_UPDATE"] = "Не загружать изображения по ссылкам для уже созданных элементов";
$MESS["FIELDS_PROPERTIES_SKIP_DOWNLOAD_URL_UPDATE_NOTE"] = "Ускоряет процесс импорта и не нагружает дисковое пространство сервера временными файлами. Актуально для планов импорта где происходит только добавление новых позиций.";
$MESS["FIELDS_PROPERTIES_WHATERMARK"] = "Водяной знак на изображения";
$MESS["FIELDS_NO_W"] = "Не наносить";
$MESS["FIELDS_W_FROM_PREVIEW"] = "Брать настройки из изображения для анонса";
$MESS["FIELDS_W_FROM_DETAIL"] = "Брать настройки из детального изображения";
$MESS["FIELDS_PROPERTIES_INCREMENT_TO_MULTIPLE"] = "Инкрементировать множественные свойства";
$MESS["FIELDS_PROPERTIES_INCREMENT_TO_MULTIPLE_NOTE"] = "Для значений множественного свойства будет добавляться отсутствующее значение с сохранением уже указанных";

$MESS["FIELDS_IMPORT_CATALOG_PRODUCTS"] = "<strong>Обрабатывать и искать товары</strong>";
$MESS["FIELDS_PRODUCTS_QUANTITY_TRACE"] = "Включить количественный учет";
$MESS["FIELDS_PRODUCTS_USE_STORE"] = "Разрешить покупку при отсутствии товара";
$MESS["FIELDS_PRODUCTS_SUBSCRIBE"] = "Разрешить подписку при отсутствии товара";
$MESS["FIELDS_PRODUCTS_ADD"] = "Добавлять новые товары";
$MESS["FIELDS_PRODUCTS_UPDATE"] = "Обновлять товары";
$MESS["FIELDS_D"] = "По умолчанию";
$MESS["FIELDS_Y"] = "Да";
$MESS["FIELDS_N"] = "Нет";

$MESS["FIELDS_IMPORT_CATALOG_PRODUCT_OFFERS"] = "<strong>Обрабатывать и искать элементы торговых предложений</strong>";
$MESS["FIELDS_OFFERS_ADD"] = "Добавлять новые торговые предложения";
$MESS["FIELDS_OFFERS_UPDATE"] = "Обновлять предложения";
$MESS["FIELDS_OFFERS_SET_NAME_FROM_ELEMENT"] = "Название для торгового предложения брать у родительского элемента";
$MESS["FIELDS_OFFERS_OUT_ACTION"] = "Торговые предложения, которых не было в файле";
$MESS["FIELDS_OFFERS_OUT_ACTION_FILTER"] = "Условия для обработки отсутствующих торговых предложений";
$MESS["FIELDS_OFFERS_IN_ACTION"] = "Неактивные торговые предложения, которые есть в файле";
$MESS["FIELDS_OFFERS_IN_ACTION_FILTER"] = "Условия для обработки неактивных торговых предложений";

$MESS["FIELDS_IMPORT_CATALOG_PRICES"] = "<strong>Обрабатывать и искать цены</strong>";
$MESS["FIELDS_PRICES_ADD"] = "Добавлять новые цены";
$MESS["FIELDS_PRICES_UPDATE"] = "Обновлять цены";

$MESS["FIELDS_IMPORT_CATALOG_STORE_AMOUNT"] = "<strong>Обрабатывать и искать остатки</strong>";
$MESS["FIELDS_STORE_AMOUNT_ADD"] = "Добавлять новые остатки";
$MESS["FIELDS_STORE_AMOUNT_UPDATE"] = "Обновлять остатки";

$MESS["FIELDS_IMPORT_HIGHLOAD_BLOCK_ENTITIES"] = "<strong>Обрабатывать и искать сущности</strong>";
$MESS["FIELDS_ENTITIES_ADD"] = "Добавлять новые сущности";
$MESS["FIELDS_ENTITIES_UPDATE"] = "Обновлять сущности";
$MESS["FIELDS_ENTITIES_TRANSLATE_XML_ID"] = "Транслитерировать Внешний код из Названия если не указан";

$MESS["FIELDS_IMPORT_FILE"] = "Файл для импорта";
$MESS["FIELDS_IMPORT_FILE_SHARSET"] = "Кодировка файла для импорта";
$MESS["FIELDS_IMPORT_FILE_SHARSET_NOTE"] = "windows-1251 применимо для CSV или XML. XLS и XLSX всегда возвращают данные в UTF-8. Дальнейшая конвертация зависит от кодировки сайта.";
$MESS["FIELDS_IMPORT_FILE_SHARSET_UTF_8"] = "UTF-8";
$MESS["FIELDS_IMPORT_FILE_SHARSET_WINDOWS_1251"] = "windows-1251";
$MESS["FIELDS_IMPORT_FILE_URL"] = "Ссылка для скачивания файла импорта";
$MESS["FIELDS_IMPORT_FILE_OPEN"] = "Открыть";
$MESS["FIELDS_IMPORT_FILE_DELETE"] = "Удалять файл после успешного импорта";
$MESS["FIELDS_IMPORT_FILE_CHECK"] = "Проверять структуру файла";
$MESS["FIELDS_IMPORT_FILE_CHECK_NOTE"] = "Будет осуществляться проверка названий сущностей при повторном импорте. Необходимо активировать, если есть вероятность изменения очередности или наименования структуры в файле";
$MESS["FIELDS_PATH_TO_IMAGES"] = "Директория с изображениями";
$MESS["FIELDS_PATH_TO_IMAGES_URL"] = "Ссылка для скачивания Zip-архива с изображениями";
$MESS["FIELDS_RESIZE_IMAGE"] = "Использовать настройки инфоблока для обработки изображений";
$MESS["FIELDS_CLEAR_IMAGES_DIR"] = "Очищать директорию изображений после успешного импорта";
$MESS["FIELDS_CLEAR_UPLOAD_TMP_DIR"] = "Очищать временную директорию изображений после успешного импорта";
$MESS["FIELDS_CLEAR_UPLOAD_TMP_DIR_NOTE"] = "Если фотографии или файлы загружаются через url, то для них создается временный файл в директории /upload/tmp/ Рекомендуется очищать данную директорию после импорта.";
$MESS["FIELDS_PATH_TO_FILES"] = "Директория с файлами";
$MESS["FIELDS_PATH_TO_FILES_URL"] = "Ссылка для скачивания Zip-архива с файлами";
$MESS["FIELDS_CLEAR_FILES_DIR"] = "Очищать директорию файлов после успешного импорта";
$MESS["FIELDS_AGENT_ID"] = "Агент ID";
$MESS["FIELDS_AGENT_OPEN"] = "Перейти";
$MESS["FIELDS_AGENT_NO"] = "Не указан";
//$MESS["FIELDS_AGENT_NO_NOTE"] = "Агент будет добавлен при сохранении плана, если установить активность";
$MESS["FIELDS_AGENT_INTERVAL"] = "Интервал обновления (сек.)";
$MESS["FIELDS_AGENT_INTERVAL_URL"] = "Интервал обновления загрузки (сек.)";
$MESS["FIELDS_CURL_TIMEOUT"] = "Время ожидания загрузки файла с внешнего ресурса (сек.)";
$MESS["FIELDS_CURL_FOLLOWLOCATION"] = "Выполнять переадресацию по ссылкам";
$MESS["FIELDS_CURL_FOLLOWLOCATION_DESCRIPTION"] = "Следование любому заголовку Location отправленному сервером в своем ответе";
$MESS["FIELDS_RAW_URL_DECODE"] = "Декодировать URL-кодированные ссылки";

$MESS["FIELDS_CSV_DELIMITER"] = "Разделитель полей";
$MESS["FIELDS_CSV_DELIMITER_TZP"] = "точка с запятой";
$MESS["FIELDS_CSV_DELIMITER_ZPT"] = "запятая";
$MESS["FIELDS_CSV_DELIMITER_TAB"] = "табуляция";
$MESS["FIELDS_CSV_DELIMITER_SPS"] = "пробел";

$MESS["FIELDS_CSV_XLS_FIRST_LINE_ZERO"] = "Нумерация строк в массиве начинается с 0";
$MESS["FIELDS_CSV_XLS_NAME_LINE"] = "Порядковый номер строки с именами полей";
$MESS["FIELDS_CSV_XLS_START_LINE"] = "Порядковый номер строки с которой начинаются записи для импорта";
$MESS["FIELDS_CSV_XLS_MAX_DEPTH_LEVEL"] = "Максимальный уровень вложенности для разделов";

$MESS["FIELDS_XLS_SHEET"] = "Используемый лист";
$MESS["FIELDS_XLS_SHEET_NUMBER"] = "Порядковый номер листа";

$MESS["FIELDS_XML_ENTITY"] = "XML сущность";
$MESS["FIELDS_XML_USE_ENTITY_NAME"] = "Использовать название сущности для поиска значения";
$MESS["FIELDS_XML_PARSE_PARAMS_TO_PROPERTIES"] = "Автоматически сопоставлять param со свойствами";
$MESS["FIELDS_XML_PARSE_PARAMS_TO_PROPERTIES_NOTE"] = "Допустимо только для формата файла YML";
$MESS["FIELDS_XML_ADD_PROPERTIES_FOR_PARAMS"] = "Создавать свойства для param, если не найдено";

$MESS["FIELDS_DEBUG_EVENTS"] = "Включить логирование";

$MESS["FIELDS_DEBUG_IMAGES"] = "Логировать поиск изображений";
$MESS["FIELDS_DEBUG_FILES"] = "Логировать поиск файлов";
$MESS["FIELDS_DEBUG_URL"] = "Логировать получение по url";

$MESS["FIELDS_DEBUG_IMPORT_SECTION"] = "Логировать импорт раздела";
$MESS["FIELDS_DEBUG_IMPORT_ELEMENTS"] = "Логировать импорт элемента";
$MESS["FIELDS_DEBUG_IMPORT_PROPERTIES"] = "Логировать импорт свойств";
$MESS["FIELDS_DEBUG_IMPORT_PRODUCTS"] = "Логировать импорт товара";
$MESS["FIELDS_DEBUG_IMPORT_OFFERS"] = "Логировать импорт торгового предложения";
$MESS["FIELDS_DEBUG_IMPORT_PRICES"] = "Логировать импорт цен";
$MESS["FIELDS_DEBUG_IMPORT_STORE_AMOUNT"] = "Логировать импорт остатков на складах";
$MESS["FIELDS_DEBUG_IMPORT_ENTITIES"] = "Логировать импорт сущностей";

//$MESS["NOTE_ADD_PLAN"] = "Настройки формата импорта будут доступны после добавления плана.";

//$MESS["WINDOW_DIALOG_OPEN"] = "Изменить";
//$MESS["WINDOW_DIALOG_SAVE"] = "Сохранить";
?>