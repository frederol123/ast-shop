<?
$MESS["ACCESS_DENIED"] = "Доступ запрещен";

$MESS["ELEMENT_TAB"] = "Импорт сущностей";

$MESS["ELEMENT_TAB_TITLE"] = "Импорт сущностей в сопоставления";

$MESS["IMPORT_PAGE_TITLE"] = "Импорт сущностей";

$MESS["CONNECTIONS_LIST"] = "Список";
$MESS["CONNECTIONS_LIST_TITLE"] = "Список сопоставлений";

$MESS["TABLE_HEADING_PLAN_ID"] = "План импорта";
$MESS["TABLE_HEADING_ACTIVE"] = "Активировать сопоставления";

$MESS["MESSAGE_IMPORT_ERROR"] = "Возникли ошибки во время импорта";
$MESS["ERROR_NO_CONNECTIONS"] = "Не удалось получить список сущностей. Проверьте настройки плана импорта.";
?>