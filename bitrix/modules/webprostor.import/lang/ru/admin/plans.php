<?
$MESS["PAGE_TITLE"] = "Список планов импорта";

$MESS["ACTIVE"] = "Активность";
$MESS["NAME"] = "Название";
$MESS["SORT"] = "Сортировка";

$MESS["IBLOCK_ID"] = "ID инфоблока";
$MESS["HIGHLOAD_BLOCK"] = "Highload-блок";

$MESS["BLOCK_ID_NO"] = "-- Не использовать --";

$MESS["IMPORT_FORMAT"] = "Формат импорта";
$MESS["IMPORT_FILE_SHARSET"] = "Кодировка файла";
$MESS["ITEMS_PER_ROUND"] = "Количество записей за шаг";
$MESS["IMPORT_FILE"] = "Файл для импорта";
$MESS["PATH_TO_IMAGES"] = "Директория с изображениями";
$MESS["PATH_TO_FILES"] = "Директория с файлами";
$MESS["DEBUG_EVENTS"] = "Логирование";
$MESS["AGENT_ID"] = "Агент";
$MESS["AGENT_INTERVAL"] = "Интервал обновления (сек.)";
$MESS["AGENT_OPEN"] = "Перейти";
$MESS["NO_AGENT"] = "Не задан";
$MESS["LAST_IMPORT_DATE"] = "Дата последнего запуска";

$MESS["LIST_DEACTIVATE"] = "Деактивировать";
$MESS["LIST_ACTIVATE"] = "Активировать";
$MESS["LIST_DEBUG"] = "Включить логирование";
$MESS["LIST_UNDEBUG"] = "Выключить логирование";

$MESS["EDIT_ELEMENT"] = "Редактировать план";
$MESS["EDIT_CONNECTIONS"] = "Редактировать сопоставления";
$MESS["CONNECTIONS_LIST"] = "Список сопоставлений";
$MESS["IMPORT_CONNECTIONS"] = "Импортировать сущности";
$MESS["COPY_ELEMENT"] = "Копировать план";
$MESS["ADD_ELEMENT"] = "Добавить план";
$MESS["ADD_ELEMENT_TITLE"] = "Добавить новый план импорта";

$MESS["DELETE_ELEMENT"] = "Удалить план";
$MESS["CONFIRM_DELETING"] = "Подтверждаете удаление?";

$MESS["SAVING_ERROR"] = "Произошла ошибка при сохранении изменений.";
$MESS["DELETING_ERROR"] = "Возникли ошибки при удалении";
$MESS["ELEMENT_DOS_NOT_EXIST"] = "Такого элемента не существует";
$MESS["UPDATING_ERROR"] = "Произошли ошибки при массовом обновлении";
?>