<?
$MESS["WEBPROSTOR_IMPORT_MODULE_NAME"] = "Импорт номенклатуры по плану (CSV, XLS, XLSX, XML, YML)";
$MESS["WEBPROSTOR_IMPORT_MODULE_DESC"] = "Решение позволяет настроить автоматическую загрузку информации по номенклатурам из файлов по расписанию";
$MESS["WEBPROSTOR_IMPORT_PARTNER_NAME"] = "Студия ВебПростор";
$MESS["WEBPROSTOR_IMPORT_PARTNER_URI"] = "https://webprostor.ru/";

$MESS["WEBPROSTOR_CORE_ERROR"] = "Модуль \"Ядро для модулей\" не установлен";
$MESS["WEBPROSTOR_MODULE_INSTALL"] = "Проверка системы перед установкой модуля \"Импорт номенклатуры по плану (CSV, XLS, XLSX, XML, YML)\"";
$MESS["WEBPROSTOR_MODULE_DELETE"] = "Удаление модуля \"Импорт номенклатуры по плану (CSV, XLS, XLSX, XML, YML)\"";
?>